# README #

This is the SunSpot code with four virtual sensors running on a Spot.

* Home owner Task (default, periodically measures light intensity)
* City Admin Task (periodically checks for fire eruptions)
* Semantic Task (periodically checks for fire eruptions)
* Annotation Task (periodically checks for fire eruptions and annotates the data)1.


### What is this repository for? ###

Shows the concept of WSN virtualization by running 4 different application tasks concurrently as virtual sensors in Java SunSpot devices 

### How do I get set up? ###

In order to use the virtual sensors remotely, connect a base station node to the computer via USB.

1 - Get the info of the remote SunSpot

ant remote_SunSpot_ID info
(ant -DremoteId=0014.4F01.0000.74B8 info)

2 - Deploy a MIDlet bundle 

ant remote_SunSpot_ID deploy
(ant -DremoteId=0014.4F01.0000.74B8 deploy)

3 - Co-deploy a MIDlet bundle without disturbing the existing one

ant remote_SunSpot_ID codeploy
(ant -DremoteId=0014.4F01.0000.74B8 codeploy)

4 - Run the virtual sensors from the newly installed MIDlet bundle

ant remote_SunSpot_ID addstartupmidlet -Duri=MIDlet_suite_path -Dmidlet=2
(ant -DremoteId=0014.4F01.0000.74B8 addstartupmidlet -Duri=spotsuite://Oracle/App -Dmidlet=2)

5 - Remove a virtual sensor from execution

Same as above by replacing "addstartupmidlet" with the "removestartupmidlet"

NOTE: You may need to specify the COM port of your computer, where the Base Station node is connected, when executing above commands in WindowsOS.


### Who do I talk to? ###

Mail to: imrankhan1984@gmail.com