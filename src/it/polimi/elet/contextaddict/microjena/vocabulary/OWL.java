/*   1:    */ package it.polimi.elet.contextaddict.microjena.vocabulary;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ModelFactory;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*   7:    */ 
/*   8:    */ public class OWL
/*   9:    */ {
/*  10: 38 */   private static Model m_model = ModelFactory.createDefaultModel();
/*  11:    */   public static final String NS = "http://www.w3.org/2002/07/owl#";
/*  12:    */   
/*  13:    */   public static String getURI()
/*  14:    */   {
/*  15: 45 */     return "http://www.w3.org/2002/07/owl#";
/*  16:    */   }
/*  17:    */   
/*  18: 48 */   public static final Resource NAMESPACE = m_model.createResource("http://www.w3.org/2002/07/owl#");
/*  19: 51 */   public static final Resource FULL_LANG = m_model.getResource(getURI());
/*  20: 54 */   public static final Resource DL_LANG = m_model.getResource("http://www.w3.org/TR/owl-features/#term_OWLDL");
/*  21: 57 */   public static final Resource LITE_LANG = m_model.getResource("http://www.w3.org/TR/owl-features/#term_OWLLite");
/*  22: 62 */   public static final Property maxCardinality = m_model.createProperty("http://www.w3.org/2002/07/owl#maxCardinality");
/*  23: 64 */   public static final Property versionInfo = m_model.createProperty("http://www.w3.org/2002/07/owl#versionInfo");
/*  24: 66 */   public static final Property equivalentClass = m_model.createProperty("http://www.w3.org/2002/07/owl#equivalentClass");
/*  25: 68 */   public static final Property distinctMembers = m_model.createProperty("http://www.w3.org/2002/07/owl#distinctMembers");
/*  26: 70 */   public static final Property oneOf = m_model.createProperty("http://www.w3.org/2002/07/owl#oneOf");
/*  27: 72 */   public static final Property sameAs = m_model.createProperty("http://www.w3.org/2002/07/owl#sameAs");
/*  28: 74 */   public static final Property incompatibleWith = m_model.createProperty("http://www.w3.org/2002/07/owl#incompatibleWith");
/*  29: 76 */   public static final Property minCardinality = m_model.createProperty("http://www.w3.org/2002/07/owl#minCardinality");
/*  30: 78 */   public static final Property complementOf = m_model.createProperty("http://www.w3.org/2002/07/owl#complementOf");
/*  31: 80 */   public static final Property onProperty = m_model.createProperty("http://www.w3.org/2002/07/owl#onProperty");
/*  32: 82 */   public static final Property equivalentProperty = m_model.createProperty("http://www.w3.org/2002/07/owl#equivalentProperty");
/*  33: 84 */   public static final Property inverseOf = m_model.createProperty("http://www.w3.org/2002/07/owl#inverseOf");
/*  34: 86 */   public static final Property backwardCompatibleWith = m_model.createProperty("http://www.w3.org/2002/07/owl#backwardCompatibleWith");
/*  35: 88 */   public static final Property differentFrom = m_model.createProperty("http://www.w3.org/2002/07/owl#differentFrom");
/*  36: 90 */   public static final Property priorVersion = m_model.createProperty("http://www.w3.org/2002/07/owl#priorVersion");
/*  37: 92 */   public static final Property imports = m_model.createProperty("http://www.w3.org/2002/07/owl#imports");
/*  38: 94 */   public static final Property allValuesFrom = m_model.createProperty("http://www.w3.org/2002/07/owl#allValuesFrom");
/*  39: 96 */   public static final Property unionOf = m_model.createProperty("http://www.w3.org/2002/07/owl#unionOf");
/*  40: 98 */   public static final Property hasValue = m_model.createProperty("http://www.w3.org/2002/07/owl#hasValue");
/*  41:100 */   public static final Property someValuesFrom = m_model.createProperty("http://www.w3.org/2002/07/owl#someValuesFrom");
/*  42:102 */   public static final Property disjointWith = m_model.createProperty("http://www.w3.org/2002/07/owl#disjointWith");
/*  43:104 */   public static final Property cardinality = m_model.createProperty("http://www.w3.org/2002/07/owl#cardinality");
/*  44:106 */   public static final Property intersectionOf = m_model.createProperty("http://www.w3.org/2002/07/owl#intersectionOf");
/*  45:112 */   public static final Resource Thing = m_model.createResource("http://www.w3.org/2002/07/owl#Thing");
/*  46:114 */   public static final Resource DataRange = m_model.createResource("http://www.w3.org/2002/07/owl#DataRange");
/*  47:116 */   public static final Resource Ontology = m_model.createResource("http://www.w3.org/2002/07/owl#Ontology");
/*  48:118 */   public static final Resource DeprecatedClass = m_model.createResource("http://www.w3.org/2002/07/owl#DeprecatedClass");
/*  49:120 */   public static final Resource AllDifferent = m_model.createResource("http://www.w3.org/2002/07/owl#AllDifferent");
/*  50:122 */   public static final Resource DatatypeProperty = m_model.createResource("http://www.w3.org/2002/07/owl#DatatypeProperty");
/*  51:124 */   public static final Resource SymmetricProperty = m_model.createResource("http://www.w3.org/2002/07/owl#SymmetricProperty");
/*  52:126 */   public static final Resource TransitiveProperty = m_model.createResource("http://www.w3.org/2002/07/owl#TransitiveProperty");
/*  53:128 */   public static final Resource DeprecatedProperty = m_model.createResource("http://www.w3.org/2002/07/owl#DeprecatedProperty");
/*  54:130 */   public static final Resource AnnotationProperty = m_model.createResource("http://www.w3.org/2002/07/owl#AnnotationProperty");
/*  55:132 */   public static final Resource Restriction = m_model.createResource("http://www.w3.org/2002/07/owl#Restriction");
/*  56:134 */   public static final Resource Class = m_model.createResource("http://www.w3.org/2002/07/owl#Class");
/*  57:136 */   public static final Resource OntologyProperty = m_model.createResource("http://www.w3.org/2002/07/owl#OntologyProperty");
/*  58:138 */   public static final Resource ObjectProperty = m_model.createResource("http://www.w3.org/2002/07/owl#ObjectProperty");
/*  59:140 */   public static final Resource FunctionalProperty = m_model.createResource("http://www.w3.org/2002/07/owl#FunctionalProperty");
/*  60:142 */   public static final Resource InverseFunctionalProperty = m_model.createResource("http://www.w3.org/2002/07/owl#InverseFunctionalProperty");
/*  61:144 */   public static final Resource Nothing = m_model.createResource("http://www.w3.org/2002/07/owl#Nothing");
/*  62:    */ }
