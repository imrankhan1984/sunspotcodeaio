/*  1:   */ package it.polimi.elet.contextaddict.microjena.vocabulary;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResourceFactory;
/*  7:   */ 
/*  8:   */ public class RDF
/*  9:   */ {
/* 10:   */   protected static final String uri = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
/* 11:   */   
/* 12:   */   public static String getURI()
/* 13:   */   {
/* 14:28 */     return "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
/* 15:   */   }
/* 16:   */   
/* 17:   */   protected static final Resource resource(String local)
/* 18:   */   {
/* 19:33 */     return ResourceFactory.createResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#" + local);
/* 20:   */   }
/* 21:   */   
/* 22:   */   protected static final Property property(String local)
/* 23:   */   {
/* 24:36 */     return ResourceFactory.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#", local);
/* 25:   */   }
/* 26:   */   
/* 27:   */   public static Property li(int i)
/* 28:   */   {
/* 29:39 */     return property("_" + i);
/* 30:   */   }
/* 31:   */   
/* 32:41 */   public static final Resource Alt = resource("Alt");
/* 33:42 */   public static final Resource Bag = resource("Bag");
/* 34:43 */   public static final Resource Property = resource("Property");
/* 35:44 */   public static final Resource Seq = resource("Seq");
/* 36:45 */   public static final Resource Statement = resource("Statement");
/* 37:46 */   public static final Resource List = resource("List");
/* 38:47 */   public static final Resource nil = resource("nil");
/* 39:49 */   public static final Property first = property("first");
/* 40:50 */   public static final Property rest = property("rest");
/* 41:51 */   public static final Property subject = property("subject");
/* 42:52 */   public static final Property predicate = property("predicate");
/* 43:53 */   public static final Property object = property("object");
/* 44:54 */   public static final Property type = property("type");
/* 45:55 */   public static final Property value = property("value");
/* 46:   */   
/* 47:   */   public static final class Nodes
/* 48:   */   {
/* 49:63 */     public static final Node Alt = RDF.Alt.asNode();
/* 50:64 */     public static final Node Bag = RDF.Bag.asNode();
/* 51:65 */     public static final Node Property = RDF.Property.asNode();
/* 52:66 */     public static final Node Seq = RDF.Seq.asNode();
/* 53:67 */     public static final Node Statement = RDF.Statement.asNode();
/* 54:68 */     public static final Node List = RDF.List.asNode();
/* 55:69 */     public static final Node nil = RDF.nil.asNode();
/* 56:70 */     public static final Node first = RDF.first.asNode();
/* 57:71 */     public static final Node rest = RDF.rest.asNode();
/* 58:72 */     public static final Node subject = RDF.subject.asNode();
/* 59:73 */     public static final Node predicate = RDF.predicate.asNode();
/* 60:74 */     public static final Node object = RDF.object.asNode();
/* 61:75 */     public static final Node type = RDF.type.asNode();
/* 62:76 */     public static final Node value = RDF.value.asNode();
/* 63:   */   }
/* 64:   */ }
