/*  1:   */ package it.polimi.elet.contextaddict.microjena.vocabulary;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResourceFactory;
/*  7:   */ 
/*  8:   */ public class RDFS
/*  9:   */ {
/* 10:   */   protected static final String uri = "http://www.w3.org/2000/01/rdf-schema#";
/* 11:   */   
/* 12:   */   protected static final Resource resource(String local)
/* 13:   */   {
/* 14:20 */     return ResourceFactory.createResource("http://www.w3.org/2000/01/rdf-schema#" + local);
/* 15:   */   }
/* 16:   */   
/* 17:   */   protected static final Property property(String local)
/* 18:   */   {
/* 19:24 */     return ResourceFactory.createProperty("http://www.w3.org/2000/01/rdf-schema#", local);
/* 20:   */   }
/* 21:   */   
/* 22:27 */   public static final Resource Class = resource("Class");
/* 23:28 */   public static final Resource Datatype = resource("Datatype");
/* 24:   */   /**
/* 25:   */   
/* 27:33 */   public static final Resource ConstraintProperty = resource("ConstraintProperty");
/* 28:35 */   public static final Resource Container = resource("Container");
/* 29:37 */   public static final Resource ContainerMembershipProperty = resource("ContainerMembershipProperty");
/* 30:   */   /**
/* 31:   */    
/* 33:42 */   public static final Resource ConstraintResource = resource("ConstraintResource");
/* 34:44 */   public static final Resource Literal = resource("Literal");
/* 35:45 */   public static final Resource Resource = resource("Resource");
/* 36:47 */   public static final Property comment = property("comment");
/* 37:48 */   public static final Property domain = property("domain");
/* 38:49 */   public static final Property label = property("label");
/* 39:50 */   public static final Property isDefinedBy = property("isDefinedBy");
/* 40:51 */   public static final Property range = property("range");
/* 41:52 */   public static final Property seeAlso = property("seeAlso");
/* 42:53 */   public static final Property subClassOf = property("subClassOf");
/* 43:54 */   public static final Property subPropertyOf = property("subPropertyOf");
/* 44:55 */   public static final Property member = property("member");
/* 45:   */   
/* 46:   */   public static class Nodes
/* 47:   */   {
/* 48:61 */     public static final Node Class = RDFS.Class.asNode();
/* 49:62 */     public static final Node Datatype = RDFS.Datatype.asNode();
/* 50:63 */     public static final Node ConstraintProperty = RDFS.ConstraintProperty.asNode();
/* 51:64 */     public static final Node Container = RDFS.Container.asNode();
/* 52:65 */     public static final Node ContainerMembershipProperty = RDFS.ContainerMembershipProperty.asNode();
/* 53:66 */     public static final Node Literal = RDFS.Literal.asNode();
/* 54:67 */     public static final Node Resource = RDFS.Resource.asNode();
/* 55:68 */     public static final Node comment = RDFS.comment.asNode();
/* 56:69 */     public static final Node domain = RDFS.domain.asNode();
/* 57:70 */     public static final Node label = RDFS.label.asNode();
/* 58:71 */     public static final Node isDefinedBy = RDFS.isDefinedBy.asNode();
/* 59:72 */     public static final Node range = RDFS.range.asNode();
/* 60:73 */     public static final Node seeAlso = RDFS.seeAlso.asNode();
/* 61:74 */     public static final Node subClassOf = RDFS.subClassOf.asNode();
/* 62:75 */     public static final Node subPropertyOf = RDFS.subPropertyOf.asNode();
/* 63:76 */     public static final Node member = RDFS.member.asNode();
/* 64:   */   }
/* 65:   */   
/* 66:   */   public static String getURI()
/* 67:   */   {
/* 68:84 */     return "http://www.w3.org/2000/01/rdf-schema#";
/* 69:   */   }
/* 70:   */ }
