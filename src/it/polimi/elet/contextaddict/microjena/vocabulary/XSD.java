/*  1:   */ package it.polimi.elet.contextaddict.microjena.vocabulary;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.XSDDatatype;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResourceFactory;
/*  6:   */ 
/*  7:   */ public class XSD
/*  8:   */ {
/*  9:   */   /**
/* 10:   */    
/* 12:29 */   public static String NS = "http://www.w3.org/2001/XMLSchema";
/* 13:   */   
/* 14:   */   public static String getURI()
/* 15:   */   {
/* 16:35 */     return NS + "#";
/* 17:   */   }
/* 18:   */   
/* 19:54 */   public static Resource xfloat = ResourceFactory.createResource(XSDDatatype.XSDfloat.getURI());
/* 20:55 */   public static Resource xdouble = ResourceFactory.createResource(XSDDatatype.XSDdouble.getURI());
/* 21:56 */   public static Resource xint = ResourceFactory.createResource(XSDDatatype.XSDint.getURI());
/* 22:57 */   public static Resource xlong = ResourceFactory.createResource(XSDDatatype.XSDlong.getURI());
/* 23:58 */   public static Resource integer = ResourceFactory.createResource(XSDDatatype.XSDinteger.getURI());
/* 24:   */ }

