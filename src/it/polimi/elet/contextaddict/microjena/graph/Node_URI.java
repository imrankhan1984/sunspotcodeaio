/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.util.Util;
/*  5:   */ 
/*  6:   */ public class Node_URI
/*  7:   */   extends Node_Concrete
/*  8:   */ {
/*  9:   */   protected Node_URI(Object uri)
/* 10:   */   {
/* 11:18 */     super(uri);
/* 12:   */   }
/* 13:   */   
/* 14:   */   public String getURI()
/* 15:   */   {
/* 16:22 */     return (String)this.label;
/* 17:   */   }
/* 18:   */   
/* 19:   */   public boolean isURI()
/* 20:   */   {
/* 21:26 */     return true;
/* 22:   */   }
/* 23:   */   
/* 24:   */   public String toString(PrefixMapping pm, boolean quoting)
/* 25:   */   {
/* 26:35 */     return pm == null ? (String)this.label : pm.shortForm((String)this.label);
/* 27:   */   }
/* 28:   */   
/* 29:   */   public boolean matches(Node other)
/* 30:   */   {
/* 31:39 */     if ((other instanceof Node_ANY)) {
/* 32:40 */       return true;
/* 33:   */     }
/* 34:42 */     return equals(other);
/* 35:   */   }
/* 36:   */   
/* 37:   */   public boolean equals(Object other)
/* 38:   */   {
/* 39:46 */     return ((other instanceof Node_URI)) && (same((Node_URI)other));
/* 40:   */   }
/* 41:   */   
/* 42:   */   final boolean same(Node_URI other)
/* 43:   */   {
/* 44:50 */     return this.label.equals(other.label);
/* 45:   */   }
/* 46:   */   
/* 47:   */   public String getNameSpace()
/* 48:   */   {
/* 49:54 */     String s = (String)this.label;
/* 50:55 */     return s.substring(0, Util.splitNamespace(s));
/* 51:   */   }
/* 52:   */   
/* 53:   */   public String getLocalName()
/* 54:   */   {
/* 55:60 */     String s = (String)this.label;
/* 56:61 */     return s.substring(Util.splitNamespace(s));
/* 57:   */   }
/* 58:   */   
/* 59:   */   public boolean hasURI(String uri)
/* 60:   */   {
/* 61:65 */     return this.label.equals(uri);
/* 62:   */   }
/* 63:   */ }

