package it.polimi.elet.contextaddict.microjena.graph;

import it.polimi.elet.contextaddict.microjena.shared.JenaException;

public abstract interface GraphAdd
{
  public abstract void add(Triple paramTriple)
    throws JenaException;
}
