/*   1:    */ package it.polimi.elet.contextaddict.microjena.graph;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*   5:    */ 
/*   6:    */ public class Triple
/*   7:    */   implements TripleMatch
/*   8:    */ {
/*   9:    */   private final Node subj;
/*  10:    */   private final Node pred;
/*  11:    */   private final Node obj;
/*  12:    */   
/*  13:    */   public Triple(Node s, Node p, Node o)
/*  14:    */   {
/*  15: 24 */     if (s == null) {
/*  16: 24 */       throw new JenaException("subject cannot be null");
/*  17:    */     }
/*  18: 25 */     if (p == null) {
/*  19: 25 */       throw new JenaException("predicate cannot be null");
/*  20:    */     }
/*  21: 26 */     if (o == null) {
/*  22: 26 */       throw new JenaException("object cannot be null");
/*  23:    */     }
/*  24: 27 */     this.subj = s;
/*  25: 28 */     this.pred = p;
/*  26: 29 */     this.obj = o;
/*  27:    */   }
/*  28:    */   
/*  29:    */   public String toString()
/*  30:    */   {
/*  31: 36 */     return toString(PrefixMapping.Standard);
/*  32:    */   }
/*  33:    */   
/*  34:    */   public String toString(PrefixMapping pm)
/*  35:    */   {
/*  36: 40 */     return this.subj.toString(pm, true) + " @" + this.pred.toString(pm, true) + " " + this.obj.toString(pm, true);
/*  37:    */   }
/*  38:    */   
/*  39:    */   public final Node getSubject()
/*  40:    */   {
/*  41: 49 */     return this.subj;
/*  42:    */   }
/*  43:    */   
/*  44:    */   public final Node getPredicate()
/*  45:    */   {
/*  46: 56 */     return this.pred;
/*  47:    */   }
/*  48:    */   
/*  49:    */   public final Node getObject()
/*  50:    */   {
/*  51: 63 */     return this.obj;
/*  52:    */   }
/*  53:    */   
/*  54:    */   public Node getMatchSubject()
/*  55:    */   {
/*  56: 67 */     return anyToNull(this.subj);
/*  57:    */   }
/*  58:    */   
/*  59:    */   public Node getMatchPredicate()
/*  60:    */   {
/*  61: 71 */     return anyToNull(this.pred);
/*  62:    */   }
/*  63:    */   
/*  64:    */   public Node getMatchObject()
/*  65:    */   {
/*  66: 75 */     return anyToNull(this.obj);
/*  67:    */   }
/*  68:    */   
/*  69:    */   private static Node anyToNull(Node n)
/*  70:    */   {
/*  71: 79 */     return Node.ANY.equals(n) ? null : n;
/*  72:    */   }
/*  73:    */   
/*  74:    */   private static Node nullToAny(Node n)
/*  75:    */   {
/*  76: 83 */     return n == null ? Node.ANY : n;
/*  77:    */   }
/*  78:    */   
/*  79:    */   public Triple asTriple()
/*  80:    */   {
/*  81: 87 */     return this;
/*  82:    */   }
/*  83:    */   
/*  84:    */   public boolean isConcrete()
/*  85:    */   {
/*  86: 91 */     return (this.subj.isConcrete()) && (this.pred.isConcrete()) && (this.obj.isConcrete());
/*  87:    */   }
/*  88:    */   
/*  89:    */   public boolean equals(Object o)
/*  90:    */   {
/*  91: 99 */     return ((o instanceof Triple)) && (((Triple)o).sameAs(this.subj, this.pred, this.obj));
/*  92:    */   }
/*  93:    */   
/*  94:    */   public boolean sameAs(Node s, Node p, Node o)
/*  95:    */   {
/*  96:106 */     return (this.subj.equals(s)) && (this.pred.equals(p)) && (this.obj.equals(o));
/*  97:    */   }
/*  98:    */   
/*  99:    */   public boolean matches(Triple other)
/* 100:    */   {
/* 101:110 */     return other.matchedBy(this.subj, this.pred, this.obj);
/* 102:    */   }
/* 103:    */   
/* 104:    */   public boolean matches(Node s, Node p, Node o)
/* 105:    */   {
/* 106:114 */     return (this.subj.matches(s)) && (this.pred.matches(p)) && (this.obj.matches(o));
/* 107:    */   }
/* 108:    */   
/* 109:    */   private boolean matchedBy(Node s, Node p, Node o)
/* 110:    */   {
/* 111:118 */     return (s.matches(this.subj)) && (p.matches(this.pred)) && (o.matches(this.obj));
/* 112:    */   }
/* 113:    */   
/* 114:    */   public boolean subjectMatches(Node s)
/* 115:    */   {
/* 116:122 */     return this.subj.matches(s);
/* 117:    */   }
/* 118:    */   
/* 119:    */   public boolean predicateMatches(Node p)
/* 120:    */   {
/* 121:126 */     return this.pred.matches(p);
/* 122:    */   }
/* 123:    */   
/* 124:    */   public boolean objectMatches(Node o)
/* 125:    */   {
/* 126:130 */     return this.obj.matches(o);
/* 127:    */   }
/* 128:    */   
/* 129:    */   public int hashCode()
/* 130:    */   {
/* 131:138 */     return hashCode(this.subj, this.pred, this.obj);
/* 132:    */   }
/* 133:    */   
/* 134:    */   public static int hashCode(Node s, Node p, Node o)
/* 135:    */   {
/* 136:148 */     return s.hashCode() >> 1 ^ p.hashCode() ^ o.hashCode() << 1;
/* 137:    */   }
/* 138:    */   
/* 139:    */   public static Triple create(Node s, Node p, Node o)
/* 140:    */   {
/* 141:158 */     return new Triple(s, p, o);
/* 142:    */   }
/* 143:    */   
/* 144:    */   public static Triple createMatch(Node s, Node p, Node o)
/* 145:    */   {
/* 146:162 */     return create(nullToAny(s), nullToAny(p), nullToAny(o));
/* 147:    */   }
/* 148:    */   
/* 149:168 */   public static final Triple ANY = create(Node.ANY, Node.ANY, Node.ANY);
/* 150:    */ }
