/*   1:    */ package it.polimi.elet.contextaddict.microjena.graph;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.DatatypeFormatException;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.datatypes.TypeMapper;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.XSDDatatype;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.impl.LiteralLabel;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.AnonId;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*  12:    */ 
/*  13:    */ public abstract class Node
/*  14:    */ {
/*  15:    */   protected final Object label;
/*  16: 34 */   static final Map present = new Map();
/*  17: 39 */   public static final Node ANY = new Node_ANY();
/*  18:    */   static final String RDFprefix = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
/*  19:    */   
/*  20:    */   public static Node create(String x)
/*  21:    */   {
/*  22: 59 */     return create(PrefixMapping.Standard, x);
/*  23:    */   }
/*  24:    */   
/*  25:    */   public static Node create(PrefixMapping pm, String x)
/*  26:    */   {
/*  27: 70 */     if (x.equals("")) {
/*  28: 71 */       throw new JenaException("Node.create does not accept an empty string as argument");
/*  29:    */     }
/*  30: 72 */     char first = x.charAt(0);
/*  31: 73 */     if ((first == '\'') || (first == '"')) {
/*  32: 74 */       return createLiteral(newString(pm, first, x));
/*  33:    */     }
/*  34: 75 */     if (Character.isDigit(first)) {
/*  35: 76 */       return createLiteral(x, "", XSDDatatype.XSDinteger);
/*  36:    */     }
/*  37: 77 */     if (first == '_') {
/*  38: 78 */       return createAnon(new AnonId(x));
/*  39:    */     }
/*  40: 79 */     if (x.equals("??")) {
/*  41: 80 */       return ANY;
/*  42:    */     }
/*  43: 81 */     if (first == '&') {
/*  44: 82 */       return createURI("q:" + x.substring(1));
/*  45:    */     }
/*  46: 83 */     int colon = x.indexOf(':');
/*  47: 84 */     String d = pm.getNsPrefixURI("");
/*  48: 85 */     return colon < 0 ? createURI((d == null ? "eh:/" : d) + x) : createURI(pm.expandPrefix(x));
/*  49:    */   }
/*  50:    */   
/*  51:    */   private static String unEscape(String spelling)
/*  52:    */   {
/*  53: 92 */     if (spelling.indexOf('\\') < 0) {
/*  54: 92 */       return spelling;
/*  55:    */     }
/*  56: 93 */     StringBuffer result = new StringBuffer(spelling.length());
/*  57: 94 */     int start = 0;
/*  58:    */     for (;;)
/*  59:    */     {
/*  60: 96 */       int b = spelling.indexOf('\\', start);
/*  61: 97 */       if (b < 0) {
/*  62:    */         break;
/*  63:    */       }
/*  64: 98 */       result.append(spelling.substring(start, b));
/*  65: 99 */       result.append(unEscape(spelling.charAt(b + 1)));
/*  66:100 */       start = b + 2;
/*  67:    */     }
/*  68:102 */     result.append(spelling.substring(start));
/*  69:103 */     return result.toString();
/*  70:    */   }
/*  71:    */   
/*  72:    */   private static char unEscape(char ch)
/*  73:    */   {
/*  74:107 */     switch (ch)
/*  75:    */     {
/*  76:    */     case '"': 
/*  77:    */     case '\'': 
/*  78:    */     case '\\': 
/*  79:110 */       return ch;
/*  80:    */     case 'n': 
/*  81:111 */       return '\n';
/*  82:    */     case 's': 
/*  83:112 */       return ' ';
/*  84:    */     case 't': 
/*  85:113 */       return '\t';
/*  86:    */     }
/*  87:114 */     return 'Z';
/*  88:    */   }
/*  89:    */   
/*  90:    */   private static LiteralLabel newString(PrefixMapping pm, char quote, String nodeString)
/*  91:    */   {
/*  92:119 */     int close = nodeString.lastIndexOf(quote);
/*  93:120 */     return literal(pm, nodeString.substring(1, close), nodeString.substring(close + 1));
/*  94:    */   }
/*  95:    */   
/*  96:    */   private static LiteralLabel literal(PrefixMapping pm, String spelling, String langOrType)
/*  97:    */   {
/*  98:124 */     String content = unEscape(spelling);
/*  99:125 */     int colon = langOrType.indexOf(':');
/* 100:126 */     return colon < 0 ? new LiteralLabel(content, langOrType, false) : LiteralLabel.createLiteralLabel(content, "", getType(pm.expandPrefix(langOrType)));
/* 101:    */   }
/* 102:    */   
/* 103:    */   private static RDFDatatype getType(String s)
/* 104:    */   {
/* 105:133 */     return TypeMapper.getInstance().getSafeTypeByName(s);
/* 106:    */   }
/* 107:    */   
/* 108:    */   public static Node createAnon()
/* 109:    */   {
/* 110:138 */     return createAnon(AnonId.create());
/* 111:    */   }
/* 112:    */   
/* 113:    */   public static Node createAnon(AnonId id)
/* 114:    */   {
/* 115:142 */     return create(makeAnon, id);
/* 116:    */   }
/* 117:    */   
/* 118:    */   public static Node createLiteral(LiteralLabel lit)
/* 119:    */   {
/* 120:146 */     return create(makeLiteral, lit);
/* 121:    */   }
/* 122:    */   
/* 123:    */   public static Node createURI(String uri)
/* 124:    */   {
/* 125:150 */     return create(makeURI, uri);
/* 126:    */   }
/* 127:    */   
/* 128:    */   public static Node createLiteral(String value)
/* 129:    */   {
/* 130:153 */     return createLiteral(value, "", false);
/* 131:    */   }
/* 132:    */   
/* 133:    */   public static Node createLiteral(String lit, String lang, boolean isXml)
/* 134:    */   {
/* 135:161 */     if (lit == null) {
/* 136:162 */       throw new NullPointerException("null for literals has been illegal since Jena 2.0");
/* 137:    */     }
/* 138:163 */     return createLiteral(new LiteralLabel(lit, lang, isXml));
/* 139:    */   }
/* 140:    */   
/* 141:    */   public static Node createLiteral(String lex, String lang, RDFDatatype dtype)
/* 142:    */     throws DatatypeFormatException
/* 143:    */   {
/* 144:178 */     return createLiteral(LiteralLabel.createLiteralLabel(lex, lang, dtype));
/* 145:    */   }
/* 146:    */   
/* 147:    */   public static Node createUncachedLiteral(Object value, String lang, RDFDatatype dtype)
/* 148:    */     throws DatatypeFormatException
/* 149:    */   {
/* 150:182 */     return new Node_Literal(new LiteralLabel(value, lang, dtype));
/* 151:    */   }
/* 152:    */   
/* 153:    */   public abstract boolean isConcrete();
/* 154:    */   
/* 155:    */   public boolean isLiteral()
/* 156:    */   {
/* 157:193 */     return false;
/* 158:    */   }
/* 159:    */   
/* 160:    */   public boolean isBlank()
/* 161:    */   {
/* 162:200 */     return false;
/* 163:    */   }
/* 164:    */   
/* 165:    */   public boolean isURI()
/* 166:    */   {
/* 167:207 */     return false;
/* 168:    */   }
/* 169:    */   
/* 170:    */   public boolean isVariable()
/* 171:    */   {
/* 172:214 */     return false;
/* 173:    */   }
/* 174:    */   
/* 175:    */   public AnonId getBlankNodeId()
/* 176:    */   {
/* 177:219 */     throw new JenaException(this + " is not a blank node");
/* 178:    */   }
/* 179:    */   
/* 180:    */   public String getBlankNodeLabel()
/* 181:    */   {
/* 182:227 */     return getBlankNodeId().getLabelString();
/* 183:    */   }
/* 184:    */   
/* 185:    */   public LiteralLabel getLiteral()
/* 186:    */   {
/* 187:235 */     throw new JenaException(this + " is not a literal node");
/* 188:    */   }
/* 189:    */   
/* 190:    */   public Object getLiteralValue()
/* 191:    */   {
/* 192:243 */     throw new NotLiteral(this);
/* 193:    */   }
/* 194:    */   
/* 195:    */   public String getLiteralLexicalForm()
/* 196:    */   {
/* 197:251 */     throw new NotLiteral(this);
/* 198:    */   }
/* 199:    */   
/* 200:    */   public String getLiteralLanguage()
/* 201:    */   {
/* 202:259 */     throw new NotLiteral(this);
/* 203:    */   }
/* 204:    */   
/* 205:    */   public String getLiteralDatatypeURI()
/* 206:    */   {
/* 207:267 */     throw new NotLiteral(this);
/* 208:    */   }
/* 209:    */   
/* 210:    */   public RDFDatatype getLiteralDatatype()
/* 211:    */   {
/* 212:275 */     throw new NotLiteral(this);
/* 213:    */   }
/* 214:    */   
/* 215:    */   public boolean getLiteralIsXML()
/* 216:    */   {
/* 217:279 */     throw new NotLiteral(this);
/* 218:    */   }
/* 219:    */   
/* 220:    */   static abstract class NodeMaker
/* 221:    */   {
/* 222:    */     abstract Node construct(Object paramObject);
/* 223:    */   }
/* 224:    */   
/* 225:    */   public static class NotLiteral
/* 226:    */     extends JenaException
/* 227:    */   {
/* 228:    */     public NotLiteral(Node it)
/* 229:    */     {
/* 230:288 */       super();
/* 231:    */     }
/* 232:    */   }
/* 233:    */   
/* 234:    */   public String getURI()
/* 235:    */   {
/* 236:294 */     throw new JenaException(this + " is not a URI node");
/* 237:    */   }
/* 238:    */   
/* 239:    */   public String getNameSpace()
/* 240:    */   {
/* 241:299 */     throw new JenaException(this + " is not a URI node");
/* 242:    */   }
/* 243:    */   
/* 244:    */   public String getLocalName()
/* 245:    */   {
/* 246:304 */     throw new JenaException(this + " is not a URI node");
/* 247:    */   }
/* 248:    */   
/* 249:    */   public String getName()
/* 250:    */   {
/* 251:309 */     throw new JenaException("this (" + getClass() + ") is not a variable node");
/* 252:    */   }
/* 253:    */   
/* 254:    */   public boolean hasURI(String uri)
/* 255:    */   {
/* 256:314 */     return false;
/* 257:    */   }
/* 258:    */   
/* 259:322 */   static final NodeMaker makeAnon = new NodeMaker()
/* 260:    */   {
/* 261:    */     Node construct(Object x)
/* 262:    */     {
/* 263:324 */       return new Node_Blank(x);
/* 264:    */     }
/* 265:    */   };
/* 266:328 */   static final NodeMaker makeLiteral = new NodeMaker()
/* 267:    */   {
/* 268:    */     Node construct(Object x)
/* 269:    */     {
/* 270:330 */       return new Node_Literal(x);
/* 271:    */     }
/* 272:    */   };
/* 273:334 */   static final NodeMaker makeURI = new NodeMaker()
/* 274:    */   {
/* 275:    */     Node construct(Object x)
/* 276:    */     {
/* 277:336 */       return new Node_URI(x);
/* 278:    */     }
/* 279:    */   };
/* 280:345 */   public static final Node NULL = new Node_NULL();
/* 281:    */   
/* 282:    */   Node(Object label)
/* 283:    */   {
/* 284:353 */     this.label = label;
/* 285:    */   }
/* 286:    */   
/* 287:    */   public static synchronized Node create(NodeMaker maker, Object label)
/* 288:    */   {
/* 289:364 */     if (label == null) {
/* 290:364 */       throw new JenaException("Node.make: null label");
/* 291:    */     }
/* 292:365 */     Node node = (Node)present.get(label);
/* 293:366 */     return node == null ? cacheNewNode(label, maker.construct(label)) : node;
/* 294:    */   }
/* 295:    */   
/* 296:    */   private static Node cacheNewNode(Object label, Node n)
/* 297:    */   {
/* 298:374 */     present.put(label, n);
/* 299:375 */     return n;
/* 300:    */   }
/* 301:    */   
/* 302:    */   public abstract boolean equals(Object paramObject);
/* 303:    */   
/* 304:    */   public boolean sameValueAs(Object o)
/* 305:    */   {
/* 306:394 */     return equals(o);
/* 307:    */   }
/* 308:    */   
/* 309:    */   public int hashCode()
/* 310:    */   {
/* 311:397 */     return this.label.hashCode() * 31;
/* 312:    */   }
/* 313:    */   
/* 314:    */   public boolean matches(Node other)
/* 315:    */   {
/* 316:408 */     return equals(other);
/* 317:    */   }
/* 318:    */   
/* 319:    */   public String toString()
/* 320:    */   {
/* 321:416 */     return toString(null);
/* 322:    */   }
/* 323:    */   
/* 324:    */   public String toString(boolean quoting)
/* 325:    */   {
/* 326:423 */     return toString(null, quoting);
/* 327:    */   }
/* 328:    */   
/* 329:    */   public String toString(PrefixMapping pm)
/* 330:    */   {
/* 331:430 */     return toString(pm, true);
/* 332:    */   }
/* 333:    */   
/* 334:    */   public String toString(PrefixMapping pm, boolean quoting)
/* 335:    */   {
/* 336:437 */     return this.label.toString();
/* 337:    */   }
/* 338:    */ }
