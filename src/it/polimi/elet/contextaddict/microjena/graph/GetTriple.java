package it.polimi.elet.contextaddict.microjena.graph;

public abstract interface GetTriple
{
  public abstract Triple getTriple(Node paramNode);
}
