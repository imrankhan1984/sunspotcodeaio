/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*  4:   */ 
/*  5:   */ public class Node_ANY
/*  6:   */   extends Node_Fluid
/*  7:   */ {
/*  8:   */   Node_ANY()
/*  9:   */   {
/* 10:20 */     super("");
/* 11:   */   }
/* 12:   */   
/* 13:   */   public boolean equals(Object other)
/* 14:   */   {
/* 15:25 */     return other instanceof Node_ANY;
/* 16:   */   }
/* 17:   */   
/* 18:   */   public boolean matches(Node other)
/* 19:   */   {
/* 20:29 */     return other != null;
/* 21:   */   }
/* 22:   */   
/* 23:   */   public String toString()
/* 24:   */   {
/* 25:33 */     return "ANY";
/* 26:   */   }
/* 27:   */   
/* 28:   */   public String toString(PrefixMapping pm, boolean quoting)
/* 29:   */   {
/* 30:37 */     return "ANY";
/* 31:   */   }
/* 32:   */ }

