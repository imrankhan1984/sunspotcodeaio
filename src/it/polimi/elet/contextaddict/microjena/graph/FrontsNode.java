package it.polimi.elet.contextaddict.microjena.graph;

public abstract interface FrontsNode
{
  public abstract Node asNode();
}
