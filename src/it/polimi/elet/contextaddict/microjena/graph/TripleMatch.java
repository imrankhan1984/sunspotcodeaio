package it.polimi.elet.contextaddict.microjena.graph;

public abstract interface TripleMatch
{
  public abstract Node getMatchSubject();
  
  public abstract Node getMatchPredicate();
  
  public abstract Node getMatchObject();
  
  public abstract Triple asTriple();
}
