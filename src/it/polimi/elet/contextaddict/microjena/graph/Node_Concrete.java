/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph;
/*  2:   */ 
/*  3:   */ public abstract class Node_Concrete
/*  4:   */   extends Node
/*  5:   */ {
/*  6:   */   protected Node_Concrete(Object label)
/*  7:   */   {
/*  8:17 */     super(label);
/*  9:   */   }
/* 10:   */   
/* 11:   */   public boolean isConcrete()
/* 12:   */   {
/* 13:21 */     return true;
/* 14:   */   }
/* 15:   */ }

