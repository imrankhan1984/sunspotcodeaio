/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.impl.GraphBase;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.NullIterator;
/*  7:   */ 
/*  8:   */ public abstract interface Graph
/*  9:   */   extends GraphAdd
/* 10:   */ {
/* 11:30 */   public static final Graph emptyGraph = new GraphBase()
/* 12:   */   {
/* 13:   */     public ExtendedIterator graphBaseFind(TripleMatch tm)
/* 14:   */     {
/* 15:32 */       return NullIterator.instance;
/* 16:   */     }
/* 17:   */   };
/* 18:   */   
/* 19:   */   public abstract boolean dependsOn(Graph paramGraph);
/* 20:   */   
/* 21:   */   public abstract Reifier getReifier();
/* 22:   */   
/* 23:   */   public abstract PrefixMapping getPrefixMapping();
/* 24:   */   
/* 25:   */   public abstract void delete(Triple paramTriple);
/* 26:   */   
/* 27:   */   public abstract void delete(Triple paramTriple, boolean paramBoolean);
/* 28:   */   
/* 29:   */   public abstract ExtendedIterator find(TripleMatch paramTripleMatch);
/* 30:   */   
/* 31:   */   public abstract ExtendedIterator find(Node paramNode1, Node paramNode2, Node paramNode3);
/* 32:   */   
/* 33:   */   public abstract boolean contains(Node paramNode1, Node paramNode2, Node paramNode3);
/* 34:   */   
/* 35:   */   public abstract boolean contains(Triple paramTriple);
/* 36:   */   
/* 37:   */   public abstract void close();
/* 38:   */   
/* 39:   */   public abstract boolean isEmpty();
/* 40:   */   
/* 41:   */   public abstract int size();
/* 42:   */   
/* 43:   */   public abstract boolean isClosed();
/* 44:   */ }
