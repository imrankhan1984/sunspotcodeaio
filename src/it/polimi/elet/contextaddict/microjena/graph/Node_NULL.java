/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph;
/*  2:   */ 
/*  3:   */ public class Node_NULL
/*  4:   */   extends Node_Concrete
/*  5:   */ {
/*  6:   */   Node_NULL()
/*  7:   */   {
/*  8:17 */     super("");
/*  9:   */   }
/* 10:   */   
/* 11:   */   public boolean equals(Object other)
/* 12:   */   {
/* 13:25 */     return other instanceof Node_NULL;
/* 14:   */   }
/* 15:   */   
/* 16:   */   public String toString()
/* 17:   */   {
/* 18:29 */     return "NULL";
/* 19:   */   }
/* 20:   */ }
