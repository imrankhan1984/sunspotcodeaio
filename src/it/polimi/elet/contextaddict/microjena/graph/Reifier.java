/*   1:    */ package it.polimi.elet.contextaddict.microjena.graph;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.shared.ReificationStyle;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF.Nodes;
              import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*   6:    */ 
/*   7:    */ public abstract interface Reifier
/*   8:    */   extends GetTriple
/*   9:    */ {
/*  10:    */   public abstract ExtendedIterator find(TripleMatch paramTripleMatch);
/*  11:    */   
/*  12:    */   public abstract ExtendedIterator findExposed(TripleMatch paramTripleMatch);
/*  13:    */   
/*  14:    */   public abstract ExtendedIterator findEither(TripleMatch paramTripleMatch, boolean paramBoolean);
/*  15:    */   
/*  16:    */   public abstract int size();
/*  17:    */   
/*  18:    */   public abstract ReificationStyle getStyle();
/*  19:    */   
/*  20:    */   public abstract Graph getParentGraph();
/*  21:    */   
/*  22:    */   public abstract Node reifyAs(Node paramNode, Triple paramTriple);
/*  23:    */   
/*  24:    */   public abstract boolean hasTriple(Node paramNode);
/*  25:    */   
/*  26:    */   public abstract boolean hasTriple(Triple paramTriple);
/*  27:    */   
/*  28:    */   public abstract ExtendedIterator allNodes();
/*  29:    */   
/*  30:    */   public abstract ExtendedIterator allNodes(Triple paramTriple);
/*  31:    */   
/*  32:    */   public abstract void remove(Node paramNode, Triple paramTriple);
/*  33:    */   
/*  34:    */   public abstract void remove(Triple paramTriple);
/*  35:    */   
/*  36:    */   public abstract boolean handledAdd(Triple paramTriple);
/*  37:    */   
/*  38:    */   public abstract boolean handledRemove(Triple paramTriple);
/*  39:    */   
/*  40:    */   public abstract void close();
/*  41:    */   
/*  42:    */   public static class Util
/*  43:    */   {
/*  44:    */     public static boolean isReificationPredicate(Node node)
/*  45:    */     {
/*  46:112 */       return (node.equals(RDF.Nodes.subject)) || (node.equals(RDF.Nodes.predicate)) || (node.equals(RDF.Nodes.object));
/*  47:    */     }
/*  48:    */     
/*  49:    */     public static boolean couldBeStatement(Node node)
/*  50:    */     {
/*  51:120 */       return (node.isVariable()) || (node.equals(Node.ANY)) || (node.equals(RDF.Nodes.Statement));
/*  52:    */     }
/*  53:    */     
/*  54:    */     public static boolean isReificationType(Node P, Node O)
/*  55:    */     {
/*  56:128 */       return (P.equals(RDF.Nodes.type)) && (couldBeStatement(O));
/*  57:    */     }
/*  58:    */   }
/*  59:    */ }

