/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.graph.impl.LiteralLabel;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*  6:   */ 
/*  7:   */ public class Node_Literal
/*  8:   */   extends Node_Concrete
/*  9:   */ {
/* 10:   */   Node_Literal(Object label)
/* 11:   */   {
/* 12:20 */     super(label);
/* 13:   */   }
/* 14:   */   
/* 15:   */   public LiteralLabel getLiteral()
/* 16:   */   {
/* 17:24 */     return (LiteralLabel)this.label;
/* 18:   */   }
/* 19:   */   
/* 20:   */   public final Object getLiteralValue()
/* 21:   */   {
/* 22:28 */     return getLiteral().getValue();
/* 23:   */   }
/* 24:   */   
/* 25:   */   public final String getLiteralLexicalForm()
/* 26:   */   {
/* 27:32 */     return getLiteral().getLexicalForm();
/* 28:   */   }
/* 29:   */   
/* 30:   */   public final String getLiteralLanguage()
/* 31:   */   {
/* 32:36 */     return getLiteral().language();
/* 33:   */   }
/* 34:   */   
/* 35:   */   public final String getLiteralDatatypeURI()
/* 36:   */   {
/* 37:40 */     return getLiteral().getDatatypeURI();
/* 38:   */   }
/* 39:   */   
/* 40:   */   public final RDFDatatype getLiteralDatatype()
/* 41:   */   {
/* 42:44 */     return getLiteral().getDatatype();
/* 43:   */   }
/* 44:   */   
/* 45:   */   public String toString(PrefixMapping pm, boolean quoting)
/* 46:   */   {
/* 47:48 */     return ((LiteralLabel)this.label).toString(quoting);
/* 48:   */   }
/* 49:   */   
/* 50:   */   public boolean isLiteral()
/* 51:   */   {
/* 52:52 */     return true;
/* 53:   */   }
/* 54:   */   
/* 55:   */   public boolean equals(Object other)
/* 56:   */   {
/* 57:56 */     return ((other instanceof Node_Literal)) && (this.label.equals(((Node_Literal)other).label));
/* 58:   */   }
/* 59:   */   
/* 60:   */   public boolean sameValueAs(Object o)
/* 61:   */   {
/* 62:70 */     return ((o instanceof Node_Literal)) && (((LiteralLabel)this.label).sameValueAs(((Node_Literal)o).getLiteral()));
/* 63:   */   }
/* 64:   */   
/* 65:   */   public boolean matches(Node x)
/* 66:   */   {
/* 67:75 */     if ((x instanceof Node_ANY)) {
/* 68:76 */       return true;
/* 69:   */     }
/* 70:78 */     return sameValueAs(x);
/* 71:   */   }
/* 72:   */ }

