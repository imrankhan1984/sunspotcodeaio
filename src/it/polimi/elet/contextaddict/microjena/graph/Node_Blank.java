/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.AnonId;
/*  4:   */ 
/*  5:   */ public class Node_Blank
/*  6:   */   extends Node_Concrete
/*  7:   */ {
/*  8:   */   Node_Blank(Object id)
/*  9:   */   {
/* 10:19 */     super(id);
/* 11:   */   }
/* 12:   */   
/* 13:   */   public boolean isBlank()
/* 14:   */   {
/* 15:22 */     return true;
/* 16:   */   }
/* 17:   */   
/* 18:   */   public AnonId getBlankNodeId()
/* 19:   */   {
/* 20:24 */     return (AnonId)this.label;
/* 21:   */   }
/* 22:   */   
/* 23:   */   public boolean matches(Node other)
/* 24:   */   {
/* 25:27 */     if ((other instanceof Node_ANY)) {
/* 26:28 */       return true;
/* 27:   */     }
/* 28:30 */     return equals(other);
/* 29:   */   }
/* 30:   */   
/* 31:   */   public boolean equals(Object other)
/* 32:   */   {
/* 33:34 */     return ((other instanceof Node_Blank)) && (this.label.equals(((Node_Blank)other).label));
/* 34:   */   }
/* 35:   */ }

