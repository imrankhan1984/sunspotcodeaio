package it.polimi.elet.contextaddict.microjena.graph;

public abstract interface FrontsTriple
{
  public abstract Triple asTriple();
}
