/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph;
/*  2:   */ 
/*  3:   */ public abstract class Node_Fluid
/*  4:   */   extends Node
/*  5:   */ {
/*  6:   */   protected Node_Fluid(Object label)
/*  7:   */   {
/*  8:16 */     super(label);
/*  9:   */   }
/* 10:   */   
/* 11:   */   public boolean isConcrete()
/* 12:   */   {
/* 13:20 */     return false;
/* 14:   */   }
/* 15:   */ }
