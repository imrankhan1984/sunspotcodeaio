package it.polimi.elet.contextaddict.microjena.graph.impl;

import it.polimi.elet.contextaddict.microjena.graph.Node;
import it.polimi.elet.contextaddict.microjena.graph.Triple;

public abstract interface ReifierFragmentHandler
{
  public abstract boolean clashedWith(Node paramNode1, Node paramNode2, Triple paramTriple);
  
  public abstract Triple reifyIfCompleteQuad(Triple paramTriple, Node paramNode1, Node paramNode2);
  
  public abstract Triple removeFragment(Node paramNode, Triple paramTriple1, Triple paramTriple2);
}

