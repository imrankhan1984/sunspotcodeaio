/*   1:    */ package it.polimi.elet.contextaddict.microjena.graph.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.BaseDatatype;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.datatypes.DatatypeFormatException;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.XSDDatatype;
/*   7:    */ 
/*   8:    */ public final class LiteralLabel
/*   9:    */ {
/*  10:    */   private String lexicalForm;
/*  11:    */   private Object value;
/*  12:    */   private RDFDatatype dtype;
/*  13:    */   private final String lang;
/*  14:    */   
/*  15:    */   public static LiteralLabel createLiteralLabel(String lex, String lang, RDFDatatype dtype)
/*  16:    */     throws DatatypeFormatException
/*  17:    */   {
/*  18: 28 */     return new LiteralLabel(lex, lang, dtype);
/*  19:    */   }
/*  20:    */   
/*  21: 64 */   private boolean wellformed = true;
/*  22:    */   
/*  23:    */   private LiteralLabel(String lex, String lang, RDFDatatype dtype)
/*  24:    */     throws DatatypeFormatException
/*  25:    */   {
/*  26: 80 */     this.lexicalForm = lex;
/*  27: 81 */     this.dtype = dtype;
/*  28: 82 */     this.lang = (lang == null ? "" : lang);
/*  29: 83 */     if (dtype == null) {
/*  30: 84 */       this.value = lex;
/*  31:    */     } else {
/*  32: 86 */       setValue(lex);
/*  33:    */     }
/*  34: 88 */     normalize();
/*  35:    */   }
/*  36:    */   
/*  37:    */   public LiteralLabel(String lex, String lang)
/*  38:    */   {
/*  39: 97 */     this(lex, lang, null);
/*  40:    */   }
/*  41:    */   
/*  42:    */   public LiteralLabel(Object value, String lang, RDFDatatype dtype)
/*  43:    */     throws DatatypeFormatException
/*  44:    */   {
/*  45:109 */     this.dtype = dtype;
/*  46:110 */     this.lang = (lang == null ? "" : lang);
/*  47:111 */     if ((value instanceof String))
/*  48:    */     {
/*  49:112 */       String lex = (String)value;
/*  50:113 */       this.lexicalForm = lex;
/*  51:114 */       if (dtype == null) {
/*  52:115 */         this.value = lex;
/*  53:    */       } else {
/*  54:117 */         setValue(lex);
/*  55:    */       }
/*  56:    */     }
/*  57:    */     else
/*  58:    */     {
/*  59:120 */       this.value = (dtype == null ? value : dtype.cannonicalise(value));
/*  60:    */     }
/*  61:123 */     normalize();
/*  62:125 */     if ((dtype != null) && (this.lexicalForm == null))
/*  63:    */     {
/*  64:128 */       this.wellformed = this.dtype.isValidValue(value);
/*  65:129 */       if (!this.wellformed) {
/*  66:130 */         throw new DatatypeFormatException(value.toString(), dtype, "in literal creation");
/*  67:    */       }
/*  68:    */     }
/*  69:    */   }
/*  70:    */   
/*  71:    */   public LiteralLabel(String lex, Object value, String lang, RDFDatatype dtype)
/*  72:    */   {
/*  73:146 */     this(value, lang, dtype);
/*  74:147 */     this.lexicalForm = lex;
/*  75:    */   }
/*  76:    */   
/*  77:    */   public LiteralLabel(Object value)
/*  78:    */   {
/*  79:157 */     this(value, "", null);
/*  80:    */   }
/*  81:    */   
/*  82:    */   private void setValue(String lex)
/*  83:    */     throws DatatypeFormatException
/*  84:    */   {
/*  85:167 */     this.value = this.dtype.parse(lex);
/*  86:168 */     this.wellformed = true;
/*  87:    */   }
/*  88:    */   
/*  89:    */   public LiteralLabel(String s, String lg, boolean xml)
/*  90:    */   {
/*  91:178 */     this.lexicalForm = s;
/*  92:179 */     this.lang = (lg == null ? "" : lg);
/*  93:180 */     this.value = s;
/*  94:181 */     if (xml) {
/*  95:183 */       this.dtype = new BaseDatatype("http://www.w3.org/1999/02/22-rdf-syntax-ns#XMLLiteral");
/*  96:    */     } else {
/*  97:186 */       this.dtype = null;
/*  98:    */     }
/*  99:    */   }
/* 100:    */   
/* 101:    */   protected void normalize()
/* 102:    */   {
/* 103:197 */     if ((this.dtype != null) && (this.value != null)) {
/* 104:198 */       this.dtype = this.dtype.normalizeSubType(this.value, this.dtype);
/* 105:    */     }
/* 106:    */   }
/* 107:    */   
/* 108:    */   public boolean isWellFormed()
/* 109:    */   {
/* 110:209 */     return (this.dtype != null) && (this.wellformed);
/* 111:    */   }
/* 112:    */   
/* 113:    */   public String toString(boolean quoting)
/* 114:    */   {
/* 115:217 */     StringBuffer b = new StringBuffer();
/* 116:218 */     if (quoting) {
/* 117:218 */       b.append('"');
/* 118:    */     }
/* 119:219 */     b.append(getLexicalForm());
/* 120:220 */     if (quoting) {
/* 121:220 */       b.append('"');
/* 122:    */     }
/* 123:221 */     if ((this.lang != null) && (!this.lang.equals(""))) {
/* 124:221 */       b.append("@").append(this.lang);
/* 125:    */     }
/* 126:222 */     if (this.dtype != null) {
/* 127:222 */       b.append("^^").append(this.dtype.getURI());
/* 128:    */     }
/* 129:223 */     return b.toString();
/* 130:    */   }
/* 131:    */   
/* 132:    */   public String toString()
/* 133:    */   {
/* 134:227 */     return toString(false);
/* 135:    */   }
/* 136:    */   
/* 137:    */   public String getLexicalForm()
/* 138:    */   {
/* 139:235 */     if (this.lexicalForm == null) {
/* 140:236 */       this.lexicalForm = (this.dtype == null ? this.value.toString() : this.dtype.unparse(this.value));
/* 141:    */     }
/* 142:237 */     return this.lexicalForm;
/* 143:    */   }
/* 144:    */   
/* 145:    */   public String language()
/* 146:    */   {
/* 147:245 */     return this.lang;
/* 148:    */   }
/* 149:    */   
/* 150:    */   public Object getValue()
/* 151:    */     throws DatatypeFormatException
/* 152:    */   {
/* 153:253 */     if (this.wellformed) {
/* 154:254 */       return this.value;
/* 155:    */     }
/* 156:256 */     throw new DatatypeFormatException(this.lexicalForm, this.dtype, " in getValue()");
/* 157:    */   }
/* 158:    */   
/* 159:    */   public RDFDatatype getDatatype()
/* 160:    */   {
/* 161:264 */     return this.dtype;
/* 162:    */   }
/* 163:    */   
/* 164:    */   public String getDatatypeURI()
/* 165:    */   {
/* 166:271 */     if (this.dtype == null) {
/* 167:272 */       return null;
/* 168:    */     }
/* 169:273 */     return this.dtype.getURI();
/* 170:    */   }
/* 171:    */   
/* 172:    */   public boolean equals(Object other)
/* 173:    */   {
/* 174:281 */     if ((other == null) || (!(other instanceof LiteralLabel))) {
/* 175:282 */       return false;
/* 176:    */     }
/* 177:284 */     LiteralLabel otherLiteral = (LiteralLabel)other;
/* 178:285 */     boolean typeEqual = this.dtype == null ? false : otherLiteral.dtype == null ? true : this.dtype.equals(otherLiteral.dtype);
/* 179:    */     
/* 180:    */ 
/* 181:    */ 
/* 182:289 */     boolean langEqual = this.dtype == null ? this.lang.equals(otherLiteral.lang) : true;
/* 183:    */     
/* 184:291 */     return (typeEqual) && (langEqual) && (getLexicalForm().equals(otherLiteral.getLexicalForm()));
/* 185:    */   }
/* 186:    */   
/* 187:    */   public boolean sameValueAs(LiteralLabel other)
/* 188:    */   {
/* 189:301 */     if (other == null) {
/* 190:302 */       return false;
/* 191:    */     }
/* 192:303 */     if ((!this.wellformed) || (!other.wellformed))
/* 193:    */     {
/* 194:304 */       if (!other.wellformed) {
/* 195:307 */         return (this.lexicalForm.equals(other.lexicalForm)) && (this.lang.toLowerCase().equals(other.lang.toLowerCase()));
/* 196:    */       }
/* 197:310 */       return false;
/* 198:    */     }
/* 199:313 */     if (this.dtype == null)
/* 200:    */     {
/* 201:315 */       if ((other.dtype == null) || (other.dtype.equals(XSDDatatype.XSDstring))) {
/* 202:316 */         return (this.lexicalForm.equals(other.lexicalForm)) && (this.lang.toLowerCase().equals(other.lang.toLowerCase()));
/* 203:    */       }
/* 204:318 */       return false;
/* 205:    */     }
/* 206:322 */     return this.dtype.isEqual(this, other);
/* 207:    */   }
/* 208:    */   
/* 209:    */   public int hashCode()
/* 210:    */   {
/* 211:331 */     return this.dtype == null ? getDefaultHashcode() : this.dtype.getHashCode(this);
/* 212:    */   }
/* 213:    */   
/* 214:    */   public int getDefaultHashcode()
/* 215:    */   {
/* 216:340 */     return (this.wellformed ? this.value : getLexicalForm()).hashCode();
/* 217:    */   }
/* 218:    */ }
