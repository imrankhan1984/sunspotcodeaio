/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.GraphAdd;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.util.Map.Entry;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.NiceIterator;
            import it.polimi.elet.contextaddict.microjena.util.Map;
/*  9:   */ 
/* 10:   */ public abstract class FragmentTripleIterator
/* 11:   */   extends NiceIterator
/* 12:   */ {
/* 13:   */   private final GraphAddList pending;
/* 14:   */   private final Iterator it;
/* 15:   */   
/* 16:   */   public FragmentTripleIterator(Triple match, Iterator it)
/* 17:   */   {
/* 18:33 */     this.it = it;
/* 19:34 */     this.pending = new GraphAddList(match);
/* 20:   */   }
/* 21:   */   
/* 22:   */   public boolean hasNext()
/* 23:   */   {
/* 24:44 */     refill();
/* 25:45 */     return this.pending.size() > 0;
/* 26:   */   }
/* 27:   */   
/* 28:   */   public Object next()
/* 29:   */   {
/* 30:56 */     hasNext();
/* 31:57 */     return this.pending.remove(this.pending.size() - 1);
/* 32:   */   }
/* 33:   */   
/* 34:   */   protected abstract void fill(GraphAdd paramGraphAdd, Node paramNode, Object paramObject);
/* 35:   */   
/* 36:   */   private void refill()
/* 37:   */   {
/* 38:72 */     while ((this.pending.size() == 0) && (this.it.hasNext())) {
/* 39:73 */       refillFrom(this.pending, this.it.next());
/* 40:   */     }
/* 41:   */   }
/* 42:   */   
/* 43:   */   protected void refillFrom(GraphAdd pending, Object next)
/* 44:   */   {
/* 45:82 */     Map.Entry e = (Map.Entry)next;
/* 46:83 */     fill(pending, (Node)e.getKey(), e.getValue());
/* 47:   */   }
/* 48:   */ }

