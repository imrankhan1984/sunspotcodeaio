/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*  5:   */ 
/*  6:   */ public abstract class SimpleReifierFragmentHandler
/*  7:   */   implements ReifierFragmentHandler
/*  8:   */ {
/*  9:   */   int which;
/* 10:   */   SimpleReifierFragmentsMap map;
/* 11:   */   
/* 12:   */   public SimpleReifierFragmentHandler(SimpleReifierFragmentsMap map, int n)
/* 13:   */   {
/* 14:17 */     this.which = n;
/* 15:18 */     this.map = map;
/* 16:   */   }
/* 17:   */   
/* 18:   */   public abstract boolean clashesWith(ReifierFragmentsMap paramReifierFragmentsMap, Node paramNode, Triple paramTriple);
/* 19:   */   
/* 20:   */   public boolean clashedWith(Node tag, Node fragmentObject, Triple reified)
/* 21:   */   {
/* 22:24 */     if (clashesWith(this.map, fragmentObject, reified))
/* 23:   */     {
/* 24:25 */       this.map.putAugmentedTriple(this, tag, fragmentObject, reified);
/* 25:26 */       return true;
/* 26:   */     }
/* 27:28 */     return false;
/* 28:   */   }
/* 29:   */   
/* 30:   */   public Triple reifyIfCompleteQuad(Triple fragment, Node tag, Node object)
/* 31:   */   {
/* 32:32 */     return this.map.reifyCompleteQuad(this, fragment, tag, object);
/* 33:   */   }
/* 34:   */   
/* 35:   */   public Triple removeFragment(Node tag, Triple already, Triple fragment)
/* 36:   */   {
/* 37:42 */     return this.map.removeFragment(this, tag, already, fragment);
/* 38:   */   }
/* 39:   */ }

