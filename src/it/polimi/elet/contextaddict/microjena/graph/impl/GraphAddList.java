/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.GraphAdd;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.util.List;
/*  6:   */ 
/*  7:   */ public class GraphAddList
/*  8:   */   extends List
/*  9:   */   implements GraphAdd
/* 10:   */ {
/* 11:   */   protected Triple match;
/* 12:   */   
/* 13:   */   public GraphAddList(Triple match)
/* 14:   */   {
/* 15:27 */     this.match = match;
/* 16:   */   }
/* 17:   */   
/* 18:   */   public void add(Triple t)
/* 19:   */   {
/* 20:34 */     if (this.match.matches(t)) {
/* 21:34 */       super.add(t);
/* 22:   */     }
/* 23:   */   }
/* 24:   */ }

