package it.polimi.elet.contextaddict.microjena.graph.impl;

import it.polimi.elet.contextaddict.microjena.graph.Node;
import it.polimi.elet.contextaddict.microjena.graph.Triple;
import it.polimi.elet.contextaddict.microjena.graph.TripleMatch;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface ReifierFragmentsMap
{
  public abstract ExtendedIterator find(TripleMatch paramTripleMatch);
  
  public abstract int size();
  
  public abstract ReifierFragmentHandler getFragmentHandler(Triple paramTriple);
  
  public abstract boolean hasFragments(Node paramNode);
  
  public abstract void clear();
}

