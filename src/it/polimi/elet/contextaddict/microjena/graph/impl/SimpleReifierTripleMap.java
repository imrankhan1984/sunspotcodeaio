/*   1:    */ package it.polimi.elet.contextaddict.microjena.graph.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.GraphAdd;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.TripleMatch;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.NullIterator;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  14:    */ 
/*  15:    */ public class SimpleReifierTripleMap
/*  16:    */   implements ReifierTripleMap
/*  17:    */ {
/*  18: 28 */   protected Map inverseMap = new Map();
/*  19: 29 */   protected Map forwardMap = new Map();
/*  20:    */   
/*  21:    */   public Triple getTriple(Node tag)
/*  22:    */   {
/*  23: 32 */     return (Triple)this.forwardMap.get(tag);
/*  24:    */   }
/*  25:    */   
/*  26:    */   public void clear()
/*  27:    */   {
/*  28: 36 */     this.forwardMap.clear();
/*  29: 37 */     this.inverseMap.clear();
/*  30:    */   }
/*  31:    */   
/*  32:    */   public boolean hasTriple(Triple t)
/*  33:    */   {
/*  34: 43 */     return this.inverseMap.containsKey(t);
/*  35:    */   }
/*  36:    */   
/*  37:    */   public Triple putTriple(Node key, Triple value)
/*  38:    */   {
/*  39: 47 */     this.forwardMap.put(key, value);
/*  40: 48 */     inversePut(value, key);
/*  41: 49 */     return value;
/*  42:    */   }
/*  43:    */   
/*  44:    */   public void removeTriple(Node key)
/*  45:    */   {
/*  46: 53 */     Object t = this.forwardMap.get(key);
/*  47: 54 */     this.forwardMap.remove(key);
/*  48: 55 */     if ((t instanceof Triple)) {
/*  49: 56 */       inverseRemove((Triple)t, key);
/*  50:    */     }
/*  51:    */   }
/*  52:    */   
/*  53:    */   public void removeTriple(Node key, Triple value)
/*  54:    */   {
/*  55: 60 */     this.forwardMap.remove(key);
/*  56: 61 */     inverseRemove(value, key);
/*  57:    */   }
/*  58:    */   
/*  59:    */   public void removeTriple(Triple t)
/*  60:    */   {
/*  61: 65 */     ExtendedIterator it = tagIterator(t);
/*  62: 66 */     Set nodes = new Set();
/*  63: 67 */     while (it.hasNext()) {
/*  64: 68 */       nodes.add(it.next());
/*  65:    */     }
/*  66: 69 */     Iterator them = nodes.iterator();
/*  67: 70 */     while (them.hasNext()) {
/*  68: 71 */       removeTriple((Node)them.next());
/*  69:    */     }
/*  70:    */   }
/*  71:    */   
/*  72:    */   protected void inverseRemove(Triple value, Node key)
/*  73:    */   {
/*  74: 75 */     Set s = (Set)this.inverseMap.get(value);
/*  75: 76 */     if (s != null)
/*  76:    */     {
/*  77: 77 */       s.remove(key);
/*  78: 78 */       if (s.isEmpty()) {
/*  79: 79 */         this.inverseMap.remove(value);
/*  80:    */       }
/*  81:    */     }
/*  82:    */   }
/*  83:    */   
/*  84:    */   protected void inversePut(Triple value, Node key)
/*  85:    */   {
/*  86: 84 */     Set s = (Set)this.inverseMap.get(value);
/*  87: 85 */     if (s == null) {
/*  88: 86 */       this.inverseMap.put(value, s = new Set());
/*  89:    */     }
/*  90: 87 */     s.add(key);
/*  91:    */   }
/*  92:    */   
/*  93:    */   public ExtendedIterator tagIterator(Triple t)
/*  94:    */   {
/*  95: 92 */     Set s = (Set)this.inverseMap.get(t);
/*  96: 93 */     return s == null ? NullIterator.instance : WrappedIterator.create(s.iterator());
/*  97:    */   }
/*  98:    */   
/*  99:    */   protected ExtendedIterator allTriples(TripleMatch tm)
/* 100:    */   {
/* 101: 99 */     if (this.forwardMap.isEmpty())
/* 102:100 */       return NullIterator.instance;
/* 103:    */     Triple pattern = tm.asTriple();
/* 105:102 */     Node tag = pattern.getSubject();
/* 106:103 */     if (tag.isConcrete()) {
/* 108:104 */       Triple x = getTriple(tag);
/* 109:105 */       return x == null ? NullIterator.instance : explodeTriple(pattern, tag, x);
/* 110:    */     }
                  else {
/* 111:109 */     Iterator it = this.forwardMap.entrySet().iterator();
/* 112:110 */     return new FragmentTripleIterator(pattern, it)
/* 113:    */     {
/* 114:    */       public void fill(GraphAdd ga, Node n, Object fragmentsObject)
/* 115:    */       {
/* 116:112 */         SimpleReifier.graphAddQuad(ga, n, (Triple)fragmentsObject);
/* 117:    */       }
/* 118:    */     };
                }
/* 119:    */   }
/* 120:    */   
/* 121:    */   public static ExtendedIterator explodeTriple(Triple pattern, Node tag, Triple toExplode)
/* 122:    */   {
/* 123:123 */     GraphAddList L = new GraphAddList(pattern);
/* 124:124 */     SimpleReifier.graphAddQuad(L, tag, toExplode);
/* 125:125 */     return WrappedIterator.create(L.iterator());
/* 126:    */   }
/* 127:    */   
/* 128:    */   public Graph asGraph()
/* 129:    */   {
/* 130:134 */     return new GraphBase()
/* 131:    */     {
/* 132:    */       public ExtendedIterator graphBaseFind(TripleMatch tm)
/* 133:    */       {
/* 134:136 */         return SimpleReifierTripleMap.this.allTriples(tm);
/* 135:    */       }
/* 136:    */     };
/* 137:    */   }
/* 138:    */   
/* 139:    */   public ExtendedIterator find(TripleMatch m)
/* 140:    */   {
/* 141:142 */     return allTriples(m);
/* 142:    */   }
/* 143:    */   
/* 144:    */   public int size()
/* 145:    */   {
/* 146:146 */     return this.forwardMap.size() * 4;
/* 147:    */   }
/* 148:    */   
/* 149:    */   public ExtendedIterator tagIterator()
/* 150:    */   {
/* 151:153 */     return WrappedIterator.create(this.forwardMap.keySet().iterator());
/* 152:    */   }
/* 153:    */ }

