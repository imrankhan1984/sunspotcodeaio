/*   1:    */ package it.polimi.elet.contextaddict.microjena.graph.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.GraphUtil;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Reifier;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.graph.TripleMatch;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.shared.AddDeniedException;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.shared.ClosedException;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.shared.DeleteDeniedException;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.shared.ReificationStyle;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.shared.impl.PrefixMappingImpl;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  17:    */ 
/*  18:    */ public abstract class GraphBase
/*  19:    */   implements GraphWithPerform
/*  20:    */ {
/*  21:    */   protected final ReificationStyle style;
/*  22: 51 */   protected boolean closed = false;
/*  23: 53 */   protected Map nodes = new Map(20, 20);
/*  24:    */   
/*  25:    */   public GraphBase()
/*  26:    */   {
/*  27: 60 */     this(ReificationStyle.Minimal);
/*  28:    */   }
/*  29:    */   
/*  30:    */   public GraphBase(ReificationStyle style)
/*  31:    */   {
/*  32: 68 */     this.style = style;
/*  33:    */   }
/*  34:    */   
/*  35:    */   protected void checkOpen()
/*  36:    */   {
/*  37: 75 */     if (this.closed) {
/*  38: 75 */       throw new ClosedException("already closed", this);
/*  39:    */     }
/*  40:    */   }
/*  41:    */   
/*  42:    */   public void close()
/*  43:    */   {
/*  44: 82 */     this.closed = true;
/*  45: 83 */     if (this.reifier != null) {
/*  46: 83 */       this.reifier.close();
/*  47:    */     }
/*  48:    */   }
/*  49:    */   
/*  50:    */   public boolean isClosed()
/*  51:    */   {
/*  52: 87 */     return this.closed;
/*  53:    */   }
/*  54:    */   
/*  55:    */   public boolean dependsOn(Graph other)
/*  56:    */   {
/*  57: 95 */     return this == other;
/*  58:    */   }
/*  59:    */   
/*  60:    */   public PrefixMapping getPrefixMapping()
/*  61:    */   {
/*  62:103 */     return this.pm;
/*  63:    */   }
/*  64:    */   
/*  65:106 */   protected PrefixMapping pm = new PrefixMappingImpl();
/*  66:    */   
/*  67:    */   public void add(Triple t)
/*  68:    */   {
/*  69:114 */     checkOpen();
/*  70:115 */     performAdd(t);
/*  71:    */   }
/*  72:    */   
/*  73:    */   public void performAdd(Triple t)
/*  74:    */   {
/*  75:124 */     throw new AddDeniedException("GraphBase::performAdd");
/*  76:    */   }
/*  77:    */   
/*  78:    */   public final void delete(Triple t)
/*  79:    */   {
/*  80:133 */     delete(t, false);
/*  81:    */   }
/*  82:    */   
/*  83:    */   public final void delete(Triple t, boolean removeAxiom)
/*  84:    */   {
/*  85:137 */     checkOpen();
/*  86:138 */     performDelete(t, removeAxiom);
/*  87:    */   }
/*  88:    */   
/*  89:    */   public void performDelete(Triple t)
/*  90:    */   {
/*  91:147 */     throw new DeleteDeniedException("GraphBase::delete");
/*  92:    */   }
/*  93:    */   
/*  94:    */   public void performDelete(Triple t, boolean removeAxiom)
/*  95:    */   {
/*  96:151 */     throw new DeleteDeniedException("GraphBase::delete");
/*  97:    */   }
/*  98:    */   
/*  99:    */   public final ExtendedIterator find(TripleMatch m)
/* 100:    */   {
/* 101:161 */     checkOpen();
/* 102:162 */     return reifierTriples(m).andThen(graphBaseFind(m));
/* 103:    */   }
/* 104:    */   
/* 105:    */   protected abstract ExtendedIterator graphBaseFind(TripleMatch paramTripleMatch);
/* 106:    */   
/* 107:    */   public ExtendedIterator forTestingOnly_graphBaseFind(TripleMatch tm)
/* 108:    */   {
/* 109:173 */     return graphBaseFind(tm);
/* 110:    */   }
/* 111:    */   
/* 112:    */   public final ExtendedIterator find(Node s, Node p, Node o)
/* 113:    */   {
/* 114:177 */     checkOpen();
/* 115:178 */     return graphBaseFind(s, p, o);
/* 116:    */   }
/* 117:    */   
/* 118:    */   protected ExtendedIterator graphBaseFind(Node s, Node p, Node o)
/* 119:    */   {
/* 120:182 */     return find(Triple.createMatch(s, p, o));
/* 121:    */   }
/* 122:    */   
/* 123:    */   public final boolean contains(Triple t)
/* 124:    */   {
/* 125:192 */     checkOpen();
/* 126:193 */     return (reifierContains(t)) || (graphBaseContains(t));
/* 127:    */   }
/* 128:    */   
/* 129:    */   protected boolean reifierContains(Triple t)
/* 130:    */   {
/* 131:203 */     ExtendedIterator it = getReifier().findExposed(t);
/* 132:    */     try
/* 133:    */     {
/* 134:205 */       return it.hasNext();
/* 135:    */     }
/* 136:    */     finally
/* 137:    */     {
/* 138:207 */       it.close();
/* 139:    */     }
/* 140:    */   }
/* 141:    */   
/* 142:    */   protected boolean graphBaseContains(Triple t)
/* 143:    */   {
/* 144:217 */     return containsByFind(t);
/* 145:    */   }
/* 146:    */   
/* 147:    */   public final boolean contains(Node s, Node p, Node o)
/* 148:    */   {
/* 149:225 */     checkOpen();
/* 150:226 */     return contains(Triple.create(s, p, o));
/* 151:    */   }
/* 152:    */   
/* 153:    */   protected final boolean containsByFind(Triple t)
/* 154:    */   {
/* 155:237 */     ExtendedIterator it = find(t);
/* 156:    */     try
/* 157:    */     {
/* 158:239 */       return it.hasNext();
/* 159:    */     }
/* 160:    */     finally
/* 161:    */     {
/* 162:241 */       it.close();
/* 163:    */     }
/* 164:    */   }
/* 165:    */   
/* 166:    */   protected ExtendedIterator reifierTriples(TripleMatch m)
/* 167:    */   {
/* 168:251 */     return getReifier().findExposed(m);
/* 169:    */   }
/* 170:    */   
/* 171:    */   public Reifier getReifier()
/* 172:    */   {
/* 173:261 */     if (this.reifier == null) {
/* 174:261 */       this.reifier = constructReifier();
/* 175:    */     }
/* 176:262 */     return this.reifier;
/* 177:    */   }
/* 178:    */   
/* 179:    */   protected Reifier constructReifier()
/* 180:    */   {
/* 181:270 */     return new SimpleReifier(this, this.style);
/* 182:    */   }
/* 183:    */   
/* 184:276 */   protected Reifier reifier = null;
/* 185:    */   
/* 186:    */   public final int size()
/* 187:    */   {
/* 188:285 */     checkOpen();
/* 189:286 */     int baseSize = graphBaseSize();
/* 190:287 */     int reifierSize = reifierSize();
/* 191:288 */     return baseSize + reifierSize;
/* 192:    */   }
/* 193:    */   
/* 194:    */   private String leafName(String name)
/* 195:    */   {
/* 196:292 */     int dot = name.lastIndexOf('.');
/* 197:293 */     return name.substring(dot + 1);
/* 198:    */   }
/* 199:    */   
/* 200:    */   protected int reifierSize()
/* 201:    */   {
/* 202:302 */     return getReifier().size();
/* 203:    */   }
/* 204:    */   
/* 205:    */   protected int graphBaseSize()
/* 206:    */   {
                int tripleCount;
/* 207:311 */     ExtendedIterator it = GraphUtil.findAll(this);
/* 208:312 */     for (tripleCount = 0; it.hasNext(); tripleCount++) {
/* 209:313 */       it.next();
/* 210:    */     }
/* 211:314 */     return tripleCount;
/* 212:    */   }
/* 213:    */   
/* 214:    */   public boolean isEmpty()
/* 215:    */   {
/* 216:325 */     return size() == 0;
/* 217:    */   }
/* 218:    */   
/* 219:    */   public String toString()
/* 220:    */   {
/* 221:334 */     return toString(this.closed ? "closed " : "", this);
/* 222:    */   }
/* 223:    */   
/* 224:    */   public static String toString(String prefix, Graph that)
/* 225:    */   {
/* 226:345 */     PrefixMapping pm = that.getPrefixMapping();
/* 227:346 */     StringBuffer b = new StringBuffer(prefix + " {");
/* 228:347 */     String gap = "";
/* 229:348 */     ExtendedIterator it = GraphUtil.findAll(that);
/* 230:349 */     while (it.hasNext())
/* 231:    */     {
/* 232:350 */       b.append(gap);
/* 233:351 */       gap = "; ";
/* 234:352 */       b.append(((Triple)it.next()).toString(pm));
/* 235:    */     }
/* 236:354 */     b.append("}");
/* 237:355 */     return b.toString();
/* 238:    */   }
/* 239:    */ }

