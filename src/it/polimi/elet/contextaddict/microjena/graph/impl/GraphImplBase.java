/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.shared.ReificationStyle;
/*  6:   */ 
/*  7:   */ public abstract class GraphImplBase
/*  8:   */   extends GraphBase
/*  9:   */ {
/* 10:   */   protected int count;
/* 11:   */   
/* 12:   */   public GraphImplBase(ReificationStyle style)
/* 13:   */   {
/* 14:34 */     super(style);
/* 15:35 */     this.count = 1;
/* 16:   */   }
/* 17:   */   
/* 18:   */   public GraphImplBase openAgain()
/* 19:   */   {
/* 20:43 */     this.count += 1;
/* 21:44 */     return this;
/* 22:   */   }
/* 23:   */   
/* 24:   */   protected abstract void destroy();
/* 25:   */   
/* 26:   */   public void close()
/* 27:   */   {
/* 28:58 */     if (--this.count == 0)
/* 29:   */     {
/* 30:59 */       destroy();
/* 31:60 */       super.close();
/* 32:   */     }
/* 33:   */   }
/* 34:   */   
/* 35:   */   public abstract void clear();
/* 36:   */   
/* 37:   */   protected final boolean isSafeForEquality(Triple t)
/* 38:   */   {
/* 39:74 */     return (t.isConcrete()) && (!t.getObject().isLiteral());
/* 40:   */   }
/* 41:   */ }

