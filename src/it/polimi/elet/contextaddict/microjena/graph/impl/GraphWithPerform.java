package it.polimi.elet.contextaddict.microjena.graph.impl;

import it.polimi.elet.contextaddict.microjena.graph.Graph;
import it.polimi.elet.contextaddict.microjena.graph.Triple;

public abstract interface GraphWithPerform
  extends Graph
{
  public abstract void performAdd(Triple paramTriple);
  
  public abstract void performDelete(Triple paramTriple);
}

