/*   1:    */ package it.polimi.elet.contextaddict.microjena.graph.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.GraphAdd;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.TripleMatch;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.util.Map.Entry;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.NullIterator;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  15:    */ 
/*  16:    */ public class SimpleReifierFragmentsMap
/*  17:    */   implements ReifierFragmentsMap
/*  18:    */ {
/*  19: 27 */   protected Map forwardMap = new Map();
/*  20:    */   protected static final int TYPES_index = 0;
/*  21:    */   protected static final int SUBJECTS_index = 1;
/*  22:    */   protected static final int PREDICATES_index = 2;
/*  23:    */   protected static final int OBJECTS_index = 3;
/*  24:    */   
/*  25:    */   protected Fragments getFragments(Node tag)
/*  26:    */   {
/*  27: 30 */     return (Fragments)this.forwardMap.get(tag);
/*  28:    */   }
/*  29:    */   
/*  30:    */   protected void removeFragments(Node key)
/*  31:    */   {
/*  32: 34 */     this.forwardMap.remove(key);
/*  33:    */   }
/*  34:    */   
/*  35:    */   public void clear()
/*  36:    */   {
/*  37: 38 */     this.forwardMap.clear();
/*  38:    */   }
/*  39:    */   
/*  40:    */   protected Fragments putFragments(Node key, Fragments value)
/*  41:    */   {
/*  42: 45 */     this.forwardMap.put(key, value);
/*  43: 46 */     return value;
/*  44:    */   }
/*  45:    */   
/*  46:    */   protected ExtendedIterator allTriples(TripleMatch tm)
/*  47:    */   {
/*  48: 50 */     if (this.forwardMap.isEmpty()) 
/*  49: 51 */       return NullIterator.instance;
/*  50:    */     Triple t = tm.asTriple();
/*  52: 53 */     Node subject = t.getSubject();
/*  53: 54 */     if (subject.isConcrete())
/*  54:    */     {
/*  55: 55 */       Fragments x = (Fragments)this.forwardMap.get(subject);
/*  56: 56 */       return x == null ? NullIterator.instance : explodeFragments(t, subject, x);
/*  57:    */     } else {
/*  58: 60 */     final Iterator it = this.forwardMap.entrySet().iterator();

/*  59: 61 */     return new FragmentTripleIterator(t, it) {
/*  61:    */       public void fill(GraphAdd ga, Node n, Object fragmentsObject) {
/*  63: 63 */         ((SimpleReifierFragmentsMap.Fragments)fragmentsObject).includeInto(ga);
/*  64:    */       }
                  }; 
/*  65:    */     }
                
/*  66:    */   }
/*  67:    */   
/*  68:    */   protected ExtendedIterator explodeFragments(Triple t, Node subject, Fragments x)
/*  69:    */   {
/*  70: 76 */     GraphAddList L = new GraphAddList(t);
/*  71: 77 */     x.includeInto(L);
/*  72: 78 */     return WrappedIterator.create(L.iterator());
/*  73:    */   }
/*  74:    */   
/*  75:    */   public ExtendedIterator find(TripleMatch m)
/*  76:    */   {
/*  77: 82 */     return allTriples(m);
/*  78:    */   }
/*  79:    */   
/*  80:    */   public int size()
/*  81:    */   {
/*  82: 86 */     int result = 0;
/*  83: 87 */     Iterator it = this.forwardMap.entrySet().iterator();
/*  84: 88 */     while (it.hasNext())
/*  85:    */     {
/*  86: 89 */       Map.Entry e = (Map.Entry)it.next();
/*  87: 90 */       Fragments f = (Fragments)e.getValue();
/*  88: 91 */       result += f.size();
/*  89:    */     }
/*  90: 93 */     return result;
/*  91:    */   }
/*  92:    */   
/*  93:    */   public ReifierFragmentHandler getFragmentHandler(Triple t)
/*  94:    */   {
/*  95:101 */     Node p = t.getPredicate();
/*  96:102 */     ReifierFragmentHandler x = (ReifierFragmentHandler)this.selectors.get(p);
/*  97:103 */     if ((x == null) || ((p.equals(RDF.Nodes.type)) && (!t.getObject().equals(RDF.Nodes.Statement)))) {
/*  98:104 */       return null;
/*  99:    */     }
/* 100:105 */     return x;
/* 101:    */   }
/* 102:    */   
/* 103:    */   public void putAugmentedTriple(SimpleReifierFragmentHandler s, Node tag, Node object, Triple reified)
/* 104:    */   {
/* 105:109 */     Fragments partial = new Fragments(tag, reified);
/* 106:110 */     partial.add(s, object);
/* 107:111 */     putFragments(tag, partial);
/* 108:    */   }
/* 109:    */   
/* 110:    */   protected Triple reifyCompleteQuad(SimpleReifierFragmentHandler s, Triple fragment, Node tag, Node object)
/* 111:    */   {
/* 112:115 */     Fragments partial = getFragments(tag);
/* 113:116 */     if (partial == null) {
/* 114:117 */       putFragments(tag, partial = new Fragments(tag));
/* 115:    */     }
/* 116:118 */     partial.add(s, object);
/* 117:119 */     if (partial.isComplete())
/* 118:    */     {
/* 119:120 */       removeFragments(fragment.getSubject());
/* 120:121 */       return partial.asTriple();
/* 121:    */     }
/* 122:123 */     return null;
/* 123:    */   }
/* 124:    */   
/* 125:    */   protected Triple removeFragment(SimpleReifierFragmentHandler s, Node tag, Triple already, Triple fragment)
/* 126:    */   {
/* 127:128 */     Fragments partial = getFragments(tag);
/* 128:    */     
/* 129:130 */     Fragments fs = partial == null ? putFragments(tag, new Fragments(tag)) : already != null ? explode(tag, already) : partial;
/* 130:    */     
/* 131:    */ 
/* 132:    */ 
/* 133:    */ 
/* 134:    */ 
/* 135:    */ 
/* 136:    */ 
/* 137:    */ 
/* 138:    */ 
/* 139:140 */     fs.remove(s, fragment.getObject());
/* 140:141 */     if (fs.isComplete())
/* 141:    */     {
/* 142:142 */       Triple result = fs.asTriple();
/* 143:143 */       removeFragments(tag);
/* 144:144 */       return result;
/* 145:    */     }
/* 146:146 */     if (fs.isEmpty()) {
/* 147:147 */       removeFragments(tag);
/* 148:    */     }
/* 149:148 */     return null;
/* 150:    */   }
/* 151:    */   
/* 152:    */   protected Fragments explode(Node s, Triple t)
/* 153:    */   {
/* 154:153 */     return putFragments(s, new Fragments(s, t));
/* 155:    */   }
/* 156:    */   
/* 157:    */   public boolean hasFragments(Node tag)
/* 158:    */   {
/* 159:157 */     return getFragments(tag) != null;
/* 160:    */   }
/* 161:    */   
/* 162:    */   protected static class Fragments
/* 163:    */   {
/* 164:167 */     private final Set[] slots = { new Set(), new Set(), new Set(), new Set() };
/* 165:    */     private Node anchor;
/* 166:    */     
/* 167:    */     public Fragments(Node n)
/* 168:    */     {
/* 169:181 */       this.anchor = n;
/* 170:    */     }
/* 171:    */     
/* 172:    */     public Fragments(Node n, Triple t)
/* 173:    */     {
/* 174:185 */       this(n);
/* 175:186 */       addTriple(t);
/* 176:    */     }
/* 177:    */     
/* 178:    */     public int size()
/* 179:    */     {
/* 180:190 */       return this.slots[0].size() + this.slots[1].size() + this.slots[2].size() + this.slots[3].size();
/* 181:    */     }
/* 182:    */     
/* 183:    */     public boolean isComplete()
/* 184:    */     {
/* 185:198 */       return (this.slots[0].size() == 1) && (this.slots[1].size() == 1) && (this.slots[2].size() == 1) && (this.slots[3].size() == 1);
/* 186:    */     }
/* 187:    */     
/* 188:    */     public boolean isEmpty()
/* 189:    */     {
/* 190:206 */       return (this.slots[0].isEmpty()) && (this.slots[1].isEmpty()) && (this.slots[2].isEmpty()) && (this.slots[3].isEmpty());
/* 191:    */     }
/* 192:    */     
/* 193:    */     public void remove(SimpleReifierFragmentHandler w, Node n)
/* 194:    */     {
/* 195:213 */       this.slots[w.which].remove(n);
/* 196:    */     }
/* 197:    */     
/* 198:    */     public void add(SimpleReifierFragmentHandler w, Node n)
/* 199:    */     {
/* 200:220 */       this.slots[w.which].add(n);
/* 201:    */     }
/* 202:    */     
/* 203:    */     public void includeInto(GraphAdd g)
/* 204:    */     {
/* 205:228 */       includeInto(g, RDF.Nodes.subject, 1);
/* 206:229 */       includeInto(g, RDF.Nodes.predicate, 2);
/* 207:230 */       includeInto(g, RDF.Nodes.object, 3);
/* 208:231 */       includeInto(g, RDF.Nodes.type, 0);
/* 209:    */     }
/* 210:    */     
/* 211:    */     private void includeInto(GraphAdd g, Node predicate, int which)
/* 212:    */     {
/* 213:240 */       Iterator it = this.slots[which].iterator();
/* 214:241 */       while (it.hasNext()) {
/* 215:242 */         g.add(Triple.create(this.anchor, predicate, (Node)it.next()));
/* 216:    */       }
/* 217:    */     }
/* 218:    */     
/* 219:    */     public Fragments addTriple(Triple t)
/* 220:    */     {
/* 221:252 */       this.slots[1].add(t.getSubject());
/* 222:253 */       this.slots[2].add(t.getPredicate());
/* 223:254 */       this.slots[3].add(t.getObject());
/* 224:255 */       this.slots[0].add(RDF.Nodes.Statement);
/* 225:256 */       return this;
/* 226:    */     }
/* 227:    */     
/* 228:    */     Triple asTriple()
/* 229:    */     {
/* 230:266 */       return Triple.create(only(this.slots[1]), only(this.slots[2]), only(this.slots[3]));
/* 231:    */     }
/* 232:    */     
/* 233:    */     private Node only(Set s)
/* 234:    */     {
/* 235:274 */       return (Node)s.iterator().next();
/* 236:    */     }
/* 237:    */     
/* 238:    */     public String toString()
/* 239:    */     {
/* 240:280 */       return this.anchor + " s:" + this.slots[1] + " p:" + this.slots[2] + " o:" + this.slots[3] + " t:" + this.slots[0];
/* 241:    */     }
/* 242:    */   }
/* 243:    */   
/* 244:294 */   protected final ReifierFragmentHandler TYPES = new SimpleReifierFragmentHandler(this, 0)
/* 245:    */   {
/* 246:    */     public boolean clashesWith(ReifierFragmentsMap map, Node n, Triple reified)
/* 247:    */     {
/* 248:296 */       return false;
/* 249:    */     }
/* 250:    */   };
/* 251:300 */   protected final ReifierFragmentHandler SUBJECTS = new SimpleReifierFragmentHandler(this, 1)
/* 252:    */   {
/* 253:    */     public boolean clashesWith(ReifierFragmentsMap map, Node n, Triple reified)
/* 254:    */     {
/* 255:302 */       return !n.equals(reified.getSubject());
/* 256:    */     }
/* 257:    */   };
/* 258:306 */   protected final ReifierFragmentHandler PREDICATES = new SimpleReifierFragmentHandler(this, 2)
/* 259:    */   {
/* 260:    */     public boolean clashesWith(ReifierFragmentsMap map, Node n, Triple reified)
/* 261:    */     {
/* 262:308 */       return !n.equals(reified.getPredicate());
/* 263:    */     }
/* 264:    */   };
/* 265:312 */   protected final ReifierFragmentHandler OBJECTS = new SimpleReifierFragmentHandler(this, 3)
/* 266:    */   {
/* 267:    */     public boolean clashesWith(ReifierFragmentsMap map, Node n, Triple reified)
/* 268:    */     {
/* 269:314 */       return !n.equals(reified.getObject());
/* 270:    */     }
/* 271:    */   };
/* 272:318 */   public final Map selectors = makeSelectors();
/* 273:    */   
/* 274:    */   protected Map makeSelectors()
/* 275:    */   {
/* 276:324 */     Map result = new Map();
/* 277:325 */     result.put(RDF.Nodes.subject, this.SUBJECTS);
/* 278:326 */     result.put(RDF.Nodes.predicate, this.PREDICATES);
/* 279:327 */     result.put(RDF.Nodes.object, this.OBJECTS);
/* 280:328 */     result.put(RDF.Nodes.type, this.TYPES);
/* 281:329 */     return result;
/* 282:    */   }
/* 283:    */ }
