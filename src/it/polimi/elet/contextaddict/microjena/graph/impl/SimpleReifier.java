/*   1:    */ package it.polimi.elet.contextaddict.microjena.graph.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.GraphAdd;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Reifier;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.graph.TripleMatch;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.shared.AlreadyReifiedException;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.shared.CannotReifyException;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.shared.ReificationStyle;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.util.List;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.NullIterator;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  17:    */ 
/*  18:    */ public class SimpleReifier
/*  19:    */   implements Reifier
/*  20:    */ {
/*  21:    */   protected final GraphBase parent;
/*  22:    */   protected final boolean intercepting;
/*  23:    */   protected final boolean concealing;
/*  24:    */   protected final ReificationStyle style;
/*  25:    */   protected ReifierFragmentsMap fragmentsMap;
/*  26:    */   protected ReifierTripleMap tripleMap;
/*  27: 44 */   protected boolean closed = false;
/*  28:    */   
/*  29:    */   public SimpleReifier(GraphBase parent, ReificationStyle style)
/*  30:    */   {
/*  31: 53 */     this(parent, new SimpleReifierTripleMap(), new SimpleReifierFragmentsMap(), style);
/*  32:    */   }
/*  33:    */   
/*  34:    */   public SimpleReifier(GraphBase parent, ReifierTripleMap tm, ReifierFragmentsMap fm, ReificationStyle style)
/*  35:    */   {
/*  36: 57 */     this.parent = parent;
/*  37: 58 */     this.fragmentsMap = fm;
/*  38: 59 */     this.tripleMap = tm;
/*  39: 60 */     this.intercepting = style.intercepts();
/*  40: 61 */     this.concealing = style.conceals();
/*  41: 62 */     this.style = style;
/*  42:    */   }
/*  43:    */   
/*  44:    */   public ReificationStyle getStyle()
/*  45:    */   {
/*  46: 66 */     return this.style;
/*  47:    */   }
/*  48:    */   
/*  49:    */   public Graph getParentGraph()
/*  50:    */   {
/*  51: 71 */     return this.parent;
/*  52:    */   }
/*  53:    */   
/*  54:    */   public Triple getTriple(Node n)
/*  55:    */   {
/*  56: 76 */     return this.tripleMap.getTriple(n);
/*  57:    */   }
/*  58:    */   
/*  59:    */   public boolean hasTriple(Node n)
/*  60:    */   {
/*  61: 81 */     return getTriple(n) != null;
/*  62:    */   }
/*  63:    */   
/*  64:    */   public ExtendedIterator allNodes()
/*  65:    */   {
/*  66: 86 */     return this.tripleMap.tagIterator();
/*  67:    */   }
/*  68:    */   
/*  69:    */   public ExtendedIterator allNodes(Triple t)
/*  70:    */   {
/*  71: 90 */     ExtendedIterator it = this.tripleMap.tagIterator();
/*  72: 91 */     List result = new List();
/*  73: 93 */     while (it.hasNext())
/*  74:    */     {
/*  75: 94 */       Object aus = it.next();
/*  76: 95 */       if (this.tripleMap.getTriple((Node)aus).matches(t)) {
/*  77: 96 */         result.add(aus);
/*  78:    */       }
/*  79:    */     }
/*  80: 98 */     return WrappedIterator.create(result.iterator());
/*  81:    */   }
/*  82:    */   
/*  83:    */   public void clear()
/*  84:    */   {
/*  85:102 */     this.fragmentsMap.clear();
/*  86:103 */     this.tripleMap.clear();
/*  87:    */   }
/*  88:    */   
/*  89:    */   public Node reifyAs(Node tag, Triple toReify)
/*  90:    */   {
/*  91:111 */     Triple existing = this.tripleMap.getTriple(tag);
/*  92:112 */     if (existing != null)
/*  93:    */     {
/*  94:113 */       if (!toReify.equals(existing)) {
/*  95:115 */         throw new AlreadyReifiedException(tag);
/*  96:    */       }
/*  97:    */     }
/*  98:    */     else {
/*  99:119 */       reifyNewTriple(tag, toReify);
/* 100:    */     }
/* 101:121 */     if (!this.concealing) {
/* 102:123 */       graphAddQuad(this.parent, tag, toReify);
/* 103:    */     }
/* 104:124 */     return tag;
/* 105:    */   }
/* 106:    */   
/* 107:    */   protected void reifyNewTriple(Node tag, Triple toReify)
/* 108:    */   {
/* 109:138 */     if (this.fragmentsMap.hasFragments(tag))
/* 110:    */     {
/* 111:140 */       graphAddQuad(this.parent, tag, toReify);
/* 112:141 */       if (this.tripleMap.getTriple(tag) == null) {
/* 113:144 */         throw new CannotReifyException(tag);
/* 114:    */       }
/* 115:    */     }
/* 116:    */     else
/* 117:    */     {
/* 118:147 */       this.tripleMap.putTriple(tag, toReify);
/* 119:    */     }
/* 120:    */   }
/* 121:    */   
/* 122:    */   public void remove(Node n, Triple t)
/* 123:    */   {
/* 124:155 */     Triple x = this.tripleMap.getTriple(n);
/* 125:156 */     if (t.equals(x))
/* 126:    */     {
/* 127:157 */       this.tripleMap.removeTriple(n, t);
/* 128:158 */       if (!this.concealing) {
/* 129:159 */         parentRemoveQuad(n, t);
/* 130:    */       }
/* 131:    */     }
/* 132:    */   }
/* 133:    */   
/* 134:    */   public void remove(Triple t)
/* 135:    */   {
/* 136:164 */     this.tripleMap.removeTriple(t);
/* 137:    */   }
/* 138:    */   
/* 139:    */   public boolean hasTriple(Triple t)
/* 140:    */   {
/* 141:168 */     return this.tripleMap.hasTriple(t);
/* 142:    */   }
/* 143:    */   
/* 144:    */   public boolean handledAdd(Triple fragment)
/* 145:    */   {
/* 146:172 */     if (this.intercepting)
/* 147:    */     {
/* 148:173 */       ReifierFragmentHandler s = this.fragmentsMap.getFragmentHandler(fragment);
/* 149:174 */       if (s == null) {
/* 150:175 */         return false;
/* 151:    */       }
/* 152:177 */       addFragment(s, fragment);
/* 153:178 */       return true;
/* 154:    */     }
/* 155:181 */     return false;
/* 156:    */   }
/* 157:    */   
/* 158:    */   protected void addFragment(ReifierFragmentHandler s, Triple fragment)
/* 159:    */   {
/* 160:191 */     Node tag = fragment.getSubject();Node object = fragment.getObject();
/* 161:192 */     Triple reified = this.tripleMap.getTriple(tag);
/* 162:193 */     if (reified == null) {
/* 163:194 */       updateFragments(s, fragment, tag, object);
/* 164:195 */     } else if (s.clashedWith(tag, object, reified)) {
/* 165:196 */       this.tripleMap.removeTriple(tag, reified);
/* 166:    */     }
/* 167:    */   }
/* 168:    */   
/* 169:    */   private void updateFragments(ReifierFragmentHandler s, Triple fragment, Node tag, Node object)
/* 170:    */   {
/* 171:206 */     Triple t = s.reifyIfCompleteQuad(fragment, tag, object);
/* 172:207 */     if ((t instanceof Triple)) {
/* 173:208 */       this.tripleMap.putTriple(tag, t);
/* 174:    */     }
/* 175:    */   }
/* 176:    */   
/* 177:    */   public boolean handledRemove(Triple fragment)
/* 178:    */   {
/* 179:212 */     if (this.intercepting)
/* 180:    */     {
/* 181:213 */       ReifierFragmentHandler s = this.fragmentsMap.getFragmentHandler(fragment);
/* 182:214 */       if (s == null) {
/* 183:215 */         return false;
/* 184:    */       }
/* 185:217 */       removeFragment(s, fragment);
/* 186:218 */       return true;
/* 187:    */     }
/* 188:221 */     return false;
/* 189:    */   }
/* 190:    */   
/* 191:    */   private void removeFragment(ReifierFragmentHandler s, Triple fragment)
/* 192:    */   {
/* 193:229 */     Node tag = fragment.getSubject();
/* 194:230 */     Triple already = this.tripleMap.getTriple(tag);
/* 195:231 */     Triple complete = s.removeFragment(tag, already, fragment);
/* 196:232 */     if (complete == null) {
/* 197:233 */       this.tripleMap.removeTriple(tag);
/* 198:    */     } else {
/* 199:235 */       this.tripleMap.putTriple(tag, complete);
/* 200:    */     }
/* 201:    */   }
/* 202:    */   
/* 203:    */   public ExtendedIterator find(TripleMatch m)
/* 204:    */   {
/* 205:239 */     return this.tripleMap.find(m).andThen(this.fragmentsMap.find(m));
/* 206:    */   }
/* 207:    */   
/* 208:    */   public ExtendedIterator findExposed(TripleMatch m)
/* 209:    */   {
/* 210:243 */     return findEither(m, false);
/* 211:    */   }
/* 212:    */   
/* 213:    */   public ExtendedIterator findEither(TripleMatch m, boolean showHidden)
/* 214:    */   {
/* 215:247 */     return showHidden == this.concealing ? find(m) : NullIterator.instance;
/* 216:    */   }
/* 217:    */   
/* 218:    */   public int size()
/* 219:    */   {
/* 220:251 */     return this.concealing ? 0 : this.tripleMap.size() + this.fragmentsMap.size();
/* 221:    */   }
/* 222:    */   
/* 223:    */   private void parentRemoveQuad(Node n, Triple t)
/* 224:    */   {
/* 225:259 */     this.parent.delete(Triple.create(n, RDF.Nodes.type, RDF.Nodes.Statement));
/* 226:260 */     this.parent.delete(Triple.create(n, RDF.Nodes.subject, t.getSubject()));
/* 227:261 */     this.parent.delete(Triple.create(n, RDF.Nodes.predicate, t.getPredicate()));
/* 228:262 */     this.parent.delete(Triple.create(n, RDF.Nodes.object, t.getObject()));
/* 229:    */   }
/* 230:    */   
/* 231:    */   public static void graphAddQuad(GraphAdd g, Node node, Triple t)
/* 232:    */   {
/* 233:266 */     g.add(Triple.create(node, RDF.Nodes.subject, t.getSubject()));
/* 234:267 */     g.add(Triple.create(node, RDF.Nodes.predicate, t.getPredicate()));
/* 235:268 */     g.add(Triple.create(node, RDF.Nodes.object, t.getObject()));
/* 236:269 */     g.add(Triple.create(node, RDF.Nodes.type, RDF.Nodes.Statement));
/* 237:    */   }
/* 238:    */   
/* 239:    */   public String toString()
/* 240:    */   {
/* 241:277 */     return "<R " + this.fragmentsMap + "|" + this.tripleMap + ">";
/* 242:    */   }
/* 243:    */   
/* 244:    */   public void close()
/* 245:    */   {
/* 246:284 */     this.fragmentsMap = null;
/* 247:285 */     this.tripleMap = null;
/* 248:286 */     this.closed = true;
/* 249:    */   }
/* 250:    */   
/* 251:    */   public boolean isClosed()
/* 252:    */   {
/* 253:293 */     return this.closed;
/* 254:    */   }
/* 255:    */ }

