/*   1:    */ package it.polimi.elet.contextaddict.microjena.graph.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.Axiom;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Reifier;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.graph.TripleMatch;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.shared.ReificationStyle;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.IteratorImpl;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  15:    */ import java.util.Vector;
/*  16:    */ 
/*  17:    */ public class GraphImpl
/*  18:    */   extends GraphImplBase
/*  19:    */   implements Graph
/*  20:    */ {
/*  21: 33 */   protected Vector triples = new Vector(10, 10);
/*  22:    */   
/*  23:    */   public GraphImpl()
/*  24:    */   {
/*  25: 36 */     this(ReificationStyle.Minimal);
/*  26:    */   }
/*  27:    */   
/*  28:    */   public GraphImpl(ReificationStyle style)
/*  29:    */   {
/*  30: 45 */     super(style);
/*  31:    */   }
/*  32:    */   
/*  33:    */   public void performAdd(Triple t)
/*  34:    */   {
/*  35: 49 */     if ((!getReifier().handledAdd(t)) && 
/*  36: 50 */       (!contains(t)))
/*  37:    */     {
/*  38: 51 */       this.triples.addElement(t);
/*  39: 52 */       cacheTriple(t);
/*  40:    */     }
/*  41:    */   }
/*  42:    */   
/*  43:    */   private void cacheTriple(Triple t)
/*  44:    */   {
/*  45: 57 */     cacheNode(t.getSubject());
/*  46: 58 */     cacheNode(t.getPredicate());
/*  47: 59 */     cacheNode(t.getObject());
/*  48:    */   }
/*  49:    */   
/*  50:    */   private boolean cacheNode(Node n)
/*  51:    */   {
/*  52: 63 */     if (n.isURI())
/*  53:    */     {
/*  54: 64 */       this.nodes.put(n.getURI(), n);
/*  55: 65 */       return true;
/*  56:    */     }
/*  57: 67 */     if (n.isBlank())
/*  58:    */     {
/*  59: 68 */       this.nodes.put(n.getBlankNodeLabel(), n);
/*  60: 69 */       return true;
/*  61:    */     }
/*  62: 71 */     return false;
/*  63:    */   }
/*  64:    */   
/*  65:    */   public void performDelete(Triple t)
/*  66:    */   {
/*  67: 77 */     performDelete(t, false);
/*  68:    */   }
/*  69:    */   
/*  70:    */   public void performDelete(Triple t, boolean removeAxiom)
/*  71:    */   {
/*  72: 81 */     if ((!getReifier().handledRemove(t)) && (
/*  73: 82 */       (removeAxiom) || (!(t instanceof Axiom)))) {
/*  74: 83 */       this.triples.removeElement(t);
/*  75:    */     }
/*  76:    */   }
/*  77:    */   
/*  78:    */   public int graphBaseSize()
/*  79:    */   {
/*  80: 87 */     return this.triples.size();
/*  81:    */   }
/*  82:    */   
/*  83:    */   public ExtendedIterator graphBaseFind(TripleMatch m)
/*  84:    */   {
/*  85: 95 */     Vector newVector = new Vector();
/*  86: 96 */     Iterator it = new IteratorImpl(this.triples);
/*  87: 98 */     while (it.hasNext())
/*  88:    */     {
/*  89: 99 */       Triple aus = (Triple)it.next();
/*  90:100 */       if (aus.matches((Triple)m)) {
/*  91:101 */         newVector.addElement(aus);
/*  92:    */       }
/*  93:    */     }
/*  94:103 */     IteratorImpl result = new IteratorImpl(newVector);
/*  95:104 */     return WrappedIterator.create(result);
/*  96:    */   }
/*  97:    */   
/*  98:    */   public boolean graphBaseContains(Triple t)
/*  99:    */   {
/* 100:113 */     return isSafeForEquality(t) ? graphBaseFind(t).hasNext() : super.graphBaseContains(t);
/* 101:    */   }
/* 102:    */   
/* 103:    */   public void clear()
/* 104:    */   {
/* 105:122 */     this.triples.removeAllElements();
/* 106:123 */     this.triples.trimToSize();
/* 107:124 */     ((SimpleReifier)getReifier()).clear();
/* 108:    */   }
/* 109:    */   
/* 110:    */   protected void destroy()
/* 111:    */   {
/* 112:128 */     this.triples.setSize(0);
/* 113:    */   }
/* 114:    */ }

