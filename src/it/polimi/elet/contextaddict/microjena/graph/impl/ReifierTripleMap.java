package it.polimi.elet.contextaddict.microjena.graph.impl;

import it.polimi.elet.contextaddict.microjena.graph.Node;
import it.polimi.elet.contextaddict.microjena.graph.Triple;
import it.polimi.elet.contextaddict.microjena.graph.TripleMatch;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface ReifierTripleMap
{
  public abstract Triple getTriple(Node paramNode);
  
  public abstract boolean hasTriple(Triple paramTriple);
  
  public abstract Triple putTriple(Node paramNode, Triple paramTriple);
  
  public abstract void removeTriple(Node paramNode);
  
  public abstract void removeTriple(Node paramNode, Triple paramTriple);
  
  public abstract void removeTriple(Triple paramTriple);
  
  public abstract ExtendedIterator find(TripleMatch paramTripleMatch);
  
  public abstract int size();
  
  public abstract ExtendedIterator tagIterator();
  
  public abstract ExtendedIterator tagIterator(Triple paramTriple);
  
  public abstract void clear();
}
