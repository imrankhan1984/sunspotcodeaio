/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.impl.GraphImpl;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.shared.ReificationStyle;
/*  5:   */ 
/*  6:   */ public class Factory
/*  7:   */ {
/*  8:   */   public static Graph createDefaultGraph()
/*  9:   */   {
/* 10:27 */     return createDefaultGraph(ReificationStyle.Standard);
/* 11:   */   }
/* 12:   */   
/* 13:   */   public static Graph createDefaultGraph(ReificationStyle style)
/* 14:   */   {
/* 15:34 */     return new GraphImpl(style);
/* 16:   */   }
/* 17:   */ }
