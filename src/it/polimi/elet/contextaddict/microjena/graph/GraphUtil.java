/*  1:   */ package it.polimi.elet.contextaddict.microjena.graph;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  4:   */ 
/*  5:   */ public class GraphUtil
/*  6:   */ {
/*  7:   */   public static ExtendedIterator findAll(Graph g)
/*  8:   */   {
/*  9:30 */     return g.find(Triple.ANY);
/* 10:   */   }
/* 11:   */ }
