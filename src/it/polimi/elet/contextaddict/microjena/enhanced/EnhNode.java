/*   1:    */ package it.polimi.elet.contextaddict.microjena.enhanced;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.FrontsNode;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   5:    */ 
/*   6:    */ public class EnhNode
/*   7:    */   implements FrontsNode
/*   8:    */ {
/*   9:    */   protected final Node node;
/*  10:    */   protected final EnhGraph enhGraph;
/*  11:    */   
/*  12:    */   public EnhNode(Node n, EnhGraph g)
/*  13:    */   {
/*  14: 32 */     this.node = n;
/*  15: 33 */     this.enhGraph = g;
/*  16: 34 */     if (g != null) {
/*  17: 35 */       g.addNode(this);
/*  18:    */     }
/*  19:    */   }
/*  20:    */   
/*  21:    */   public Node asNode()
/*  22:    */   {
/*  23: 45 */     return this.node;
/*  24:    */   }
/*  25:    */   
/*  26:    */   public EnhGraph getGraph()
/*  27:    */   {
/*  28: 53 */     return this.enhGraph;
/*  29:    */   }
/*  30:    */   
/*  31:    */   public final boolean isAnon()
/*  32:    */   {
/*  33: 60 */     return this.node.isBlank();
/*  34:    */   }
/*  35:    */   
/*  36:    */   public final boolean isLiteral()
/*  37:    */   {
/*  38: 67 */     return this.node.isLiteral();
/*  39:    */   }
/*  40:    */   
/*  41:    */   public final boolean isURIResource()
/*  42:    */   {
/*  43: 74 */     return this.node.isURI();
/*  44:    */   }
/*  45:    */   
/*  46:    */   public final boolean isResource()
/*  47:    */   {
/*  48: 81 */     return (this.node.isURI()) || (this.node.isBlank());
/*  49:    */   }
/*  50:    */   
/*  51:    */   public final int hashCode()
/*  52:    */   {
/*  53: 89 */     return this.node.hashCode();
/*  54:    */   }
/*  55:    */   
/*  56:    */   public final boolean equals(Object o)
/*  57:    */   {
/*  58:104 */     return ((o instanceof FrontsNode)) && (this.node.equals(((FrontsNode)o).asNode()));
/*  59:    */   }
/*  60:    */   
/*  61:    */   public boolean isValid()
/*  62:    */   {
/*  63:108 */     return true;
/*  64:    */   }
/*  65:    */ }
