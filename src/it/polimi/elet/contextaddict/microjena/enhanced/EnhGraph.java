/*   1:    */ package it.polimi.elet.contextaddict.microjena.enhanced;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*   6:    */ 
/*   7:    */ public class EnhGraph
/*   8:    */ {
/*   9:    */   protected Graph graph;
/*  10: 34 */   private static int cnt = 0;
/*  11: 38 */   protected Set enhNodes = new Set();
/*  12:    */   
/*  13:    */   public boolean isValid()
/*  14:    */   {
/*  15: 41 */     return true;
/*  16:    */   }
/*  17:    */   
/*  18:    */   public EnhGraph(Graph g)
/*  19:    */   {
/*  20: 55 */     this.graph = g;
/*  21:    */   }
/*  22:    */   
/*  23:    */   public Graph asGraph()
/*  24:    */   {
/*  25: 65 */     return this.graph;
/*  26:    */   }
/*  27:    */   
/*  28:    */   public final int hashCode()
/*  29:    */   {
/*  30: 73 */     return this.graph.hashCode();
/*  31:    */   }
/*  32:    */   
/*  33:    */   public final boolean equals(Object o)
/*  34:    */   {
/*  35: 91 */     return (this == o) || (((o instanceof EnhGraph)) && (this.graph.equals(((EnhGraph)o).asGraph())));
/*  36:    */   }
/*  37:    */   
/*  38:    */   public int getCnt()
/*  39:    */   {
/*  40: 97 */     return cnt;
/*  41:    */   }
/*  42:    */   
/*  43:    */   public EnhNode getNode(Node n)
/*  44:    */   {
/*  45:108 */     int index = this.enhNodes.indexOf(n);
/*  46:109 */     if (index != -1) {
/*  47:110 */       return (EnhNode)this.enhNodes.get(index);
/*  48:    */     }
/*  49:113 */     return new EnhNode(n, this);
/*  50:    */   }
/*  51:    */   
/*  52:    */   protected EnhNode addNode(EnhNode enh)
/*  53:    */   {
/*  54:124 */     this.enhNodes.add(enh);
/*  55:125 */     return enh;
/*  56:    */   }
/*  57:    */ }

