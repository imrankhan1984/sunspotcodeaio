package it.polimi.elet.contextaddict.microjena.enhanced;

import it.polimi.elet.contextaddict.microjena.graph.Node;

public abstract class Implementation
{
  public abstract EnhNode wrap(Node paramNode, EnhGraph paramEnhGraph);
  
  public abstract boolean canWrap(Node paramNode, EnhGraph paramEnhGraph);
}
