package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface OntProperty
  extends OntResource, Property
{
  public abstract void setSuperProperty(Property paramProperty);
  
  public abstract void addSuperProperty(Property paramProperty);
  
  public abstract OntProperty getSuperProperty();
  
  public abstract ExtendedIterator listSuperProperties();
  
  public abstract ExtendedIterator listSuperProperties(boolean paramBoolean);
  
  public abstract boolean hasSuperProperty(Property paramProperty, boolean paramBoolean);
  
  public abstract void removeSuperProperty(Property paramProperty);
  
  public abstract void setSubProperty(Property paramProperty);
  
  public abstract void addSubProperty(Property paramProperty);
  
  public abstract OntProperty getSubProperty();
  
  public abstract ExtendedIterator listSubProperties();
  
  public abstract ExtendedIterator listSubProperties(boolean paramBoolean);
  
  public abstract boolean hasSubProperty(Property paramProperty, boolean paramBoolean);
  
  public abstract void removeSubProperty(Property paramProperty);
  
  public abstract void setDomain(Resource paramResource);
  
  public abstract void addDomain(Resource paramResource);
  
  public abstract OntResource getDomain();
  
  public abstract ExtendedIterator listDomain();
  
  public abstract boolean hasDomain(Resource paramResource);
  
  public abstract void removeDomain(Resource paramResource);
  
  public abstract void setRange(Resource paramResource);
  
  public abstract void addRange(Resource paramResource);
  
  public abstract OntResource getRange();
  
  public abstract ExtendedIterator listRange();
  
  public abstract boolean hasRange(Resource paramResource);
  
  public abstract void removeRange(Resource paramResource);
  
  public abstract void setEquivalentProperty(Property paramProperty);
  
  public abstract void addEquivalentProperty(Property paramProperty);
  
  public abstract OntProperty getEquivalentProperty();
  
  public abstract ExtendedIterator listEquivalentProperties();
  
  public abstract boolean hasEquivalentProperty(Property paramProperty);
  
  public abstract void removeEquivalentProperty(Property paramProperty);
  
  public abstract void setInverseOf(Property paramProperty);
  
  public abstract void addInverseOf(Property paramProperty);
  
  public abstract OntProperty getInverseOf();
  
  public abstract ExtendedIterator listInverseOf();
  
  public abstract boolean isInverseOf(Property paramProperty);
  
  public abstract void removeInverseProperty(Property paramProperty);
  
  public abstract boolean isFunctionalProperty();
  
  public abstract boolean isDatatypeProperty();
  
  public abstract boolean isObjectProperty();
  
  public abstract boolean isTransitiveProperty();
  
  public abstract boolean isInverseFunctionalProperty();
  
  public abstract boolean isSymmetricProperty();
  
  public abstract OntProperty getInverse();
  
  public abstract ExtendedIterator listInverse();
  
  public abstract boolean hasInverse();
}
