package it.polimi.elet.contextaddict.microjena.ontology;

public abstract interface CardinalityQRestriction
  extends QualifiedRestriction
{
  public abstract void setCardinalityQ(int paramInt);
  
  public abstract int getCardinalityQ();
  
  public abstract boolean hasCardinalityQ(int paramInt);
  
  public abstract void removeCardinalityQ(int paramInt);
}

