package it.polimi.elet.contextaddict.microjena.ontology;

public abstract interface CardinalityRestriction
  extends Restriction
{
  public abstract void setCardinality(int paramInt);
  
  public abstract int getCardinality();
  
  public abstract boolean hasCardinality(int paramInt);
  
  public abstract void removeCardinality(int paramInt);
}

