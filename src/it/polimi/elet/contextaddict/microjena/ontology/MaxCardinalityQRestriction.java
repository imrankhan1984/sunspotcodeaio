package it.polimi.elet.contextaddict.microjena.ontology;

public abstract interface MaxCardinalityQRestriction
  extends QualifiedRestriction
{
  public abstract void setMaxCardinalityQ(int paramInt);
  
  public abstract int getMaxCardinalityQ();
  
  public abstract boolean hasMaxCardinalityQ(int paramInt);
  
  public abstract void removeMaxCardinalityQ(int paramInt);
}


