package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Property;

public abstract interface AnnotationProperty
  extends OntResource, Property
{}


