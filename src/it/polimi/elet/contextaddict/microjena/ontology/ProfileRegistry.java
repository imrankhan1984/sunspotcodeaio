/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.ontology.impl.OWLDLProfile;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.ontology.impl.OWLLiteProfile;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.ontology.impl.OWLProfile;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.ontology.impl.RDFSProfile;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.OWL;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*  11:    */ 
/*  12:    */ public class ProfileRegistry
/*  13:    */ {
/*  14: 43 */   public static final String OWL_LANG = OWL.FULL_LANG.getURI();
/*  15: 46 */   public static final String OWL_DL_LANG = OWL.DL_LANG.getURI();
/*  16: 49 */   public static final String OWL_LITE_LANG = OWL.LITE_LANG.getURI();
/*  17: 52 */   public static final String RDFS_LANG = RDFS.getURI();
/*  18: 54 */   private static Object[][] s_initData = { { OWL_LANG, new OWLProfile() }, { OWL_DL_LANG, new OWLDLProfile() }, { OWL_LITE_LANG, new OWLLiteProfile() }, { RDFS_LANG, new RDFSProfile() } };
/*  19: 62 */   private static ProfileRegistry s_instance = new ProfileRegistry();
/*  20: 65 */   private Map m_registry = new Map();
/*  21:    */   
/*  22:    */   private ProfileRegistry()
/*  23:    */   {
/*  24: 73 */     for (int i = 0; i < s_initData.length; i++) {
/*  25: 74 */       registerProfile((String)s_initData[i][0], (Profile)s_initData[i][1]);
/*  26:    */     }
/*  27:    */   }
/*  28:    */   
/*  29:    */   public static ProfileRegistry getInstance()
/*  30:    */   {
/*  31: 86 */     return s_instance;
/*  32:    */   }
/*  33:    */   
/*  34:    */   public void registerProfile(String uri, Profile profile)
/*  35:    */   {
/*  36: 99 */     this.m_registry.put(uri, profile);
/*  37:    */   }
/*  38:    */   
/*  39:    */   public Profile getProfile(String uri)
/*  40:    */   {
/*  41:112 */     return (Profile)this.m_registry.get(uri);
/*  42:    */   }
/*  43:    */ }
