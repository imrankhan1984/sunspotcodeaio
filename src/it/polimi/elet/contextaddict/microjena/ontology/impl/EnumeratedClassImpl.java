/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.EnumeratedClass;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.RDFListImpl;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  20:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  21:    */ 
/*  22:    */ public class EnumeratedClassImpl
/*  23:    */   extends OntClassImpl
/*  24:    */   implements EnumeratedClass
/*  25:    */ {
/*  26: 54 */   public static Implementation factory = new Implementation()
/*  27:    */   {
/*  28:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  29:    */     {
/*  30: 56 */       if (canWrap(n, eg)) {
/*  31: 57 */         return new EnumeratedClassImpl(n, eg);
/*  32:    */       }
/*  33: 59 */       throw new ConversionException("Cannot convert node " + n + " to EnumeratedClass");
/*  34:    */     }
/*  35:    */     
/*  36:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  37:    */     {
/*  38: 65 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  39: 66 */       return (profile != null) && (profile.isSupported(node, eg, OntClass.class)) && (eg.asGraph().contains(node, profile.ONE_OF().asNode(), Node.ANY));
/*  40:    */     }
/*  41:    */   };
/*  42:    */   
/*  43:    */   public EnumeratedClassImpl(Node n, EnhGraph g)
/*  44:    */   {
/*  45: 81 */     super(n, g);
/*  46:    */   }
/*  47:    */   
/*  48:    */   public void setOneOf(RDFList en)
/*  49:    */   {
/*  50: 93 */     setPropertyValue(getProfile().ONE_OF(), "ONE_OF", en);
/*  51:    */   }
/*  52:    */   
/*  53:    */   public void addOneOf(Resource res)
/*  54:    */   {
/*  55:102 */     addListPropertyValue(getProfile().ONE_OF(), "ONE_OF", res);
/*  56:    */   }
/*  57:    */   
/*  58:    */   public void addOneOf(Iterator individuals)
/*  59:    */   {
/*  60:112 */     while (individuals.hasNext()) {
/*  61:113 */       addOneOf((Resource)individuals.next());
/*  62:    */     }
/*  63:    */   }
/*  64:    */   
/*  65:    */   public RDFList getOneOf()
/*  66:    */   {
/*  67:123 */     RDFNode n = getProperty(getProfile().ONE_OF()).getObject();
/*  68:124 */     return new RDFListImpl(n.asNode(), getModelCom());
/*  69:    */   }
/*  70:    */   
/*  71:    */   public ExtendedIterator listOneOf()
/*  72:    */   {
/*  73:134 */     RDFList result = getOneOf();
/*  74:135 */     return result.iterator();
/*  75:    */   }
/*  76:    */   
/*  77:    */   public boolean hasOneOf(Resource res)
/*  78:    */   {
/*  79:146 */     return getOneOf().contains(res);
/*  80:    */   }
/*  81:    */   
/*  82:    */   public void removeOneOf(Resource res)
/*  83:    */   {
/*  84:156 */     setOneOf(getOneOf().remove(res));
/*  85:    */   }
/*  86:    */ }

