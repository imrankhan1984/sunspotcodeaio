/*    1:     */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*    2:     */ 
/*    3:     */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.XSDDatatype;
/*    4:     */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*    5:     */ import it.polimi.elet.contextaddict.microjena.graph.Axiom;
/*    6:     */ import it.polimi.elet.contextaddict.microjena.graph.Factory;
/*    7:     */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*    8:     */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*    9:     */ import it.polimi.elet.contextaddict.microjena.ontology.AllDifferent;
/*   10:     */ import it.polimi.elet.contextaddict.microjena.ontology.AllValuesFromRestriction;
/*   11:     */ import it.polimi.elet.contextaddict.microjena.ontology.AnnotationProperty;
/*   12:     */ import it.polimi.elet.contextaddict.microjena.ontology.CardinalityRestriction;
/*   13:     */ import it.polimi.elet.contextaddict.microjena.ontology.ComplementClass;
/*   14:     */ import it.polimi.elet.contextaddict.microjena.ontology.DataRange;
/*   15:     */ import it.polimi.elet.contextaddict.microjena.ontology.DatatypeProperty;
/*   16:     */ import it.polimi.elet.contextaddict.microjena.ontology.EnumeratedClass;
/*   17:     */ import it.polimi.elet.contextaddict.microjena.ontology.HasValueRestriction;
/*   18:     */ import it.polimi.elet.contextaddict.microjena.ontology.Individual;
/*   19:     */ import it.polimi.elet.contextaddict.microjena.ontology.IntersectionClass;
/*   20:     */ import it.polimi.elet.contextaddict.microjena.ontology.InverseFunctionalProperty;
/*   21:     */ import it.polimi.elet.contextaddict.microjena.ontology.MaxCardinalityRestriction;
/*   22:     */ import it.polimi.elet.contextaddict.microjena.ontology.MinCardinalityRestriction;
/*   23:     */ import it.polimi.elet.contextaddict.microjena.ontology.ObjectProperty;
/*   24:     */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/*   25:     */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*   26:     */ import it.polimi.elet.contextaddict.microjena.ontology.OntProperty;
/*   27:     */ import it.polimi.elet.contextaddict.microjena.ontology.OntResource;
/*   28:     */ import it.polimi.elet.contextaddict.microjena.ontology.Ontology;
/*   29:     */ import it.polimi.elet.contextaddict.microjena.ontology.OntologyException;
/*   30:     */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*   31:     */ import it.polimi.elet.contextaddict.microjena.ontology.ProfileException;
/*   32:     */ import it.polimi.elet.contextaddict.microjena.ontology.ProfileRegistry;
/*   33:     */ import it.polimi.elet.contextaddict.microjena.ontology.Restriction;
/*   34:     */ import it.polimi.elet.contextaddict.microjena.ontology.SomeValuesFromRestriction;
/*   35:     */ import it.polimi.elet.contextaddict.microjena.ontology.SymmetricProperty;
/*   36:     */ import it.polimi.elet.contextaddict.microjena.ontology.TransitiveProperty;
/*   37:     */ import it.polimi.elet.contextaddict.microjena.ontology.UnionClass;
/*   38:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*   39:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*   40:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*   41:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*   42:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*   43:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*   44:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*   45:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.ModelCom;
/*   46:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.RDFListImpl;
/*   47:     */ import it.polimi.elet.contextaddict.microjena.util.List;
/*   48:     */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*   49:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*   50:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*   51:     */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*   52:     */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*   53:     */ 
/*   54:     */ public class OntModelImpl
/*   55:     */   extends ModelCom
/*   56:     */   implements OntModel
/*   57:     */ {
/*   58:  89 */   protected boolean m_strictMode = true;
/*   59:     */   private Profile profile;
/*   60:     */   
/*   61:     */   public OntModelImpl(Model model)
/*   62:     */   {
/*   63:  92 */     this(ProfileRegistry.OWL_LANG, model.getGraph());
/*   64:     */   }
/*   65:     */   
/*   66:     */   public OntModelImpl(String languageURI)
/*   67:     */   {
/*   68:  96 */     this(languageURI, it.polimi.elet.contextaddict.microjena.graph.Factory.createDefaultGraph());
/*   69:     */   }
/*   70:     */   
/*   71:     */   public OntModelImpl(String languageURI, Graph model)
/*   72:     */   {
/*   73: 100 */     super(model);
/*   74: 101 */     OWLAxiomWriter.writeAxioms(getGraph());
/*   75: 102 */     if (languageURI.equals(ProfileRegistry.OWL_LANG)) {
/*   76: 103 */       this.profile = new OWLProfile();
/*   77: 105 */     } else if (languageURI.equals(ProfileRegistry.OWL_DL_LANG)) {
/*   78: 106 */       this.profile = new OWLDLProfile();
/*   79: 108 */     } else if (languageURI.equals(ProfileRegistry.OWL_LITE_LANG)) {
/*   80: 109 */       this.profile = new OWLLiteProfile();
/*   81: 111 */     } else if (languageURI.equals(ProfileRegistry.RDFS_LANG)) {
/*   82: 112 */       this.profile = new RDFSProfile();
/*   83:     */     } else {
/*   84: 114 */       throw new OntologyException("tried to instantiate an unknown/unsupported language Profile");
/*   85:     */     }
/*   86:     */   }
/*   87:     */   
/*   88:     */   public ExtendedIterator listOntologies()
/*   89:     */   {
/*   90: 137 */     checkProfileEntry(getProfile().ONTOLOGY(), "ONTOLOGY");
/*   91: 138 */     StmtIterator it = listStatements((Resource)null, RDF.type, getProfile().ONTOLOGY());
/*   92: 139 */     Set result = new Set();
/*   93: 140 */     while (it.hasNext()) {
/*   94: 141 */       result.add(new OntologyImpl(it.nextStatement().getSubject().asNode(), this));
/*   95:     */     }
/*   96: 142 */     return WrappedIterator.create(result.iterator());
/*   97:     */   }
/*   98:     */   
/*   99:     */   public ExtendedIterator listOntProperties()
/*  100:     */   {
/*  101: 167 */     Set result = new Set();
/*  102:     */     
/*  103: 169 */     StmtIterator it = listStatements((Resource)null, RDF.type, getProfile().SYMMETRIC_PROPERTY());
/*  104: 170 */     while (it.hasNext()) {
/*  105: 171 */       result.add(new SymmetricPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  106:     */     }
/*  107: 173 */     it = listStatements((Resource)null, RDF.type, RDF.Property);
/*  108: 174 */     while (it.hasNext()) {
/*  109: 175 */       result.add(new OntPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  110:     */     }
/*  111: 177 */     it = listStatements((Resource)null, RDF.type, getProfile().DATATYPE_PROPERTY());
/*  112: 178 */     while (it.hasNext()) {
/*  113: 179 */       result.add(new DatatypePropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  114:     */     }
/*  115: 181 */     it = listStatements((Resource)null, RDF.type, getProfile().OBJECT_PROPERTY());
/*  116: 182 */     while (it.hasNext()) {
/*  117: 183 */       result.add(new ObjectPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  118:     */     }
/*  119: 185 */     it = listStatements((Resource)null, RDF.type, getProfile().FUNCTIONAL_PROPERTY());
/*  120: 186 */     while (it.hasNext()) {
/*  121: 187 */       result.add(new FunctionalPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  122:     */     }
/*  123: 189 */     it = listStatements((Resource)null, RDF.type, getProfile().TRANSITIVE_PROPERTY());
/*  124: 190 */     while (it.hasNext()) {
/*  125: 191 */       result.add(new TransitivePropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  126:     */     }
/*  127: 193 */     it = listStatements((Resource)null, RDF.type, getProfile().INVERSE_FUNCTIONAL_PROPERTY());
/*  128: 194 */     while (it.hasNext()) {
/*  129: 195 */       result.add(new InverseFunctionalPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  130:     */     }
/*  131: 197 */     return WrappedIterator.create(result.iterator());
/*  132:     */   }
/*  133:     */   
/*  134:     */   public ExtendedIterator listObjectProperties()
/*  135:     */   {
/*  136: 222 */     checkProfileEntry(getProfile().OBJECT_PROPERTY(), "OBJECT_PROPERTY");
/*  137:     */     
/*  138: 224 */     Set result = new Set();
/*  139:     */     
/*  140: 226 */     StmtIterator it = listStatements((Resource)null, RDF.type, getProfile().SYMMETRIC_PROPERTY());
/*  141: 227 */     while (it.hasNext()) {
/*  142: 228 */       result.add(new SymmetricPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  143:     */     }
/*  144: 230 */     it = listStatements((Resource)null, RDF.type, getProfile().OBJECT_PROPERTY());
/*  145: 231 */     while (it.hasNext()) {
/*  146: 232 */       result.add(new ObjectPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  147:     */     }
/*  148: 234 */     it = listStatements((Resource)null, RDF.type, getProfile().TRANSITIVE_PROPERTY());
/*  149: 235 */     while (it.hasNext()) {
/*  150: 236 */       result.add(new TransitivePropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  151:     */     }
/*  152: 238 */     it = listStatements((Resource)null, RDF.type, getProfile().INVERSE_FUNCTIONAL_PROPERTY());
/*  153: 239 */     while (it.hasNext()) {
/*  154: 240 */       result.add(new InverseFunctionalPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  155:     */     }
/*  156: 242 */     return WrappedIterator.create(result.iterator());
/*  157:     */   }
/*  158:     */   
/*  159:     */   public ExtendedIterator listDatatypeProperties()
/*  160:     */   {
/*  161: 267 */     StmtIterator it = listStatements((Resource)null, RDF.type, getProfile().DATATYPE_PROPERTY());
/*  162: 268 */     Set result = new Set();
/*  163: 269 */     while (it.hasNext()) {
/*  164: 270 */       result.add(new DatatypePropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  165:     */     }
/*  166: 271 */     return WrappedIterator.create(result.iterator());
/*  167:     */   }
/*  168:     */   
/*  169:     */   public ExtendedIterator listFunctionalProperties()
/*  170:     */   {
/*  171: 291 */     checkProfileEntry(getProfile().FUNCTIONAL_PROPERTY(), "FUNCTIONAL_PROPERTY");
/*  172: 292 */     StmtIterator it = listStatements((Resource)null, RDF.type, getProfile().FUNCTIONAL_PROPERTY());
/*  173: 293 */     Set result = new Set();
/*  174: 294 */     while (it.hasNext()) {
/*  175: 295 */       result.add(new FunctionalPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  176:     */     }
/*  177: 296 */     return WrappedIterator.create(result.iterator());
/*  178:     */   }
/*  179:     */   
/*  180:     */   public ExtendedIterator listTransitiveProperties()
/*  181:     */   {
/*  182: 314 */     checkProfileEntry(getProfile().TRANSITIVE_PROPERTY(), "TRANSITIVE_PROPERTY");
/*  183: 315 */     StmtIterator it = listStatements((Resource)null, RDF.type, getProfile().TRANSITIVE_PROPERTY());
/*  184: 316 */     Set result = new Set();
/*  185: 317 */     while (it.hasNext()) {
/*  186: 318 */       result.add(new TransitivePropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  187:     */     }
/*  188: 319 */     return WrappedIterator.create(result.iterator());
/*  189:     */   }
/*  190:     */   
/*  191:     */   public ExtendedIterator listSymmetricProperties()
/*  192:     */   {
/*  193: 337 */     checkProfileEntry(getProfile().SYMMETRIC_PROPERTY(), "SYMMETRIC_PROPERTY");
/*  194: 338 */     StmtIterator it = listStatements((Resource)null, RDF.type, getProfile().SYMMETRIC_PROPERTY());
/*  195: 339 */     List result = new List();
/*  196: 340 */     while (it.hasNext()) {
/*  197: 341 */       result.add(new SymmetricPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  198:     */     }
/*  199: 342 */     return WrappedIterator.create(result.iterator());
/*  200:     */   }
/*  201:     */   
/*  202:     */   public ExtendedIterator listInverseFunctionalProperties()
/*  203:     */   {
/*  204: 359 */     checkProfileEntry(getProfile().INVERSE_FUNCTIONAL_PROPERTY(), "INVERSE_FUNCTIONAL_PROPERTY");
/*  205: 360 */     StmtIterator it = listStatements((Resource)null, RDF.type, getProfile().INVERSE_FUNCTIONAL_PROPERTY());
/*  206: 361 */     Set result = new Set();
/*  207: 362 */     while (it.hasNext()) {
/*  208: 363 */       result.add(new InverseFunctionalPropertyImpl(it.nextStatement().getSubject().asNode(), this));
/*  209:     */     }
/*  210: 364 */     return WrappedIterator.create(result.iterator());
/*  211:     */   }
/*  212:     */   
/*  213:     */   public ExtendedIterator listIndividuals()
/*  214:     */   {
/*  215: 383 */     StmtIterator types = listStatements((Resource)null, RDF.type, getProfile().CLASS());
/*  216: 384 */     List result = new List();
/*  217: 386 */     while (types.hasNext())
/*  218:     */     {
/*  219: 387 */       StmtIterator individuals = listStatements((Resource)null, RDF.type, types.nextStatement().getSubject());
/*  220: 388 */       while (individuals.hasNext()) {
/*  221: 389 */         result.add(new IndividualImpl(individuals.nextStatement().getSubject().asNode(), this));
/*  222:     */       }
/*  223:     */     }
/*  224: 391 */     return WrappedIterator.create(result.iterator());
/*  225:     */   }
/*  226:     */   
/*  227:     */   public ExtendedIterator listIndividuals(Resource cls)
/*  228:     */   {
/*  229: 411 */     List result = new List();
/*  230: 413 */     if (OntClassImpl.factory.canWrap(cls.asNode(), this))
/*  231:     */     {
/*  232: 414 */       cls = new OntClassImpl(cls.asNode(), this);
/*  233: 415 */       List classList = new List();
/*  234: 416 */       ExtendedIterator classes = ((OntClass)cls).listSubClasses();
/*  235: 417 */       while (classes.hasNext()) {
/*  236: 418 */         classList.add(classes.next());
/*  237:     */       }
/*  238: 419 */       StmtIterator individuals = listStatements((Resource)null, RDF.type, (RDFNode)null);
/*  239: 421 */       while (individuals.hasNext())
/*  240:     */       {
/*  241: 422 */         Statement aus = individuals.nextStatement();
/*  242: 423 */         if (classList.contains(aus.getObject())) {
/*  243: 424 */           result.add(new IndividualImpl(aus.getSubject().asNode(), this));
/*  244:     */         }
/*  245:     */       }
/*  246:     */     }
/*  247:     */     else
/*  248:     */     {
/*  249: 428 */       StmtIterator individuals = listStatements((Resource)null, RDF.type, cls);
/*  250: 429 */       while (individuals.hasNext()) {
/*  251: 430 */         result.add(new IndividualImpl(individuals.nextStatement().getSubject().asNode(), this));
/*  252:     */       }
/*  253:     */     }
/*  254: 432 */     return WrappedIterator.create(result.iterator());
/*  255:     */   }
/*  256:     */   
/*  257:     */   public ExtendedIterator listClasses()
/*  258:     */   {
/*  259: 460 */     StmtIterator types = listStatements((Resource)null, RDF.type, getProfile().CLASS());
/*  260: 461 */     Set result = new Set();
/*  261: 462 */     while (types.hasNext()) {
/*  262: 463 */       result.add(new OntClassImpl(types.nextStatement().getSubject().asNode(), this));
/*  263:     */     }
/*  264: 465 */     return WrappedIterator.create(result.iterator());
/*  265:     */   }
/*  266:     */   
/*  267:     */   public ExtendedIterator listHierarchyRootClasses()
/*  268:     */   {
/*  269: 477 */     ExtendedIterator allClasses = listClasses();
/*  270:     */     
/*  271: 479 */     Set result = new Set();
/*  272: 480 */     while (allClasses.hasNext())
/*  273:     */     {
/*  274: 481 */       OntClass aus = (OntClass)allClasses.next();
/*  275: 482 */       if (aus.isHierarchyRoot()) {
/*  276: 483 */         result.add(aus);
/*  277:     */       }
/*  278:     */     }
/*  279: 485 */     return WrappedIterator.create(result.iterator());
/*  280:     */   }
/*  281:     */   
/*  282:     */   public ExtendedIterator listEnumeratedClasses()
/*  283:     */   {
/*  284: 504 */     checkProfileEntry(getProfile().ONE_OF(), "ONE_OF");
/*  285: 505 */     StmtIterator st = listStatements((Resource)null, getProfile().ONE_OF(), (RDFNode)null);
/*  286: 506 */     Set result = new Set();
/*  287: 507 */     while (st.hasNext()) {
/*  288: 508 */       result.add(new EnumeratedClassImpl(st.nextStatement().getSubject().asNode(), this));
/*  289:     */     }
/*  290: 510 */     return WrappedIterator.create(result.iterator());
/*  291:     */   }
/*  292:     */   
/*  293:     */   public ExtendedIterator listUnionClasses()
/*  294:     */   {
/*  295: 530 */     StmtIterator st = listStatements((Resource)null, getProfile().UNION_OF(), (RDFNode)null);
/*  296: 531 */     Set result = new Set();
/*  297: 533 */     while (st.hasNext()) {
/*  298: 534 */       result.add(new UnionClassImpl(st.nextStatement().getSubject().asNode(), this));
/*  299:     */     }
/*  300: 536 */     return WrappedIterator.create(result.iterator());
/*  301:     */   }
/*  302:     */   
/*  303:     */   public ExtendedIterator listComplementClasses()
/*  304:     */   {
/*  305: 556 */     checkProfileEntry(getProfile().COMPLEMENT_OF(), "COMPLEMENT_OF");
/*  306: 557 */     StmtIterator st = listStatements((Resource)null, getProfile().COMPLEMENT_OF(), (RDFNode)null);
/*  307: 558 */     Set result = new Set();
/*  308: 559 */     while (st.hasNext()) {
/*  309: 560 */       result.add(new ComplementClassImpl(st.nextStatement().getSubject().asNode(), this));
/*  310:     */     }
/*  311: 562 */     return WrappedIterator.create(result.iterator());
/*  312:     */   }
/*  313:     */   
/*  314:     */   public ExtendedIterator listIntersectionClasses()
/*  315:     */   {
/*  316: 582 */     checkProfileEntry(getProfile().INTERSECTION_OF(), "INTERSECTION_OF");
/*  317: 583 */     StmtIterator st = listStatements((Resource)null, getProfile().INTERSECTION_OF(), (RDFNode)null);
/*  318: 584 */     Set result = new Set();
/*  319: 585 */     while (st.hasNext()) {
/*  320: 586 */       result.add(new IntersectionClassImpl(st.nextStatement().getSubject().asNode(), this));
/*  321:     */     }
/*  322: 588 */     return WrappedIterator.create(result.iterator());
/*  323:     */   }
/*  324:     */   
/*  325:     */   public ExtendedIterator listNamedClasses()
/*  326:     */   {
/*  327: 607 */     StmtIterator st = listStatements((Resource)null, RDF.type, getProfile().CLASS());
/*  328: 608 */     Set result = new Set();
/*  329: 610 */     while (st.hasNext())
/*  330:     */     {
/*  331: 611 */       Resource aus = new OntClassImpl(st.nextStatement().getSubject().asNode(), this);
/*  332: 613 */       if (aus.isURIResource()) {
/*  333: 614 */         result.add(aus);
/*  334:     */       }
/*  335:     */     }
/*  336: 616 */     return WrappedIterator.create(result.iterator());
/*  337:     */   }
/*  338:     */   
/*  339:     */   public ExtendedIterator listRestrictions()
/*  340:     */   {
/*  341: 635 */     checkProfileEntry(getProfile().RESTRICTION(), "RESTRICTION");
/*  342: 636 */     StmtIterator st = listStatements((Resource)null, RDF.type, getProfile().RESTRICTION());
/*  343: 637 */     Set result = new Set();
/*  344: 638 */     while (st.hasNext()) {
/*  345: 639 */       result.add(new RestrictionImpl(st.nextStatement().getSubject().asNode(), this));
/*  346:     */     }
/*  347: 641 */     return WrappedIterator.create(result.iterator());
/*  348:     */   }
/*  349:     */   
/*  350:     */   public ExtendedIterator listAllDifferent()
/*  351:     */   {
/*  352: 659 */     checkProfileEntry(getProfile().ALL_DIFFERENT(), "ALL_DIFFERENT");
/*  353: 660 */     StmtIterator st = listStatements((Resource)null, RDF.type, getProfile().ALL_DIFFERENT());
/*  354: 661 */     Set result = new Set();
/*  355: 662 */     while (st.hasNext()) {
/*  356: 663 */       result.add(new AllDifferentImpl(st.nextStatement().getSubject().asNode(), this));
/*  357:     */     }
/*  358: 665 */     return WrappedIterator.create(result.iterator());
/*  359:     */   }
/*  360:     */   
/*  361:     */   public ExtendedIterator listDataRanges()
/*  362:     */   {
/*  363: 674 */     checkProfileEntry(getProfile().DATARANGE(), "DATARANGE");
/*  364: 675 */     StmtIterator st = listStatements((Resource)null, RDF.type, getProfile().DATARANGE());
/*  365: 676 */     Set result = new Set();
/*  366: 677 */     while (st.hasNext()) {
/*  367: 678 */       result.add(new DataRangeImpl(st.nextStatement().getSubject().asNode(), this));
/*  368:     */     }
/*  369: 680 */     return WrappedIterator.create(result.iterator());
/*  370:     */   }
/*  371:     */   
/*  372:     */   public ExtendedIterator listAnnotationProperties()
/*  373:     */   {
/*  374: 700 */     checkProfileEntry(getProfile().ANNOTATION_PROPERTY(), "ANNOTATION_PROPERTY");
/*  375: 701 */     StmtIterator st = listStatements((Resource)null, RDF.type, getProfile().ANNOTATION_PROPERTY());
/*  376: 702 */     Set result = new Set();
/*  377: 703 */     while (st.hasNext()) {
/*  378: 704 */       result.add(new AnnotationPropertyImpl(st.nextStatement().getSubject().asNode(), this));
/*  379:     */     }
/*  380: 706 */     return WrappedIterator.create(result.iterator());
/*  381:     */   }
/*  382:     */   
/*  383:     */   public Ontology getOntology(String uri)
/*  384:     */   {
/*  385: 722 */     Node result = Node.createURI(uri);
/*  386: 723 */     if (OntologyImpl.factory.canWrap(result, this)) {
/*  387: 724 */       return new OntologyImpl(result, this);
/*  388:     */     }
/*  389: 726 */     return null;
/*  390:     */   }
/*  391:     */   
/*  392:     */   public Individual getIndividual(String uri)
/*  393:     */   {
/*  394: 741 */     Node result = Node.createURI(uri);
/*  395: 742 */     if (IndividualImpl.factory.canWrap(result, this)) {
/*  396: 743 */       return new IndividualImpl(result, this);
/*  397:     */     }
/*  398: 745 */     return null;
/*  399:     */   }
/*  400:     */   
/*  401:     */   public OntProperty getOntProperty(String uri)
/*  402:     */   {
/*  403: 760 */     Node result = Node.createURI(uri);
/*  404: 761 */     if (OntPropertyImpl.factory.canWrap(result, this)) {
/*  405: 762 */       return new OntPropertyImpl(result, this);
/*  406:     */     }
/*  407: 764 */     return null;
/*  408:     */   }
/*  409:     */   
/*  410:     */   public ObjectProperty getObjectProperty(String uri)
/*  411:     */   {
/*  412: 779 */     Node result = Node.createURI(uri);
/*  413: 780 */     if (ObjectPropertyImpl.factory.canWrap(result, this)) {
/*  414: 781 */       return new ObjectPropertyImpl(result, this);
/*  415:     */     }
/*  416: 783 */     return null;
/*  417:     */   }
/*  418:     */   
/*  419:     */   public TransitiveProperty getTransitiveProperty(String uri)
/*  420:     */   {
/*  421: 795 */     Node result = Node.createURI(uri);
/*  422: 796 */     if (TransitivePropertyImpl.factory.canWrap(result, this)) {
/*  423: 797 */       return new TransitivePropertyImpl(result, this);
/*  424:     */     }
/*  425: 799 */     return null;
/*  426:     */   }
/*  427:     */   
/*  428:     */   public SymmetricProperty getSymmetricProperty(String uri)
/*  429:     */   {
/*  430: 811 */     Node result = Node.createURI(uri);
/*  431: 812 */     if (SymmetricPropertyImpl.factory.canWrap(result, this)) {
/*  432: 813 */       return new SymmetricPropertyImpl(result, this);
/*  433:     */     }
/*  434: 815 */     return null;
/*  435:     */   }
/*  436:     */   
/*  437:     */   public InverseFunctionalProperty getInverseFunctionalProperty(String uri)
/*  438:     */   {
/*  439: 827 */     Node result = Node.createURI(uri);
/*  440: 828 */     if (InverseFunctionalPropertyImpl.factory.canWrap(result, this)) {
/*  441: 829 */       return new InverseFunctionalPropertyImpl(result, this);
/*  442:     */     }
/*  443: 831 */     return null;
/*  444:     */   }
/*  445:     */   
/*  446:     */   public DatatypeProperty getDatatypeProperty(String uri)
/*  447:     */   {
/*  448: 846 */     Node result = Node.createURI(uri);
/*  449: 847 */     if (DatatypePropertyImpl.factory.canWrap(result, this)) {
/*  450: 848 */       return new DatatypePropertyImpl(result, this);
/*  451:     */     }
/*  452: 850 */     return null;
/*  453:     */   }
/*  454:     */   
/*  455:     */   public AnnotationProperty getAnnotationProperty(String uri)
/*  456:     */   {
/*  457: 865 */     Node result = Node.createURI(uri);
/*  458: 866 */     if (AnnotationPropertyImpl.factory.canWrap(result, this)) {
/*  459: 867 */       return new AnnotationPropertyImpl(result, this);
/*  460:     */     }
/*  461: 869 */     return null;
/*  462:     */   }
/*  463:     */   
/*  464:     */   public OntClass getOntClass(String uri)
/*  465:     */   {
/*  466: 884 */     Node result = Node.createURI(uri);
/*  467: 885 */     if (OntClassImpl.factory.canWrap(result, this)) {
/*  468: 886 */       return new OntClassImpl(result, this);
/*  469:     */     }
/*  470: 889 */     if (getProfile().THING().getURI().equals(uri)) {
/*  471: 890 */       return new OntClassImpl(getProfile().THING().asNode(), this);
/*  472:     */     }
/*  473: 892 */     if (getProfile().NOTHING().getURI().equals(uri)) {
/*  474: 893 */       return new OntClassImpl(getProfile().NOTHING().asNode(), this);
/*  475:     */     }
/*  476: 895 */     return null;
/*  477:     */   }
/*  478:     */   
/*  479:     */   public ComplementClass getComplementClass(String uri)
/*  480:     */   {
/*  481: 907 */     Node result = Node.createURI(uri);
/*  482: 908 */     if (ComplementClassImpl.factory.canWrap(result, this)) {
/*  483: 909 */       return new ComplementClassImpl(result, this);
/*  484:     */     }
/*  485: 911 */     return null;
/*  486:     */   }
/*  487:     */   
/*  488:     */   public EnumeratedClass getEnumeratedClass(String uri)
/*  489:     */   {
/*  490: 923 */     Node result = Node.createURI(uri);
/*  491: 924 */     if (EnumeratedClassImpl.factory.canWrap(result, this)) {
/*  492: 925 */       return new EnumeratedClassImpl(result, this);
/*  493:     */     }
/*  494: 927 */     return null;
/*  495:     */   }
/*  496:     */   
/*  497:     */   public UnionClass getUnionClass(String uri)
/*  498:     */   {
/*  499: 939 */     Node result = Node.createURI(uri);
/*  500: 940 */     if (UnionClassImpl.factory.canWrap(result, this)) {
/*  501: 941 */       return new UnionClassImpl(result, this);
/*  502:     */     }
/*  503: 943 */     return null;
/*  504:     */   }
/*  505:     */   
/*  506:     */   public IntersectionClass getIntersectionClass(String uri)
/*  507:     */   {
/*  508: 955 */     Node result = Node.createURI(uri);
/*  509: 956 */     if (IntersectionClassImpl.factory.canWrap(result, this)) {
/*  510: 957 */       return new IntersectionClassImpl(result, this);
/*  511:     */     }
/*  512: 959 */     return null;
/*  513:     */   }
/*  514:     */   
/*  515:     */   public Restriction getRestriction(String uri)
/*  516:     */   {
/*  517: 973 */     Node result = Node.createURI(uri);
/*  518: 974 */     if (RestrictionImpl.factory.canWrap(result, this)) {
/*  519: 975 */       return new RestrictionImpl(result, this);
/*  520:     */     }
/*  521: 977 */     return null;
/*  522:     */   }
/*  523:     */   
/*  524:     */   public HasValueRestriction getHasValueRestriction(String uri)
/*  525:     */   {
/*  526: 991 */     Node result = Node.createURI(uri);
/*  527: 992 */     if (HasValueRestrictionImpl.factory.canWrap(result, this)) {
/*  528: 993 */       return new HasValueRestrictionImpl(result, this);
/*  529:     */     }
/*  530: 995 */     return null;
/*  531:     */   }
/*  532:     */   
/*  533:     */   public SomeValuesFromRestriction getSomeValuesFromRestriction(String uri)
/*  534:     */   {
/*  535:1009 */     Node result = Node.createURI(uri);
/*  536:1010 */     if (SomeValuesFromRestrictionImpl.factory.canWrap(result, this)) {
/*  537:1011 */       return new SomeValuesFromRestrictionImpl(result, this);
/*  538:     */     }
/*  539:1013 */     return null;
/*  540:     */   }
/*  541:     */   
/*  542:     */   public AllValuesFromRestriction getAllValuesFromRestriction(String uri)
/*  543:     */   {
/*  544:1027 */     Node result = Node.createURI(uri);
/*  545:1028 */     if (AllValuesFromRestrictionImpl.factory.canWrap(result, this)) {
/*  546:1029 */       return new AllValuesFromRestrictionImpl(result, this);
/*  547:     */     }
/*  548:1031 */     return null;
/*  549:     */   }
/*  550:     */   
/*  551:     */   public CardinalityRestriction getCardinalityRestriction(String uri)
/*  552:     */   {
/*  553:1045 */     Node result = Node.createURI(uri);
/*  554:1046 */     if (CardinalityRestrictionImpl.factory.canWrap(result, this)) {
/*  555:1047 */       return new CardinalityRestrictionImpl(result, this);
/*  556:     */     }
/*  557:1049 */     return null;
/*  558:     */   }
/*  559:     */   
/*  560:     */   public MinCardinalityRestriction getMinCardinalityRestriction(String uri)
/*  561:     */   {
/*  562:1062 */     Node result = Node.createURI(uri);
/*  563:1063 */     if (MinCardinalityRestrictionImpl.factory.canWrap(result, this)) {
/*  564:1064 */       return new MinCardinalityRestrictionImpl(result, this);
/*  565:     */     }
/*  566:1066 */     return null;
/*  567:     */   }
/*  568:     */   
/*  569:     */   public MaxCardinalityRestriction getMaxCardinalityRestriction(String uri)
/*  570:     */   {
/*  571:1080 */     Node result = Node.createURI(uri);
/*  572:1081 */     if (MaxCardinalityRestrictionImpl.factory.canWrap(result, this)) {
/*  573:1082 */       return new MaxCardinalityRestrictionImpl(result, this);
/*  574:     */     }
/*  575:1084 */     return null;
/*  576:     */   }
/*  577:     */   
/*  578:     */   public Ontology createOntology(String uri)
/*  579:     */   {
/*  580:1099 */     checkProfileEntry(getProfile().ONTOLOGY(), "ONTOLOGY");
/*  581:1100 */     Ontology result = new OntologyImpl(Node.createURI(uri), this);
/*  582:1101 */     add(result, RDF.type, getProfile().ONTOLOGY());
/*  583:1102 */     return result;
/*  584:     */   }
/*  585:     */   
/*  586:     */   public Individual createIndividual(Resource cls)
/*  587:     */   {
/*  588:1116 */     Individual result = new IndividualImpl(Node.createAnon(), this);
/*  589:1117 */     result.addProperty(RDF.type, cls);
/*  590:1118 */     return result;
/*  591:     */   }
/*  592:     */   
/*  593:     */   public Individual createIndividual(String uri, Resource cls)
/*  594:     */   {
/*  595:1134 */     Individual result = new IndividualImpl(Node.createURI(uri), this);
/*  596:1135 */     result.addProperty(RDF.type, cls);
/*  597:1136 */     return result;
/*  598:     */   }
/*  599:     */   
/*  600:     */   public OntProperty createOntProperty(String uri)
/*  601:     */   {
/*  602:1152 */     Property p = createProperty(uri);
/*  603:1153 */     p.addProperty(RDF.type, getProfile().PROPERTY());
/*  604:     */     
/*  605:1155 */     getGraph().add(new Axiom(p.asNode(), RDF.type.asNode(), RDFS.Resource.asNode()));
/*  606:1156 */     getGraph().add(new Axiom(p.asNode(), RDFS.subPropertyOf.asNode(), p.asNode()));
/*  607:     */     
/*  608:1158 */     return new OntPropertyImpl(p.asNode(), this);
/*  609:     */   }
/*  610:     */   
/*  611:     */   public ObjectProperty createObjectProperty(String uri)
/*  612:     */   {
/*  613:1173 */     return createObjectProperty(uri, false);
/*  614:     */   }
/*  615:     */   
/*  616:     */   public ObjectProperty createObjectProperty(String uri, boolean functional)
/*  617:     */   {
/*  618:1192 */     checkProfileEntry(getProfile().OBJECT_PROPERTY(), "OBJECT_PROPERTY");
/*  619:1193 */     ObjectProperty p = new ObjectPropertyImpl(Node.createURI(uri), this);
/*  620:1194 */     p.addProperty(RDF.type, getProfile().OBJECT_PROPERTY());
/*  621:1195 */     if (functional)
/*  622:     */     {
/*  623:1196 */       checkProfileEntry(getProfile().FUNCTIONAL_PROPERTY(), "FUNCTIONAL_PROPERTY");
/*  624:1197 */       p.addProperty(RDF.type, getProfile().FUNCTIONAL_PROPERTY());
/*  625:     */     }
/*  626:1199 */     return p;
/*  627:     */   }
/*  628:     */   
/*  629:     */   public TransitiveProperty createTransitiveProperty(String uri)
/*  630:     */   {
/*  631:1210 */     return createTransitiveProperty(uri, false);
/*  632:     */   }
/*  633:     */   
/*  634:     */   public TransitiveProperty createTransitiveProperty(String uri, boolean functional)
/*  635:     */   {
/*  636:1224 */     checkProfileEntry(getProfile().TRANSITIVE_PROPERTY(), "TRANSITIVE_PROPERTY");
/*  637:1225 */     TransitiveProperty p = new TransitivePropertyImpl(Node.createURI(uri), this);
/*  638:1226 */     p.addProperty(RDF.type, getProfile().TRANSITIVE_PROPERTY());
/*  639:1227 */     if (functional)
/*  640:     */     {
/*  641:1228 */       checkProfileEntry(getProfile().FUNCTIONAL_PROPERTY(), "FUNCTIONAL_PROPERTY");
/*  642:1229 */       p.addProperty(RDF.type, getProfile().FUNCTIONAL_PROPERTY());
/*  643:     */     }
/*  644:1232 */     return p;
/*  645:     */   }
/*  646:     */   
/*  647:     */   public SymmetricProperty createSymmetricProperty(String uri)
/*  648:     */   {
/*  649:1243 */     return createSymmetricProperty(uri, false);
/*  650:     */   }
/*  651:     */   
/*  652:     */   public SymmetricProperty createSymmetricProperty(String uri, boolean functional)
/*  653:     */   {
/*  654:1255 */     checkProfileEntry(getProfile().SYMMETRIC_PROPERTY(), "SYMMETRIC_PROPERTY");
/*  655:1256 */     SymmetricProperty p = new SymmetricPropertyImpl(Node.createURI(uri), this);
/*  656:1257 */     p.addProperty(RDF.type, getProfile().SYMMETRIC_PROPERTY());
/*  657:1258 */     if (functional)
/*  658:     */     {
/*  659:1259 */       checkProfileEntry(getProfile().FUNCTIONAL_PROPERTY(), "FUNCTIONAL_PROPERTY");
/*  660:1260 */       p.addProperty(RDF.type, getProfile().FUNCTIONAL_PROPERTY());
/*  661:     */     }
/*  662:1262 */     return p;
/*  663:     */   }
/*  664:     */   
/*  665:     */   public InverseFunctionalProperty createInverseFunctionalProperty(String uri)
/*  666:     */   {
/*  667:1273 */     return createInverseFunctionalProperty(uri, false);
/*  668:     */   }
/*  669:     */   
/*  670:     */   public InverseFunctionalProperty createInverseFunctionalProperty(String uri, boolean functional)
/*  671:     */   {
/*  672:1285 */     checkProfileEntry(getProfile().INVERSE_FUNCTIONAL_PROPERTY(), "INVERSE_FUNCTIONAL_PROPERTY");
/*  673:1286 */     InverseFunctionalProperty result = getInverseFunctionalProperty(uri);
/*  674:1287 */     if (result != null) {
/*  675:1288 */       return result;
/*  676:     */     }
/*  677:1290 */     result = new InverseFunctionalPropertyImpl(Node.createURI(uri), this);
/*  678:1291 */     result.addProperty(RDF.type, getProfile().INVERSE_FUNCTIONAL_PROPERTY());
/*  679:1292 */     if (functional)
/*  680:     */     {
/*  681:1293 */       checkProfileEntry(getProfile().FUNCTIONAL_PROPERTY(), "FUNCTIONAL_PROPERTY");
/*  682:1294 */       result.addProperty(RDF.type, getProfile().FUNCTIONAL_PROPERTY());
/*  683:     */     }
/*  684:1296 */     return result;
/*  685:     */   }
/*  686:     */   
/*  687:     */   public DatatypeProperty createDatatypeProperty(String uri)
/*  688:     */   {
/*  689:1312 */     return createDatatypeProperty(uri, false);
/*  690:     */   }
/*  691:     */   
/*  692:     */   public DatatypeProperty createDatatypeProperty(String uri, boolean functional)
/*  693:     */   {
/*  694:1331 */     checkProfileEntry(getProfile().DATATYPE_PROPERTY(), "DATATYPE_PROPERTY");
/*  695:1332 */     DatatypeProperty result = getDatatypeProperty(uri);
/*  696:1333 */     if (result != null) {
/*  697:1334 */       return result;
/*  698:     */     }
/*  699:1336 */     result = new DatatypePropertyImpl(Node.createURI(uri), this);
/*  700:1337 */     result.addProperty(RDF.type, getProfile().DATATYPE_PROPERTY());
/*  701:1338 */     if (functional)
/*  702:     */     {
/*  703:1339 */       checkProfileEntry(getProfile().FUNCTIONAL_PROPERTY(), "FUNCTIONAL_PROPERTY");
/*  704:1340 */       result.addProperty(RDF.type, getProfile().FUNCTIONAL_PROPERTY());
/*  705:     */     }
/*  706:1342 */     return result;
/*  707:     */   }
/*  708:     */   
/*  709:     */   public AnnotationProperty createAnnotationProperty(String uri)
/*  710:     */   {
/*  711:1358 */     checkProfileEntry(getProfile().ANNOTATION_PROPERTY(), "ANNOTATION_PROPERTY");
/*  712:1359 */     AnnotationProperty result = getAnnotationProperty(uri);
/*  713:1360 */     if (result != null) {
/*  714:1361 */       return result;
/*  715:     */     }
/*  716:1363 */     result = new AnnotationPropertyImpl(Node.createURI(uri), this);
/*  717:1364 */     result.addProperty(RDF.type, getProfile().ANNOTATION_PROPERTY());
/*  718:1365 */     return result;
/*  719:     */   }
/*  720:     */   
/*  721:     */   public OntClass createClass()
/*  722:     */   {
/*  723:1380 */     checkProfileEntry(getProfile().CLASS(), "CLASS");
/*  724:1381 */     OntClass result = new OntClassImpl(Node.createAnon(), this);
/*  725:1382 */     result.addProperty(RDF.type, getProfile().CLASS());
/*  726:1383 */     addResourceClassAxioms(result);
/*  727:1384 */     getGraph().add(new Axiom(result.asNode(), RDFS.subClassOf.asNode(), result.asNode()));
/*  728:     */     
/*  729:1386 */     return result;
/*  730:     */   }
/*  731:     */   
/*  732:     */   public OntClass createClass(String uri)
/*  733:     */   {
/*  734:1401 */     checkProfileEntry(getProfile().CLASS(), "CLASS");
/*  735:1402 */     OntClass result = getOntClass(uri);
/*  736:1403 */     if (result != null) {
/*  737:1404 */       return result;
/*  738:     */     }
/*  739:1406 */     result = new OntClassImpl(Node.createURI(uri), this);
/*  740:1407 */     result.addProperty(RDF.type, getProfile().CLASS());
/*  741:1408 */     addResourceClassAxioms(result);
/*  742:1409 */     getGraph().add(new Axiom(result.asNode(), RDFS.subClassOf.asNode(), result.asNode()));
/*  743:1410 */     return result;
/*  744:     */   }
/*  745:     */   
/*  746:     */   public ComplementClass createComplementClass(String uri, Resource cls)
/*  747:     */   {
/*  748:1422 */     checkProfileEntry(getProfile().CLASS(), "CLASS");
/*  749:1423 */     checkProfileEntry(getProfile().COMPLEMENT_OF(), "COMPLEMENT_OF");
/*  750:1424 */     ComplementClass result = getComplementClass(uri);
/*  751:1425 */     if (result != null) {
/*  752:1426 */       return result;
/*  753:     */     }
/*  754:1428 */     result = new ComplementClassImpl(Node.createURI(uri), this);
/*  755:1429 */     result.addProperty(RDF.type, getProfile().CLASS());
/*  756:1430 */     addResourceClassAxioms(result);
/*  757:1431 */     if (cls == null) {
/*  758:1432 */       cls = getProfile().NOTHING();
/*  759:     */     }
/*  760:1433 */     result.addProperty(getProfile().COMPLEMENT_OF(), cls);
/*  761:1434 */     return result;
/*  762:     */   }
/*  763:     */   
/*  764:     */   public EnumeratedClass createEnumeratedClass(String uri, RDFList members)
/*  765:     */   {
/*  766:1446 */     checkProfileEntry(getProfile().CLASS(), "CLASS");
/*  767:1447 */     checkProfileEntry(getProfile().ONE_OF(), "ONE_OF");
/*  768:1448 */     EnumeratedClass result = getEnumeratedClass(uri);
/*  769:1449 */     if (result != null) {
/*  770:1450 */       return result;
/*  771:     */     }
/*  772:1452 */     result = new EnumeratedClassImpl(Node.createURI(uri), this);
/*  773:1453 */     result.addProperty(RDF.type, getProfile().CLASS());
/*  774:1454 */     if (members == null) {
/*  775:1455 */       result.addProperty(getProfile().ONE_OF(), RDF.nil);
/*  776:     */     } else {
/*  777:1457 */       result.addProperty(getProfile().ONE_OF(), members);
/*  778:     */     }
/*  779:1458 */     addResourceClassAxioms(result);
/*  780:1459 */     return result;
/*  781:     */   }
/*  782:     */   
/*  783:     */   public UnionClass createUnionClass(String uri, RDFList members)
/*  784:     */   {
/*  785:1472 */     checkProfileEntry(getProfile().CLASS(), "CLASS");
/*  786:1473 */     checkProfileEntry(getProfile().UNION_OF(), "UNION_OF");
/*  787:1474 */     UnionClass result = getUnionClass(uri);
/*  788:1475 */     if (result != null) {
/*  789:1476 */       return result;
/*  790:     */     }
/*  791:1478 */     result = new UnionClassImpl(Node.createURI(uri), this);
/*  792:1479 */     result.addProperty(RDF.type, getProfile().CLASS());
/*  793:1480 */     addResourceClassAxioms(result);
/*  794:1481 */     if (members == null) {
/*  795:1482 */       result.addProperty(getProfile().UNION_OF(), RDF.nil);
/*  796:     */     } else {
/*  797:1484 */       result.addProperty(getProfile().UNION_OF(), members);
/*  798:     */     }
/*  799:1485 */     return result;
/*  800:     */   }
/*  801:     */   
/*  802:     */   public IntersectionClass createIntersectionClass(String uri, RDFList members)
/*  803:     */   {
/*  804:1497 */     IntersectionClass result = getIntersectionClass(uri);
/*  805:1498 */     if (result != null) {
/*  806:1499 */       return result;
/*  807:     */     }
/*  808:1501 */     result = new IntersectionClassImpl(Node.createURI(uri), this);
/*  809:1502 */     result.addProperty(RDF.type, getProfile().CLASS());
/*  810:1503 */     addResourceClassAxioms(result);
/*  811:1504 */     if (members == null) {
/*  812:1505 */       result.addProperty(getProfile().INTERSECTION_OF(), RDF.nil);
/*  813:     */     } else {
/*  814:1507 */       result.addProperty(getProfile().INTERSECTION_OF(), members);
/*  815:     */     }
/*  816:1508 */     return result;
/*  817:     */   }
/*  818:     */   
/*  819:     */   public Restriction createRestriction(Property p)
/*  820:     */   {
/*  821:1524 */     checkProfileEntry(getProfile().RESTRICTION(), "RESTRICTION");
/*  822:1525 */     Restriction result = new RestrictionImpl(Node.createAnon(), this);
/*  823:1526 */     result.addProperty(RDF.type, getProfile().RESTRICTION());
/*  824:1527 */     if (p != null) {
/*  825:1528 */       result.addProperty(getProfile().ON_PROPERTY(), p);
/*  826:     */     }
/*  827:1529 */     addResourceClassAxioms(result);
/*  828:1530 */     return result;
/*  829:     */   }
/*  830:     */   
/*  831:     */   public Restriction createRestriction(String uri, Property p)
/*  832:     */   {
/*  833:1546 */     checkProfileEntry(getProfile().RESTRICTION(), "RESTRICTION");
/*  834:1547 */     Restriction result = getRestriction(uri);
/*  835:1548 */     if (result != null) {
/*  836:1549 */       return result;
/*  837:     */     }
/*  838:1551 */     result = new RestrictionImpl(Node.createURI(uri), this);
/*  839:1552 */     result.addProperty(RDF.type, getProfile().RESTRICTION());
/*  840:1553 */     if (p != null) {
/*  841:1554 */       result.addProperty(getProfile().ON_PROPERTY(), p);
/*  842:     */     }
/*  843:1555 */     addResourceClassAxioms(result);
/*  844:1556 */     return result;
/*  845:     */   }
/*  846:     */   
/*  847:     */   public HasValueRestriction createHasValueRestriction(String uri, Property prop, RDFNode value)
/*  848:     */   {
/*  849:1572 */     checkProfileEntry(getProfile().RESTRICTION(), "RESTRICTION");
/*  850:1573 */     if ((prop == null) || (value == null)) {
/*  851:1574 */       throw new IllegalArgumentException("Cannot create hasValueRestriction with a null property or value");
/*  852:     */     }
/*  853:1576 */     checkProfileEntry(getProfile().HAS_VALUE(), "HAS_VALUE");
/*  854:1577 */     HasValueRestriction r = new HasValueRestrictionImpl(Node.createURI(uri), this);
/*  855:1578 */     r.addProperty(RDF.type, getProfile().RESTRICTION());
/*  856:1579 */     r.addProperty(getProfile().ON_PROPERTY(), prop);
/*  857:1580 */     r.addProperty(getProfile().HAS_VALUE(), value);
/*  858:1581 */     addResourceClassAxioms(r);
/*  859:1582 */     return r;
/*  860:     */   }
/*  861:     */   
/*  862:     */   public SomeValuesFromRestriction createSomeValuesFromRestriction(String uri, Property prop, Resource cls)
/*  863:     */   {
/*  864:1597 */     checkProfileEntry(getProfile().RESTRICTION(), "RESTRICTION");
/*  865:1598 */     if ((prop == null) || (cls == null)) {
/*  866:1599 */       throw new IllegalArgumentException("Cannot create someValuesFromRestriction with a null property or class");
/*  867:     */     }
/*  868:1601 */     checkProfileEntry(getProfile().SOME_VALUES_FROM(), "SOME_VALUES_FROM");
/*  869:     */     SomeValuesFromRestriction result;
/*  870:1603 */     if (uri != null)
/*  871:     */     {
/*  872:1604 */       result = getSomeValuesFromRestriction(uri);
/*  873:1605 */       if (result != null) {
/*  874:1606 */         return result;
/*  875:     */       }
/*  876:1608 */       result = new SomeValuesFromRestrictionImpl(Node.createURI(uri), this);
/*  877:     */     }
/*  878:     */     else
/*  879:     */     {
/*  880:1611 */       result = new SomeValuesFromRestrictionImpl(Node.createAnon(), this);
/*  881:     */     }
/*  882:1613 */     result.addProperty(RDF.type, getProfile().RESTRICTION());
/*  883:1614 */     result.addProperty(getProfile().ON_PROPERTY(), prop);
/*  884:1615 */     result.addProperty(getProfile().SOME_VALUES_FROM(), cls);
/*  885:1616 */     addResourceClassAxioms(result);
/*  886:1617 */     return result;
/*  887:     */   }
/*  888:     */   
/*  889:     */   public AllValuesFromRestriction createAllValuesFromRestriction(String uri, Property prop, Resource cls)
/*  890:     */   {
/*  891:1632 */     checkProfileEntry(getProfile().RESTRICTION(), "RESTRICTION");
/*  892:1633 */     if ((prop == null) || (cls == null)) {
/*  893:1634 */       throw new IllegalArgumentException("Cannot create allValuesFromRestriction with a null property or class");
/*  894:     */     }
/*  895:1636 */     checkProfileEntry(getProfile().ALL_VALUES_FROM(), "ALL_VALUES_FROM");
/*  896:     */     AllValuesFromRestriction result;
/*  897:1638 */     if (uri != null)
/*  898:     */     {
/*  899:1639 */       result = getAllValuesFromRestriction(uri);
/*  900:1640 */       if (result != null) {
/*  901:1641 */         return result;
/*  902:     */       }
/*  903:1643 */       result = new AllValuesFromRestrictionImpl(Node.createURI(uri), this);
/*  904:     */     }
/*  905:     */     else
/*  906:     */     {
/*  907:1646 */       result = new AllValuesFromRestrictionImpl(Node.createAnon(), this);
/*  908:     */     }
/*  909:1648 */     result.addProperty(RDF.type, getProfile().RESTRICTION());
/*  910:1649 */     result.addProperty(getProfile().ON_PROPERTY(), prop);
/*  911:1650 */     result.addProperty(getProfile().ALL_VALUES_FROM(), cls);
/*  912:1651 */     addResourceClassAxioms(result);
/*  913:1652 */     return result;
/*  914:     */   }
/*  915:     */   
/*  916:     */   public CardinalityRestriction createCardinalityRestriction(String uri, Property prop, int cardinality)
/*  917:     */   {
/*  918:1667 */     checkProfileEntry(getProfile().RESTRICTION(), "RESTRICTION");
/*  919:1668 */     if (prop == null) {
/*  920:1669 */       throw new IllegalArgumentException("Cannot create cardinalityRestriction with a null property");
/*  921:     */     }
/*  922:     */     CardinalityRestriction result;
/*  924:1672 */     if (uri != null) {
/*  925:1673 */       result = new CardinalityRestrictionImpl(Node.createURI(uri), this);
/*  926:     */     } else {
/*  927:1675 */       result = new CardinalityRestrictionImpl(Node.createAnon(), this);
/*  928:     */     }
/*  929:1676 */     result.addProperty(RDF.type, getProfile().RESTRICTION());
/*  930:1677 */     result.addProperty(getProfile().ON_PROPERTY(), prop);
/*  931:1678 */     result.addProperty(getProfile().CARDINALITY(), createTypedLiteral(new Integer(cardinality), XSDDatatype.XSDint));
/*  932:1679 */     addResourceClassAxioms(result);
/*  933:1680 */     return result;
/*  934:     */   }
/*  935:     */   
/*  936:     */   public MinCardinalityRestriction createMinCardinalityRestriction(String uri, Property prop, int cardinality)
/*  937:     */   {
/*  938:1695 */     checkProfileEntry(getProfile().RESTRICTION(), "RESTRICTION");
/*  939:1696 */     if (prop == null) {
/*  940:1697 */       throw new IllegalArgumentException("Cannot create MinCardinalityRestriction with a null property");
/*  941:     */     }
/*  942:     */     MinCardinalityRestriction result;
/*  944:1700 */     if (uri != null) {
/*  945:1701 */       result = new MinCardinalityRestrictionImpl(Node.createURI(uri), this);
/*  946:     */     } else {
/*  947:1703 */       result = new MinCardinalityRestrictionImpl(Node.createAnon(), this);
/*  948:     */     }
/*  949:1704 */     result.addProperty(RDF.type, getProfile().RESTRICTION());
/*  950:1705 */     result.addProperty(getProfile().ON_PROPERTY(), prop);
/*  951:1706 */     result.addProperty(getProfile().MIN_CARDINALITY(), createTypedLiteral(new Integer(cardinality), XSDDatatype.XSDint));
/*  952:1707 */     addResourceClassAxioms(result);
/*  953:1708 */     return result;
/*  954:     */   }
/*  955:     */   
/*  956:     */   public MaxCardinalityRestriction createMaxCardinalityRestriction(String uri, Property prop, int cardinality)
/*  957:     */   {
/*  958:1723 */     checkProfileEntry(getProfile().RESTRICTION(), "RESTRICTION");
/*  959:1724 */     if (prop == null) {
/*  960:1725 */       throw new IllegalArgumentException("Cannot create maxCardinalityRestriction with a null property");
/*  961:     */     }
/*  962:     */     MaxCardinalityRestriction result;
/*  964:1728 */     if (uri != null) {
/*  965:1729 */       result = new MaxCardinalityRestrictionImpl(Node.createURI(uri), this);
/*  966:     */     } else {
/*  967:1731 */       result = new MaxCardinalityRestrictionImpl(Node.createAnon(), this);
/*  968:     */     }
/*  969:1732 */     result.addProperty(RDF.type, getProfile().RESTRICTION());
/*  970:1733 */     result.addProperty(getProfile().ON_PROPERTY(), prop);
/*  971:1734 */     result.addProperty(getProfile().MAX_CARDINALITY(), createTypedLiteral(new Integer(cardinality), XSDDatatype.XSDint));
/*  972:1735 */     addResourceClassAxioms(result);
/*  973:1736 */     return result;
/*  974:     */   }
/*  975:     */   
/*  976:     */   public DataRange createDataRange(RDFList literals)
/*  977:     */   {
/*  978:1748 */     checkProfileEntry(getProfile().DATARANGE(), "DATARANGE");
/*  979:1749 */     checkProfileEntry(getProfile().ONE_OF(), "ONE_OF");
/*  980:1750 */     DataRange result = new DataRangeImpl(Node.createAnon(), this);
/*  981:1751 */     result.addProperty(RDF.type, getProfile().DATARANGE());
/*  982:1752 */     if (literals == null) {
/*  983:1753 */       result.addProperty(getProfile().ONE_OF(), RDF.nil);
/*  984:     */     } else {
/*  985:1755 */       result.addProperty(getProfile().ONE_OF(), literals);
/*  986:     */     }
/*  987:1756 */     return result;
/*  988:     */   }
/*  989:     */   
/*  990:     */   public AllDifferent createAllDifferent()
/*  991:     */   {
/*  992:1771 */     return createAllDifferent(null);
/*  993:     */   }
/*  994:     */   
/*  995:     */   public AllDifferent createAllDifferent(RDFList differentMembers)
/*  996:     */   {
/*  997:1786 */     checkProfileEntry(getProfile().ALL_DIFFERENT(), "ALL_DIFFERENT");
/*  998:1787 */     AllDifferent result = new AllDifferentImpl(Node.createAnon(), this);
/*  999:1788 */     result.addProperty(RDF.type, getProfile().ALL_DIFFERENT());
/* 1000:1789 */     if (differentMembers == null) {
/* 1001:1790 */       result.addProperty(getProfile().DISTINCT_MEMBERS(), RDF.nil);
/* 1002:     */     } else {
/* 1003:1792 */       result.addProperty(getProfile().DISTINCT_MEMBERS(), differentMembers);
/* 1004:     */     }
/* 1005:1793 */     return result;
/* 1006:     */   }
/* 1007:     */   
/* 1008:     */   public OntResource createOntResource(String uri)
/* 1009:     */   {
/* 1010:1843 */     return new OntResourceImpl(Node.createURI(uri), this);
/* 1011:     */   }
/* 1012:     */   
/* 1013:     */   public RDFList createList()
/* 1014:     */   {
/* 1015:1853 */     Resource list = getResource(getProfile().NIL().getURI());
/* 1016:1854 */     if (list == null) {
/* 1017:1855 */       return new RDFListImpl(getProfile().NIL().asNode(), this);
/* 1018:     */     }
/* 1019:1857 */     return new RDFListImpl(list.asNode(), this);
/* 1020:     */   }
/* 1021:     */   
/* 1022:     */   public Profile getProfile()
/* 1023:     */   {
/* 1024:1872 */     return this.profile;
/* 1025:     */   }
/* 1026:     */   
/* 1027:     */   public boolean strictMode()
/* 1028:     */   {
/* 1029:1887 */     return this.m_strictMode;
/* 1030:     */   }
/* 1031:     */   
/* 1032:     */   public void setStrictMode(boolean strict)
/* 1033:     */   {
/* 1034:1900 */     this.m_strictMode = strict;
/* 1035:     */   }
/* 1036:     */   
/* 1037:     */   public OntResource getOntResource(String uri)
/* 1038:     */   {
/* 1039:1911 */     Resource r = getResource(uri);
/* 1040:1912 */     if (containsResource(r)) {
/* 1041:1913 */       return new OntResourceImpl(r.asNode(), this);
/* 1042:     */     }
/* 1043:1915 */     return null;
/* 1044:     */   }
/* 1045:     */   
/* 1046:     */   public OntResource getOntResource(Resource res)
/* 1047:     */   {
/* 1048:1926 */     return new OntResourceImpl(res.asNode(), this);
/* 1049:     */   }
/* 1050:     */   
/* 1051:     */   protected void checkProfileEntry(Object profileTerm, String desc)
/* 1052:     */   {
/* 1053:1937 */     if (profileTerm == null) {
/* 1054:1939 */       throw new ProfileException(desc, getProfile());
/* 1055:     */     }
/* 1056:     */   }
/* 1057:     */   
/* 1058:     */   private void addResourceClassAxioms(RDFNode value)
/* 1059:     */   {
/* 1060:1988 */     getGraph().add(new Axiom(value.asNode(), RDF.type.asNode(), RDFS.Resource.asNode()));
/* 1061:1989 */     getGraph().add(new Axiom(value.asNode(), RDF.type.asNode(), RDFS.Class.asNode()));
/* 1062:     */   }
/* 1063:     */ }

