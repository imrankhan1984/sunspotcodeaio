/*  1:   */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*  9:   */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/* 10:   */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/* 11:   */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/* 12:   */ import it.polimi.elet.contextaddict.microjena.ontology.UnionClass;
/* 13:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/* 14:   */ 
/* 15:   */ public class UnionClassImpl
/* 16:   */   extends BooleanClassDescriptionImpl
/* 17:   */   implements UnionClass
/* 18:   */ {
/* 19:49 */   public static Implementation factory = new Implementation()
/* 20:   */   {
/* 21:   */     public EnhNode wrap(Node n, EnhGraph eg)
/* 22:   */     {
/* 23:51 */       if (canWrap(n, eg)) {
/* 24:52 */         return new UnionClassImpl(n, eg);
/* 25:   */       }
/* 26:54 */       throw new ConversionException("Cannot convert node " + n + " to UnionClass");
/* 27:   */     }
/* 28:   */     
/* 29:   */     public boolean canWrap(Node node, EnhGraph eg)
/* 30:   */     {
/* 31:59 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/* 32:60 */       Property union = profile == null ? null : profile.UNION_OF();
/* 33:   */       
/* 34:62 */       return (profile != null) && (profile.isSupported(node, eg, OntClass.class)) && (union != null) && (eg.asGraph().contains(node, union.asNode(), Node.ANY));
/* 35:   */     }
/* 36:   */   };
/* 37:   */   
/* 38:   */   public UnionClassImpl(Node n, EnhGraph g)
/* 39:   */   {
/* 40:78 */     super(n, g);
/* 41:   */   }
/* 42:   */   
/* 43:   */   public Property operator()
/* 44:   */   {
/* 45:82 */     return getProfile().UNION_OF();
/* 46:   */   }
/* 47:   */   
/* 48:   */   public String getOperatorName()
/* 49:   */   {
/* 50:86 */     return "UNION_OF";
/* 51:   */   }
/* 52:   */ }
