/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntProperty;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*  16:    */ 
/*  17:    */ public class RDFSProfile
/*  18:    */   extends AbstractProfile
/*  19:    */ {
/*  20:    */   public String NAMESPACE()
/*  21:    */   {
/*  22: 47 */     return RDFS.getURI();
/*  23:    */   }
/*  24:    */   
/*  25:    */   public Resource CLASS()
/*  26:    */   {
/*  27: 49 */     return RDFS.Class;
/*  28:    */   }
/*  29:    */   
/*  30:    */   public Resource RESTRICTION()
/*  31:    */   {
/*  32: 50 */     return null;
/*  33:    */   }
/*  34:    */   
/*  35:    */   public Resource THING()
/*  36:    */   {
/*  37: 51 */     return null;
/*  38:    */   }
/*  39:    */   
/*  40:    */   public Resource NOTHING()
/*  41:    */   {
/*  42: 52 */     return null;
/*  43:    */   }
/*  44:    */   
/*  45:    */   public Resource PROPERTY()
/*  46:    */   {
/*  47: 53 */     return RDF.Property;
/*  48:    */   }
/*  49:    */   
/*  50:    */   public Resource OBJECT_PROPERTY()
/*  51:    */   {
/*  52: 54 */     return null;
/*  53:    */   }
/*  54:    */   
/*  55:    */   public Resource DATATYPE_PROPERTY()
/*  56:    */   {
/*  57: 55 */     return null;
/*  58:    */   }
/*  59:    */   
/*  60:    */   public Resource TRANSITIVE_PROPERTY()
/*  61:    */   {
/*  62: 56 */     return null;
/*  63:    */   }
/*  64:    */   
/*  65:    */   public Resource SYMMETRIC_PROPERTY()
/*  66:    */   {
/*  67: 57 */     return null;
/*  68:    */   }
/*  69:    */   
/*  70:    */   public Resource FUNCTIONAL_PROPERTY()
/*  71:    */   {
/*  72: 58 */     return null;
/*  73:    */   }
/*  74:    */   
/*  75:    */   public Resource INVERSE_FUNCTIONAL_PROPERTY()
/*  76:    */   {
/*  77: 59 */     return null;
/*  78:    */   }
/*  79:    */   
/*  80:    */   public Resource ALL_DIFFERENT()
/*  81:    */   {
/*  82: 60 */     return null;
/*  83:    */   }
/*  84:    */   
/*  85:    */   public Resource ONTOLOGY()
/*  86:    */   {
/*  87: 61 */     return null;
/*  88:    */   }
/*  89:    */   
/*  90:    */   public Resource DEPRECATED_CLASS()
/*  91:    */   {
/*  92: 62 */     return null;
/*  93:    */   }
/*  94:    */   
/*  95:    */   public Resource DEPRECATED_PROPERTY()
/*  96:    */   {
/*  97: 63 */     return null;
/*  98:    */   }
/*  99:    */   
/* 100:    */   public Resource ANNOTATION_PROPERTY()
/* 101:    */   {
/* 102: 64 */     return null;
/* 103:    */   }
/* 104:    */   
/* 105:    */   public Resource ONTOLOGY_PROPERTY()
/* 106:    */   {
/* 107: 65 */     return null;
/* 108:    */   }
/* 109:    */   
/* 110:    */   public Resource LIST()
/* 111:    */   {
/* 112: 66 */     return RDF.List;
/* 113:    */   }
/* 114:    */   
/* 115:    */   public Resource NIL()
/* 116:    */   {
/* 117: 67 */     return RDF.nil;
/* 118:    */   }
/* 119:    */   
/* 120:    */   public Resource DATARANGE()
/* 121:    */   {
/* 122: 68 */     return null;
/* 123:    */   }
/* 124:    */   
/* 125:    */   public Property EQUIVALENT_PROPERTY()
/* 126:    */   {
/* 127: 70 */     return null;
/* 128:    */   }
/* 129:    */   
/* 130:    */   public Property EQUIVALENT_CLASS()
/* 131:    */   {
/* 132: 71 */     return null;
/* 133:    */   }
/* 134:    */   
/* 135:    */   public Property DISJOINT_WITH()
/* 136:    */   {
/* 137: 72 */     return null;
/* 138:    */   }
/* 139:    */   
/* 140:    */   public Property SAME_INDIVIDUAL_AS()
/* 141:    */   {
/* 142: 73 */     return null;
/* 143:    */   }
/* 144:    */   
/* 145:    */   public Property SAME_AS()
/* 146:    */   {
/* 147: 74 */     return null;
/* 148:    */   }
/* 149:    */   
/* 150:    */   public Property DIFFERENT_FROM()
/* 151:    */   {
/* 152: 75 */     return null;
/* 153:    */   }
/* 154:    */   
/* 155:    */   public Property DISTINCT_MEMBERS()
/* 156:    */   {
/* 157: 76 */     return null;
/* 158:    */   }
/* 159:    */   
/* 160:    */   public Property UNION_OF()
/* 161:    */   {
/* 162: 77 */     return null;
/* 163:    */   }
/* 164:    */   
/* 165:    */   public Property INTERSECTION_OF()
/* 166:    */   {
/* 167: 78 */     return null;
/* 168:    */   }
/* 169:    */   
/* 170:    */   public Property COMPLEMENT_OF()
/* 171:    */   {
/* 172: 79 */     return null;
/* 173:    */   }
/* 174:    */   
/* 175:    */   public Property ONE_OF()
/* 176:    */   {
/* 177: 80 */     return null;
/* 178:    */   }
/* 179:    */   
/* 180:    */   public Property ON_PROPERTY()
/* 181:    */   {
/* 182: 81 */     return null;
/* 183:    */   }
/* 184:    */   
/* 185:    */   public Property ALL_VALUES_FROM()
/* 186:    */   {
/* 187: 82 */     return null;
/* 188:    */   }
/* 189:    */   
/* 190:    */   public Property HAS_VALUE()
/* 191:    */   {
/* 192: 83 */     return null;
/* 193:    */   }
/* 194:    */   
/* 195:    */   public Property SOME_VALUES_FROM()
/* 196:    */   {
/* 197: 84 */     return null;
/* 198:    */   }
/* 199:    */   
/* 200:    */   public Property MIN_CARDINALITY()
/* 201:    */   {
/* 202: 85 */     return null;
/* 203:    */   }
/* 204:    */   
/* 205:    */   public Property MAX_CARDINALITY()
/* 206:    */   {
/* 207: 86 */     return null;
/* 208:    */   }
/* 209:    */   
/* 210:    */   public Property CARDINALITY()
/* 211:    */   {
/* 212: 87 */     return null;
/* 213:    */   }
/* 214:    */   
/* 215:    */   public Property INVERSE_OF()
/* 216:    */   {
/* 217: 88 */     return null;
/* 218:    */   }
/* 219:    */   
/* 220:    */   public Property IMPORTS()
/* 221:    */   {
/* 222: 89 */     return null;
/* 223:    */   }
/* 224:    */   
/* 225:    */   public Property PRIOR_VERSION()
/* 226:    */   {
/* 227: 90 */     return null;
/* 228:    */   }
/* 229:    */   
/* 230:    */   public Property BACKWARD_COMPATIBLE_WITH()
/* 231:    */   {
/* 232: 91 */     return null;
/* 233:    */   }
/* 234:    */   
/* 235:    */   public Property INCOMPATIBLE_WITH()
/* 236:    */   {
/* 237: 92 */     return null;
/* 238:    */   }
/* 239:    */   
/* 240:    */   public Property SUB_PROPERTY_OF()
/* 241:    */   {
/* 242: 93 */     return RDFS.subPropertyOf;
/* 243:    */   }
/* 244:    */   
/* 245:    */   public Property SUB_CLASS_OF()
/* 246:    */   {
/* 247: 94 */     return RDFS.subClassOf;
/* 248:    */   }
/* 249:    */   
/* 250:    */   public Property DOMAIN()
/* 251:    */   {
/* 252: 95 */     return RDFS.domain;
/* 253:    */   }
/* 254:    */   
/* 255:    */   public Property RANGE()
/* 256:    */   {
/* 257: 96 */     return RDFS.range;
/* 258:    */   }
/* 259:    */   
/* 260:    */   public Property FIRST()
/* 261:    */   {
/* 262: 97 */     return RDF.first;
/* 263:    */   }
/* 264:    */   
/* 265:    */   public Property REST()
/* 266:    */   {
/* 267: 98 */     return RDF.rest;
/* 268:    */   }
/* 269:    */   
/* 270:    */   public Property MIN_CARDINALITY_Q()
/* 271:    */   {
/* 272: 99 */     return null;
/* 273:    */   }
/* 274:    */   
/* 275:    */   public Property MAX_CARDINALITY_Q()
/* 276:    */   {
/* 277:100 */     return null;
/* 278:    */   }
/* 279:    */   
/* 280:    */   public Property CARDINALITY_Q()
/* 281:    */   {
/* 282:101 */     return null;
/* 283:    */   }
/* 284:    */   
/* 285:    */   public Property HAS_CLASS_Q()
/* 286:    */   {
/* 287:102 */     return null;
/* 288:    */   }
/* 289:    */   
/* 290:    */   public Property VERSION_INFO()
/* 291:    */   {
/* 292:105 */     return null;
/* 293:    */   }
/* 294:    */   
/* 295:    */   public Property LABEL()
/* 296:    */   {
/* 297:106 */     return RDFS.label;
/* 298:    */   }
/* 299:    */   
/* 300:    */   public Property COMMENT()
/* 301:    */   {
/* 302:107 */     return RDFS.comment;
/* 303:    */   }
/* 304:    */   
/* 305:    */   public Property SEE_ALSO()
/* 306:    */   {
/* 307:108 */     return RDFS.seeAlso;
/* 308:    */   }
/* 309:    */   
/* 310:    */   public Property IS_DEFINED_BY()
/* 311:    */   {
/* 312:109 */     return RDFS.isDefinedBy;
/* 313:    */   }
/* 314:    */   
/* 315:    */   protected Resource[][] aliasTable()
/* 316:    */   {
/* 317:112 */     return new Resource[][] { new Resource[0] };
/* 318:    */   }
/* 319:    */   
/* 320:    */   public Iterator getAxiomTypes()
/* 321:    */   {
/* 322:119 */     return arrayToIterator(new Resource[0]);
/* 323:    */   }
/* 324:    */   
/* 325:    */   public Iterator getAnnotationProperties()
/* 326:    */   {
/* 327:127 */     return arrayToIterator(new Resource[] { RDFS.label, RDFS.seeAlso, RDFS.comment, RDFS.isDefinedBy });
/* 328:    */   }
/* 329:    */   
/* 330:    */   public Iterator getClassDescriptionTypes()
/* 331:    */   {
/* 332:138 */     return arrayToIterator(new Resource[] { RDFS.Class });
/* 333:    */   }
/* 334:    */   
/* 335:    */   public boolean isSupported(Node n, EnhGraph g, Class type)
/* 336:    */   {
/* 337:162 */     if ((g instanceof OntModel))
/* 338:    */     {
/* 339:163 */       OntModel m = (OntModel)g;
/* 340:164 */       if (!m.strictMode()) {
/* 341:166 */         return true;
/* 342:    */       }
/* 343:169 */       SupportsCheck check = (SupportsCheck)s_supportsChecks.get(type);
/* 344:    */       
/* 345:171 */       return (check == null) || (check.doCheck(n, g));
/* 346:    */     }
/* 347:174 */     return false;
/* 348:    */   }
/* 349:    */   
/* 350:    */   public String getLabel()
/* 351:    */   {
/* 352:185 */     return "RDFS";
/* 353:    */   }
/* 354:    */   
/* 355:    */   protected static class SupportsCheck
/* 356:    */   {
/* 357:    */     public boolean doCheck(Node n, EnhGraph g)
/* 358:    */     {
/* 359:191 */       return true;
/* 360:    */     }
/* 361:    */   }
/* 362:    */   
/* 363:195 */   private static Object[][] s_supportsCheckTable = { { OntClass.class, new SupportsCheck()
/* 364:    */   {
/* 365:    */     public boolean doCheck(Node n, EnhGraph g)
/* 366:    */     {
/* 367:199 */       return (g.asGraph().contains(n, RDF.type.asNode(), RDFS.Class.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), RDFS.Datatype.asNode())) || (n.equals(RDFS.Resource.asNode())) || (g.asGraph().contains(Node.ANY, RDFS.domain.asNode(), n)) || (g.asGraph().contains(Node.ANY, RDFS.range.asNode(), n));
/* 368:    */     }
/* 369:195 */   } }, { RDFList.class, new SupportsCheck()
/* 370:    */   {
/* 371:    */     public boolean doCheck(Node n, EnhGraph g)
/* 372:    */     {
/* 373:211 */       return (n.equals(RDF.nil.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), RDF.List.asNode()));
/* 374:    */     }
/* 375:195 */   } }, { OntProperty.class, new SupportsCheck()
/* 376:    */   {
/* 377:    */     public boolean doCheck(Node n, EnhGraph g)
/* 378:    */     {
/* 379:218 */       return g.asGraph().contains(n, RDF.type.asNode(), RDF.Property.asNode());
/* 380:    */     }
/* 381:195 */   } } };
/* 382:225 */   protected static Map s_supportsChecks = new Map();
/* 383:    */   
/* 384:    */   static
/* 385:    */   {
/* 386:229 */     for (int i = 0; i < s_supportsCheckTable.length; i++) {
/* 387:230 */       s_supportsChecks.put(s_supportsCheckTable[i][0], s_supportsCheckTable[i][1]);
/* 388:    */     }
/* 389:    */   }
/* 390:    */ }

