/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node_Blank;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Node_URI;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.AllDifferent;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.AllValuesFromRestriction;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.AnnotationProperty;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.CardinalityRestriction;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.ontology.DataRange;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.ontology.DatatypeProperty;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.ontology.FunctionalProperty;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.ontology.HasValueRestriction;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.ontology.Individual;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.ontology.InverseFunctionalProperty;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.ontology.MaxCardinalityRestriction;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.ontology.MinCardinalityRestriction;
/*  20:    */ import it.polimi.elet.contextaddict.microjena.ontology.ObjectProperty;
/*  21:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/*  22:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  23:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntProperty;
/*  24:    */ import it.polimi.elet.contextaddict.microjena.ontology.Ontology;
/*  25:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  26:    */ import it.polimi.elet.contextaddict.microjena.ontology.Restriction;
/*  27:    */ import it.polimi.elet.contextaddict.microjena.ontology.SomeValuesFromRestriction;
/*  28:    */ import it.polimi.elet.contextaddict.microjena.ontology.SymmetricProperty;
/*  29:    */ import it.polimi.elet.contextaddict.microjena.ontology.TransitiveProperty;
/*  30:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  31:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*  32:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  33:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  34:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*  35:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.OWL;
/*  36:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  37:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*  38:    */ 
/*  39:    */ public class OWLDLProfile
/*  40:    */   extends OWLProfile
/*  41:    */ {
/*  42:    */   public String getLabel()
/*  43:    */   {
/*  44: 74 */     return "OWL DL";
/*  45:    */   }
/*  46:    */   
/*  47: 77 */   protected static Object[][] s_supportsCheckData = { { AllDifferent.class, new OWLProfile.SupportsCheck()
/*  48:    */   {
/*  49:    */     public boolean doCheck(Node n, EnhGraph g)
/*  50:    */     {
/*  51: 81 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.AllDifferent.asNode());
/*  52:    */     }
/*  53: 77 */   } }, { AnnotationProperty.class, new OWLProfile.SupportsCheck()
/*  54:    */   {
/*  55:    */     public boolean doCheck(Node n, EnhGraph g)
/*  56:    */     {
/*  57: 87 */       for (Iterator i = ((OntModel)g).getProfile().getAnnotationProperties(); i.hasNext();) {
/*  58: 88 */         if (((Resource)i.next()).asNode().equals(n)) {
/*  59: 90 */           return true;
/*  60:    */         }
/*  61:    */       }
/*  62: 93 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.AnnotationProperty.asNode());
/*  63:    */     }
/*  64: 77 */   } }, { OntClass.class, new OWLProfile.SupportsCheck()
/*  65:    */   {
/*  66:    */     public boolean doCheck(Node n, EnhGraph eg)
/*  67:    */     {
/*  68: 99 */       Graph g = eg.asGraph();
/*  69:100 */       Node rdfTypeNode = RDF.type.asNode();
/*  70:101 */       return (g.contains(n, rdfTypeNode, OWL.Class.asNode())) || (g.contains(n, rdfTypeNode, OWL.Restriction.asNode())) || (g.contains(n, rdfTypeNode, RDFS.Class.asNode())) || (g.contains(n, rdfTypeNode, RDFS.Datatype.asNode())) || (n.equals(OWL.Thing.asNode())) || (n.equals(OWL.Nothing.asNode())) || (g.contains(Node.ANY, RDFS.domain.asNode(), n)) || (g.contains(Node.ANY, RDFS.range.asNode(), n)) || (g.contains(n, OWL.intersectionOf.asNode(), Node.ANY)) || (g.contains(n, OWL.unionOf.asNode(), Node.ANY)) || (g.contains(n, OWL.complementOf.asNode(), Node.ANY));
/*  71:    */     }
/*  72: 77 */   } }, { DatatypeProperty.class, new OWLProfile.SupportsCheck()
/*  73:    */   {
/*  74:    */     public boolean doCheck(Node n, EnhGraph g)
/*  75:    */     {
/*  76:119 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode());
/*  77:    */     }
/*  78: 77 */   } }, { ObjectProperty.class, new OWLProfile.SupportsCheck()
/*  79:    */   {
/*  80:    */     public boolean doCheck(Node n, EnhGraph g)
/*  81:    */     {
/*  82:125 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.ObjectProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.TransitiveProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.SymmetricProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.InverseFunctionalProperty.asNode()));
/*  83:    */     }
/*  84: 77 */   } }, { FunctionalProperty.class, new OWLProfile.SupportsCheck()
/*  85:    */   {
/*  86:    */     public boolean doCheck(Node n, EnhGraph g)
/*  87:    */     {
/*  88:134 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.FunctionalProperty.asNode());
/*  89:    */     }
/*  90: 77 */   } }, { InverseFunctionalProperty.class, new OWLProfile.SupportsCheck()
/*  91:    */   {
/*  92:    */     public boolean doCheck(Node n, EnhGraph g)
/*  93:    */     {
/*  94:140 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.InverseFunctionalProperty.asNode())) && (!g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode()));
/*  95:    */     }
/*  96: 77 */   } }, { RDFList.class, new OWLProfile.SupportsCheck()
/*  97:    */   {
/*  98:    */     public boolean doCheck(Node n, EnhGraph g)
/*  99:    */     {
/* 100:147 */       return (n.equals(RDF.nil.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), RDF.List.asNode()));
/* 101:    */     }
/* 102: 77 */   } }, { OntProperty.class, new OWLProfile.SupportsCheck()
/* 103:    */   {
/* 104:    */     public boolean doCheck(Node n, EnhGraph g)
/* 105:    */     {
/* 106:154 */       return (g.asGraph().contains(n, RDF.type.asNode(), RDF.Property.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.ObjectProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.AnnotationProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.TransitiveProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.SymmetricProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.FunctionalProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.InverseFunctionalProperty.asNode()));
/* 107:    */     }
/* 108: 77 */   } }, { Ontology.class, new OWLProfile.SupportsCheck()
/* 109:    */   {
/* 110:    */     public boolean doCheck(Node n, EnhGraph g)
/* 111:    */     {
/* 112:167 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.Ontology.asNode());
/* 113:    */     }
/* 114: 77 */   } }, { Restriction.class, new OWLProfile.SupportsCheck()
/* 115:    */   {
/* 116:    */     public boolean doCheck(Node n, EnhGraph g)
/* 117:    */     {
/* 118:173 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode());
/* 119:    */     }
/* 120: 77 */   } }, { AllValuesFromRestriction.class, new OWLProfile.SupportsCheck()
/* 121:    */   {
/* 122:    */     public boolean doCheck(Node n, EnhGraph g)
/* 123:    */     {
/* 124:179 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLDLProfile.containsSome(g, n, OWL.allValuesFrom)) && (OWLDLProfile.containsSome(g, n, OWL.onProperty));
/* 125:    */     }
/* 126: 77 */   } }, { SomeValuesFromRestriction.class, new OWLProfile.SupportsCheck()
/* 127:    */   {
/* 128:    */     public boolean doCheck(Node n, EnhGraph g)
/* 129:    */     {
/* 130:187 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLDLProfile.containsSome(g, n, OWL.someValuesFrom)) && (OWLDLProfile.containsSome(g, n, OWL.onProperty));
/* 131:    */     }
/* 132: 77 */   } }, { HasValueRestriction.class, new OWLProfile.SupportsCheck()
/* 133:    */   {
/* 134:    */     public boolean doCheck(Node n, EnhGraph g)
/* 135:    */     {
/* 136:195 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLDLProfile.containsSome(g, n, OWL.hasValue)) && (OWLDLProfile.containsSome(g, n, OWL.onProperty));
/* 137:    */     }
/* 138: 77 */   } }, { CardinalityRestriction.class, new OWLProfile.SupportsCheck()
/* 139:    */   {
/* 140:    */     public boolean doCheck(Node n, EnhGraph g)
/* 141:    */     {
/* 142:203 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLDLProfile.containsSome(g, n, OWL.cardinality)) && (OWLDLProfile.containsSome(g, n, OWL.onProperty));
/* 143:    */     }
/* 144: 77 */   } }, { MinCardinalityRestriction.class, new OWLProfile.SupportsCheck()
/* 145:    */   {
/* 146:    */     public boolean doCheck(Node n, EnhGraph g)
/* 147:    */     {
/* 148:211 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLDLProfile.containsSome(g, n, OWL.minCardinality)) && (OWLDLProfile.containsSome(g, n, OWL.onProperty));
/* 149:    */     }
/* 150: 77 */   } }, { MaxCardinalityRestriction.class, new OWLProfile.SupportsCheck()
/* 151:    */   {
/* 152:    */     public boolean doCheck(Node n, EnhGraph g)
/* 153:    */     {
/* 154:219 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLDLProfile.containsSome(g, n, OWL.maxCardinality)) && (OWLDLProfile.containsSome(g, n, OWL.onProperty));
/* 155:    */     }
/* 156: 77 */   } }, { SymmetricProperty.class, new OWLProfile.SupportsCheck()
/* 157:    */   {
/* 158:    */     public boolean doCheck(Node n, EnhGraph g)
/* 159:    */     {
/* 160:227 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.SymmetricProperty.asNode())) && (!g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode()));
/* 161:    */     }
/* 162: 77 */   } }, { TransitiveProperty.class, new OWLProfile.SupportsCheck()
/* 163:    */   {
/* 164:    */     public boolean doCheck(Node n, EnhGraph g)
/* 165:    */     {
/* 166:234 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.TransitiveProperty.asNode())) && (!g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode()));
/* 167:    */     }
/* 168: 77 */   } }, { Individual.class, new OWLProfile.SupportsCheck()
/* 169:    */   {
/* 170:    */     public boolean doCheck(Node n, EnhGraph eg)
/* 171:    */     {
/* 172:241 */       if (((n instanceof Node_URI)) || ((n instanceof Node_Blank)))
/* 173:    */       {
/* 174:243 */         Graph g = eg.asGraph();
/* 175:    */         
/* 176:    */ 
/* 177:246 */         return (!g.contains(n, RDF.type.asNode(), RDFS.Class.asNode())) && (!g.contains(n, RDF.type.asNode(), RDF.Property.asNode()));
/* 178:    */       }
/* 179:249 */       return false;
/* 180:    */     }
/* 181: 77 */   } }, { DataRange.class, new OWLProfile.SupportsCheck()
/* 182:    */   {
/* 183:    */     public boolean doCheck(Node n, EnhGraph g)
/* 184:    */     {
/* 185:256 */       return ((n instanceof Node_Blank)) && (g.asGraph().contains(n, RDF.type.asNode(), OWL.DataRange.asNode()));
/* 186:    */     }
/* 187: 77 */   } } };
/* 188:    */   
/* 189:    */   public static boolean containsSome(EnhGraph g, Node n, Property p)
/* 190:    */   {
/* 191:265 */     return AbstractProfile.containsSome(g, n, p);
/* 192:    */   }
/* 193:    */   
/* 194:269 */   private static Map s_supportsChecks = new Map();
/* 195:    */   
/* 196:    */   static
/* 197:    */   {
/* 198:273 */     for (int i = 0; i < s_supportsCheckData.length; i++) {
/* 199:274 */       s_supportsChecks.put(s_supportsCheckData[i][0], s_supportsCheckData[i][1]);
/* 200:    */     }
/* 201:    */   }
/* 202:    */   
/* 203:    */   protected Map getCheckTable()
/* 204:    */   {
/* 205:279 */     return s_supportsChecks;
/* 206:    */   }
/* 207:    */ }
