/*  1:   */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.ontology.FunctionalProperty;
/*  9:   */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/* 10:   */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/* 11:   */ 
/* 12:   */ public class FunctionalPropertyImpl
/* 13:   */   extends OntPropertyImpl
/* 14:   */   implements FunctionalProperty
/* 15:   */ {
/* 16:47 */   public static Implementation factory = new Implementation()
/* 17:   */   {
/* 18:   */     public EnhNode wrap(Node n, EnhGraph eg)
/* 19:   */     {
/* 20:49 */       if (canWrap(n, eg)) {
/* 21:50 */         return new FunctionalPropertyImpl(n, eg);
/* 22:   */       }
/* 23:52 */       throw new ConversionException("Cannot convert node " + n + " to FunctionalProperty");
/* 24:   */     }
/* 25:   */     
/* 26:   */     public boolean canWrap(Node node, EnhGraph eg)
/* 27:   */     {
/* 28:58 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/* 29:59 */       return (profile != null) && (profile.isSupported(node, eg, FunctionalProperty.class));
/* 30:   */     }
/* 31:   */   };
/* 32:   */   
/* 33:   */   public FunctionalPropertyImpl(Node n, EnhGraph g)
/* 34:   */   {
/* 35:72 */     super(n, g);
/* 36:   */   }
/* 37:   */ }

