/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.Individual;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntResource;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  14:    */ 
/*  15:    */ public class IndividualImpl
/*  16:    */   extends OntResourceImpl
/*  17:    */   implements Individual
/*  18:    */ {
/*  19: 50 */   public static Implementation factory = new Implementation()
/*  20:    */   {
/*  21:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  22:    */     {
/*  23: 52 */       if (canWrap(n, eg)) {
/*  24: 53 */         return new IndividualImpl(n, eg);
/*  25:    */       }
/*  26: 55 */       throw new ConversionException("Cannot convert node " + n.toString() + " to Individual");
/*  27:    */     }
/*  28:    */     
/*  29:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  30:    */     {
/*  31: 61 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  32: 62 */       return (profile != null) && (profile.isSupported(node, eg, Individual.class));
/*  33:    */     }
/*  34:    */   };
/*  35:    */   
/*  36:    */   public IndividualImpl(Node n, EnhGraph g)
/*  37:    */   {
/*  38: 75 */     super(n, g);
/*  39:    */   }
/*  40:    */   
/*  41:    */   public void setSameIndividualAs(Resource res)
/*  42:    */   {
/*  43: 86 */     setPropertyValue(getProfile().SAME_INDIVIDUAL_AS(), "SAME_INDIVIDUAL_AS", res);
/*  44:    */   }
/*  45:    */   
/*  46:    */   public void addSameIndividualAs(Resource res)
/*  47:    */   {
/*  48: 96 */     addPropertyValue(getProfile().SAME_INDIVIDUAL_AS(), "SAME_INDIVIDUAL_AS", res);
/*  49:    */   }
/*  50:    */   
/*  51:    */   public OntResource getSameIndividualAs()
/*  52:    */   {
/*  53:107 */     return objectAsResource(getProfile().SAME_INDIVIDUAL_AS(), "SAME_INDIVIDUAL_AS");
/*  54:    */   }
/*  55:    */   
/*  56:    */   public ExtendedIterator listSameIndividualAs()
/*  57:    */   {
/*  58:118 */     return listAsOntResources(getProfile().SAME_INDIVIDUAL_AS());
/*  59:    */   }
/*  60:    */   
/*  61:    */   public boolean isSameIndividualAs(Resource res)
/*  62:    */   {
/*  63:127 */     return hasPropertyValue(getProfile().SAME_INDIVIDUAL_AS(), "SAME_INDIVIDUAL_AS", res);
/*  64:    */   }
/*  65:    */   
/*  66:    */   public void removeSameIndividualAs(Resource res)
/*  67:    */   {
/*  68:136 */     removePropertyValue(getProfile().SAME_INDIVIDUAL_AS(), "SAME_INDIVIDUAL_AS", res);
/*  69:    */   }
/*  70:    */ }

