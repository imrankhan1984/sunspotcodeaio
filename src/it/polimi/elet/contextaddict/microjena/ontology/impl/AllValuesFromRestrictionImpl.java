/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.AllValuesFromRestriction;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  13:    */ 
/*  14:    */ public class AllValuesFromRestrictionImpl
/*  15:    */   extends RestrictionImpl
/*  16:    */   implements AllValuesFromRestriction
/*  17:    */ {
/*  18: 48 */   public static Implementation factory = new Implementation()
/*  19:    */   {
/*  20:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  21:    */     {
/*  22: 50 */       if (canWrap(n, eg)) {
/*  23: 51 */         return new AllValuesFromRestrictionImpl(n, eg);
/*  24:    */       }
/*  25: 53 */       throw new ConversionException("Cannot convert node " + n + " to AllValuesFromRestriction");
/*  26:    */     }
/*  27:    */     
/*  28:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  29:    */     {
/*  30: 60 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  31: 61 */       return (profile != null) && (profile.isSupported(node, eg, AllValuesFromRestriction.class));
/*  32:    */     }
/*  33:    */   };
/*  34:    */   
/*  35:    */   public AllValuesFromRestrictionImpl(Node n, EnhGraph g)
/*  36:    */   {
/*  37: 74 */     super(n, g);
/*  38:    */   }
/*  39:    */   
/*  40:    */   public void setAllValuesFrom(Resource cls)
/*  41:    */   {
/*  42: 87 */     setPropertyValue(getProfile().ALL_VALUES_FROM(), "ALL_VALUES_FROM", cls);
/*  43:    */   }
/*  44:    */   
/*  45:    */   public Resource getAllValuesFrom()
/*  46:    */   {
/*  47: 98 */     checkProfile(getProfile().ALL_VALUES_FROM(), "ALL_VALUES_FROM");
/*  48: 99 */     Resource r = (Resource)getRequiredProperty(getProfile().ALL_VALUES_FROM()).getObject();
/*  49:    */     
/*  50:101 */     boolean currentStrict = ((OntModel)getModel()).strictMode();
/*  51:102 */     ((OntModel)getModel()).setStrictMode(true);
/*  52:103 */     return r;
/*  53:    */   }
/*  54:    */   
/*  55:    */   public boolean hasAllValuesFrom(Resource cls)
/*  56:    */   {
/*  57:114 */     return hasPropertyValue(getProfile().ALL_VALUES_FROM(), "ALL_VALUES_FROM", cls);
/*  58:    */   }
/*  59:    */   
/*  60:    */   public void removeAllValuesFrom(Resource cls)
/*  61:    */   {
/*  62:124 */     removePropertyValue(getProfile().ALL_VALUES_FROM(), "ALL_VALUES_FROM", cls);
/*  63:    */   }
/*  64:    */ }

