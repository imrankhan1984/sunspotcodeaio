/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.AllDifferent;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.RDFListImpl;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  18:    */ 
/*  19:    */ public class AllDifferentImpl
/*  20:    */   extends OntResourceImpl
/*  21:    */   implements AllDifferent
/*  22:    */ {
/*  23: 52 */   public static Implementation factory = new Implementation()
/*  24:    */   {
/*  25:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  26:    */     {
/*  27: 54 */       if (canWrap(n, eg)) {
/*  28: 55 */         return new AllDifferentImpl(n, eg);
/*  29:    */       }
/*  30: 57 */       throw new ConversionException("Cannot convert node " + n + " to AllDifferent");
/*  31:    */     }
/*  32:    */     
/*  33:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  34:    */     {
/*  35: 63 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  36: 64 */       return (profile != null) && (profile.isSupported(node, eg, AllDifferent.class));
/*  37:    */     }
/*  38:    */   };
/*  39:    */   
/*  40:    */   public AllDifferentImpl(Node n, EnhGraph g)
/*  41:    */   {
/*  42: 77 */     super(n, g);
/*  43:    */   }
/*  44:    */   
/*  45:    */   public void setDistinctMembers(RDFList members)
/*  46:    */   {
/*  47: 88 */     setPropertyValue(getProfile().DISTINCT_MEMBERS(), "DISTINCT_MEMBERS", members);
/*  48:    */   }
/*  49:    */   
/*  50:    */   public void addDistinctMember(Resource res)
/*  51:    */   {
/*  52: 97 */     addListPropertyValue(getProfile().DISTINCT_MEMBERS(), "DISTINCT_MEMBERS", res);
/*  53:    */   }
/*  54:    */   
/*  55:    */   public void addDistinctMembers(Iterator individuals)
/*  56:    */   {
/*  57:106 */     while (individuals.hasNext()) {
/*  58:107 */       addDistinctMember((Resource)individuals.next());
/*  59:    */     }
/*  60:    */   }
/*  61:    */   
/*  62:    */   public RDFList getDistinctMembers()
/*  63:    */   {
/*  64:117 */     return new RDFListImpl(getProperty(getProfile().DISTINCT_MEMBERS()).getObject().asNode(), getModelCom());
/*  65:    */   }
/*  66:    */   
/*  67:    */   public ExtendedIterator listDistinctMembers()
/*  68:    */   {
/*  69:127 */     return getDistinctMembers().iterator();
/*  70:    */   }
/*  71:    */   
/*  72:    */   public boolean hasDistinctMember(Resource res)
/*  73:    */   {
/*  74:137 */     return getDistinctMembers().contains(res);
/*  75:    */   }
/*  76:    */   
/*  77:    */   public void removeDistinctMember(Resource res)
/*  78:    */   {
/*  79:146 */     setDistinctMembers(getDistinctMembers().remove(res));
/*  80:    */   }
/*  81:    */ }
