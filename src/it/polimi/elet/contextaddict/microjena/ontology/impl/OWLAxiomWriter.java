/*  1:   */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Axiom;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.vocabulary.OWL;
/*  9:   */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/* 10:   */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/* 11:   */ 
/* 12:   */ public class OWLAxiomWriter
/* 13:   */ {
/* 14:25 */   private static Node rdfType = RDF.type.asNode();
/* 15:26 */   private static Node rdfsResource = RDFS.Resource.asNode();
/* 16:27 */   private static Node rdfsClass = RDFS.Class.asNode();
/* 17:   */   
/* 18:   */   public static void writeAxioms(Graph graph)
/* 19:   */   {
/* 20:31 */     writeClassResourceAxioms(OWL.Class.asNode(), graph);
/* 21:32 */     writeClassResourceAxioms(OWL.Restriction.asNode(), graph);
/* 22:33 */     writeClassResourceAxioms(OWL.ObjectProperty.asNode(), graph);
/* 23:34 */     writeClassResourceAxioms(OWL.SymmetricProperty.asNode(), graph);
/* 24:35 */     writeClassResourceAxioms(OWL.TransitiveProperty.asNode(), graph);
/* 25:36 */     writeClassResourceAxioms(OWL.InverseFunctionalProperty.asNode(), graph);
/* 26:37 */     writeClassResourceAxioms(OWL.AnnotationProperty.asNode(), graph);
/* 27:38 */     writeClassResourceAxioms(OWL.FunctionalProperty.asNode(), graph);
/* 28:39 */     writeClassResourceAxioms(OWL.AllDifferent.asNode(), graph);
/* 29:40 */     writeClassResourceAxioms(OWL.DataRange.asNode(), graph);
/* 30:41 */     writeClassResourceAxioms(OWL.Ontology.asNode(), graph);
/* 31:42 */     writeClassResourceAxioms(OWL.DatatypeProperty.asNode(), graph);
/* 32:   */   }
/* 33:   */   
/* 34:   */   private static void writeClassResourceAxioms(Node n, Graph graph)
/* 35:   */   {
/* 36:46 */     graph.add(new Axiom(n, rdfType, rdfsResource));
/* 37:47 */     graph.add(new Axiom(n, rdfType, rdfsClass));
/* 38:   */   }
/* 39:   */ }
