/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.ontology.BooleanClassDescription;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.RDFListImpl;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  15:    */ 
/*  16:    */ public abstract class BooleanClassDescriptionImpl
/*  17:    */   extends OntClassImpl
/*  18:    */   implements BooleanClassDescription
/*  19:    */ {
/*  20:    */   public BooleanClassDescriptionImpl(Node n, EnhGraph g)
/*  21:    */   {
/*  22: 53 */     super(n, g);
/*  23:    */   }
/*  24:    */   
/*  25:    */   public void setOperands(RDFList operands)
/*  26:    */   {
/*  27: 66 */     setPropertyValue(operator(), getOperatorName(), operands);
/*  28:    */   }
/*  29:    */   
/*  30:    */   public void addOperand(Resource cls)
/*  31:    */   {
/*  32: 75 */     addListPropertyValue(operator(), getOperatorName(), cls);
/*  33:    */   }
/*  34:    */   
/*  35:    */   public void addOperands(Iterator classes)
/*  36:    */   {
/*  37: 84 */     while (classes.hasNext()) {
/*  38: 85 */       addOperand((Resource)classes.next());
/*  39:    */     }
/*  40:    */   }
/*  41:    */   
/*  42:    */   public RDFList getOperands()
/*  43:    */   {
/*  44: 96 */     Statement s = getProperty(getProfile().UNION_OF());
/*  45: 97 */     if (s == null)
/*  46:    */     {
/*  47: 98 */       s = getProperty(getProfile().INTERSECTION_OF());
/*  48: 99 */       if (s == null) {
/*  49:100 */         s = getProperty(getProfile().COMPLEMENT_OF());
/*  50:    */       }
/*  51:    */     }
/*  52:102 */     if (s != null) {
/*  53:103 */       return new RDFListImpl(s.getObject().asNode(), getModelCom());
/*  54:    */     }
/*  55:106 */     return null;
/*  56:    */   }
/*  57:    */   
/*  58:    */   public ExtendedIterator listOperands()
/*  59:    */   {
/*  60:117 */     return getOperands().iterator();
/*  61:    */   }
/*  62:    */   
/*  63:    */   public boolean hasOperand(Resource cls)
/*  64:    */   {
/*  65:127 */     return getOperands().contains(cls);
/*  66:    */   }
/*  67:    */   
/*  68:    */   public void removeOperand(Resource res)
/*  69:    */   {
/*  70:136 */     setOperands(getOperands().remove(res));
/*  71:    */   }
/*  72:    */   
/*  73:    */   public abstract Property operator();
/*  74:    */   
/*  75:    */   protected abstract String getOperatorName();
/*  76:    */ }

