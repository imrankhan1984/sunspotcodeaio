/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.impl.XSDBaseNumericType;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.MinCardinalityRestriction;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  13:    */ 
/*  14:    */ public class MinCardinalityRestrictionImpl
/*  15:    */   extends RestrictionImpl
/*  16:    */   implements MinCardinalityRestriction
/*  17:    */ {
/*  18: 48 */   public static Implementation factory = new Implementation()
/*  19:    */   {
/*  20:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  21:    */     {
/*  22: 50 */       if (canWrap(n, eg)) {
/*  23: 51 */         return new MinCardinalityRestrictionImpl(n, eg);
/*  24:    */       }
/*  25: 53 */       throw new ConversionException("Cannot convert node " + n + " to MinCardinalityRestriction");
/*  26:    */     }
/*  27:    */     
/*  28:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  29:    */     {
/*  30: 60 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  31: 61 */       return (profile != null) && (profile.isSupported(node, eg, MinCardinalityRestriction.class));
/*  32:    */     }
/*  33:    */   };
/*  34:    */   
/*  35:    */   public MinCardinalityRestrictionImpl(Node n, EnhGraph g)
/*  36:    */   {
/*  37: 74 */     super(n, g);
/*  38:    */   }
/*  39:    */   
/*  40:    */   public void setMinCardinality(int cardinality)
/*  41:    */   {
/*  42: 87 */     setPropertyValue(getProfile().MIN_CARDINALITY(), "MIN_CARDINALITY", getModel().createTypedLiteral(new Integer(cardinality), XSDBaseNumericType.XSDint));
/*  43:    */   }
/*  44:    */   
/*  45:    */   public int getMinCardinality()
/*  46:    */   {
/*  47: 96 */     return objectAsInt(getProfile().MIN_CARDINALITY(), "MIN_CARDINALITY");
/*  48:    */   }
/*  49:    */   
/*  50:    */   public boolean hasMinCardinality(int cardinality)
/*  51:    */   {
/*  52:106 */     return hasPropertyValue(getProfile().MIN_CARDINALITY(), "MIN_CARDINALITY", getModel().createTypedLiteral(new Integer(cardinality), XSDBaseNumericType.XSDint));
/*  53:    */   }
/*  54:    */   
/*  55:    */   public void removeMinCardinality(int cardinality)
/*  56:    */   {
/*  57:116 */     removePropertyValue(getProfile().MIN_CARDINALITY(), "MIN_CARDINALITY", getModel().createTypedLiteral(new Integer(cardinality), XSDBaseNumericType.XSDint));
/*  58:    */   }
/*  59:    */ }
