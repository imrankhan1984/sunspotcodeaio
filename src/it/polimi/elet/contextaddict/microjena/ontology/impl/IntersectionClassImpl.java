/*  1:   */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.ontology.IntersectionClass;
/*  9:   */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/* 10:   */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/* 11:   */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/* 12:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/* 13:   */ 
/* 14:   */ public class IntersectionClassImpl
/* 15:   */   extends BooleanClassDescriptionImpl
/* 16:   */   implements IntersectionClass
/* 17:   */ {
/* 18:49 */   public static Implementation factory = new Implementation()
/* 19:   */   {
/* 20:   */     public EnhNode wrap(Node n, EnhGraph eg)
/* 21:   */     {
/* 22:51 */       if (canWrap(n, eg)) {
/* 23:52 */         return new IntersectionClassImpl(n, eg);
/* 24:   */       }
/* 25:54 */       throw new ConversionException("Cannot convert node " + n + " to IntersectionClass");
/* 26:   */     }
/* 27:   */     
/* 28:   */     public boolean canWrap(Node node, EnhGraph eg)
/* 29:   */     {
/* 30:60 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/* 31:61 */       return (profile != null) && (profile.isSupported(node, eg, OntClass.class)) && (AbstractProfile.containsSome(eg, node, profile.INTERSECTION_OF()));
/* 32:   */     }
/* 33:   */   };
/* 34:   */   
/* 35:   */   public IntersectionClassImpl(Node n, EnhGraph g)
/* 36:   */   {
/* 37:76 */     super(n, g);
/* 38:   */   }
/* 39:   */   
/* 40:   */   public Property operator()
/* 41:   */   {
/* 42:80 */     return getProfile().INTERSECTION_OF();
/* 43:   */   }
/* 44:   */   
/* 45:   */   public String getOperatorName()
/* 46:   */   {
/* 47:84 */     return "INTERSECTION_OF";
/* 48:   */   }
/* 49:   */ }

