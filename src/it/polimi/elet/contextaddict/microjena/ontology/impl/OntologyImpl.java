/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntResource;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.Ontology;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  20:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  21:    */ 
/*  22:    */ public class OntologyImpl
/*  23:    */   extends OntResourceImpl
/*  24:    */   implements Ontology
/*  25:    */ {
/*  26: 54 */   public static Implementation factory = new Implementation()
/*  27:    */   {
/*  28:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  29:    */     {
/*  30: 56 */       if (canWrap(n, eg)) {
/*  31: 57 */         return new OntologyImpl(n, eg);
/*  32:    */       }
/*  33: 59 */       throw new ConversionException("Cannot convert node " + n + " to Ontology");
/*  34:    */     }
/*  35:    */     
/*  36:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  37:    */     {
/*  38: 64 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  39: 65 */       return (profile != null) && (profile.isSupported(node, eg, Ontology.class));
/*  40:    */     }
/*  41:    */   };
/*  42:    */   
/*  43:    */   public OntologyImpl(Node n, EnhGraph g)
/*  44:    */   {
/*  45: 78 */     super(n, g);
/*  46:    */   }
/*  47:    */   
/*  48:    */   public void setImport(Resource res)
/*  49:    */   {
/*  50: 90 */     setPropertyValue(getProfile().IMPORTS(), "IMPORTS", res);
/*  51:    */   }
/*  52:    */   
/*  53:    */   public void addImport(Resource res)
/*  54:    */   {
/*  55:100 */     addPropertyValue(getProfile().IMPORTS(), "IMPORTS", res);
/*  56:    */   }
/*  57:    */   
/*  58:    */   public OntResource getImport()
/*  59:    */   {
/*  60:110 */     return objectAsResource(getProfile().IMPORTS(), "IMPORTS");
/*  61:    */   }
/*  62:    */   
/*  63:    */   public ExtendedIterator listImports()
/*  64:    */   {
/*  65:120 */     return listAs(getProfile().IMPORTS());
/*  66:    */   }
/*  67:    */   
/*  68:    */   public boolean imports(Resource res)
/*  69:    */   {
/*  70:130 */     return hasPropertyValue(getProfile().IMPORTS(), "IMPORTS", res);
/*  71:    */   }
/*  72:    */   
/*  73:    */   public void removeImport(Resource res)
/*  74:    */   {
/*  75:139 */     removePropertyValue(getProfile().IMPORTS(), "IMPORTS", res);
/*  76:    */   }
/*  77:    */   
/*  78:    */   public void setBackwardCompatibleWith(Resource res)
/*  79:    */   {
/*  80:152 */     setPropertyValue(getProfile().BACKWARD_COMPATIBLE_WITH(), "BACKWARD_COMPATIBLE_WITH", res);
/*  81:    */   }
/*  82:    */   
/*  83:    */   public void addBackwardCompatibleWith(Resource res)
/*  84:    */   {
/*  85:162 */     addPropertyValue(getProfile().BACKWARD_COMPATIBLE_WITH(), "BACKWARD_COMPATIBLE_WITH", res);
/*  86:    */   }
/*  87:    */   
/*  88:    */   public OntResource getBackwardCompatibleWith()
/*  89:    */   {
/*  90:172 */     return objectAsResource(getProfile().BACKWARD_COMPATIBLE_WITH(), "BACKWARD_COMPATIBLE_WITH");
/*  91:    */   }
/*  92:    */   
/*  93:    */   public ExtendedIterator listBackwardCompatibleWith()
/*  94:    */   {
/*  95:183 */     return listAs(getProfile().BACKWARD_COMPATIBLE_WITH());
/*  96:    */   }
/*  97:    */   
/*  98:    */   public boolean isBackwardCompatibleWith(Resource res)
/*  99:    */   {
/* 100:193 */     return hasPropertyValue(getProfile().BACKWARD_COMPATIBLE_WITH(), "BACKWARD_COMPATIBLE_WITH", res);
/* 101:    */   }
/* 102:    */   
/* 103:    */   public void removeBackwardCompatibleWith(Resource res)
/* 104:    */   {
/* 105:203 */     removePropertyValue(getProfile().BACKWARD_COMPATIBLE_WITH(), "BACKWARD_COMPATIBLE_WITH", res);
/* 106:    */   }
/* 107:    */   
/* 108:    */   public void setPriorVersion(Resource res)
/* 109:    */   {
/* 110:216 */     setPropertyValue(getProfile().PRIOR_VERSION(), "PRIOR_VERSION", res);
/* 111:    */   }
/* 112:    */   
/* 113:    */   public void addPriorVersion(Resource res)
/* 114:    */   {
/* 115:226 */     addPropertyValue(getProfile().PRIOR_VERSION(), "PRIOR_VERSION", res);
/* 116:    */   }
/* 117:    */   
/* 118:    */   public OntResource getPriorVersion()
/* 119:    */   {
/* 120:236 */     return objectAsResource(getProfile().PRIOR_VERSION(), "PRIOR_VERSION");
/* 121:    */   }
/* 122:    */   
/* 123:    */   public ExtendedIterator listPriorVersion()
/* 124:    */   {
/* 125:247 */     return listAs(getProfile().PRIOR_VERSION());
/* 126:    */   }
/* 127:    */   
/* 128:    */   public boolean hasPriorVersion(Resource res)
/* 129:    */   {
/* 130:257 */     return hasPropertyValue(getProfile().PRIOR_VERSION(), "PRIOR_VERSION", res);
/* 131:    */   }
/* 132:    */   
/* 133:    */   public void removePriorVersion(Resource res)
/* 134:    */   {
/* 135:266 */     removePropertyValue(getProfile().PRIOR_VERSION(), "PRIOR_VERSION", res);
/* 136:    */   }
/* 137:    */   
/* 138:    */   public void setIncompatibleWith(Resource res)
/* 139:    */   {
/* 140:279 */     setPropertyValue(getProfile().INCOMPATIBLE_WITH(), "INCOMPATIBLE_WITH", res);
/* 141:    */   }
/* 142:    */   
/* 143:    */   public void addIncompatibleWith(Resource res)
/* 144:    */   {
/* 145:289 */     addPropertyValue(getProfile().INCOMPATIBLE_WITH(), "INCOMPATIBLE_WITH", res);
/* 146:    */   }
/* 147:    */   
/* 148:    */   public OntResource getIncompatibleWith()
/* 149:    */   {
/* 150:299 */     return objectAsResource(getProfile().INCOMPATIBLE_WITH(), "INCOMPATIBLE_WITH");
/* 151:    */   }
/* 152:    */   
/* 153:    */   public ExtendedIterator listIncompatibleWith()
/* 154:    */   {
/* 155:310 */     return listAs(getProfile().INCOMPATIBLE_WITH());
/* 156:    */   }
/* 157:    */   
/* 158:    */   public boolean isIncompatibleWith(Resource res)
/* 159:    */   {
/* 160:320 */     return hasPropertyValue(getProfile().INCOMPATIBLE_WITH(), "INCOMPATIBLE_WITH", res);
/* 161:    */   }
/* 162:    */   
/* 163:    */   public void removeIncompatibleWith(Resource res)
/* 164:    */   {
/* 165:329 */     removePropertyValue(getProfile().INCOMPATIBLE_WITH(), "INCOMPATIBLE_WITH", res);
/* 166:    */   }
/* 167:    */   
/* 168:    */   private ExtendedIterator listAs(Property p)
/* 169:    */   {
/* 170:333 */     StmtIterator it = getModel().listStatements(this, p, (RDFNode)null);
/* 171:334 */     Set result = new Set();
/* 172:335 */     while (it.hasNext()) {
/* 173:336 */       result.add(new OntResourceImpl(it.nextStatement().getObject().asNode(), getModelCom()));
/* 174:    */     }
/* 175:337 */     return WrappedIterator.create(result.iterator());
/* 176:    */   }
/* 177:    */ }
