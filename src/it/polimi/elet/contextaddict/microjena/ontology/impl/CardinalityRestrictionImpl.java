/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.impl.XSDBaseNumericType;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.CardinalityRestriction;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  13:    */ 
/*  14:    */ public class CardinalityRestrictionImpl
/*  15:    */   extends RestrictionImpl
/*  16:    */   implements CardinalityRestriction
/*  17:    */ {
/*  18: 50 */   public static Implementation factory = new Implementation()
/*  19:    */   {
/*  20:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  21:    */     {
/*  22: 52 */       if (canWrap(n, eg)) {
/*  23: 53 */         return new CardinalityRestrictionImpl(n, eg);
/*  24:    */       }
/*  25: 55 */       throw new ConversionException("Cannot convert node " + n + " to CardinalityRestriction");
/*  26:    */     }
/*  27:    */     
/*  28:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  29:    */     {
/*  30: 62 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  31: 63 */       return (profile != null) && (profile.isSupported(node, eg, CardinalityRestriction.class));
/*  32:    */     }
/*  33:    */   };
/*  34:    */   
/*  35:    */   public CardinalityRestrictionImpl(Node n, EnhGraph g)
/*  36:    */   {
/*  37: 76 */     super(n, g);
/*  38:    */   }
/*  39:    */   
/*  40:    */   public void setCardinality(int cardinality)
/*  41:    */   {
/*  42: 89 */     setPropertyValue(getProfile().CARDINALITY(), "CARDINALITY", getModel().createTypedLiteral(new Integer(cardinality), XSDBaseNumericType.XSDint));
/*  43:    */   }
/*  44:    */   
/*  45:    */   public int getCardinality()
/*  46:    */   {
/*  47: 98 */     return objectAsInt(getProfile().CARDINALITY(), "CARDINALITY");
/*  48:    */   }
/*  49:    */   
/*  50:    */   public boolean hasCardinality(int cardinality)
/*  51:    */   {
/*  52:108 */     return hasPropertyValue(getProfile().CARDINALITY(), "CARDINALITY", getModel().createTypedLiteral(new Integer(cardinality), XSDBaseNumericType.XSDint));
/*  53:    */   }
/*  54:    */   
/*  55:    */   public void removeCardinality(int cardinality)
/*  56:    */   {
/*  57:118 */     removePropertyValue(getProfile().CARDINALITY(), "CARDINALITY", getModel().createTypedLiteral(new Integer(cardinality), XSDBaseNumericType.XSDint));
/*  58:    */   }
/*  59:    */ }
