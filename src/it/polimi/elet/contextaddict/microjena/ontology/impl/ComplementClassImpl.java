/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.ComplementClass;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaUnsupportedOperationException;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  20:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*  21:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  22:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  23:    */ 
/*  24:    */ public class ComplementClassImpl
/*  25:    */   extends OntClassImpl
/*  26:    */   implements ComplementClass
/*  27:    */ {
/*  28: 56 */   public static Implementation factory = new Implementation()
/*  29:    */   {
/*  30:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  31:    */     {
/*  32: 58 */       if (canWrap(n, eg)) {
/*  33: 59 */         return new ComplementClassImpl(n, eg);
/*  34:    */       }
/*  35: 61 */       throw new ConversionException("Cannot convert node " + n + " to ComplementClass");
/*  36:    */     }
/*  37:    */     
/*  38:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  39:    */     {
/*  40: 67 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  41: 68 */       Property comp = profile == null ? null : profile.COMPLEMENT_OF();
/*  42:    */       
/*  43: 70 */       return (profile != null) && (profile.isSupported(node, eg, OntClass.class)) && (comp != null) && (eg.asGraph().contains(node, comp.asNode(), Node.ANY));
/*  44:    */     }
/*  45:    */   };
/*  46:    */   
/*  47:    */   public ComplementClassImpl(Node n, EnhGraph g)
/*  48:    */   {
/*  49: 86 */     super(n, g);
/*  50:    */   }
/*  51:    */   
/*  52:    */   public void setOperands(RDFList operands)
/*  53:    */   {
/*  54:100 */     throw new JenaUnsupportedOperationException("ComplementClass takes a single operand, not a list.");
/*  55:    */   }
/*  56:    */   
/*  57:    */   public void setOperand(Resource cls)
/*  58:    */   {
/*  59:111 */     setPropertyValue(getProfile().COMPLEMENT_OF(), "COMPLEMENT_OF", cls);
/*  60:    */   }
/*  61:    */   
/*  62:    */   public void addOperand(Resource cls)
/*  63:    */   {
/*  64:121 */     throw new JenaUnsupportedOperationException("ComplementClass is only defined for  a single operand.");
/*  65:    */   }
/*  66:    */   
/*  67:    */   public void addOperands(Iterator classes)
/*  68:    */   {
/*  69:131 */     throw new JenaUnsupportedOperationException("ComplementClass is only defined for  a single operand.");
/*  70:    */   }
/*  71:    */   
/*  72:    */   public RDFList getOperands()
/*  73:    */   {
/*  74:140 */     throw new JenaUnsupportedOperationException("ComplementClass takes a single operand, not a list.");
/*  75:    */   }
/*  76:    */   
/*  77:    */   public ExtendedIterator listOperands()
/*  78:    */   {
/*  79:150 */     OntClass c = getOperand();
/*  80:151 */     Set result = new Set();
/*  81:152 */     result.add(c);
/*  82:153 */     return WrappedIterator.create(result.iterator());
/*  83:    */   }
/*  84:    */   
/*  85:    */   public boolean hasOperand(Resource cls)
/*  86:    */   {
/*  87:163 */     return hasPropertyValue(getProfile().COMPLEMENT_OF(), "COMPLEMENT_OF", cls);
/*  88:    */   }
/*  89:    */   
/*  90:    */   public OntClass getOperand()
/*  91:    */   {
/*  92:172 */     return (OntClass)OntClassImpl.factory.wrap(getProperty(getProfile().COMPLEMENT_OF()).getObject().asNode(), getModelCom());
/*  93:    */   }
/*  94:    */   
/*  95:    */   public void removeOperand(Resource res)
/*  96:    */   {
/*  97:180 */     removePropertyValue(getProfile().COMPLEMENT_OF(), "COMPLEMENT_OF", res);
/*  98:    */   }
/*  99:    */   
/* 100:    */   public Property operator()
/* 101:    */   {
/* 102:190 */     return getProfile().COMPLEMENT_OF();
/* 103:    */   }
/* 104:    */ }
