/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.AnnotationProperty;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  13:    */ 
/*  14:    */ public class AnnotationPropertyImpl
/*  15:    */   extends OntResourceImpl
/*  16:    */   implements AnnotationProperty
/*  17:    */ {
/*  18: 49 */   public static Implementation factory = new Implementation()
/*  19:    */   {
/*  20:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  21:    */     {
/*  22: 51 */       if (canWrap(n, eg)) {
/*  23: 52 */         return new AnnotationPropertyImpl(n, eg);
/*  24:    */       }
/*  25: 54 */       throw new ConversionException("Cannot convert node " + n + " to AnnotationProperty");
/*  26:    */     }
/*  27:    */     
/*  28:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  29:    */     {
/*  30: 60 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  31: 61 */       return (profile != null) && (profile.isSupported(node, eg, AnnotationProperty.class));
/*  32:    */     }
/*  33:    */   };
/*  34:    */   
/*  35:    */   public AnnotationPropertyImpl(Node n, EnhGraph g)
/*  36:    */   {
/*  37: 74 */     super(n, g);
/*  38:    */   }
/*  39:    */   
/*  40:    */   public boolean isProperty()
/*  41:    */   {
/*  42: 85 */     return true;
/*  43:    */   }
/*  44:    */   
/*  45:    */   public int getOrdinal()
/*  46:    */   {
/*  47: 93 */     String localName = getLocalName();
/*  48:    */     
/*  49:    */ 
/*  50:    */ 
/*  51: 97 */     boolean aus = true;
/*  52: 98 */     if (localName.charAt(0) != '_')
/*  53:    */     {
/*  54: 99 */       aus = false;
/*  55:    */     }
/*  56:    */     else
/*  57:    */     {
/*  58:101 */       int i = localName.length();
/*  59:103 */       while (aus)
/*  60:    */       {
/*  61:103 */         i--;
/*  62:103 */         if (i <= 0) {
/*  63:    */           break;
/*  64:    */         }
/*  65:104 */         char ausCh = localName.charAt(i);
/*  66:105 */         if ((ausCh < '0') || (ausCh > '9')) {
/*  67:106 */           aus = false;
/*  68:    */         }
/*  69:    */       }
/*  70:    */     }
/*  71:110 */     if ((getNameSpace().equals(RDF.getURI())) && (aus)) {
/*  72:111 */       return parseInt(localName.substring(1));
/*  73:    */     }
/*  74:112 */     return 0;
/*  75:    */   }
/*  76:    */   
/*  77:    */   private int parseInt(String digits)
/*  78:    */   {
/*  79:    */     try
/*  80:    */     {
/*  81:117 */       return Integer.parseInt(digits);
/*  82:    */     }
/*  83:    */     catch (NumberFormatException e)
/*  84:    */     {
/*  85:119 */       throw new JenaException("checkOrdinal fails on " + digits, e);
/*  86:    */     }
/*  87:    */   }
/*  88:    */ }

