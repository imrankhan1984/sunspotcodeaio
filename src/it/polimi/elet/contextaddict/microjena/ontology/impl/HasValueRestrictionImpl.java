/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.HasValueRestriction;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  12:    */ 
/*  13:    */ public class HasValueRestrictionImpl
/*  14:    */   extends RestrictionImpl
/*  15:    */   implements HasValueRestriction
/*  16:    */ {
/*  17: 48 */   public static Implementation factory = new Implementation()
/*  18:    */   {
/*  19:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  20:    */     {
/*  21: 50 */       if (canWrap(n, eg)) {
/*  22: 51 */         return new HasValueRestrictionImpl(n, eg);
/*  23:    */       }
/*  24: 53 */       throw new ConversionException("Cannot convert node " + n + " to HasValueRestriction");
/*  25:    */     }
/*  26:    */     
/*  27:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  28:    */     {
/*  29: 60 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  30: 61 */       return (profile != null) && (profile.isSupported(node, eg, HasValueRestriction.class));
/*  31:    */     }
/*  32:    */   };
/*  33:    */   
/*  34:    */   public HasValueRestrictionImpl(Node n, EnhGraph g)
/*  35:    */   {
/*  36: 74 */     super(n, g);
/*  37:    */   }
/*  38:    */   
/*  39:    */   public void setHasValue(RDFNode value)
/*  40:    */   {
/*  41: 89 */     setPropertyValue(getProfile().HAS_VALUE(), "HAS_VALUE", value);
/*  42:    */   }
/*  43:    */   
/*  44:    */   public RDFNode getHasValue()
/*  45:    */   {
/*  46: 98 */     checkProfile(getProfile().HAS_VALUE(), "HAS_VALUE");
/*  47: 99 */     RDFNode n = getPropertyValue(getProfile().HAS_VALUE());
/*  48:100 */     return n;
/*  49:    */   }
/*  50:    */   
/*  51:    */   public boolean hasValue(RDFNode value)
/*  52:    */   {
/*  53:111 */     return hasPropertyValue(getProfile().HAS_VALUE(), "HAS_VALUE", value);
/*  54:    */   }
/*  55:    */   
/*  56:    */   public void removeHasValue(RDFNode value)
/*  57:    */   {
/*  58:121 */     removePropertyValue(getProfile().HAS_VALUE(), "HAS_VALUE", value);
/*  59:    */   }
/*  60:    */ }
