/*  1:   */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  9:   */ import it.polimi.elet.contextaddict.microjena.util.Set;
/* 10:   */ 
/* 11:   */ public abstract class AbstractProfile
/* 12:   */   implements Profile
/* 13:   */ {
/* 14:   */   public static boolean containsSome(EnhGraph g, Node n, Property p)
/* 15:   */   {
/* 16:43 */     return g.asGraph().contains(n, p.asNode(), Node.ANY);
/* 17:   */   }
/* 18:   */   
/* 19:   */   protected Iterator arrayToIterator(Object[] array)
/* 20:   */   {
/* 21:47 */     Set result = new Set();
/* 22:48 */     for (int i = 0; i < array.length; i++) {
/* 23:49 */       result.add(array[i]);
/* 24:   */     }
/* 25:51 */     return result.iterator();
/* 26:   */   }
/* 27:   */ }
