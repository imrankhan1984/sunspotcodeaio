/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.SomeValuesFromRestriction;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  13:    */ 
/*  14:    */ public class SomeValuesFromRestrictionImpl
/*  15:    */   extends RestrictionImpl
/*  16:    */   implements SomeValuesFromRestriction
/*  17:    */ {
/*  18: 48 */   public static Implementation factory = new Implementation()
/*  19:    */   {
/*  20:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  21:    */     {
/*  22: 50 */       if (canWrap(n, eg)) {
/*  23: 51 */         return new SomeValuesFromRestrictionImpl(n, eg);
/*  24:    */       }
/*  25: 53 */       throw new ConversionException("Cannot convert node " + n + " to SomeValuesFromRestriction");
/*  26:    */     }
/*  27:    */     
/*  28:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  29:    */     {
/*  30: 60 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  31: 61 */       return (profile != null) && (profile.isSupported(node, eg, SomeValuesFromRestriction.class));
/*  32:    */     }
/*  33:    */   };
/*  34:    */   
/*  35:    */   public SomeValuesFromRestrictionImpl(Node n, EnhGraph g)
/*  36:    */   {
/*  37: 74 */     super(n, g);
/*  38:    */   }
/*  39:    */   
/*  40:    */   public void setSomeValuesFrom(Resource cls)
/*  41:    */   {
/*  42: 91 */     setPropertyValue(getProfile().SOME_VALUES_FROM(), "SOME_VALUES_FROM", cls);
/*  43:    */   }
/*  44:    */   
/*  45:    */   public Resource getSomeValuesFrom()
/*  46:    */   {
/*  47:102 */     Resource r = (Resource)getRequiredProperty(getProfile().SOME_VALUES_FROM()).getObject();
/*  48:103 */     boolean currentStrict = ((OntModel)getModel()).strictMode();
/*  49:104 */     ((OntModel)getModel()).setStrictMode(true);
/*  50:105 */     return r;
/*  51:    */   }
/*  52:    */   
/*  53:    */   public boolean hasSomeValuesFrom(Resource cls)
/*  54:    */   {
/*  55:116 */     return hasPropertyValue(getProfile().SOME_VALUES_FROM(), "SOME_VALUES_FROM", cls);
/*  56:    */   }
/*  57:    */   
/*  58:    */   public void removeSomeValuesFrom(Resource cls)
/*  59:    */   {
/*  60:126 */     removePropertyValue(getProfile().SOME_VALUES_FROM(), "SOME_VALUES_FROM", cls);
/*  61:    */   }
/*  62:    */ }

