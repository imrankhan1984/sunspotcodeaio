/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.Individual;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.ResourceImpl;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  20:    */ import it.polimi.elet.contextaddict.microjena.util.List;
/*  21:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*  22:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  23:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  24:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  25:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*  26:    */ 
/*  27:    */ public class OntClassImpl
/*  28:    */   extends OntResourceImpl
/*  29:    */   implements OntClass
/*  30:    */ {
/*  31: 58 */   private static final String[] IGNORE_NAMESPACES = { "http://www.w3.org/2002/07/owl#", RDF.getURI(), RDFS.getURI() };
/*  32: 69 */   public static Implementation factory = new Implementation()
/*  33:    */   {
/*  34:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  35:    */     {
/*  36: 71 */       if (canWrap(n, eg)) {
/*  37: 72 */         return new OntClassImpl(n, eg);
/*  38:    */       }
/*  39: 74 */       throw new ConversionException("Cannot convert node " + n.toString() + " to OntClass: it does not have rdf:type owl:Class or equivalent");
/*  40:    */     }
/*  41:    */     
/*  42:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  43:    */     {
/*  44: 80 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  45: 81 */       return (profile != null) && (profile.isSupported(node, eg, OntClass.class));
/*  46:    */     }
/*  47:    */   };
/*  48:    */   
/*  49:    */   public OntClassImpl(Node n, EnhGraph g)
/*  50:    */   {
/*  51: 94 */     super(n, g);
/*  52:    */   }
/*  53:    */   
/*  54:    */   public void setSuperClass(Resource cls)
/*  55:    */   {
/*  56:106 */     setPropertyValue(getProfile().SUB_CLASS_OF(), "SUB_CLASS_OF", cls);
/*  57:    */   }
/*  58:    */   
/*  59:    */   public void addSuperClass(Resource cls)
/*  60:    */   {
/*  61:115 */     addPropertyValue(getProfile().SUB_CLASS_OF(), "SUB_CLASS_OF", cls);
/*  62:    */   }
/*  63:    */   
/*  64:    */   public OntClass getSuperClass()
/*  65:    */   {
/*  66:125 */     RDFNode rdfNode = getModel().getProperty(this, getProfile().SUB_CLASS_OF()).getObject();
/*  67:126 */     if (rdfNode != null)
/*  68:    */     {
/*  69:127 */       OntClass result = new OntClassImpl(rdfNode.asNode(), getModelCom());
/*  70:128 */       return result;
/*  71:    */     }
/*  72:130 */     return null;
/*  73:    */   }
/*  74:    */   
/*  75:    */   public ExtendedIterator listSuperClasses()
/*  76:    */   {
/*  77:141 */     return listSuperClasses(false);
/*  78:    */   }
/*  79:    */   
/*  80:    */   public ExtendedIterator listSuperClasses(boolean direct)
/*  81:    */   {
/*  82:158 */     if (direct)
/*  83:    */     {
/*  84:159 */       StmtIterator stmts = getModel().listStatements(this, getProfile().SUB_CLASS_OF(), (RDFNode)null);
/*  85:160 */       Set result = new Set();
/*  86:161 */       while (stmts.hasNext()) {
/*  87:162 */         result.add(stmts.nextStatement().getObject());
/*  88:    */       }
/*  89:163 */       return WrappedIterator.create(result.iterator());
/*  90:    */     }
/*  91:165 */     Set lower = new Set();
/*  92:166 */     Set computed = new Set();
/*  93:167 */     catchSuperClasses(this, lower, computed, getProfile().SUB_CLASS_OF());
/*  94:168 */     return WrappedIterator.create(computed.iterator());
/*  95:    */   }
/*  96:    */   
/*  97:    */   public boolean hasSuperClass(Resource cls)
/*  98:    */   {
/*  99:179 */     return hasSuperClass(cls, false);
/* 100:    */   }
/* 101:    */   
/* 102:    */   public boolean hasSuperClass()
/* 103:    */   {
/* 104:189 */     return getSuperClass() != null;
/* 105:    */   }
/* 106:    */   
/* 107:    */   public boolean hasSuperClass(Resource cls, boolean direct)
/* 108:    */   {
/* 109:204 */     if (!direct) {
/* 110:206 */       return hasPropertyValue(getProfile().SUB_CLASS_OF(), "SUB_CLASS_OF", cls);
/* 111:    */     }
/* 112:208 */     ExtendedIterator subClasses = listSuperClasses(false);
/* 113:209 */     if (subClasses != null)
/* 114:    */     {
/* 115:210 */       boolean result = false;
/* 116:211 */       while ((!result) && (subClasses.hasNext())) {
/* 117:212 */         result = subClasses.next().equals(cls);
/* 118:    */       }
/* 119:213 */       return result;
/* 120:    */     }
/* 121:215 */     return false;
/* 122:    */   }
/* 123:    */   
/* 124:    */   public void removeSuperClass(Resource cls)
/* 125:    */   {
/* 126:226 */     removePropertyValue(getProfile().SUB_CLASS_OF(), "SUB_CLASS_OF", cls);
/* 127:    */   }
/* 128:    */   
/* 129:    */   public void setSubClass(Resource cls)
/* 130:    */   {
/* 131:237 */     checkProfile(getProfile().SUB_CLASS_OF(), "SUB_CLASS_OF");
/* 132:238 */     getModel().remove(getModel().listStatements(null, getProfile().SUB_CLASS_OF(), this));
/* 133:239 */     OntClass aus = new OntClassImpl(cls.asNode(), getModelCom());
/* 134:240 */     aus.addSuperClass(this);
/* 135:    */   }
/* 136:    */   
/* 137:    */   public void addSubClass(Resource cls)
/* 138:    */   {
/* 139:249 */     OntClass aus = (OntClass)factory.wrap(cls.asNode(), getModelCom());
/* 140:250 */     aus.addSuperClass(this);
/* 141:    */   }
/* 142:    */   
/* 143:    */   public OntClass getSubClass()
/* 144:    */   {
/* 145:262 */     StmtIterator i = getModel().listStatements(null, getProfile().SUB_CLASS_OF(), this);
/* 146:    */     try
/* 147:    */     {
/* 148:    */       OntClassImpl localOntClassImpl;
/* 149:264 */       if (i.hasNext()) {
/* 150:265 */         return new OntClassImpl(i.nextStatement().getSubject().asNode(), getModelCom());
/* 151:    */       }
/* 152:268 */       return null;
/* 153:    */     }
/* 154:    */     finally
/* 155:    */     {
/* 156:271 */       i.close();
/* 157:    */     }
/* 158:    */   }
/* 159:    */   
/* 160:    */   public ExtendedIterator listSubClasses()
/* 161:    */   {
/* 162:282 */     return listSubClasses(false);
/* 163:    */   }
/* 164:    */   
/* 165:    */   public ExtendedIterator listSubClasses(boolean direct)
/* 166:    */   {
/* 167:330 */     if (direct)
/* 168:    */     {
/* 169:331 */       StmtIterator stmts = getModel().listStatements(null, getProfile().SUB_CLASS_OF(), this);
/* 170:332 */       Set result = new Set();
/* 171:333 */       while (stmts.hasNext()) {
/* 172:334 */         result.add(stmts.nextStatement().getSubject());
/* 173:    */       }
/* 174:335 */       return WrappedIterator.create(result.iterator());
/* 175:    */     }
/* 176:338 */     Set lower = new Set();
/* 177:339 */     Set computed = new Set();
/* 178:340 */     catchSubClasses(this, lower, computed, getProfile().SUB_CLASS_OF());
/* 179:341 */     ExtendedIterator it = WrappedIterator.create(computed.iterator());
/* 180:342 */     Set result = new Set();
/* 181:343 */     while (it.hasNext()) {
/* 182:344 */       result.add(new OntClassImpl(((RDFNode)it.next()).asNode(), getModelCom()));
/* 183:    */     }
/* 184:345 */     return WrappedIterator.create(result.iterator());
/* 185:    */   }
/* 186:    */   
/* 187:    */   protected void catchSubClasses(Resource actualClass, Set lower, Set computed, Property relation)
/* 188:    */   {
/* 189:357 */     StmtIterator subClasses = getModel().listStatements(null, relation, actualClass);
/* 190:359 */     while (subClasses.hasNext())
/* 191:    */     {
/* 192:360 */       Resource aus = new ResourceImpl(subClasses.nextStatement().getSubject().asNode(), getModelCom());
/* 193:361 */       if (!computed.contains(aus))
/* 194:    */       {
/* 195:362 */         computed.add(aus);
/* 196:363 */         lower.remove(aus);
/* 197:364 */         catchSubClasses(aus, lower, computed, relation);
/* 198:    */       }
/* 199:    */       else
/* 200:    */       {
/* 201:367 */         lower.remove(aus);
/* 202:    */       }
/* 203:    */     }
/* 204:    */   }
/* 205:    */   
/* 206:    */   public boolean hasSubClass(Resource cls)
/* 207:    */   {
/* 208:379 */     return hasSubClass(cls, false);
/* 209:    */   }
/* 210:    */   
/* 211:    */   public boolean hasSubClass()
/* 212:    */   {
/* 213:389 */     return getSubClass() != null;
/* 214:    */   }
/* 215:    */   
/* 216:    */   public boolean hasSubClass(Resource cls, boolean direct)
/* 217:    */   {
/* 218:404 */     if (((getModel() instanceof OntModel)) && ((cls.getModel() == null) || (!(cls.getModel() instanceof OntModel)))) {
/* 219:407 */       cls = (Resource)cls.inModel(getModel());
/* 220:    */     }
/* 221:409 */     return new OntClassImpl(cls.asNode(), getModelCom()).hasSuperClass(this, direct);
/* 222:    */   }
/* 223:    */   
/* 224:    */   public void removeSubClass(Resource cls)
/* 225:    */   {
/* 226:419 */     new OntClassImpl(cls.asNode(), getModelCom()).removeSuperClass(this);
/* 227:    */   }
/* 228:    */   
/* 229:    */   public void setEquivalentClass(Resource cls)
/* 230:    */   {
/* 231:432 */     setPropertyValue(getProfile().EQUIVALENT_CLASS(), "EQUIVALENT_CLASS", cls);
/* 232:    */   }
/* 233:    */   
/* 234:    */   public void addEquivalentClass(Resource cls)
/* 235:    */   {
/* 236:441 */     addPropertyValue(getProfile().EQUIVALENT_CLASS(), "EQUIVALENT_CLASS", cls);
/* 237:    */   }
/* 238:    */   
/* 239:    */   public OntClass getEquivalentClass()
/* 240:    */   {
/* 241:451 */     ExtendedIterator aus = listEquivalentClasses();
/* 242:452 */     if (!aus.hasNext()) {
/* 243:453 */       return null;
/* 244:    */     }
/* 245:455 */     return (OntClass)aus.next();
/* 246:    */   }
/* 247:    */   
/* 248:    */   public ExtendedIterator listEquivalentClasses()
/* 249:    */   {
/* 250:465 */     StmtIterator stmt = listProperties(getProfile().EQUIVALENT_CLASS());
/* 251:466 */     Set result = new Set();
/* 252:467 */     while (stmt.hasNext()) {
/* 253:468 */       result.add(new OntClassImpl(stmt.nextStatement().getObject().asNode(), getModelCom()));
/* 254:    */     }
/* 255:469 */     return WrappedIterator.create(result.iterator());
/* 256:    */   }
/* 257:    */   
/* 258:    */   public boolean hasEquivalentClass(Resource cls)
/* 259:    */   {
/* 260:479 */     return hasPropertyValue(getProfile().EQUIVALENT_CLASS(), "EQUIVALENT_CLASS", cls);
/* 261:    */   }
/* 262:    */   
/* 263:    */   public void removeEquivalentClass(Resource cls)
/* 264:    */   {
/* 265:490 */     removePropertyValue(getProfile().EQUIVALENT_CLASS(), "EQUIVALENT_CLASS", cls);
/* 266:    */   }
/* 267:    */   
/* 268:    */   public void setDisjointWith(Resource cls)
/* 269:    */   {
/* 270:502 */     setPropertyValue(getProfile().DISJOINT_WITH(), "DISJOINT_WITH", cls);
/* 271:    */   }
/* 272:    */   
/* 273:    */   public void addDisjointWith(Resource cls)
/* 274:    */   {
/* 275:511 */     addPropertyValue(getProfile().DISJOINT_WITH(), "DISJOINT_WITH", cls);
/* 276:    */   }
/* 277:    */   
/* 278:    */   public OntClass getDisjointWith()
/* 279:    */   {
/* 280:521 */     ExtendedIterator aus = listDisjointWith();
/* 281:522 */     if (!aus.hasNext()) {
/* 282:523 */       return null;
/* 283:    */     }
/* 284:525 */     return (OntClass)aus.next();
/* 285:    */   }
/* 286:    */   
/* 287:    */   public ExtendedIterator listDisjointWith()
/* 288:    */   {
/* 289:535 */     StmtIterator stmt = listProperties(getProfile().DISJOINT_WITH());
/* 290:536 */     Set result = new Set();
/* 291:537 */     while (stmt.hasNext()) {
/* 292:538 */       result.add(new OntClassImpl(stmt.nextStatement().getObject().asNode(), getModelCom()));
/* 293:    */     }
/* 294:539 */     return WrappedIterator.create(result.iterator());
/* 295:    */   }
/* 296:    */   
/* 297:    */   public boolean isDisjointWith(Resource cls)
/* 298:    */   {
/* 299:549 */     return hasPropertyValue(getProfile().DISJOINT_WITH(), "DISJOINT_WITH", cls);
/* 300:    */   }
/* 301:    */   
/* 302:    */   public void removeDisjointWith(Resource cls)
/* 303:    */   {
/* 304:560 */     removePropertyValue(getProfile().DISJOINT_WITH(), "DISJOINT_WITH", cls);
/* 305:    */   }
/* 306:    */   
/* 307:    */   public ExtendedIterator listDeclaredProperties()
/* 308:    */   {
/* 309:579 */     return listDeclaredProperties(false);
/* 310:    */   }
/* 311:    */   
/* 312:    */   public ExtendedIterator listDeclaredProperties(boolean direct)
/* 313:    */   {
/* 314:598 */     Set candSet = new Set();
/* 315:601 */     for (Iterator i = listAllProperties(); i.hasNext();) {
/* 316:602 */       candSet.add(new OntPropertyImpl(((Statement)i.next()).getSubject().asNode(), getModelCom()));
/* 317:    */     }
/* 318:605 */     List cands = new List();
/* 319:606 */     Iterator it = candSet.iterator();
/* 320:607 */     while (it.hasNext()) {
/* 321:608 */       cands.add(it.next());
/* 322:    */     }
/* 323:609 */     for (int j = cands.size() - 1; j >= 0; j--)
/* 324:    */     {
/* 325:610 */       Property cand = (Property)cands.get(j);
/* 326:611 */       if (!hasDeclaredProperty(cand, direct)) {
/* 327:612 */         cands.remove(j);
/* 328:    */       }
/* 329:    */     }
/* 330:615 */     return WrappedIterator.create(cands.iterator());
/* 331:    */   }
/* 332:    */   
/* 333:    */   public boolean hasDeclaredProperty(Property p, boolean direct)
/* 334:    */   {
/* 335:628 */     return testDomain(p, direct);
/* 336:    */   }
/* 337:    */   
/* 338:    */   public ExtendedIterator listInstances()
/* 339:    */   {
/* 340:639 */     return listInstances(false);
/* 341:    */   }
/* 342:    */   
/* 343:    */   public ExtendedIterator listInstances(boolean direct)
/* 344:    */   {
/* 345:653 */     StmtIterator it = getModel().listStatements((Resource)null, RDF.type, this);
/* 346:654 */     Set result = new Set();
/* 347:655 */     while (it.hasNext()) {
/* 348:656 */       result.add(it.nextStatement().getSubject());
/* 349:    */     }
/* 350:657 */     if (!direct)
/* 351:    */     {
/* 352:658 */       ExtendedIterator subClasses = listSubClasses();
/* 353:659 */       while (subClasses.hasNext())
/* 354:    */       {
/* 355:660 */         StmtIterator it2 = getModel().listStatements((Resource)null, RDF.type, (RDFNode)subClasses.next());
/* 356:661 */         while (it2.hasNext()) {
/* 357:662 */           result.add(it2.nextStatement().getSubject());
/* 358:    */         }
/* 359:    */       }
/* 360:    */     }
/* 361:665 */     return WrappedIterator.create(result.iterator());
/* 362:    */   }
/* 363:    */   
/* 364:    */   public Individual createIndividual()
/* 365:    */   {
/* 366:674 */     return ((OntModel)getModel()).createIndividual(this);
/* 367:    */   }
/* 368:    */   
/* 369:    */   public Individual createIndividual(String uri)
/* 370:    */   {
/* 371:684 */     return ((OntModel)getModel()).createIndividual(uri, this);
/* 372:    */   }
/* 373:    */   
/* 374:    */   public boolean isHierarchyRoot()
/* 375:    */   {
/* 376:698 */     if (equals(getProfile().NOTHING())) {
/* 377:699 */       return false;
/* 378:    */     }
/* 379:708 */     ExtendedIterator i = listSuperClasses(true);
/* 380:    */     try
/* 381:    */     {
/* 382:710 */       while (i.hasNext())
/* 383:    */       {
/* 384:711 */         Resource sup = (Resource)i.next();
/* 385:712 */         if ((!sup.equals(getProfile().THING())) && (!sup.equals(RDFS.Resource)) && (!sup.equals(this))) {
/* 386:716 */           return false;
/* 387:    */         }
/* 388:    */       }
/* 389:    */     }
/* 390:    */     finally
/* 391:    */     {
/* 392:720 */       i.close();
/* 393:    */     }
/* 394:722 */     return true;
/* 395:    */   }
/* 396:    */   
/* 397:    */   public boolean isEnumeratedClass()
/* 398:    */   {
/* 399:733 */     return hasProperty(getProfile().ONE_OF());
/* 400:    */   }
/* 401:    */   
/* 402:    */   public boolean isUnionClass()
/* 403:    */   {
/* 404:741 */     return hasProperty(getProfile().UNION_OF());
/* 405:    */   }
/* 406:    */   
/* 407:    */   public boolean isIntersectionClass()
/* 408:    */   {
/* 409:749 */     return hasProperty(getProfile().INTERSECTION_OF());
/* 410:    */   }
/* 411:    */   
/* 412:    */   public boolean isComplementClass()
/* 413:    */   {
/* 414:757 */     return hasProperty(getProfile().COMPLEMENT_OF());
/* 415:    */   }
/* 416:    */   
/* 417:    */   public boolean isRestriction()
/* 418:    */   {
/* 419:765 */     return (hasProperty(getProfile().ON_PROPERTY())) || (hasProperty(RDF.type, getProfile().RESTRICTION()));
/* 420:    */   }
/* 421:    */   
/* 422:    */   protected boolean testDomain(Property p, boolean direct)
/* 423:    */   {
/* 424:777 */     String namespace = p.getNameSpace();
/* 425:778 */     for (int i = 0; i < IGNORE_NAMESPACES.length; i++) {
/* 426:779 */       if (namespace.equals(IGNORE_NAMESPACES[i])) {
/* 427:780 */         return false;
/* 428:    */       }
/* 429:    */     }
/* 430:785 */     boolean isGlobal = true;
/* 431:    */     
/* 432:    */ 
/* 433:788 */     boolean seenDirect = false;
/* 434:790 */     for (StmtIterator i = getModel().listStatements(p, getProfile().DOMAIN(), (RDFNode)null); i.hasNext();)
/* 435:    */     {
/* 436:791 */       Resource domain = i.nextStatement().getResource();
/* 437:794 */       if ((!domain.equals(getProfile().THING())) && (!domain.equals(RDFS.Resource)))
/* 438:    */       {
/* 439:796 */         isGlobal = false;
/* 440:798 */         if (domain.equals(this)) {
/* 441:801 */           seenDirect = true;
/* 442:802 */         } else if (!canProveSuperClass(domain)) {
/* 443:804 */           return false;
/* 444:    */         }
/* 445:    */       }
/* 446:    */     }
/* 447:809 */     if (direct) {
/* 448:812 */       return (seenDirect) || ((isGlobal) && (isHierarchyRoot()));
/* 449:    */     }
/* 450:816 */     return true;
/* 451:    */   }
/* 452:    */   
/* 453:    */   protected ExtendedIterator listAllProperties()
/* 454:    */   {
/* 455:826 */     OntModel mOnt = (OntModel)getModel();
/* 456:827 */     ExtendedIterator pi = mOnt.listStatements(null, RDF.type, getProfile().PROPERTY());
/* 457:829 */     if (getProfile().OBJECT_PROPERTY() != null) {
/* 458:830 */       pi = pi.andThen(mOnt.listStatements(null, RDF.type, getProfile().OBJECT_PROPERTY()));
/* 459:    */     }
/* 460:832 */     if (getProfile().DATATYPE_PROPERTY() != null) {
/* 461:833 */       pi = pi.andThen(mOnt.listStatements(null, RDF.type, getProfile().DATATYPE_PROPERTY()));
/* 462:    */     }
/* 463:835 */     if (getProfile().FUNCTIONAL_PROPERTY() != null) {
/* 464:836 */       pi = pi.andThen(mOnt.listStatements(null, RDF.type, getProfile().FUNCTIONAL_PROPERTY()));
/* 465:    */     }
/* 466:838 */     if (getProfile().INVERSE_FUNCTIONAL_PROPERTY() != null) {
/* 467:839 */       pi = pi.andThen(mOnt.listStatements(null, RDF.type, getProfile().INVERSE_FUNCTIONAL_PROPERTY()));
/* 468:    */     }
/* 469:841 */     if (getProfile().SYMMETRIC_PROPERTY() != null) {
/* 470:842 */       pi = pi.andThen(mOnt.listStatements(null, RDF.type, getProfile().SYMMETRIC_PROPERTY()));
/* 471:    */     }
/* 472:844 */     if (getProfile().TRANSITIVE_PROPERTY() != null) {
/* 473:845 */       pi = pi.andThen(mOnt.listStatements(null, RDF.type, getProfile().TRANSITIVE_PROPERTY()));
/* 474:    */     }
/* 475:847 */     if (getProfile().ANNOTATION_PROPERTY() != null) {
/* 476:848 */       pi = pi.andThen(mOnt.listStatements(null, RDF.type, getProfile().ANNOTATION_PROPERTY()));
/* 477:    */     }
/* 478:850 */     return pi;
/* 479:    */   }
/* 480:    */   
/* 481:    */   protected boolean canProveSuperClass(Resource sup)
/* 482:    */   {
/* 483:862 */     Set seen = new Set();
/* 484:863 */     List queue = new List();
/* 485:864 */     queue.add(this);
/* 486:    */     Iterator i;
/* 487:865 */     while (!queue.isEmpty())
/* 488:    */     {
/* 489:866 */       OntClass c = (OntClass)queue.remove(0);
/* 490:867 */       if (!seen.contains(c))
/* 491:    */       {
/* 492:868 */         seen.add(c);
/* 493:869 */         if (c.equals(sup)) {
/* 494:871 */           return true;
/* 495:    */         }
/* 496:874 */         for (i = c.listSuperClasses(); i.hasNext();) {
/* 497:875 */           queue.add(i.next());
/* 498:    */         }
/* 499:    */       }
/* 500:    */     }
/* 501:881 */     return false;
/* 502:    */   }
/* 503:    */ }

