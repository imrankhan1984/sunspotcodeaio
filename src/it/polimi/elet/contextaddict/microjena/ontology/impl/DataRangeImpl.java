/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.DataRange;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.RDFListImpl;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  18:    */ 
/*  19:    */ public class DataRangeImpl
/*  20:    */   extends OntResourceImpl
/*  21:    */   implements DataRange
/*  22:    */ {
/*  23: 53 */   public static Implementation factory = new Implementation()
/*  24:    */   {
/*  25:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  26:    */     {
/*  27: 55 */       if (canWrap(n, eg)) {
/*  28: 56 */         return new DataRangeImpl(n, eg);
/*  29:    */       }
/*  30: 58 */       throw new ConversionException("Cannot convert node " + n + " to DataRange");
/*  31:    */     }
/*  32:    */     
/*  33:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  34:    */     {
/*  35: 64 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  36: 65 */       return (profile != null) && (profile.isSupported(node, eg, DataRange.class));
/*  37:    */     }
/*  38:    */   };
/*  39:    */   
/*  40:    */   public DataRangeImpl(Node n, EnhGraph g)
/*  41:    */   {
/*  42: 78 */     super(n, g);
/*  43:    */   }
/*  44:    */   
/*  45:    */   public void setOneOf(RDFList en)
/*  46:    */   {
/*  47: 90 */     setPropertyValue(getProfile().ONE_OF(), "ONE_OF", en);
/*  48:    */   }
/*  49:    */   
/*  50:    */   public void addOneOf(Literal lit)
/*  51:    */   {
/*  52: 99 */     addListPropertyValue(getProfile().ONE_OF(), "ONE_OF", lit);
/*  53:    */   }
/*  54:    */   
/*  55:    */   public void addOneOf(Iterator literals)
/*  56:    */   {
/*  57:109 */     while (literals.hasNext()) {
/*  58:110 */       addOneOf((Literal)literals.next());
/*  59:    */     }
/*  60:    */   }
/*  61:    */   
/*  62:    */   public RDFList getOneOf()
/*  63:    */   {
/*  64:120 */     return new RDFListImpl(getProperty(getProfile().ONE_OF()).getObject().asNode(), getModelCom());
/*  65:    */   }
/*  66:    */   
/*  67:    */   public ExtendedIterator listOneOf()
/*  68:    */   {
/*  69:130 */     return getOneOf().iterator();
/*  70:    */   }
/*  71:    */   
/*  72:    */   public boolean hasOneOf(Literal lit)
/*  73:    */   {
/*  74:141 */     return getOneOf().contains(lit);
/*  75:    */   }
/*  76:    */   
/*  77:    */   public void removeOneOf(Literal lit)
/*  78:    */   {
/*  79:151 */     setOneOf(getOneOf().remove(lit));
/*  80:    */   }
/*  81:    */ }

