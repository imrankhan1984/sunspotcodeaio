/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node_Blank;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Node_URI;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.AllDifferent;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.AllValuesFromRestriction;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.AnnotationProperty;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.CardinalityRestriction;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.ontology.DataRange;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.ontology.DatatypeProperty;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.ontology.FunctionalProperty;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.ontology.HasValueRestriction;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.ontology.Individual;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.ontology.InverseFunctionalProperty;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.ontology.MaxCardinalityRestriction;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.ontology.MinCardinalityRestriction;
/*  20:    */ import it.polimi.elet.contextaddict.microjena.ontology.ObjectProperty;
/*  21:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/*  22:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  23:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntProperty;
/*  24:    */ import it.polimi.elet.contextaddict.microjena.ontology.Ontology;
/*  25:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  26:    */ import it.polimi.elet.contextaddict.microjena.ontology.Restriction;
/*  27:    */ import it.polimi.elet.contextaddict.microjena.ontology.SomeValuesFromRestriction;
/*  28:    */ import it.polimi.elet.contextaddict.microjena.ontology.SymmetricProperty;
/*  29:    */ import it.polimi.elet.contextaddict.microjena.ontology.TransitiveProperty;
/*  30:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  31:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*  32:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  33:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  34:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*  35:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.OWL;
/*  36:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  37:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*  38:    */ 
/*  39:    */ public class OWLProfile
/*  40:    */   extends AbstractProfile
/*  41:    */ {
/*  42:    */   public String NAMESPACE()
/*  43:    */   {
/*  44: 68 */     return OWL.getURI();
/*  45:    */   }
/*  46:    */   
/*  47:    */   public Resource CLASS()
/*  48:    */   {
/*  49: 70 */     return OWL.Class;
/*  50:    */   }
/*  51:    */   
/*  52:    */   public Resource RESTRICTION()
/*  53:    */   {
/*  54: 71 */     return OWL.Restriction;
/*  55:    */   }
/*  56:    */   
/*  57:    */   public Resource THING()
/*  58:    */   {
/*  59: 72 */     return OWL.Thing;
/*  60:    */   }
/*  61:    */   
/*  62:    */   public Resource NOTHING()
/*  63:    */   {
/*  64: 73 */     return OWL.Nothing;
/*  65:    */   }
/*  66:    */   
/*  67:    */   public Resource PROPERTY()
/*  68:    */   {
/*  69: 74 */     return RDF.Property;
/*  70:    */   }
/*  71:    */   
/*  72:    */   public Resource OBJECT_PROPERTY()
/*  73:    */   {
/*  74: 75 */     return OWL.ObjectProperty;
/*  75:    */   }
/*  76:    */   
/*  77:    */   public Resource DATATYPE_PROPERTY()
/*  78:    */   {
/*  79: 76 */     return OWL.DatatypeProperty;
/*  80:    */   }
/*  81:    */   
/*  82:    */   public Resource TRANSITIVE_PROPERTY()
/*  83:    */   {
/*  84: 77 */     return OWL.TransitiveProperty;
/*  85:    */   }
/*  86:    */   
/*  87:    */   public Resource SYMMETRIC_PROPERTY()
/*  88:    */   {
/*  89: 78 */     return OWL.SymmetricProperty;
/*  90:    */   }
/*  91:    */   
/*  92:    */   public Resource FUNCTIONAL_PROPERTY()
/*  93:    */   {
/*  94: 79 */     return OWL.FunctionalProperty;
/*  95:    */   }
/*  96:    */   
/*  97:    */   public Resource INVERSE_FUNCTIONAL_PROPERTY()
/*  98:    */   {
/*  99: 80 */     return OWL.InverseFunctionalProperty;
/* 100:    */   }
/* 101:    */   
/* 102:    */   public Resource ALL_DIFFERENT()
/* 103:    */   {
/* 104: 81 */     return OWL.AllDifferent;
/* 105:    */   }
/* 106:    */   
/* 107:    */   public Resource ONTOLOGY()
/* 108:    */   {
/* 109: 82 */     return OWL.Ontology;
/* 110:    */   }
/* 111:    */   
/* 112:    */   public Resource DEPRECATED_CLASS()
/* 113:    */   {
/* 114: 83 */     return OWL.DeprecatedClass;
/* 115:    */   }
/* 116:    */   
/* 117:    */   public Resource DEPRECATED_PROPERTY()
/* 118:    */   {
/* 119: 84 */     return OWL.DeprecatedProperty;
/* 120:    */   }
/* 121:    */   
/* 122:    */   public Resource ANNOTATION_PROPERTY()
/* 123:    */   {
/* 124: 85 */     return OWL.AnnotationProperty;
/* 125:    */   }
/* 126:    */   
/* 127:    */   public Resource ONTOLOGY_PROPERTY()
/* 128:    */   {
/* 129: 86 */     return OWL.OntologyProperty;
/* 130:    */   }
/* 131:    */   
/* 132:    */   public Resource LIST()
/* 133:    */   {
/* 134: 87 */     return RDF.List;
/* 135:    */   }
/* 136:    */   
/* 137:    */   public Resource NIL()
/* 138:    */   {
/* 139: 88 */     return RDF.nil;
/* 140:    */   }
/* 141:    */   
/* 142:    */   public Resource DATARANGE()
/* 143:    */   {
/* 144: 89 */     return OWL.DataRange;
/* 145:    */   }
/* 146:    */   
/* 147:    */   public Property EQUIVALENT_PROPERTY()
/* 148:    */   {
/* 149: 91 */     return OWL.equivalentProperty;
/* 150:    */   }
/* 151:    */   
/* 152:    */   public Property EQUIVALENT_CLASS()
/* 153:    */   {
/* 154: 92 */     return OWL.equivalentClass;
/* 155:    */   }
/* 156:    */   
/* 157:    */   public Property DISJOINT_WITH()
/* 158:    */   {
/* 159: 93 */     return OWL.disjointWith;
/* 160:    */   }
/* 161:    */   
/* 162:    */   public Property SAME_INDIVIDUAL_AS()
/* 163:    */   {
/* 164: 94 */     return null;
/* 165:    */   }
/* 166:    */   
/* 167:    */   public Property SAME_AS()
/* 168:    */   {
/* 169: 95 */     return OWL.sameAs;
/* 170:    */   }
/* 171:    */   
/* 172:    */   public Property DIFFERENT_FROM()
/* 173:    */   {
/* 174: 96 */     return OWL.differentFrom;
/* 175:    */   }
/* 176:    */   
/* 177:    */   public Property DISTINCT_MEMBERS()
/* 178:    */   {
/* 179: 97 */     return OWL.distinctMembers;
/* 180:    */   }
/* 181:    */   
/* 182:    */   public Property UNION_OF()
/* 183:    */   {
/* 184: 98 */     return OWL.unionOf;
/* 185:    */   }
/* 186:    */   
/* 187:    */   public Property INTERSECTION_OF()
/* 188:    */   {
/* 189: 99 */     return OWL.intersectionOf;
/* 190:    */   }
/* 191:    */   
/* 192:    */   public Property COMPLEMENT_OF()
/* 193:    */   {
/* 194:100 */     return OWL.complementOf;
/* 195:    */   }
/* 196:    */   
/* 197:    */   public Property ONE_OF()
/* 198:    */   {
/* 199:101 */     return OWL.oneOf;
/* 200:    */   }
/* 201:    */   
/* 202:    */   public Property ON_PROPERTY()
/* 203:    */   {
/* 204:102 */     return OWL.onProperty;
/* 205:    */   }
/* 206:    */   
/* 207:    */   public Property ALL_VALUES_FROM()
/* 208:    */   {
/* 209:103 */     return OWL.allValuesFrom;
/* 210:    */   }
/* 211:    */   
/* 212:    */   public Property HAS_VALUE()
/* 213:    */   {
/* 214:104 */     return OWL.hasValue;
/* 215:    */   }
/* 216:    */   
/* 217:    */   public Property SOME_VALUES_FROM()
/* 218:    */   {
/* 219:105 */     return OWL.someValuesFrom;
/* 220:    */   }
/* 221:    */   
/* 222:    */   public Property MIN_CARDINALITY()
/* 223:    */   {
/* 224:106 */     return OWL.minCardinality;
/* 225:    */   }
/* 226:    */   
/* 227:    */   public Property MAX_CARDINALITY()
/* 228:    */   {
/* 229:107 */     return OWL.maxCardinality;
/* 230:    */   }
/* 231:    */   
/* 232:    */   public Property CARDINALITY()
/* 233:    */   {
/* 234:108 */     return OWL.cardinality;
/* 235:    */   }
/* 236:    */   
/* 237:    */   public Property INVERSE_OF()
/* 238:    */   {
/* 239:109 */     return OWL.inverseOf;
/* 240:    */   }
/* 241:    */   
/* 242:    */   public Property IMPORTS()
/* 243:    */   {
/* 244:110 */     return OWL.imports;
/* 245:    */   }
/* 246:    */   
/* 247:    */   public Property PRIOR_VERSION()
/* 248:    */   {
/* 249:111 */     return OWL.priorVersion;
/* 250:    */   }
/* 251:    */   
/* 252:    */   public Property BACKWARD_COMPATIBLE_WITH()
/* 253:    */   {
/* 254:112 */     return OWL.backwardCompatibleWith;
/* 255:    */   }
/* 256:    */   
/* 257:    */   public Property INCOMPATIBLE_WITH()
/* 258:    */   {
/* 259:113 */     return OWL.incompatibleWith;
/* 260:    */   }
/* 261:    */   
/* 262:    */   public Property SUB_PROPERTY_OF()
/* 263:    */   {
/* 264:114 */     return RDFS.subPropertyOf;
/* 265:    */   }
/* 266:    */   
/* 267:    */   public Property SUB_CLASS_OF()
/* 268:    */   {
/* 269:115 */     return RDFS.subClassOf;
/* 270:    */   }
/* 271:    */   
/* 272:    */   public Property DOMAIN()
/* 273:    */   {
/* 274:116 */     return RDFS.domain;
/* 275:    */   }
/* 276:    */   
/* 277:    */   public Property RANGE()
/* 278:    */   {
/* 279:117 */     return RDFS.range;
/* 280:    */   }
/* 281:    */   
/* 282:    */   public Property FIRST()
/* 283:    */   {
/* 284:118 */     return RDF.first;
/* 285:    */   }
/* 286:    */   
/* 287:    */   public Property REST()
/* 288:    */   {
/* 289:119 */     return RDF.rest;
/* 290:    */   }
/* 291:    */   
/* 292:    */   public Property MIN_CARDINALITY_Q()
/* 293:    */   {
/* 294:120 */     return null;
/* 295:    */   }
/* 296:    */   
/* 297:    */   public Property MAX_CARDINALITY_Q()
/* 298:    */   {
/* 299:121 */     return null;
/* 300:    */   }
/* 301:    */   
/* 302:    */   public Property CARDINALITY_Q()
/* 303:    */   {
/* 304:122 */     return null;
/* 305:    */   }
/* 306:    */   
/* 307:    */   public Property HAS_CLASS_Q()
/* 308:    */   {
/* 309:123 */     return null;
/* 310:    */   }
/* 311:    */   
/* 312:    */   public Property VERSION_INFO()
/* 313:    */   {
/* 314:126 */     return OWL.versionInfo;
/* 315:    */   }
/* 316:    */   
/* 317:    */   public Property LABEL()
/* 318:    */   {
/* 319:127 */     return RDFS.label;
/* 320:    */   }
/* 321:    */   
/* 322:    */   public Property COMMENT()
/* 323:    */   {
/* 324:128 */     return RDFS.comment;
/* 325:    */   }
/* 326:    */   
/* 327:    */   public Property SEE_ALSO()
/* 328:    */   {
/* 329:129 */     return RDFS.seeAlso;
/* 330:    */   }
/* 331:    */   
/* 332:    */   public Property IS_DEFINED_BY()
/* 333:    */   {
/* 334:130 */     return RDFS.isDefinedBy;
/* 335:    */   }
/* 336:    */   
/* 337:    */   protected Resource[][] aliasTable()
/* 338:    */   {
/* 339:134 */     return new Resource[0][];
/* 340:    */   }
/* 341:    */   
/* 342:    */   public Iterator getAxiomTypes()
/* 343:    */   {
/* 344:140 */     return arrayToIterator(new Resource[] { OWL.AllDifferent });
/* 345:    */   }
/* 346:    */   
/* 347:    */   public Iterator getAnnotationProperties()
/* 348:    */   {
/* 349:149 */     return arrayToIterator(new Resource[] { OWL.versionInfo, RDFS.label, RDFS.seeAlso, RDFS.comment, RDFS.isDefinedBy });
/* 350:    */   }
/* 351:    */   
/* 352:    */   public Iterator getClassDescriptionTypes()
/* 353:    */   {
/* 354:161 */     return arrayToIterator(new Resource[] { OWL.Class, OWL.Restriction });
/* 355:    */   }
/* 356:    */   
/* 357:    */   public boolean isSupported(Node n, EnhGraph g, Class type)
/* 358:    */   {
/* 359:185 */     if ((g instanceof OntModel))
/* 360:    */     {
/* 361:186 */       OntModel m = (OntModel)g;
/* 362:188 */       if (!m.strictMode()) {
/* 363:190 */         return true;
/* 364:    */       }
/* 365:193 */       SupportsCheck check = (SupportsCheck)getCheckTable().get(type);
/* 366:    */       
/* 367:    */ 
/* 368:196 */       boolean result = (check != null) && (check.doCheck(n, g));
/* 369:197 */       return result;
/* 370:    */     }
/* 371:200 */     return false;
/* 372:    */   }
/* 373:    */   
/* 374:    */   public String getLabel()
/* 375:    */   {
/* 376:211 */     return "OWL Full";
/* 377:    */   }
/* 378:    */   
/* 379:    */   protected static class SupportsCheck
/* 380:    */   {
/* 381:    */     public boolean doCheck(Node n, EnhGraph g)
/* 382:    */     {
/* 383:217 */       return true;
/* 384:    */     }
/* 385:    */   }
/* 386:    */   
/* 387:221 */   private static Object[][] s_supportsCheckData = { { AllDifferent.class, new SupportsCheck()
/* 388:    */   {
/* 389:    */     public boolean doCheck(Node n, EnhGraph g)
/* 390:    */     {
/* 391:225 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.AllDifferent.asNode());
/* 392:    */     }
/* 393:221 */   } }, { AnnotationProperty.class, new SupportsCheck()
/* 394:    */   {
/* 395:    */     public boolean doCheck(Node n, EnhGraph g)
/* 396:    */     {
/* 397:231 */       for (Iterator i = ((OntModel)g).getProfile().getAnnotationProperties(); i.hasNext();) {
/* 398:232 */         if (((Resource)i.next()).asNode().equals(n)) {
/* 399:234 */           return true;
/* 400:    */         }
/* 401:    */       }
/* 402:237 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.AnnotationProperty.asNode());
/* 403:    */     }
/* 404:221 */   } }, { OntClass.class, new SupportsCheck()
/* 405:    */   {
/* 406:    */     public boolean doCheck(Node n, EnhGraph eg)
/* 407:    */     {
/* 408:243 */       Graph g = eg.asGraph();
/* 409:244 */       Node rdfTypeNode = RDF.type.asNode();
/* 410:245 */       return (g.contains(n, rdfTypeNode, OWL.Class.asNode())) || (g.contains(n, rdfTypeNode, OWL.Restriction.asNode())) || (g.contains(n, rdfTypeNode, RDFS.Class.asNode())) || (g.contains(n, rdfTypeNode, RDFS.Datatype.asNode())) || (n.equals(OWL.Thing.asNode())) || (n.equals(OWL.Nothing.asNode())) || (g.contains(Node.ANY, RDFS.domain.asNode(), n)) || (g.contains(Node.ANY, RDFS.range.asNode(), n)) || (g.contains(n, OWL.intersectionOf.asNode(), Node.ANY)) || (g.contains(n, OWL.unionOf.asNode(), Node.ANY)) || (g.contains(n, OWL.complementOf.asNode(), Node.ANY));
/* 411:    */     }
/* 412:221 */   } }, { DatatypeProperty.class, new SupportsCheck()
/* 413:    */   {
/* 414:    */     public boolean doCheck(Node n, EnhGraph g)
/* 415:    */     {
/* 416:263 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode());
/* 417:    */     }
/* 418:221 */   } }, { ObjectProperty.class, new SupportsCheck()
/* 419:    */   {
/* 420:    */     public boolean doCheck(Node n, EnhGraph g)
/* 421:    */     {
/* 422:269 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.ObjectProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.TransitiveProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.SymmetricProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.InverseFunctionalProperty.asNode()));
/* 423:    */     }
/* 424:221 */   } }, { FunctionalProperty.class, new SupportsCheck()
/* 425:    */   {
/* 426:    */     public boolean doCheck(Node n, EnhGraph g)
/* 427:    */     {
/* 428:279 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.FunctionalProperty.asNode());
/* 429:    */     }
/* 430:221 */   } }, { InverseFunctionalProperty.class, new SupportsCheck()
/* 431:    */   {
/* 432:    */     public boolean doCheck(Node n, EnhGraph g)
/* 433:    */     {
/* 434:285 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.InverseFunctionalProperty.asNode());
/* 435:    */     }
/* 436:221 */   } }, { RDFList.class, new SupportsCheck()
/* 437:    */   {
/* 438:    */     public boolean doCheck(Node n, EnhGraph g)
/* 439:    */     {
/* 440:291 */       return (n.equals(RDF.nil.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), RDF.List.asNode()));
/* 441:    */     }
/* 442:221 */   } }, { OntProperty.class, new SupportsCheck()
/* 443:    */   {
/* 444:    */     public boolean doCheck(Node n, EnhGraph g)
/* 445:    */     {
/* 446:298 */       return (g.asGraph().contains(n, RDF.type.asNode(), RDF.Property.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.ObjectProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.AnnotationProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.TransitiveProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.SymmetricProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.FunctionalProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.InverseFunctionalProperty.asNode()));
/* 447:    */     }
/* 448:221 */   } }, { Ontology.class, new SupportsCheck()
/* 449:    */   {
/* 450:    */     public boolean doCheck(Node n, EnhGraph g)
/* 451:    */     {
/* 452:311 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.Ontology.asNode());
/* 453:    */     }
/* 454:221 */   } }, { Restriction.class, new SupportsCheck()
/* 455:    */   {
/* 456:    */     public boolean doCheck(Node n, EnhGraph g)
/* 457:    */     {
/* 458:317 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode());
/* 459:    */     }
/* 460:221 */   } }, { HasValueRestriction.class, new SupportsCheck()
/* 461:    */   {
/* 462:    */     public boolean doCheck(Node n, EnhGraph g)
/* 463:    */     {
/* 464:323 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLProfile.containsSome(g, n, OWL.hasValue)) && (OWLProfile.containsSome(g, n, OWL.onProperty));
/* 465:    */     }
/* 466:221 */   } }, { AllValuesFromRestriction.class, new SupportsCheck()
/* 467:    */   {
/* 468:    */     public boolean doCheck(Node n, EnhGraph g)
/* 469:    */     {
/* 470:331 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLProfile.containsSome(g, n, OWL.allValuesFrom)) && (OWLProfile.containsSome(g, n, OWL.onProperty));
/* 471:    */     }
/* 472:221 */   } }, { SomeValuesFromRestriction.class, new SupportsCheck()
/* 473:    */   {
/* 474:    */     public boolean doCheck(Node n, EnhGraph g)
/* 475:    */     {
/* 476:339 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLProfile.containsSome(g, n, OWL.someValuesFrom)) && (OWLProfile.containsSome(g, n, OWL.onProperty));
/* 477:    */     }
/* 478:221 */   } }, { CardinalityRestriction.class, new SupportsCheck()
/* 479:    */   {
/* 480:    */     public boolean doCheck(Node n, EnhGraph g)
/* 481:    */     {
/* 482:347 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLProfile.containsSome(g, n, OWL.cardinality)) && (OWLProfile.containsSome(g, n, OWL.onProperty));
/* 483:    */     }
/* 484:221 */   } }, { MinCardinalityRestriction.class, new SupportsCheck()
/* 485:    */   {
/* 486:    */     public boolean doCheck(Node n, EnhGraph g)
/* 487:    */     {
/* 488:355 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLProfile.containsSome(g, n, OWL.minCardinality)) && (OWLProfile.containsSome(g, n, OWL.onProperty));
/* 489:    */     }
/* 490:221 */   } }, { MaxCardinalityRestriction.class, new SupportsCheck()
/* 491:    */   {
/* 492:    */     public boolean doCheck(Node n, EnhGraph g)
/* 493:    */     {
/* 494:363 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLProfile.containsSome(g, n, OWL.maxCardinality)) && (OWLProfile.containsSome(g, n, OWL.onProperty));
/* 495:    */     }
/* 496:221 */   } }, { SymmetricProperty.class, new SupportsCheck()
/* 497:    */   {
/* 498:    */     public boolean doCheck(Node n, EnhGraph g)
/* 499:    */     {
/* 500:371 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.SymmetricProperty.asNode())) && (!g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode()));
/* 501:    */     }
/* 502:221 */   } }, { TransitiveProperty.class, new SupportsCheck()
/* 503:    */   {
/* 504:    */     public boolean doCheck(Node n, EnhGraph g)
/* 505:    */     {
/* 506:378 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.TransitiveProperty.asNode())) && (!g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode()));
/* 507:    */     }
/* 508:221 */   } }, { Individual.class, new SupportsCheck()
/* 509:    */   {
/* 510:    */     public boolean doCheck(Node n, EnhGraph g)
/* 511:    */     {
/* 512:385 */       return ((n instanceof Node_URI)) || ((n instanceof Node_Blank));
/* 513:    */     }
/* 514:221 */   } }, { DataRange.class, new SupportsCheck()
/* 515:    */   {
/* 516:    */     public boolean doCheck(Node n, EnhGraph g)
/* 517:    */     {
/* 518:391 */       return ((n instanceof Node_Blank)) && (g.asGraph().contains(n, RDF.type.asNode(), OWL.DataRange.asNode()));
/* 519:    */     }
/* 520:221 */   } } };
/* 521:    */   
/* 522:    */   public static boolean containsSome(EnhGraph g, Node n, Property p)
/* 523:    */   {
/* 524:399 */     return AbstractProfile.containsSome(g, n, p);
/* 525:    */   }
/* 526:    */   
/* 527:403 */   private static Map s_supportsChecks = new Map();
/* 528:    */   
/* 529:    */   static
/* 530:    */   {
/* 531:407 */     for (int i = 0; i < s_supportsCheckData.length; i++) {
/* 532:408 */       s_supportsChecks.put(s_supportsCheckData[i][0], s_supportsCheckData[i][1]);
/* 533:    */     }
/* 534:    */   }
/* 535:    */   
/* 536:    */   protected Map getCheckTable()
/* 537:    */   {
/* 538:413 */     return s_supportsChecks;
/* 539:    */   }
/* 540:    */ }
