/*  1:   */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.ontology.DatatypeProperty;
/*  9:   */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/* 10:   */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/* 11:   */ 
/* 12:   */ public class DatatypePropertyImpl
/* 13:   */   extends OntPropertyImpl
/* 14:   */   implements DatatypeProperty
/* 15:   */ {
/* 16:49 */   public static Implementation factory = new Implementation()
/* 17:   */   {
/* 18:   */     public EnhNode wrap(Node n, EnhGraph eg)
/* 19:   */     {
/* 20:51 */       if (canWrap(n, eg)) {
/* 21:52 */         return new DatatypePropertyImpl(n, eg);
/* 22:   */       }
/* 23:54 */       throw new ConversionException("Cannot convert node " + n + " to DatatypeProperty");
/* 24:   */     }
/* 25:   */     
/* 26:   */     public boolean canWrap(Node node, EnhGraph eg)
/* 27:   */     {
/* 28:60 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/* 29:61 */       return (profile != null) && (profile.isSupported(node, eg, DatatypeProperty.class));
/* 30:   */     }
/* 31:   */   };
/* 32:   */   
/* 33:   */   public DatatypePropertyImpl(Node n, EnhGraph g)
/* 34:   */   {
/* 35:74 */     super(n, g);
/* 36:   */   }
/* 37:   */ }

