/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntProperty;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.Restriction;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*  17:    */ 
/*  18:    */ public class RestrictionImpl
/*  19:    */   extends OntClassImpl
/*  20:    */   implements Restriction
/*  21:    */ {
/*  22: 51 */   public static Implementation factory = new Implementation()
/*  23:    */   {
/*  24:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  25:    */     {
/*  26: 53 */       if (canWrap(n, eg)) {
/*  27: 54 */         return new RestrictionImpl(n, eg);
/*  28:    */       }
/*  29: 56 */       throw new ConversionException("Cannot convert node " + n + " to Restriction");
/*  30:    */     }
/*  31:    */     
/*  32:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  33:    */     {
/*  34: 62 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  35: 63 */       return (profile != null) && (profile.isSupported(node, eg, Restriction.class));
/*  36:    */     }
/*  37:    */   };
/*  38:    */   
/*  39:    */   public RestrictionImpl(Node n, EnhGraph g)
/*  40:    */   {
/*  41: 83 */     super(n, g);
/*  42:    */   }
/*  43:    */   
/*  44:    */   public void setOnProperty(Property prop)
/*  45:    */   {
/*  46: 93 */     setPropertyValue(getProfile().ON_PROPERTY(), "ON_PROPERTY", prop);
/*  47:    */   }
/*  48:    */   
/*  49:    */   public OntProperty getOnProperty()
/*  50:    */   {
/*  51:104 */     StmtIterator stmt = getModel().listStatements(this, getProfile().ON_PROPERTY(), (RDFNode)null);
/*  52:105 */     if (stmt.hasNext()) {
/*  53:106 */       return new OntPropertyImpl(stmt.nextStatement().getObject().asNode(), getModelCom());
/*  54:    */     }
/*  55:108 */     return null;
/*  56:    */   }
/*  57:    */   
/*  58:    */   public boolean onProperty(Property prop)
/*  59:    */   {
/*  60:118 */     return hasPropertyValue(getProfile().ON_PROPERTY(), "ON_PROPERTY", prop);
/*  61:    */   }
/*  62:    */   
/*  63:    */   public void removeOnProperty(Property prop)
/*  64:    */   {
/*  65:127 */     removePropertyValue(getProfile().ON_PROPERTY(), "ON_PROPERTY", prop);
/*  66:    */   }
/*  67:    */   
/*  68:    */   public boolean isAllValuesFromRestriction()
/*  69:    */   {
/*  70:139 */     return hasProperty(getProfile().ALL_VALUES_FROM());
/*  71:    */   }
/*  72:    */   
/*  73:    */   public boolean isSomeValuesFromRestriction()
/*  74:    */   {
/*  75:148 */     return hasProperty(getProfile().SOME_VALUES_FROM());
/*  76:    */   }
/*  77:    */   
/*  78:    */   public boolean isHasValueRestriction()
/*  79:    */   {
/*  80:157 */     return hasProperty(getProfile().HAS_VALUE());
/*  81:    */   }
/*  82:    */   
/*  83:    */   public boolean isCardinalityRestriction()
/*  84:    */   {
/*  85:168 */     return hasProperty(getProfile().CARDINALITY());
/*  86:    */   }
/*  87:    */   
/*  88:    */   public boolean isMinCardinalityRestriction()
/*  89:    */   {
/*  90:179 */     return hasProperty(getProfile().MIN_CARDINALITY());
/*  91:    */   }
/*  92:    */   
/*  93:    */   public boolean isMaxCardinalityRestriction()
/*  94:    */   {
/*  95:190 */     return hasProperty(getProfile().MAX_CARDINALITY());
/*  96:    */   }
/*  97:    */ }
