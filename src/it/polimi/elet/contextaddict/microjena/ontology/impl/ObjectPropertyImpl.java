/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.ObjectProperty;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntProperty;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  19:    */ 
/*  20:    */ public class ObjectPropertyImpl
/*  21:    */   extends OntPropertyImpl
/*  22:    */   implements ObjectProperty
/*  23:    */ {
/*  24: 54 */   public static Implementation factory = new Implementation()
/*  25:    */   {
/*  26:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  27:    */     {
/*  28: 56 */       if (canWrap(n, eg)) {
/*  29: 57 */         return new ObjectPropertyImpl(n, eg);
/*  30:    */       }
/*  31: 59 */       throw new ConversionException("Cannot convert node " + n + " to ObjectProperty");
/*  32:    */     }
/*  33:    */     
/*  34:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  35:    */     {
/*  36: 65 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  37: 66 */       return (profile != null) && (profile.isSupported(node, eg, ObjectProperty.class));
/*  38:    */     }
/*  39:    */   };
/*  40:    */   
/*  41:    */   public ObjectPropertyImpl(Node n, EnhGraph g)
/*  42:    */   {
/*  43: 79 */     super(n, g);
/*  44:    */   }
/*  45:    */   
/*  46:    */   public OntProperty getInverseOf()
/*  47:    */   {
/*  48: 89 */     ExtendedIterator it = listInverseOf();
/*  49: 90 */     if (it != null)
/*  50:    */     {
/*  51: 91 */       if (it.hasNext()) {
/*  52: 92 */         return (OntProperty)it.next();
/*  53:    */       }
/*  54: 94 */       return null;
/*  55:    */     }
/*  56: 96 */     return null;
/*  57:    */   }
/*  58:    */   
/*  59:    */   public ExtendedIterator listInverseOf()
/*  60:    */   {
/*  61:106 */     Set result = new Set();
/*  62:    */     
/*  63:    */ 
/*  64:109 */     StmtIterator it = getModel().listStatements(this, getProfile().INVERSE_OF(), (RDFNode)null);
/*  65:110 */     while (it.hasNext()) {
/*  66:111 */       result.add(new ObjectPropertyImpl(it.nextStatement().getObject().asNode(), getModelCom()));
/*  67:    */     }
/*  68:113 */     if (result.size() > 0) {
/*  69:114 */       return WrappedIterator.create(result.iterator());
/*  70:    */     }
/*  71:116 */     return null;
/*  72:    */   }
/*  73:    */   
/*  74:    */   public OntProperty getInverse()
/*  75:    */   {
/*  76:125 */     ExtendedIterator it = listInverse();
/*  77:126 */     if (it != null)
/*  78:    */     {
/*  79:127 */       if (it.hasNext()) {
/*  80:128 */         return (OntProperty)it.next();
/*  81:    */       }
/*  82:130 */       return null;
/*  83:    */     }
/*  84:132 */     return null;
/*  85:    */   }
/*  86:    */ }
