/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntProperty;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntResource;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.ModelCom;
/*  20:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.PropertyImpl;
/*  21:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  22:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  23:    */ import it.polimi.elet.contextaddict.microjena.util.List;
/*  24:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*  25:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  26:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  27:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  28:    */ 
/*  29:    */ public class OntPropertyImpl
/*  30:    */   extends OntResourceImpl
/*  31:    */   implements OntProperty
/*  32:    */ {
/*  33: 64 */   public static Implementation factory = new Implementation()
/*  34:    */   {
/*  35:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  36:    */     {
/*  37: 66 */       if (canWrap(n, eg)) {
/*  38: 67 */         return new OntPropertyImpl(n, eg);
/*  39:    */       }
/*  40: 69 */       throw new ConversionException("Cannot convert node " + n + " to OntProperty");
/*  41:    */     }
/*  42:    */     
/*  43:    */     public boolean canWrap(Node node, EnhGraph eg)
/*  44:    */     {
/*  45: 74 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/*  46: 75 */       return (profile != null) && (profile.isSupported(node, eg, OntProperty.class));
/*  47:    */     }
/*  48:    */   };
/*  49:    */   
/*  50:    */   public OntPropertyImpl(Node n, EnhGraph g)
/*  51:    */   {
/*  52: 88 */     super(n, g);
/*  53:    */   }
/*  54:    */   
/*  55:    */   public boolean isProperty()
/*  56:    */   {
/*  57: 99 */     return true;
/*  58:    */   }
/*  59:    */   
/*  60:105 */   protected int ordinal = -1;
/*  61:    */   
/*  62:    */   public int getOrdinal()
/*  63:    */   {
/*  64:108 */     if (this.ordinal < 0) {
/*  65:109 */       this.ordinal = computeOrdinal();
/*  66:    */     }
/*  67:110 */     return this.ordinal;
/*  68:    */   }
/*  69:    */   
/*  70:    */   private int computeOrdinal()
/*  71:    */   {
/*  72:114 */     String localName = getLocalName();
/*  73:    */     
/*  74:    */ 
/*  75:    */ 
/*  76:118 */     boolean aus = true;
/*  77:119 */     if (localName.charAt(0) != '_')
/*  78:    */     {
/*  79:120 */       aus = false;
/*  80:    */     }
/*  81:    */     else
/*  82:    */     {
/*  83:122 */       int i = localName.length();
/*  84:124 */       while (aus)
/*  85:    */       {
/*  86:124 */         i--;
/*  87:124 */         if (i <= 0) {
/*  88:    */           break;
/*  89:    */         }
/*  90:125 */         char ausCh = localName.charAt(i);
/*  91:126 */         if ((ausCh < '0') || (ausCh > '9')) {
/*  92:127 */           aus = false;
/*  93:    */         }
/*  94:    */       }
/*  95:    */     }
/*  96:131 */     if ((getNameSpace().equals(RDF.getURI())) && (aus)) {
/*  97:132 */       return parseInt(localName.substring(1));
/*  98:    */     }
/*  99:133 */     return 0;
/* 100:    */   }
/* 101:    */   
/* 102:    */   private int parseInt(String digits)
/* 103:    */   {
/* 104:    */     try
/* 105:    */     {
/* 106:138 */       return Integer.parseInt(digits);
/* 107:    */     }
/* 108:    */     catch (NumberFormatException e)
/* 109:    */     {
/* 110:140 */       throw new JenaException("checkOrdinal fails on " + digits, e);
/* 111:    */     }
/* 112:    */   }
/* 113:    */   
/* 114:    */   public void setSuperProperty(Property prop)
/* 115:    */   {
/* 116:154 */     setPropertyValue(getProfile().SUB_PROPERTY_OF(), "SUB_PROPERTY_OF", prop);
/* 117:    */   }
/* 118:    */   
/* 119:    */   public void addSuperProperty(Property prop)
/* 120:    */   {
/* 121:163 */     addPropertyValue(getProfile().SUB_PROPERTY_OF(), "SUB_PROPERTY_OF", prop);
/* 122:    */   }
/* 123:    */   
/* 124:    */   public OntProperty getSuperProperty()
/* 125:    */   {
/* 126:173 */     return objectAsProperty(getProfile().SUB_PROPERTY_OF(), "SUB_PROPERTY_OF");
/* 127:    */   }
/* 128:    */   
/* 129:    */   public ExtendedIterator listSuperProperties()
/* 130:    */   {
/* 131:183 */     return listSuperProperties(false);
/* 132:    */   }
/* 133:    */   
/* 134:    */   public ExtendedIterator listSuperProperties(boolean direct)
/* 135:    */   {
/* 136:196 */     if (direct)
/* 137:    */     {
/* 138:197 */       StmtIterator stmts = getModel().listStatements(this, getProfile().SUB_PROPERTY_OF(), (RDFNode)null);
/* 139:198 */       Set result = new Set();
/* 140:199 */       while (stmts.hasNext()) {
/* 141:200 */         result.add(stmts.nextStatement().getObject());
/* 142:    */       }
/* 143:201 */       return WrappedIterator.create(result.iterator());
/* 144:    */     }
/* 145:204 */     Set lower = new Set();
/* 146:205 */     Set computed = new Set();
/* 147:206 */     catchSuperClasses(this, lower, computed, getProfile().SUB_PROPERTY_OF());
/* 148:207 */     return WrappedIterator.create(computed.iterator());
/* 149:    */   }
/* 150:    */   
/* 151:    */   public boolean hasSuperProperty(Property prop, boolean direct)
/* 152:    */   {
/* 153:220 */     return hasPropertyValue(getProfile().SUB_PROPERTY_OF(), "SUB_PROPERTY_OF", prop);
/* 154:    */   }
/* 155:    */   
/* 156:    */   public void removeSuperProperty(Property prop)
/* 157:    */   {
/* 158:230 */     removePropertyValue(getProfile().SUB_PROPERTY_OF(), "SUB_PROPERTY_OF", prop);
/* 159:    */   }
/* 160:    */   
/* 161:    */   public void setSubProperty(Property prop)
/* 162:    */   {
/* 163:242 */     checkProfile(getProfile().SUB_PROPERTY_OF(), "SUB_PROPERTY_OF");
/* 164:243 */     getModel().remove(getModel().listStatements(null, getProfile().SUB_PROPERTY_OF(), this));
/* 165:244 */     getModel().add(prop, getProfile().SUB_PROPERTY_OF(), this);
/* 166:    */   }
/* 167:    */   
/* 168:    */   public void addSubProperty(Property prop)
/* 169:    */   {
/* 170:253 */     getModel().add(prop, getProfile().SUB_PROPERTY_OF(), this);
/* 171:    */   }
/* 172:    */   
/* 173:    */   public OntProperty getSubProperty()
/* 174:    */   {
/* 175:263 */     checkProfile(getProfile().SUB_PROPERTY_OF(), "SUB_PROPERTY_OF");
/* 176:264 */     return (OntProperty)factory.wrap(getModel().listStatements(null, getProfile().SUB_PROPERTY_OF(), this).nextStatement().getSubject().asNode(), (ModelCom)getModel());
/* 177:    */   }
/* 178:    */   
/* 179:    */   public ExtendedIterator listSubProperties()
/* 180:    */   {
/* 181:276 */     return listSubProperties(false);
/* 182:    */   }
/* 183:    */   
/* 184:    */   public ExtendedIterator listSubProperties(boolean direct)
/* 185:    */   {
/* 186:289 */     if (direct)
/* 187:    */     {
/* 188:290 */       StmtIterator stmts = getModel().listStatements(null, getProfile().SUB_PROPERTY_OF(), this);
/* 189:291 */       Set result = new Set();
/* 190:292 */       while (stmts.hasNext()) {
/* 191:293 */         result.add(stmts.nextStatement().getSubject());
/* 192:    */       }
/* 193:294 */       return WrappedIterator.create(result.iterator());
/* 194:    */     }
/* 195:297 */     Set lower = new Set();
/* 196:298 */     Set computed = new Set();
/* 197:299 */     catchSubProperties(this, lower, computed, getProfile().SUB_PROPERTY_OF());
/* 198:    */     
/* 199:301 */     ExtendedIterator it = WrappedIterator.create(computed.iterator());
/* 200:302 */     Set result = new Set();
/* 201:303 */     while (it.hasNext()) {
/* 202:304 */       result.add(new OntPropertyImpl(((RDFNode)it.next()).asNode(), getModelCom()));
/* 203:    */     }
/* 204:305 */     return WrappedIterator.create(result.iterator());
/* 205:    */   }
/* 206:    */   
/* 207:    */   private void catchSubProperties(Resource actualProperty, Set lower, Set computed, Property relation)
/* 208:    */   {
/* 209:310 */     StmtIterator subProperties = getModel().listStatements(null, relation, actualProperty);
/* 210:312 */     while (subProperties.hasNext())
/* 211:    */     {
/* 212:313 */       Resource aus = new PropertyImpl(subProperties.nextStatement().getSubject().asNode(), getModelCom());
/* 213:314 */       if (!computed.contains(aus))
/* 214:    */       {
/* 215:315 */         computed.add(aus);
/* 216:316 */         lower.remove(aus);
/* 217:317 */         catchSubProperties(aus, lower, computed, relation);
/* 218:    */       }
/* 219:    */       else
/* 220:    */       {
/* 221:319 */         lower.remove(aus);
/* 222:    */       }
/* 223:    */     }
/* 224:    */   }
/* 225:    */   
/* 226:    */   public boolean hasSubProperty(Property prop, boolean direct)
/* 227:    */   {
/* 228:332 */     OntProperty p = (OntProperty)factory.wrap(prop.asNode(), (ModelCom)getModel());
/* 229:333 */     return p.hasSubProperty(this, direct);
/* 230:    */   }
/* 231:    */   
/* 232:    */   public void removeSubProperty(Property prop)
/* 233:    */   {
/* 234:343 */     OntProperty p = (OntProperty)factory.wrap(prop.asNode(), (ModelCom)getModel());
/* 235:344 */     p.removeSuperProperty(this);
/* 236:    */   }
/* 237:    */   
/* 238:    */   public void setDomain(Resource res)
/* 239:    */   {
/* 240:356 */     setPropertyValue(getProfile().DOMAIN(), "DOMAIN", res);
/* 241:    */   }
/* 242:    */   
/* 243:    */   public void addDomain(Resource res)
/* 244:    */   {
/* 245:365 */     addPropertyValue(getProfile().DOMAIN(), "DOMAIN", res);
/* 246:    */   }
/* 247:    */   
/* 248:    */   public OntResource getDomain()
/* 249:    */   {
/* 250:375 */     return objectAsResource(getProfile().DOMAIN(), "DOMAIN");
/* 251:    */   }
/* 252:    */   
/* 253:    */   protected ExtendedIterator listAsClasses(Property p)
/* 254:    */   {
/* 255:379 */     Iterator i = listProperties(p);
/* 256:380 */     List result = new List();
/* 257:381 */     while (i.hasNext()) {
/* 258:382 */       result.add((OntClass)OntClassImpl.factory.wrap(((Statement)i.next()).getObject().asNode(), (ModelCom)getModel()));
/* 259:    */     }
/* 260:383 */     return WrappedIterator.create(result.iterator());
/* 261:    */   }
/* 262:    */   
/* 263:    */   protected ExtendedIterator listAsProperties(Property p)
/* 264:    */   {
/* 265:387 */     Iterator i = listProperties(p);
/* 266:388 */     List result = new List();
/* 267:389 */     while (i.hasNext()) {
/* 268:390 */       result.add((OntProperty)factory.wrap(((Statement)i.next()).getObject().asNode(), (ModelCom)getModel()));
/* 269:    */     }
/* 270:391 */     return WrappedIterator.create(result.iterator());
/* 271:    */   }
/* 272:    */   
/* 273:    */   public ExtendedIterator listDomain()
/* 274:    */   {
/* 275:401 */     return listAsClasses(getProfile().DOMAIN());
/* 276:    */   }
/* 277:    */   
/* 278:    */   public boolean hasDomain(Resource res)
/* 279:    */   {
/* 280:410 */     return hasPropertyValue(getProfile().DOMAIN(), "DOMAIN", res);
/* 281:    */   }
/* 282:    */   
/* 283:    */   public void removeDomain(Resource cls)
/* 284:    */   {
/* 285:420 */     removePropertyValue(getProfile().DOMAIN(), "DOMAIN", cls);
/* 286:    */   }
/* 287:    */   
/* 288:    */   public void setRange(Resource res)
/* 289:    */   {
/* 290:433 */     setPropertyValue(getProfile().RANGE(), "RANGE", res);
/* 291:    */   }
/* 292:    */   
/* 293:    */   public void addRange(Resource res)
/* 294:    */   {
/* 295:442 */     addPropertyValue(getProfile().RANGE(), "RANGE", res);
/* 296:    */   }
/* 297:    */   
/* 298:    */   public OntResource getRange()
/* 299:    */   {
/* 300:452 */     return objectAsResource(getProfile().RANGE(), "RANGE");
/* 301:    */   }
/* 302:    */   
/* 303:    */   public ExtendedIterator listRange()
/* 304:    */   {
/* 305:463 */     return listAsClasses(getProfile().RANGE());
/* 306:    */   }
/* 307:    */   
/* 308:    */   public boolean hasRange(Resource res)
/* 309:    */   {
/* 310:472 */     return hasPropertyValue(getProfile().RANGE(), "RANGE", res);
/* 311:    */   }
/* 312:    */   
/* 313:    */   public void removeRange(Resource cls)
/* 314:    */   {
/* 315:482 */     removePropertyValue(getProfile().RANGE(), "RANGE", cls);
/* 316:    */   }
/* 317:    */   
/* 318:    */   public void setEquivalentProperty(Property prop)
/* 319:    */   {
/* 320:497 */     setPropertyValue(getProfile().EQUIVALENT_PROPERTY(), "EQUIVALENT_PROPERTY", prop);
/* 321:    */   }
/* 322:    */   
/* 323:    */   public void addEquivalentProperty(Property prop)
/* 324:    */   {
/* 325:506 */     addPropertyValue(getProfile().EQUIVALENT_PROPERTY(), "EQUIVALENT_PROPERTY", prop);
/* 326:    */   }
/* 327:    */   
/* 328:    */   public OntProperty getEquivalentProperty()
/* 329:    */   {
/* 330:516 */     return objectAsProperty(getProfile().EQUIVALENT_PROPERTY(), "EQUIVALENT_PROPERTY");
/* 331:    */   }
/* 332:    */   
/* 333:    */   public ExtendedIterator listEquivalentProperties()
/* 334:    */   {
/* 335:526 */     return listAsProperties(getProfile().EQUIVALENT_PROPERTY());
/* 336:    */   }
/* 337:    */   
/* 338:    */   public boolean hasEquivalentProperty(Property prop)
/* 339:    */   {
/* 340:535 */     return hasPropertyValue(getProfile().EQUIVALENT_PROPERTY(), "EQUIVALENT_PROPERTY", prop);
/* 341:    */   }
/* 342:    */   
/* 343:    */   public void removeEquivalentProperty(Property prop)
/* 344:    */   {
/* 345:546 */     removePropertyValue(getProfile().EQUIVALENT_PROPERTY(), "EQUIVALENT_PROPERTY", prop);
/* 346:    */   }
/* 347:    */   
/* 348:    */   public void setInverseOf(Property prop)
/* 349:    */   {
/* 350:558 */     setPropertyValue(getProfile().INVERSE_OF(), "INVERSE_OF", prop);
/* 351:    */   }
/* 352:    */   
/* 353:    */   public void addInverseOf(Property prop)
/* 354:    */   {
/* 355:567 */     addPropertyValue(getProfile().INVERSE_OF(), "INVERSE_OF", prop);
/* 356:    */   }
/* 357:    */   
/* 358:    */   public OntProperty getInverseOf()
/* 359:    */   {
/* 360:577 */     return objectAsProperty(getProfile().INVERSE_OF(), "INVERSE_OF");
/* 361:    */   }
/* 362:    */   
/* 363:    */   public ExtendedIterator listInverseOf()
/* 364:    */   {
/* 365:587 */     return listAsProperties(getProfile().INVERSE_OF());
/* 366:    */   }
/* 367:    */   
/* 368:    */   public boolean isInverseOf(Property prop)
/* 369:    */   {
/* 370:596 */     return hasPropertyValue(getProfile().INVERSE_OF(), "INVERSE_OF", prop);
/* 371:    */   }
/* 372:    */   
/* 373:    */   public void removeInverseProperty(Property prop)
/* 374:    */   {
/* 375:606 */     removePropertyValue(getProfile().INVERSE_OF(), "INVERSE_OF", prop);
/* 376:    */   }
/* 377:    */   
/* 378:    */   public boolean isFunctionalProperty()
/* 379:    */   {
/* 380:617 */     return hasRDFType(getProfile().FUNCTIONAL_PROPERTY(), "FUNCTIONAL_PROPERTY", false);
/* 381:    */   }
/* 382:    */   
/* 383:    */   public boolean isDatatypeProperty()
/* 384:    */   {
/* 385:625 */     return hasRDFType(getProfile().DATATYPE_PROPERTY(), "DATATYPE_PROPERTY", false);
/* 386:    */   }
/* 387:    */   
/* 388:    */   public boolean isObjectProperty()
/* 389:    */   {
/* 390:633 */     return hasRDFType(getProfile().OBJECT_PROPERTY(), "OBJECT_PROPERTY", false);
/* 391:    */   }
/* 392:    */   
/* 393:    */   public boolean isTransitiveProperty()
/* 394:    */   {
/* 395:641 */     return hasRDFType(getProfile().TRANSITIVE_PROPERTY(), "TRANSITIVE_PROPERTY", false);
/* 396:    */   }
/* 397:    */   
/* 398:    */   public boolean isInverseFunctionalProperty()
/* 399:    */   {
/* 400:649 */     return hasRDFType(getProfile().INVERSE_FUNCTIONAL_PROPERTY(), "INVERSE_FUNCTIONAL_PROPERTY", false);
/* 401:    */   }
/* 402:    */   
/* 403:    */   public boolean isSymmetricProperty()
/* 404:    */   {
/* 405:657 */     return hasRDFType(getProfile().SYMMETRIC_PROPERTY(), "SYMMETRIC_PROPERTY", false);
/* 406:    */   }
/* 407:    */   
/* 408:    */   public OntProperty getInverse()
/* 409:    */   {
/* 410:667 */     ExtendedIterator i = listInverse();
/* 411:668 */     OntProperty p = i.hasNext() ? (OntProperty)i.next() : null;
/* 412:669 */     i.close();
/* 413:    */     
/* 414:671 */     return p;
/* 415:    */   }
/* 416:    */   
/* 417:    */   public ExtendedIterator listInverse()
/* 418:    */   {
/* 419:679 */     Set result = new Set();
/* 420:680 */     ExtendedIterator triples = getModel().listStatements(null, getProfile().INVERSE_OF(), this);
/* 421:681 */     while (triples.hasNext()) {
/* 422:682 */       result.add(((Statement)triples.next()).getSubject());
/* 423:    */     }
/* 424:684 */     return WrappedIterator.create(result.iterator());
/* 425:    */   }
/* 426:    */   
/* 427:    */   public boolean hasInverse()
/* 428:    */   {
/* 429:692 */     ExtendedIterator i = listInverse();
/* 430:693 */     boolean hasInv = i.hasNext();
/* 431:694 */     i.close();
/* 432:695 */     return hasInv;
/* 433:    */   }
/* 434:    */   
/* 435:    */   public RDFNode inModel(Model m)
/* 436:    */   {
/* 437:707 */     return getModel() == m ? this : m.createProperty(getURI());
/* 438:    */   }
/* 439:    */ }
