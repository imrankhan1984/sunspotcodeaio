/*   1:    */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node_Blank;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Node_URI;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.ontology.AllDifferent;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.ontology.AllValuesFromRestriction;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.AnnotationProperty;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.ontology.CardinalityRestriction;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.ontology.DatatypeProperty;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.ontology.FunctionalProperty;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.ontology.Individual;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.ontology.InverseFunctionalProperty;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.ontology.MaxCardinalityRestriction;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.ontology.MinCardinalityRestriction;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.ontology.ObjectProperty;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntClass;
/*  20:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  21:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntProperty;
/*  22:    */ import it.polimi.elet.contextaddict.microjena.ontology.Ontology;
/*  23:    */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*  24:    */ import it.polimi.elet.contextaddict.microjena.ontology.Restriction;
/*  25:    */ import it.polimi.elet.contextaddict.microjena.ontology.SomeValuesFromRestriction;
/*  26:    */ import it.polimi.elet.contextaddict.microjena.ontology.SymmetricProperty;
/*  27:    */ import it.polimi.elet.contextaddict.microjena.ontology.TransitiveProperty;
/*  28:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  29:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*  30:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  31:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  32:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*  33:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.OWL;
/*  34:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  35:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*  36:    */ 
/*  37:    */ public class OWLLiteProfile
/*  38:    */   extends OWLProfile
/*  39:    */ {
/*  40:    */   public Resource DATARANGE()
/*  41:    */   {
/*  42: 66 */     return null;
/*  43:    */   }
/*  44:    */   
/*  45:    */   public Resource NOTHING()
/*  46:    */   {
/*  47: 67 */     return null;
/*  48:    */   }
/*  49:    */   
/*  50:    */   public Property COMPLEMENT_OF()
/*  51:    */   {
/*  52: 68 */     return null;
/*  53:    */   }
/*  54:    */   
/*  55:    */   public Property DISJOINT_WITH()
/*  56:    */   {
/*  57: 69 */     return null;
/*  58:    */   }
/*  59:    */   
/*  60:    */   public Property HAS_VALUE()
/*  61:    */   {
/*  62: 70 */     return null;
/*  63:    */   }
/*  64:    */   
/*  65:    */   public Property ONE_OF()
/*  66:    */   {
/*  67: 71 */     return null;
/*  68:    */   }
/*  69:    */   
/*  70:    */   public Property UNION_OF()
/*  71:    */   {
/*  72: 72 */     return null;
/*  73:    */   }
/*  74:    */   
/*  75:    */   public Property SAME_AS()
/*  76:    */   {
/*  77: 73 */     return null;
/*  78:    */   }
/*  79:    */   
/*  80:    */   public Property SAME_INDIVIDUAL_AS()
/*  81:    */   {
/*  82: 74 */     return null;
/*  83:    */   }
/*  84:    */   
/*  85:    */   public String getLabel()
/*  86:    */   {
/*  87: 83 */     return "OWL Lite";
/*  88:    */   }
/*  89:    */   
/*  90: 86 */   protected static Object[][] s_supportsCheckData = { { AllDifferent.class, new OWLProfile.SupportsCheck()
/*  91:    */   {
/*  92:    */     public boolean doCheck(Node n, EnhGraph g)
/*  93:    */     {
/*  94: 90 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.AllDifferent.asNode());
/*  95:    */     }
/*  96: 86 */   } }, { AnnotationProperty.class, new OWLProfile.SupportsCheck()
/*  97:    */   {
/*  98:    */     public boolean doCheck(Node n, EnhGraph g)
/*  99:    */     {
/* 100: 96 */       for (Iterator i = ((OntModel)g).getProfile().getAnnotationProperties(); i.hasNext();) {
/* 101: 97 */         if (((Resource)i.next()).asNode().equals(n)) {
/* 102: 99 */           return true;
/* 103:    */         }
/* 104:    */       }
/* 105:102 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.AnnotationProperty.asNode());
/* 106:    */     }
/* 107: 86 */   } }, { OntClass.class, new OWLProfile.SupportsCheck()
/* 108:    */   {
/* 109:    */     public boolean doCheck(Node n, EnhGraph eg)
/* 110:    */     {
/* 111:108 */       Graph g = eg.asGraph();
/* 112:109 */       Node rdfTypeNode = RDF.type.asNode();
/* 113:110 */       return (g.contains(n, rdfTypeNode, OWL.Class.asNode())) || (g.contains(n, rdfTypeNode, OWL.Restriction.asNode())) || (g.contains(n, rdfTypeNode, RDFS.Class.asNode())) || (g.contains(n, rdfTypeNode, RDFS.Datatype.asNode())) || (n.equals(OWL.Thing.asNode())) || (g.contains(Node.ANY, RDFS.domain.asNode(), n)) || (g.contains(Node.ANY, RDFS.range.asNode(), n)) || (g.contains(n, OWL.intersectionOf.asNode(), Node.ANY));
/* 114:    */     }
/* 115: 86 */   } }, { DatatypeProperty.class, new OWLProfile.SupportsCheck()
/* 116:    */   {
/* 117:    */     public boolean doCheck(Node n, EnhGraph g)
/* 118:    */     {
/* 119:125 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode());
/* 120:    */     }
/* 121: 86 */   } }, { ObjectProperty.class, new OWLProfile.SupportsCheck()
/* 122:    */   {
/* 123:    */     public boolean doCheck(Node n, EnhGraph g)
/* 124:    */     {
/* 125:131 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.ObjectProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.TransitiveProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.SymmetricProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.InverseFunctionalProperty.asNode()));
/* 126:    */     }
/* 127: 86 */   } }, { FunctionalProperty.class, new OWLProfile.SupportsCheck()
/* 128:    */   {
/* 129:    */     public boolean doCheck(Node n, EnhGraph g)
/* 130:    */     {
/* 131:140 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.FunctionalProperty.asNode());
/* 132:    */     }
/* 133: 86 */   } }, { InverseFunctionalProperty.class, new OWLProfile.SupportsCheck()
/* 134:    */   {
/* 135:    */     public boolean doCheck(Node n, EnhGraph g)
/* 136:    */     {
/* 137:146 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.InverseFunctionalProperty.asNode())) && (!g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode()));
/* 138:    */     }
/* 139: 86 */   } }, { RDFList.class, new OWLProfile.SupportsCheck()
/* 140:    */   {
/* 141:    */     public boolean doCheck(Node n, EnhGraph g)
/* 142:    */     {
/* 143:153 */       return (n.equals(RDF.nil.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), RDF.List.asNode()));
/* 144:    */     }
/* 145: 86 */   } }, { OntProperty.class, new OWLProfile.SupportsCheck()
/* 146:    */   {
/* 147:    */     public boolean doCheck(Node n, EnhGraph g)
/* 148:    */     {
/* 149:160 */       return (g.asGraph().contains(n, RDF.type.asNode(), RDF.Property.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.ObjectProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.AnnotationProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.TransitiveProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.SymmetricProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.FunctionalProperty.asNode())) || (g.asGraph().contains(n, RDF.type.asNode(), OWL.InverseFunctionalProperty.asNode()));
/* 150:    */     }
/* 151: 86 */   } }, { Ontology.class, new OWLProfile.SupportsCheck()
/* 152:    */   {
/* 153:    */     public boolean doCheck(Node n, EnhGraph g)
/* 154:    */     {
/* 155:173 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.Ontology.asNode());
/* 156:    */     }
/* 157: 86 */   } }, { Restriction.class, new OWLProfile.SupportsCheck()
/* 158:    */   {
/* 159:    */     public boolean doCheck(Node n, EnhGraph g)
/* 160:    */     {
/* 161:179 */       return g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode());
/* 162:    */     }
/* 163: 86 */   } }, { AllValuesFromRestriction.class, new OWLProfile.SupportsCheck()
/* 164:    */   {
/* 165:    */     public boolean doCheck(Node n, EnhGraph g)
/* 166:    */     {
/* 167:185 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLLiteProfile.containsSome(g, n, OWL.allValuesFrom)) && (OWLLiteProfile.containsSome(g, n, OWL.onProperty));
/* 168:    */     }
/* 169: 86 */   } }, { SomeValuesFromRestriction.class, new OWLProfile.SupportsCheck()
/* 170:    */   {
/* 171:    */     public boolean doCheck(Node n, EnhGraph g)
/* 172:    */     {
/* 173:193 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLLiteProfile.containsSome(g, n, OWL.someValuesFrom)) && (OWLLiteProfile.containsSome(g, n, OWL.onProperty));
/* 174:    */     }
/* 175: 86 */   } }, { CardinalityRestriction.class, new OWLProfile.SupportsCheck()
/* 176:    */   {
/* 177:    */     public boolean doCheck(Node n, EnhGraph g)
/* 178:    */     {
/* 179:201 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLLiteProfile.containsSome(g, n, OWL.cardinality)) && (OWLLiteProfile.containsSome(g, n, OWL.onProperty));
/* 180:    */     }
/* 181: 86 */   } }, { MinCardinalityRestriction.class, new OWLProfile.SupportsCheck()
/* 182:    */   {
/* 183:    */     public boolean doCheck(Node n, EnhGraph g)
/* 184:    */     {
/* 185:209 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLLiteProfile.containsSome(g, n, OWL.minCardinality)) && (OWLLiteProfile.containsSome(g, n, OWL.onProperty));
/* 186:    */     }
/* 187: 86 */   } }, { MaxCardinalityRestriction.class, new OWLProfile.SupportsCheck()
/* 188:    */   {
/* 189:    */     public boolean doCheck(Node n, EnhGraph g)
/* 190:    */     {
/* 191:217 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.Restriction.asNode())) && (OWLLiteProfile.containsSome(g, n, OWL.maxCardinality)) && (OWLLiteProfile.containsSome(g, n, OWL.onProperty));
/* 192:    */     }
/* 193: 86 */   } }, { SymmetricProperty.class, new OWLProfile.SupportsCheck()
/* 194:    */   {
/* 195:    */     public boolean doCheck(Node n, EnhGraph g)
/* 196:    */     {
/* 197:225 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.SymmetricProperty.asNode())) && (!g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode()));
/* 198:    */     }
/* 199: 86 */   } }, { TransitiveProperty.class, new OWLProfile.SupportsCheck()
/* 200:    */   {
/* 201:    */     public boolean doCheck(Node n, EnhGraph g)
/* 202:    */     {
/* 203:232 */       return (g.asGraph().contains(n, RDF.type.asNode(), OWL.TransitiveProperty.asNode())) && (!g.asGraph().contains(n, RDF.type.asNode(), OWL.DatatypeProperty.asNode()));
/* 204:    */     }
/* 205: 86 */   } }, { Individual.class, new OWLProfile.SupportsCheck()
/* 206:    */   {
/* 207:    */     public boolean doCheck(Node n, EnhGraph eg)
/* 208:    */     {
/* 209:239 */       if (((n instanceof Node_URI)) || ((n instanceof Node_Blank)))
/* 210:    */       {
/* 211:241 */         Graph g = eg.asGraph();
/* 212:    */         
/* 213:    */ 
/* 214:244 */         return (!g.contains(n, RDF.type.asNode(), RDFS.Class.asNode())) && (!g.contains(n, RDF.type.asNode(), RDF.Property.asNode()));
/* 215:    */       }
/* 216:247 */       return false;
/* 217:    */     }
/* 218: 86 */   } } };
/* 219:    */   
/* 220:    */   public static boolean containsSome(EnhGraph g, Node n, Property p)
/* 221:    */   {
/* 222:256 */     return AbstractProfile.containsSome(g, n, p);
/* 223:    */   }
/* 224:    */   
/* 225:260 */   private static Map s_supportsChecks = new Map();
/* 226:    */   
/* 227:    */   static
/* 228:    */   {
/* 229:264 */     for (int i = 0; i < s_supportsCheckData.length; i++) {
/* 230:265 */       s_supportsChecks.put(s_supportsCheckData[i][0], s_supportsCheckData[i][1]);
/* 231:    */     }
/* 232:    */   }
/* 233:    */   
/* 234:    */   protected Map getCheckTable()
/* 235:    */   {
/* 236:270 */     return s_supportsChecks;
/* 237:    */   }
/* 238:    */ }
