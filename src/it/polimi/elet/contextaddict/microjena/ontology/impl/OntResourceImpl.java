/*    1:     */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*    2:     */ 
/*    3:     */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*    4:     */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*    5:     */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*    6:     */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*    7:     */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*    8:     */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*    9:     */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*   10:     */ import it.polimi.elet.contextaddict.microjena.ontology.OntProperty;
/*   11:     */ import it.polimi.elet.contextaddict.microjena.ontology.OntResource;
/*   12:     */ import it.polimi.elet.contextaddict.microjena.ontology.OntologyException;
/*   13:     */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*   14:     */ import it.polimi.elet.contextaddict.microjena.ontology.ProfileException;
/*   15:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
/*   16:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*   17:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.NodeIterator;
/*   18:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*   19:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*   20:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*   21:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*   22:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResourceFactory;
/*   23:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*   24:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*   25:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.ModelCom;
/*   26:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.NodeIteratorImpl;
/*   27:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.RDFListImpl;
/*   28:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.ResourceImpl;
/*   29:     */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*   30:     */ import it.polimi.elet.contextaddict.microjena.shared.PropertyNotFoundException;
/*   31:     */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*   32:     */ import it.polimi.elet.contextaddict.microjena.util.List;
/*   33:     */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*   34:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.ClosableIterator;
/*   35:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*   36:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*   37:     */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*   38:     */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*   39:     */ 
/*   40:     */ public class OntResourceImpl
/*   41:     */   extends ResourceImpl
/*   42:     */   implements OntResource
/*   43:     */ {
/*   44:  73 */   public static final String[] KNOWN_LANGUAGES = { "http://www.w3.org/2002/07/owl#", RDF.getURI(), RDFS.getURI(), "http://www.w3.org/2001/XMLSchema" };
/*   45:  83 */   public static Implementation factory = new Implementation()
/*   46:     */   {
/*   47:     */     public EnhNode wrap(Node n, EnhGraph eg)
/*   48:     */     {
/*   49:  85 */       if (canWrap(n, eg)) {
/*   50:  86 */         return new OntResourceImpl(n, eg);
/*   51:     */       }
/*   52:  88 */       throw new ConversionException("Cannot convert node " + n.toString() + " to OntResource");
/*   53:     */     }
/*   54:     */     
/*   55:     */     public boolean canWrap(Node node, EnhGraph eg)
/*   56:     */     {
/*   57:  94 */       return (node.isURI()) || (node.isBlank());
/*   58:     */     }
/*   59:     */   };
/*   60:     */   
/*   61:     */   public OntResourceImpl(Node n, EnhGraph g)
/*   62:     */   {
/*   63: 107 */     super(n, g);
/*   64:     */   }
/*   65:     */   
/*   66:     */   public OntModel getOntModel()
/*   67:     */   {
/*   68: 118 */     Model m = getModel();
/*   69: 119 */     return (m instanceof OntModel) ? (OntModel)m : null;
/*   70:     */   }
/*   71:     */   
/*   72:     */   public Profile getProfile()
/*   73:     */   {
/*   74:     */     try
/*   75:     */     {
/*   76: 134 */       return ((OntModel)getModel()).getProfile();
/*   77:     */     }
/*   78:     */     catch (ClassCastException e)
/*   79:     */     {
/*   80: 136 */       throw new JenaException("Resource " + toString() + " is not attached to an OntModel, so cannot access its language profile");
/*   81:     */     }
/*   82:     */   }
/*   83:     */   
/*   84:     */   public boolean isOntLanguageTerm()
/*   85:     */   {
/*   86: 148 */     if (!isAnon()) {
/*   87: 149 */       for (int i = 0; i < KNOWN_LANGUAGES.length; i++) {
/*   88: 150 */         if (getURI().startsWith(KNOWN_LANGUAGES[i])) {
/*   89: 151 */           return true;
/*   90:     */         }
/*   91:     */       }
/*   92:     */     }
/*   93: 155 */     return false;
/*   94:     */   }
/*   95:     */   
/*   96:     */   public void setSameAs(Resource res)
/*   97:     */   {
/*   98: 168 */     setPropertyValue(getProfile().SAME_AS(), "SAME_AS", res);
/*   99:     */   }
/*  100:     */   
/*  101:     */   public void addSameAs(Resource res)
/*  102:     */   {
/*  103: 177 */     addPropertyValue(getProfile().SAME_AS(), "SAME_AS", res);
/*  104:     */   }
/*  105:     */   
/*  106:     */   public OntResource getSameAs()
/*  107:     */   {
/*  108: 187 */     return objectAsResource(getProfile().SAME_AS(), "SAME_AS");
/*  109:     */   }
/*  110:     */   
/*  111:     */   public ExtendedIterator listSameAs()
/*  112:     */   {
/*  113: 198 */     return listAsOntResources(getProfile().SAME_AS());
/*  114:     */   }
/*  115:     */   
/*  116:     */   public boolean isSameAs(Resource res)
/*  117:     */   {
/*  118: 207 */     return hasPropertyValue(getProfile().SAME_AS(), "SAME_AS", res);
/*  119:     */   }
/*  120:     */   
/*  121:     */   public void removeSameAs(Resource res)
/*  122:     */   {
/*  123: 216 */     removePropertyValue(getProfile().SAME_AS(), "SAME_AS", res);
/*  124:     */   }
/*  125:     */   
/*  126:     */   public void setDifferentFrom(Resource res)
/*  127:     */   {
/*  128: 228 */     setPropertyValue(getProfile().DIFFERENT_FROM(), "DIFFERENT_FROM", res);
/*  129:     */   }
/*  130:     */   
/*  131:     */   public void addDifferentFrom(Resource res)
/*  132:     */   {
/*  133: 237 */     addPropertyValue(getProfile().DIFFERENT_FROM(), "DIFFERENT_FROM", res);
/*  134:     */   }
/*  135:     */   
/*  136:     */   public OntResource getDifferentFrom()
/*  137:     */   {
/*  138: 247 */     return objectAsResource(getProfile().DIFFERENT_FROM(), "DIFFERENT_FROM");
/*  139:     */   }
/*  140:     */   
/*  141:     */   public ExtendedIterator listDifferentFrom()
/*  142:     */   {
/*  143: 257 */     return listAsOntResources(getProfile().DIFFERENT_FROM());
/*  144:     */   }
/*  145:     */   
/*  146:     */   public boolean isDifferentFrom(Resource res)
/*  147:     */   {
/*  148: 266 */     return hasPropertyValue(getProfile().DIFFERENT_FROM(), "DIFFERENT_FROM", res);
/*  149:     */   }
/*  150:     */   
/*  151:     */   public void removeDifferentFrom(Resource res)
/*  152:     */   {
/*  153: 275 */     removePropertyValue(getProfile().DIFFERENT_FROM(), "DIFFERENT_FROM", res);
/*  154:     */   }
/*  155:     */   
/*  156:     */   public void setSeeAlso(Resource res)
/*  157:     */   {
/*  158: 286 */     setPropertyValue(getProfile().SEE_ALSO(), "SEE_ALSO", res);
/*  159:     */   }
/*  160:     */   
/*  161:     */   public void addSeeAlso(Resource res)
/*  162:     */   {
/*  163: 295 */     addPropertyValue(getProfile().SEE_ALSO(), "SEE_ALSO", res);
/*  164:     */   }
/*  165:     */   
/*  166:     */   public Resource getSeeAlso()
/*  167:     */   {
/*  168: 305 */     return objectAsResource(getProfile().SEE_ALSO(), "SEE_ALSO");
/*  169:     */   }
/*  170:     */   
/*  171:     */   public ExtendedIterator listSeeAlso()
/*  172:     */   {
/*  173: 315 */     checkProfile(getProfile().SEE_ALSO(), "SEE_ALSO");
/*  174: 316 */     Iterator i = listProperties(getProfile().SEE_ALSO());
/*  175: 317 */     List result = new List();
/*  176: 318 */     while (i.hasNext()) {
/*  177: 319 */       result.add(ObjectAsOntResource(i.next()));
/*  178:     */     }
/*  179: 320 */     return WrappedIterator.create(result.iterator());
/*  180:     */   }
/*  181:     */   
/*  182:     */   public boolean hasSeeAlso(Resource res)
/*  183:     */   {
/*  184: 329 */     return hasPropertyValue(getProfile().SEE_ALSO(), "SEE_ALSO", res);
/*  185:     */   }
/*  186:     */   
/*  187:     */   public void removeSeeAlso(Resource res)
/*  188:     */   {
/*  189: 339 */     removePropertyValue(getProfile().SEE_ALSO(), "SEE_ALSO", res);
/*  190:     */   }
/*  191:     */   
/*  192:     */   public void setIsDefinedBy(Resource res)
/*  193:     */   {
/*  194: 351 */     setPropertyValue(getProfile().IS_DEFINED_BY(), "IS_DEFINED_BY", res);
/*  195:     */   }
/*  196:     */   
/*  197:     */   public void addIsDefinedBy(Resource res)
/*  198:     */   {
/*  199: 360 */     addPropertyValue(getProfile().IS_DEFINED_BY(), "IS_DEFINED_BY", res);
/*  200:     */   }
/*  201:     */   
/*  202:     */   public Resource getIsDefinedBy()
/*  203:     */   {
/*  204: 370 */     return objectAsResource(getProfile().IS_DEFINED_BY(), "IS_DEFINED_BY");
/*  205:     */   }
/*  206:     */   
/*  207:     */   public ExtendedIterator listIsDefinedBy()
/*  208:     */   {
/*  209: 380 */     checkProfile(getProfile().IS_DEFINED_BY(), "IS_DEFINED_BY");
/*  210: 381 */     Iterator i = listProperties(getProfile().IS_DEFINED_BY());
/*  211: 382 */     List result = new List();
/*  212: 383 */     while (i.hasNext()) {
/*  213: 384 */       result.add(ObjectAsOntResource(i.next()));
/*  214:     */     }
/*  215: 385 */     return WrappedIterator.create(result.iterator());
/*  216:     */   }
/*  217:     */   
/*  218:     */   public boolean isDefinedBy(Resource res)
/*  219:     */   {
/*  220: 394 */     return hasPropertyValue(getProfile().IS_DEFINED_BY(), "IS_DEFINED_BY", res);
/*  221:     */   }
/*  222:     */   
/*  223:     */   public void removeDefinedBy(Resource res)
/*  224:     */   {
/*  225: 403 */     removePropertyValue(getProfile().IS_DEFINED_BY(), "IS_DEFINED_BY", res);
/*  226:     */   }
/*  227:     */   
/*  228:     */   public void setVersionInfo(String info)
/*  229:     */   {
/*  230: 416 */     checkProfile(getProfile().VERSION_INFO(), "VERSION_INFO");
/*  231: 417 */     removeAll(getProfile().VERSION_INFO());
/*  232: 418 */     addVersionInfo(info);
/*  233:     */   }
/*  234:     */   
/*  235:     */   public void addVersionInfo(String info)
/*  236:     */   {
/*  237: 427 */     checkProfile(getProfile().VERSION_INFO(), "VERSION_INFO");
/*  238: 428 */     addProperty(getProfile().VERSION_INFO(), getModel().createLiteral(info));
/*  239:     */   }
/*  240:     */   
/*  241:     */   public String getVersionInfo()
/*  242:     */   {
/*  243: 438 */     checkProfile(getProfile().VERSION_INFO(), "VERSION_INFO");
/*  244:     */     try
/*  245:     */     {
/*  246: 440 */       return getRequiredProperty(getProfile().VERSION_INFO()).getString();
/*  247:     */     }
/*  248:     */     catch (PropertyNotFoundException ignore) {}
/*  249: 442 */     return null;
/*  250:     */   }
/*  251:     */   
/*  252:     */   public ExtendedIterator listVersionInfo()
/*  253:     */   {
/*  254: 452 */     checkProfile(getProfile().VERSION_INFO(), "VERSION_INFO");
/*  255: 453 */     Iterator i = listProperties(getProfile().VERSION_INFO());
/*  256: 454 */     List result = new List();
/*  257: 455 */     while (i.hasNext()) {
/*  258: 456 */       result.add(ObjectAsOntResource(i.next()));
/*  259:     */     }
/*  260: 457 */     return WrappedIterator.create(result.iterator());
/*  261:     */   }
/*  262:     */   
/*  263:     */   public boolean hasVersionInfo(String info)
/*  264:     */   {
/*  265: 466 */     checkProfile(getProfile().VERSION_INFO(), "VERSION_INFO");
/*  266: 467 */     return hasProperty(getProfile().VERSION_INFO(), info);
/*  267:     */   }
/*  268:     */   
/*  269:     */   public void removeVersionInfo(String info)
/*  270:     */   {
/*  271: 477 */     checkProfile(getProfile().VERSION_INFO(), "VERSION_INFO");
/*  272: 478 */     Literal infoAsLiteral = ResourceFactory.createPlainLiteral(info);
/*  273: 479 */     getModel().remove(this, getProfile().VERSION_INFO(), infoAsLiteral);
/*  274:     */   }
/*  275:     */   
/*  276:     */   public void setLabel(String label, String lang)
/*  277:     */   {
/*  278: 492 */     checkProfile(getProfile().LABEL(), "LABEL");
/*  279: 493 */     removeAll(getProfile().LABEL());
/*  280: 494 */     addLabel(label, lang);
/*  281:     */   }
/*  282:     */   
/*  283:     */   public void addLabel(String label, String lang)
/*  284:     */   {
/*  285: 504 */     addLabel(getModel().createLiteral(label, lang));
/*  286:     */   }
/*  287:     */   
/*  288:     */   public void addLabel(Literal label)
/*  289:     */   {
/*  290: 513 */     addPropertyValue(getProfile().LABEL(), "LABEL", label);
/*  291:     */   }
/*  292:     */   
/*  293:     */   public String getLabel(String lang)
/*  294:     */   {
/*  295: 525 */     checkProfile(getProfile().LABEL(), "LABEL");
/*  296: 526 */     if ((lang == null) || (lang.length() == 0)) {
/*  297:     */       try
/*  298:     */       {
/*  299: 529 */         return getRequiredProperty(getProfile().LABEL()).getString();
/*  300:     */       }
/*  301:     */       catch (PropertyNotFoundException ignore)
/*  302:     */       {
/*  303: 531 */         return null;
/*  304:     */       }
/*  305:     */     }
/*  306: 535 */     return selectLang(listProperties(getProfile().LABEL()), lang);
/*  307:     */   }
/*  308:     */   
/*  309:     */   public ExtendedIterator listLabels(String lang)
/*  310:     */   {
/*  311: 546 */     checkProfile(getProfile().LABEL(), "LABEL");
/*  312: 547 */     Iterator i = listProperties(getProfile().LABEL());
/*  313: 548 */     Set result = new Set();
/*  314: 550 */     while (i.hasNext())
/*  315:     */     {
/*  316: 551 */       Literal aus = ((Statement)i.next()).getLiteral();
/*  317: 552 */       if (lang == null) {
/*  318: 553 */         result.add(aus);
/*  319: 555 */       } else if (aus.getLanguage().equals(lang)) {
/*  320: 556 */         result.add(aus);
/*  321:     */       }
/*  322:     */     }
/*  323: 558 */     return WrappedIterator.create(result.iterator());
/*  324:     */   }
/*  325:     */   
/*  326:     */   public boolean hasLabel(String label, String lang)
/*  327:     */   {
/*  328: 568 */     return hasLabel(getModel().createLiteral(label, lang));
/*  329:     */   }
/*  330:     */   
/*  331:     */   public boolean hasLabel(Literal label)
/*  332:     */   {
/*  333: 577 */     boolean found = false;
/*  334: 578 */     ExtendedIterator i = listLabels(label.getLanguage());
/*  335: 579 */     while ((!found) && (i.hasNext())) {
/*  336: 580 */       found = label.equals(i.next());
/*  337:     */     }
/*  338: 582 */     i.close();
/*  339: 583 */     return found;
/*  340:     */   }
/*  341:     */   
/*  342:     */   public void removeLabel(String label, String lang)
/*  343:     */   {
/*  344: 594 */     removeLabel(getModel().createLiteral(label, lang));
/*  345:     */   }
/*  346:     */   
/*  347:     */   public void removeLabel(Literal label)
/*  348:     */   {
/*  349: 604 */     removePropertyValue(getProfile().LABEL(), "LABEL", label);
/*  350:     */   }
/*  351:     */   
/*  352:     */   public void setComment(String comment, String lang)
/*  353:     */   {
/*  354: 617 */     checkProfile(getProfile().COMMENT(), "COMMENT");
/*  355: 618 */     removeAll(getProfile().COMMENT());
/*  356: 619 */     addComment(comment, lang);
/*  357:     */   }
/*  358:     */   
/*  359:     */   public void addComment(String comment, String lang)
/*  360:     */   {
/*  361: 629 */     addComment(getModel().createLiteral(comment, lang));
/*  362:     */   }
/*  363:     */   
/*  364:     */   public void addComment(Literal comment)
/*  365:     */   {
/*  366: 638 */     checkProfile(getProfile().COMMENT(), "COMMENT");
/*  367: 639 */     addProperty(getProfile().COMMENT(), comment);
/*  368:     */   }
/*  369:     */   
/*  370:     */   public String getComment(String lang)
/*  371:     */   {
/*  372: 651 */     checkProfile(getProfile().COMMENT(), "COMMENT");
/*  373: 652 */     if (lang == null) {
/*  374:     */       try
/*  375:     */       {
/*  376: 655 */         return getRequiredProperty(getProfile().COMMENT()).getString();
/*  377:     */       }
/*  378:     */       catch (PropertyNotFoundException ignore)
/*  379:     */       {
/*  380: 658 */         return null;
/*  381:     */       }
/*  382:     */     }
/*  383: 662 */     return selectLang(listProperties(getProfile().COMMENT()), lang);
/*  384:     */   }
/*  385:     */   
/*  386:     */   public ExtendedIterator listComments(String lang)
/*  387:     */   {
/*  388: 672 */     checkProfile(getProfile().COMMENT(), "COMMENT");
/*  389: 673 */     Iterator i = listProperties(getProfile().COMMENT());
/*  390: 674 */     Set result = new Set();
/*  391: 676 */     while (i.hasNext())
/*  392:     */     {
/*  393: 677 */       Literal aus = ((Statement)i.next()).getLiteral();
/*  394: 678 */       if (lang == null) {
/*  395: 679 */         result.add(aus);
/*  396: 681 */       } else if (aus.getLanguage().equals(lang)) {
/*  397: 682 */         result.add(aus);
/*  398:     */       }
/*  399:     */     }
/*  400: 684 */     return WrappedIterator.create(result.iterator());
/*  401:     */   }
/*  402:     */   
/*  403:     */   public boolean hasComment(String comment, String lang)
/*  404:     */   {
/*  405: 694 */     return hasComment(getModel().createLiteral(comment, lang));
/*  406:     */   }
/*  407:     */   
/*  408:     */   public boolean hasComment(Literal comment)
/*  409:     */   {
/*  410: 703 */     boolean found = false;
/*  411:     */     
/*  412: 705 */     ExtendedIterator i = listComments(comment.getLanguage());
/*  413: 706 */     while ((!found) && (i.hasNext())) {
/*  414: 707 */       found = comment.equals(i.next());
/*  415:     */     }
/*  416: 710 */     i.close();
/*  417: 711 */     return found;
/*  418:     */   }
/*  419:     */   
/*  420:     */   public void removeComment(String comment, String lang)
/*  421:     */   {
/*  422: 722 */     removeComment(getModel().createLiteral(comment, lang));
/*  423:     */   }
/*  424:     */   
/*  425:     */   public void removeComment(Literal comment)
/*  426:     */   {
/*  427: 732 */     removePropertyValue(getProfile().COMMENT(), "COMMENT", comment);
/*  428:     */   }
/*  429:     */   
/*  430:     */   public void setRDFType(Resource cls)
/*  431:     */   {
/*  432: 747 */     setPropertyValue(RDF.type, "rdf:type", cls);
/*  433:     */   }
/*  434:     */   
/*  435:     */   public void addRDFType(Resource cls)
/*  436:     */   {
/*  437: 756 */     addPropertyValue(RDF.type, "rdf:type", cls);
/*  438:     */   }
/*  439:     */   
/*  440:     */   public Resource getRDFType()
/*  441:     */   {
/*  442: 771 */     return getRDFType(false);
/*  443:     */   }
/*  444:     */   
/*  445:     */   public Resource getRDFType(boolean direct)
/*  446:     */   {
/*  447: 788 */     ExtendedIterator i = null;
/*  448:     */     try
/*  449:     */     {
/*  450: 790 */       i = listRDFTypes(direct);
/*  451: 791 */       return i.hasNext() ? (Resource)i.next() : null;
/*  452:     */     }
/*  453:     */     finally
/*  454:     */     {
/*  455: 793 */       i.close();
/*  456:     */     }
/*  457:     */   }
/*  458:     */   
/*  459:     */   private static ExtendedIterator createUniqueIterator(Iterator i)
/*  460:     */   {
/*  461: 798 */     Set result = new Set();
/*  462: 799 */     while (i.hasNext()) {
/*  463: 800 */       result.add(i.next());
/*  464:     */     }
/*  465: 801 */     return WrappedIterator.create(result.iterator());
/*  466:     */   }
/*  467:     */   
/*  468:     */   public ExtendedIterator listRDFTypes(boolean direct)
/*  469:     */   {
/*  470: 815 */     Set lower = new Set();
/*  471: 816 */     Set computed = new Set();
/*  472: 817 */     ExtendedIterator types = listProperties(RDF.type);
/*  473: 820 */     while (types.hasNext())
/*  474:     */     {
/*  475: 822 */       RDFNode aus = ((Statement)types.next()).getObject();
/*  476: 823 */       if (!computed.contains(aus))
/*  477:     */       {
/*  478: 824 */         lower.add(aus);
/*  479: 825 */         computed.add(aus);
/*  480: 826 */         catchSuperClasses(new OntResourceImpl(aus.asNode(), getModelCom()), lower, computed, getProfile().SUB_CLASS_OF());
/*  481:     */       }
/*  482:     */     }
/*  483: 830 */     if (direct) {
/*  484: 831 */       return WrappedIterator.create(lower.iterator());
/*  485:     */     }
/*  486: 833 */     return WrappedIterator.create(computed.iterator());
/*  487:     */   }
/*  488:     */   
/*  489:     */   protected void catchSuperClasses(RDFNode actualClass, Set lower, Set computed, Property relation)
/*  490:     */   {
/*  491: 844 */     StmtIterator superClasses = getModel().listStatements(new OntResourceImpl(actualClass.asNode(), getModelCom()), relation, (RDFNode)null);
/*  492: 846 */     while (superClasses.hasNext())
/*  493:     */     {
/*  494: 847 */       RDFNode aus = superClasses.nextStatement().getObject();
/*  495: 848 */       if (!computed.contains(aus))
/*  496:     */       {
/*  497: 849 */         computed.add(aus);
/*  498: 850 */         lower.remove(aus);
/*  499: 851 */         catchSuperClasses(aus, lower, computed, relation);
/*  500:     */       }
/*  501:     */       else
/*  502:     */       {
/*  503: 853 */         lower.remove(aus);
/*  504:     */       }
/*  505:     */     }
/*  506:     */   }
/*  507:     */   
/*  508:     */   public boolean hasRDFType(String uri)
/*  509:     */   {
/*  510: 867 */     return hasRDFType(getModel().getResource(uri));
/*  511:     */   }
/*  512:     */   
/*  513:     */   public boolean hasRDFType(Resource ontClass)
/*  514:     */   {
/*  515: 883 */     return hasRDFType(ontClass, "unknown", false);
/*  516:     */   }
/*  517:     */   
/*  518:     */   public boolean hasRDFType(Resource ontClass, boolean direct)
/*  519:     */   {
/*  520: 898 */     return hasRDFType(ontClass, "unknown", direct);
/*  521:     */   }
/*  522:     */   
/*  523:     */   protected boolean hasRDFType(Resource ontClass, String name, boolean direct)
/*  524:     */   {
/*  525: 902 */     checkProfile(ontClass, name);
/*  526: 903 */     if (!direct) {
/*  527: 905 */       return hasPropertyValue(RDF.type, "rdf:type", ontClass);
/*  528:     */     }
/*  529: 908 */     ExtendedIterator i = null;
/*  530:     */     try
/*  531:     */     {
/*  532: 910 */       i = listRDFTypes(true);
/*  533:     */       boolean bool;
/*  534: 911 */       while (i.hasNext()) {
/*  535: 912 */         if (ontClass.equals(i.next())) {
/*  536: 913 */           return true;
/*  537:     */         }
/*  538:     */       }
/*  539: 916 */       return false;
/*  540:     */     }
/*  541:     */     finally
/*  542:     */     {
/*  543: 918 */       i.close();
/*  544:     */     }
/*  545:     */   }
/*  546:     */   
/*  547:     */   public void removeRDFType(Resource cls)
/*  548:     */   {
/*  549: 929 */     removePropertyValue(RDF.type, "rdf:type", cls);
/*  550:     */   }
/*  551:     */   
/*  552:     */   public int getCardinality(Property p)
/*  553:     */   {
/*  554: 942 */     int n = 0;
/*  555: 943 */     for (Iterator i = createUniqueIterator(listPropertyValues(p)); i.hasNext(); n++) {
/*  556: 944 */       i.next();
/*  557:     */     }
/*  558: 946 */     return n;
/*  559:     */   }
/*  560:     */   
/*  561:     */   public void setPropertyValue(Property property, RDFNode value)
/*  562:     */   {
/*  563: 965 */     removeAll(property);
/*  564: 967 */     if (value != null) {
/*  565: 968 */       addProperty(property, value);
/*  566:     */     }
/*  567:     */   }
/*  568:     */   
/*  569:     */   public RDFNode getPropertyValue(Property property)
/*  570:     */   {
/*  571: 987 */     Statement s = getProperty(property);
/*  572: 988 */     if (s == null) {
/*  573: 989 */       return null;
/*  574:     */     }
/*  575: 991 */     return asOntResource(s.getObject());
/*  576:     */   }
/*  577:     */   
/*  578:     */   public NodeIterator listPropertyValues(Property property)
/*  579:     */   {
/*  580:1005 */     Iterator i = listProperties(property);
/*  581:1006 */     List result = new List();
/*  582:1007 */     while (i.hasNext()) {
/*  583:1008 */       result.add(ObjectAsOntResource(i.next()));
/*  584:     */     }
/*  585:1009 */     return new NodeIteratorImpl(result.iterator(), null);
/*  586:     */   }
/*  587:     */   
/*  588:     */   public void remove()
/*  589:     */   {
/*  590:1029 */     Set stmts = new Set();
/*  591:1030 */     List lists = new List();
/*  592:1031 */     List skip = new List();
/*  593:1032 */     Property first = RDF.first;
/*  594:1034 */     for (StmtIterator i = listProperties(); i.hasNext();) {
/*  595:1035 */       stmts.add(i.next());
/*  596:     */     }
/*  597:1037 */     for (StmtIterator i = getModel().listStatements(null, null, this); i.hasNext();) {
/*  598:1038 */       stmts.add(i.next());
/*  599:     */     }
/*  600:1041 */     for (Iterator i = stmts.iterator(); i.hasNext();)
/*  601:     */     {
/*  602:1042 */       Statement s = (Statement)i.next();
/*  603:1043 */       if ((s.getPredicate().equals(first)) && (s.getObject().equals(this)))
/*  604:     */       {
/*  605:1046 */         skip.add(s);
/*  606:     */       }
/*  607:1050 */       else if (ResourceImpl.factory.canWrap(s.getObject().asNode(), getGraph()))
/*  608:     */       {
/*  609:1052 */         Resource obj = s.getResource();
/*  610:1054 */         if (RDFListImpl.factory.canWrap(obj.asNode(), getGraph())) {
/*  611:1056 */           lists.add(obj);
/*  612:     */         }
/*  613:     */       }
/*  614:     */     }
/*  615:1061 */     for (Iterator i = lists.iterator(); i.hasNext();)
/*  616:     */     {
/*  617:1062 */       Resource r = (Resource)i.next();
/*  618:1063 */       Iterator aus = ((RDFListImpl)r).collectStatements().iterator();
/*  619:1064 */       while (aus.hasNext()) {
/*  620:1065 */         stmts.add(aus.next());
/*  621:     */       }
/*  622:     */     }
/*  623:1068 */     Iterator j = skip.iterator();
/*  624:1069 */     while (j.hasNext()) {
/*  625:1070 */       stmts.remove(j.next());
/*  626:     */     }
/*  627:1072 */     for (Iterator i = stmts.iterator(); i.hasNext();) {
/*  628:1073 */       ((Statement)i.next()).remove();
/*  629:     */     }
/*  630:     */   }
/*  631:     */   
/*  632:     */   public void removeProperty(Property property, RDFNode value)
/*  633:     */   {
/*  634:1084 */     getModel().remove(this, property, value);
/*  635:     */   }
/*  636:     */   
/*  637:     */   public boolean isAnnotationProperty()
/*  638:     */   {
/*  639:1095 */     return (getProfile().ANNOTATION_PROPERTY() != null) && (AnnotationPropertyImpl.factory.canWrap(asNode(), getModelCom()));
/*  640:     */   }
/*  641:     */   
/*  642:     */   public boolean isProperty()
/*  643:     */   {
/*  644:1103 */     return OntPropertyImpl.factory.canWrap(asNode(), getModelCom());
/*  645:     */   }
/*  646:     */   
/*  647:     */   public boolean isObjectProperty()
/*  648:     */   {
/*  649:1111 */     return (getProfile().OBJECT_PROPERTY() != null) && (ObjectPropertyImpl.factory.canWrap(asNode(), getModelCom()));
/*  650:     */   }
/*  651:     */   
/*  652:     */   public boolean isDatatypeProperty()
/*  653:     */   {
/*  654:1119 */     return (getProfile().DATATYPE_PROPERTY() != null) && (DatatypePropertyImpl.factory.canWrap(asNode(), getModelCom()));
/*  655:     */   }
/*  656:     */   
/*  657:     */   public boolean isIndividual()
/*  658:     */   {
    /*  674:     */   Resource rType;
/*  675:     */       boolean bool;
/*  659:1127 */     StmtIterator i = null;StmtIterator j = null;
/*  660:     */     try
/*  661:     */     {
/*  662:1131 */       for (i = listProperties(RDF.type); i.hasNext();)
/*  663:     */       {
/*  664:1132 */         rType = i.nextStatement().getResource();
/*  665:1133 */         if (rType.equals(getProfile().THING())) {
/*  666:1135 */           return true;
/*  667:     */         }
/*  668:1137 */         for (j = rType.listProperties(RDF.type); j.hasNext();) {
/*  669:1138 */           if (j.nextStatement().getResource().equals(getProfile().CLASS())) {
/*  670:1141 */             return true;
/*  671:     */           }
/*  672:     */         }
/*  673:     */       }

/*  676:1147 */       return false;
/*  677:     */     }
/*  678:     */     finally
/*  679:     */     {
/*  680:1150 */       if (i != null) {
/*  681:1150 */         i.close();
/*  682:     */       }
/*  683:1151 */       if (j != null) {
/*  684:1151 */         j.close();
/*  685:     */       }
/*  686:     */     }
/*  687:     */   }
/*  688:     */   
/*  689:     */   public boolean isClass()
/*  690:     */   {
/*  691:1160 */     return OntClassImpl.factory.canWrap(asNode(), getModelCom());
/*  692:     */   }
/*  693:     */   
/*  694:     */   public boolean isOntology()
/*  695:     */   {
/*  696:1168 */     return (getProfile().ONTOLOGY() != null) && (OntologyImpl.factory.canWrap(asNode(), getModelCom()));
/*  697:     */   }
/*  698:     */   
/*  699:     */   public boolean isDataRange()
/*  700:     */   {
/*  701:1176 */     return (getProfile().DATARANGE() != null) && (DataRangeImpl.factory.canWrap(asNode(), getModelCom()));
/*  702:     */   }
/*  703:     */   
/*  704:     */   public boolean isAllDifferent()
/*  705:     */   {
/*  706:1184 */     return (getProfile().ALL_DIFFERENT() != null) && (AllDifferentImpl.factory.canWrap(asNode(), getModelCom()));
/*  707:     */   }
/*  708:     */   
/*  709:     */   protected static boolean hasType(Node n, EnhGraph g, Resource type)
/*  710:     */   {
/*  711:1189 */     boolean hasType = false;
/*  712:1190 */     ClosableIterator i = g.asGraph().find(n, RDF.type.asNode(), type.asNode());
/*  713:1191 */     hasType = i.hasNext();
/*  714:1192 */     i.close();
/*  715:1193 */     return hasType;
/*  716:     */   }
/*  717:     */   
/*  718:     */   protected void checkProfile(Object term, String name)
/*  719:     */   {
/*  720:1203 */     if (term == null) {
/*  721:1204 */       throw new ProfileException(name, getProfile());
/*  722:     */     }
/*  723:     */   }
/*  724:     */   
/*  725:     */   protected String selectLang(StmtIterator stmts, String lang)
/*  726:     */   {
/*  727:1216 */     String found = null;
/*  728:1217 */     while (stmts.hasNext())
/*  729:     */     {
/*  730:1218 */       RDFNode n = stmts.nextStatement().getObject();
/*  731:1219 */       if ((n instanceof Literal))
/*  732:     */       {
/*  733:1220 */         Literal l = (Literal)n;
/*  734:1221 */         String lLang = l.getLanguage();
/*  735:1223 */         if (lang.equalsIgnoreCase(lLang))
/*  736:     */         {
/*  737:1225 */           found = l.getString();
/*  738:1226 */           break;
/*  739:     */         }
/*  740:1227 */         if ((lLang != null) && (lLang.length() > 1) && (lang.equalsIgnoreCase(lLang.substring(0, 2)))) {
/*  741:1230 */           found = l.getString();
/*  742:1231 */         } else if ((found == null) && (lLang == null)) {
/*  743:1233 */           found = l.getString();
/*  744:     */         }
/*  745:     */       }
/*  746:     */     }
/*  747:1237 */     stmts.close();
/*  748:1238 */     return found;
/*  749:     */   }
/*  750:     */   
/*  751:     */   protected boolean langTagMatch(String desired, String target)
/*  752:     */   {
/*  753:1243 */     return (desired == null) || (desired.equalsIgnoreCase(target)) || ((target.length() > desired.length()) && (desired.equalsIgnoreCase(target.substring(desired.length()))));
/*  754:     */   }
/*  755:     */   
/*  756:     */   protected RDFNode catchRequiredProperty(Property p)
/*  757:     */   {
/*  758:     */     try
/*  759:     */     {
/*  760:1250 */       return getRequiredProperty(p).getObject();
/*  761:     */     }
/*  762:     */     catch (PropertyNotFoundException e) {}
/*  763:1252 */     return null;
/*  764:     */   }
/*  765:     */   
/*  766:     */   protected OntResource objectAsResource(Property p, String name)
/*  767:     */   {
/*  768:1258 */     return (OntResource)factory.wrap(catchRequiredProperty(p).asNode(), (ModelCom)getModel());
/*  769:     */   }
/*  770:     */   
/*  771:     */   protected OntProperty objectAsProperty(Property p, String name)
/*  772:     */   {
/*  773:1264 */     return (OntProperty)OntPropertyImpl.factory.wrap(catchRequiredProperty(p).asNode(), (ModelCom)getModel());
/*  774:     */   }
/*  775:     */   
/*  776:     */   protected int objectAsInt(Property p, String name)
/*  777:     */   {
/*  778:1269 */     checkProfile(p, name);
/*  779:1270 */     return getRequiredProperty(p).getInt();
/*  780:     */   }
/*  781:     */   
/*  782:     */   protected ExtendedIterator listAsOntResources(Property p)
/*  783:     */   {
/*  784:1275 */     Iterator i = listProperties(p);
/*  785:1276 */     List result = new List();
/*  786:1277 */     while (i.hasNext()) {
/*  787:1278 */       result.add((OntResource)factory.wrap(((Statement)i.next()).getObject().asNode(), (ModelCom)getModel()));
/*  788:     */     }
/*  789:1279 */     return WrappedIterator.create(result.iterator());
/*  790:     */   }
/*  791:     */   
/*  792:     */   protected void addPropertyValue(Property p, String name, RDFNode value)
/*  793:     */   {
/*  794:1284 */     checkProfile(p, name);
/*  795:1285 */     addProperty(p, value);
/*  796:     */   }
/*  797:     */   
/*  798:     */   protected void setPropertyValue(Property p, String name, RDFNode value)
/*  799:     */   {
/*  800:1290 */     checkProfile(p, name);
/*  801:1291 */     removeAll(p);
/*  802:1292 */     addProperty(p, value);
/*  803:     */   }
/*  804:     */   
/*  805:     */   protected boolean hasPropertyValue(Property p, String name, RDFNode value)
/*  806:     */   {
/*  807:1297 */     checkProfile(p, name);
/*  808:1298 */     return hasProperty(p, value);
/*  809:     */   }
/*  810:     */   
/*  811:     */   protected void addListPropertyValue(Property p, String name, RDFNode value)
/*  812:     */   {
/*  813:1303 */     checkProfile(p, name);
/*  814:1305 */     if (hasProperty(p))
/*  815:     */     {
/*  816:1306 */       RDFNode cur = getRequiredProperty(p).getObject();
/*  817:1307 */       if (!getModel().getGraph().contains(cur.asNode(), RDF.type.asNode(), RDF.List.asNode())) {
/*  818:1308 */         throw new OntologyException("Tried to add a value to a list-valued property " + p + " but the current value is not a list: " + cur);
/*  819:     */       }
/*  820:1311 */       RDFList values = new RDFListImpl(cur.asNode(), (ModelCom)getModel());
/*  821:1313 */       if (!values.contains(value))
/*  822:     */       {
/*  823:1314 */         RDFList newValues = values.with(value);
/*  824:1316 */         if (newValues != values)
/*  825:     */         {
/*  826:1317 */           removeAll(p);
/*  827:1318 */           addProperty(p, newValues);
/*  828:     */         }
/*  829:     */       }
/*  830:     */     }
/*  831:     */     else
/*  832:     */     {
/*  833:1323 */       addProperty(p, ((OntModel)getModel()).createList(new RDFNode[] { value }));
/*  834:     */     }
/*  835:     */   }
/*  836:     */   
/*  837:     */   protected void removePropertyValue(Property prop, String name, RDFNode value)
/*  838:     */   {
/*  839:1329 */     checkProfile(prop, name);
/*  840:1330 */     getModel().remove(this, prop, value);
/*  841:     */   }
/*  842:     */   
/*  843:     */   private RDFNode asOntResource(RDFNode n)
/*  844:     */   {
/*  845:1335 */     return new ResourceImpl(n.asNode(), getModelCom());
/*  846:     */   }
/*  847:     */   
/*  848:     */   protected Object ObjectAsOntResource(Object x)
/*  849:     */   {
/*  850:1339 */     if ((x instanceof Statement)) {
/*  851:1340 */       return asOntResource(((Statement)x).getObject());
/*  852:     */     }
/*  853:1342 */     return x;
/*  854:     */   }
/*  855:     */ }
