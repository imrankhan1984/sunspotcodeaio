/*  1:   */ package it.polimi.elet.contextaddict.microjena.ontology.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  9:   */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/* 10:   */ import it.polimi.elet.contextaddict.microjena.ontology.SymmetricProperty;
/* 11:   */ 
/* 12:   */ public class SymmetricPropertyImpl
/* 13:   */   extends ObjectPropertyImpl
/* 14:   */   implements SymmetricProperty
/* 15:   */ {
/* 16:47 */   public static Implementation factory = new Implementation()
/* 17:   */   {
/* 18:   */     public EnhNode wrap(Node n, EnhGraph eg)
/* 19:   */     {
/* 20:49 */       if (canWrap(n, eg)) {
/* 21:50 */         return new SymmetricPropertyImpl(n, eg);
/* 22:   */       }
/* 23:52 */       throw new ConversionException("Cannot convert node " + n + " to SymmetricProperty");
/* 24:   */     }
/* 25:   */     
/* 26:   */     public boolean canWrap(Node node, EnhGraph eg)
/* 27:   */     {
/* 28:57 */       Profile profile = (eg instanceof OntModel) ? ((OntModel)eg).getProfile() : null;
/* 29:58 */       return (profile != null) && (profile.isSupported(node, eg, SymmetricProperty.class));
/* 30:   */     }
/* 31:   */   };
/* 32:   */   
/* 33:   */   public SymmetricPropertyImpl(Node n, EnhGraph g)
/* 34:   */   {
/* 35:71 */     super(n, g);
/* 36:   */   }
/* 37:   */ }

