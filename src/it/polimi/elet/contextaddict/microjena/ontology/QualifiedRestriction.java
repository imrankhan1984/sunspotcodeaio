package it.polimi.elet.contextaddict.microjena.ontology;

public abstract interface QualifiedRestriction
  extends Restriction
{
  public abstract void setHasClassQ(OntClass paramOntClass);
  
  public abstract OntResource getHasClassQ();
  
  public abstract boolean hasHasClassQ(OntClass paramOntClass);
  
  public abstract boolean hasHasClassQ(DataRange paramDataRange);
  
  public abstract void removeHasClassQ(OntClass paramOntClass);
  
  public abstract void removeHasClassQ(DataRange paramDataRange);
}

