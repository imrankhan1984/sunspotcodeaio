package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
import it.polimi.elet.contextaddict.microjena.rdf.model.NodeIterator;
import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface OntResource
  extends Resource
{
  public abstract OntModel getOntModel();
  
  public abstract Profile getProfile();
  
  public abstract boolean isOntLanguageTerm();
  
  public abstract void setSameAs(Resource paramResource);
  
  public abstract void addSameAs(Resource paramResource);
  
  public abstract OntResource getSameAs();
  
  public abstract ExtendedIterator listSameAs();
  
  public abstract boolean isSameAs(Resource paramResource);
  
  public abstract void removeSameAs(Resource paramResource);
  
  public abstract void setDifferentFrom(Resource paramResource);
  
  public abstract void addDifferentFrom(Resource paramResource);
  
  public abstract OntResource getDifferentFrom();
  
  public abstract ExtendedIterator listDifferentFrom();
  
  public abstract boolean isDifferentFrom(Resource paramResource);
  
  public abstract void removeDifferentFrom(Resource paramResource);
  
  public abstract void setSeeAlso(Resource paramResource);
  
  public abstract void addSeeAlso(Resource paramResource);
  
  public abstract Resource getSeeAlso();
  
  public abstract ExtendedIterator listSeeAlso();
  
  public abstract boolean hasSeeAlso(Resource paramResource);
  
  public abstract void removeSeeAlso(Resource paramResource);
  
  public abstract void setIsDefinedBy(Resource paramResource);
  
  public abstract void addIsDefinedBy(Resource paramResource);
  
  public abstract Resource getIsDefinedBy();
  
  public abstract ExtendedIterator listIsDefinedBy();
  
  public abstract boolean isDefinedBy(Resource paramResource);
  
  public abstract void removeDefinedBy(Resource paramResource);
  
  public abstract void setVersionInfo(String paramString);
  
  public abstract void addVersionInfo(String paramString);
  
  public abstract String getVersionInfo();
  
  public abstract ExtendedIterator listVersionInfo();
  
  public abstract boolean hasVersionInfo(String paramString);
  
  public abstract void removeVersionInfo(String paramString);
  
  public abstract void setLabel(String paramString1, String paramString2);
  
  public abstract void addLabel(String paramString1, String paramString2);
  
  public abstract void addLabel(Literal paramLiteral);
  
  public abstract String getLabel(String paramString);
  
  public abstract ExtendedIterator listLabels(String paramString);
  
  public abstract boolean hasLabel(String paramString1, String paramString2);
  
  public abstract boolean hasLabel(Literal paramLiteral);
  
  public abstract void removeLabel(String paramString1, String paramString2);
  
  public abstract void removeLabel(Literal paramLiteral);
  
  public abstract void setComment(String paramString1, String paramString2);
  
  public abstract void addComment(String paramString1, String paramString2);
  
  public abstract void addComment(Literal paramLiteral);
  
  public abstract String getComment(String paramString);
  
  public abstract ExtendedIterator listComments(String paramString);
  
  public abstract boolean hasComment(String paramString1, String paramString2);
  
  public abstract boolean hasComment(Literal paramLiteral);
  
  public abstract void removeComment(String paramString1, String paramString2);
  
  public abstract void removeComment(Literal paramLiteral);
  
  public abstract void setRDFType(Resource paramResource);
  
  public abstract void addRDFType(Resource paramResource);
  
  public abstract Resource getRDFType();
  
  public abstract Resource getRDFType(boolean paramBoolean);
  
  public abstract ExtendedIterator listRDFTypes(boolean paramBoolean);
  
  public abstract boolean hasRDFType(Resource paramResource, boolean paramBoolean);
  
  public abstract boolean hasRDFType(Resource paramResource);
  
  public abstract void removeRDFType(Resource paramResource);
  
  public abstract boolean hasRDFType(String paramString);
  
  public abstract int getCardinality(Property paramProperty);
  
  public abstract void setPropertyValue(Property paramProperty, RDFNode paramRDFNode);
  
  public abstract RDFNode getPropertyValue(Property paramProperty);
  
  public abstract NodeIterator listPropertyValues(Property paramProperty);
  
  public abstract void removeProperty(Property paramProperty, RDFNode paramRDFNode);
  
  public abstract void remove();
  
  public abstract boolean isProperty();
  
  public abstract boolean isAnnotationProperty();
  
  public abstract boolean isObjectProperty();
  
  public abstract boolean isDatatypeProperty();
  
  public abstract boolean isIndividual();
  
  public abstract boolean isClass();
  
  public abstract boolean isOntology();
  
  public abstract boolean isDataRange();
  
  public abstract boolean isAllDifferent();
}

