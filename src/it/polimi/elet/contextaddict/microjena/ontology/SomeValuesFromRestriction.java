package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;

public abstract interface SomeValuesFromRestriction
  extends Restriction
{
  public abstract void setSomeValuesFrom(Resource paramResource);
  
  public abstract Resource getSomeValuesFrom();
  
  public abstract boolean hasSomeValuesFrom(Resource paramResource);
  
  public abstract void removeSomeValuesFrom(Resource paramResource);
}
