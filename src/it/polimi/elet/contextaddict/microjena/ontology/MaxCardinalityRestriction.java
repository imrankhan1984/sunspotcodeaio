package it.polimi.elet.contextaddict.microjena.ontology;

public abstract interface MaxCardinalityRestriction
  extends Restriction
{
  public abstract void setMaxCardinality(int paramInt);
  
  public abstract int getMaxCardinality();
  
  public abstract boolean hasMaxCardinality(int paramInt);
  
  public abstract void removeMaxCardinality(int paramInt);
}


