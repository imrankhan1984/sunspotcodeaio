/*  1:   */ package it.polimi.elet.contextaddict.microjena.ontology;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  4:   */ 
/*  5:   */ public class OntologyException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public OntologyException(String msg)
/*  9:   */   {
/* 10:41 */     super(msg);
/* 11:   */   }
/* 12:   */ }


