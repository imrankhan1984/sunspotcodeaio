package it.polimi.elet.contextaddict.microjena.ontology;

public abstract interface MinCardinalityQRestriction
  extends QualifiedRestriction
{
  public abstract void setMinCardinalityQ(int paramInt);
  
  public abstract int getMinCardinalityQ();
  
  public abstract boolean hasMinCardinalityQ(int paramInt);
  
  public abstract void removeMinCardinalityQ(int paramInt);
}


