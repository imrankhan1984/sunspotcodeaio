package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Property;

public abstract interface Restriction
  extends OntClass
{
  public abstract void setOnProperty(Property paramProperty);
  
  public abstract OntProperty getOnProperty();
  
  public abstract boolean onProperty(Property paramProperty);
  
  public abstract void removeOnProperty(Property paramProperty);
  
  public abstract boolean isAllValuesFromRestriction();
  
  public abstract boolean isSomeValuesFromRestriction();
  
  public abstract boolean isHasValueRestriction();
  
  public abstract boolean isCardinalityRestriction();
  
  public abstract boolean isMinCardinalityRestriction();
  
  public abstract boolean isMaxCardinalityRestriction();
}
