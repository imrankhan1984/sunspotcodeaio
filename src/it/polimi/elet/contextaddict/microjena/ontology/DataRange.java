package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
import it.polimi.elet.contextaddict.microjena.util.Iterator;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface DataRange
  extends OntResource
{
  public abstract void setOneOf(RDFList paramRDFList);
  
  public abstract void addOneOf(Literal paramLiteral);
  
  public abstract void addOneOf(Iterator paramIterator);
  
  public abstract RDFList getOneOf();
  
  public abstract ExtendedIterator listOneOf();
  
  public abstract boolean hasOneOf(Literal paramLiteral);
  
  public abstract void removeOneOf(Literal paramLiteral);
}


