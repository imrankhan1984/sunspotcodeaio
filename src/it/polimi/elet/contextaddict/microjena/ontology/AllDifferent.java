package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.util.Iterator;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface AllDifferent
  extends OntResource
{
  public abstract void setDistinctMembers(RDFList paramRDFList);
  
  public abstract void addDistinctMember(Resource paramResource);
  
  public abstract void addDistinctMembers(Iterator paramIterator);
  
  public abstract RDFList getDistinctMembers();
  
  public abstract ExtendedIterator listDistinctMembers();
  
  public abstract boolean hasDistinctMember(Resource paramResource);
  
  public abstract void removeDistinctMember(Resource paramResource);
}

