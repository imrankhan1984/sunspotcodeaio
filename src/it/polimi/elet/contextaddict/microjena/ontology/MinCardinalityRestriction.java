package it.polimi.elet.contextaddict.microjena.ontology;

public abstract interface MinCardinalityRestriction
  extends Restriction
{
  public abstract void setMinCardinality(int paramInt);
  
  public abstract int getMinCardinality();
  
  public abstract boolean hasMinCardinality(int paramInt);
  
  public abstract void removeMinCardinality(int paramInt);
}

