/*  1:   */ package it.polimi.elet.contextaddict.microjena.ontology;
/*  2:   */ 
/*  3:   */ public class ConversionException
/*  4:   */   extends OntologyException
/*  5:   */ {
/*  6:   */   public ConversionException(String msg)
/*  7:   */   {
/*  8:43 */     super(msg);
/*  9:   */   }
/* 10:   */ }


