package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface OntClass
  extends OntResource
{
  public abstract void setSuperClass(Resource paramResource);
  
  public abstract void addSuperClass(Resource paramResource);
  
  public abstract OntClass getSuperClass();
  
  public abstract ExtendedIterator listSuperClasses();
  
  public abstract ExtendedIterator listSuperClasses(boolean paramBoolean);
  
  public abstract boolean hasSuperClass(Resource paramResource);
  
  public abstract boolean hasSuperClass();
  
  public abstract boolean hasSuperClass(Resource paramResource, boolean paramBoolean);
  
  public abstract void removeSuperClass(Resource paramResource);
  
  public abstract void setSubClass(Resource paramResource);
  
  public abstract void addSubClass(Resource paramResource);
  
  public abstract OntClass getSubClass();
  
  public abstract ExtendedIterator listSubClasses();
  
  public abstract ExtendedIterator listSubClasses(boolean paramBoolean);
  
  public abstract boolean hasSubClass(Resource paramResource);
  
  public abstract boolean hasSubClass();
  
  public abstract boolean hasSubClass(Resource paramResource, boolean paramBoolean);
  
  public abstract void removeSubClass(Resource paramResource);
  
  public abstract void setEquivalentClass(Resource paramResource);
  
  public abstract void addEquivalentClass(Resource paramResource);
  
  public abstract OntClass getEquivalentClass();
  
  public abstract ExtendedIterator listEquivalentClasses();
  
  public abstract boolean hasEquivalentClass(Resource paramResource);
  
  public abstract void removeEquivalentClass(Resource paramResource);
  
  public abstract void setDisjointWith(Resource paramResource);
  
  public abstract void addDisjointWith(Resource paramResource);
  
  public abstract OntClass getDisjointWith();
  
  public abstract ExtendedIterator listDisjointWith();
  
  public abstract boolean isDisjointWith(Resource paramResource);
  
  public abstract void removeDisjointWith(Resource paramResource);
  
  public abstract ExtendedIterator listDeclaredProperties();
  
  public abstract ExtendedIterator listDeclaredProperties(boolean paramBoolean);
  
  public abstract boolean hasDeclaredProperty(Property paramProperty, boolean paramBoolean);
  
  public abstract ExtendedIterator listInstances();
  
  public abstract ExtendedIterator listInstances(boolean paramBoolean);
  
  public abstract Individual createIndividual();
  
  public abstract Individual createIndividual(String paramString);
  
  public abstract boolean isHierarchyRoot();
  
  public abstract boolean isEnumeratedClass();
  
  public abstract boolean isUnionClass();
  
  public abstract boolean isIntersectionClass();
  
  public abstract boolean isComplementClass();
  
  public abstract boolean isRestriction();
}

