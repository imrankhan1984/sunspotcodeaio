package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface Ontology
  extends OntResource
{
  public abstract void setImport(Resource paramResource);
  
  public abstract void addImport(Resource paramResource);
  
  public abstract OntResource getImport();
  
  public abstract ExtendedIterator listImports();
  
  public abstract boolean imports(Resource paramResource);
  
  public abstract void removeImport(Resource paramResource);
  
  public abstract void setBackwardCompatibleWith(Resource paramResource);
  
  public abstract void addBackwardCompatibleWith(Resource paramResource);
  
  public abstract OntResource getBackwardCompatibleWith();
  
  public abstract ExtendedIterator listBackwardCompatibleWith();
  
  public abstract boolean isBackwardCompatibleWith(Resource paramResource);
  
  public abstract void removeBackwardCompatibleWith(Resource paramResource);
  
  public abstract void setPriorVersion(Resource paramResource);
  
  public abstract void addPriorVersion(Resource paramResource);
  
  public abstract OntResource getPriorVersion();
  
  public abstract ExtendedIterator listPriorVersion();
  
  public abstract boolean hasPriorVersion(Resource paramResource);
  
  public abstract void removePriorVersion(Resource paramResource);
  
  public abstract void setIncompatibleWith(Resource paramResource);
  
  public abstract void addIncompatibleWith(Resource paramResource);
  
  public abstract OntResource getIncompatibleWith();
  
  public abstract ExtendedIterator listIncompatibleWith();
  
  public abstract boolean isIncompatibleWith(Resource paramResource);
  
  public abstract void removeIncompatibleWith(Resource paramResource);
}
