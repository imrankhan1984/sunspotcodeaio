package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface Individual
  extends OntResource
{
  /**
   * @deprecated
   */
  public abstract void setSameIndividualAs(Resource paramResource);
  
  /**
   * @deprecated
   */
  public abstract void addSameIndividualAs(Resource paramResource);
  
  /**
   * @deprecated
   */
  public abstract OntResource getSameIndividualAs();
  
  /**
   * @deprecated
   */
  public abstract ExtendedIterator listSameIndividualAs();
  
  /**
   * @deprecated
   */
  public abstract boolean isSameIndividualAs(Resource paramResource);
  
  /**
   * @deprecated
   */
  public abstract void removeSameIndividualAs(Resource paramResource);
}

