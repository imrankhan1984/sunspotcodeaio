/*  1:   */ package it.polimi.elet.contextaddict.microjena.ontology;
/*  2:   */ 
/*  3:   */ public class ProfileException
/*  4:   */   extends OntologyException
/*  5:   */ {
/*  6:   */   public ProfileException(String element, Profile profile)
/*  7:   */   {
/*  8:40 */     super("Attempted to use language construct " + element + " that is not supported in the current language profile: " + (profile == null ? "not specified" : profile.getLabel()));
/*  9:   */   }
/* 10:   */ }

