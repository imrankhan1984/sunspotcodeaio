package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.util.Iterator;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface EnumeratedClass
  extends OntClass
{
  public abstract void setOneOf(RDFList paramRDFList);
  
  public abstract void addOneOf(Resource paramResource);
  
  public abstract void addOneOf(Iterator paramIterator);
  
  public abstract RDFList getOneOf();
  
  public abstract ExtendedIterator listOneOf();
  
  public abstract boolean hasOneOf(Resource paramResource);
  
  public abstract void removeOneOf(Resource paramResource);
}

