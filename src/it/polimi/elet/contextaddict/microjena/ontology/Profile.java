package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
import it.polimi.elet.contextaddict.microjena.graph.Node;
import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.util.Iterator;

public abstract interface Profile
{
  public abstract String NAMESPACE();
  
  public abstract Resource CLASS();
  
  public abstract Resource RESTRICTION();
  
  public abstract Resource THING();
  
  public abstract Resource NOTHING();
  
  public abstract Resource PROPERTY();
  
  public abstract Resource OBJECT_PROPERTY();
  
  public abstract Resource DATATYPE_PROPERTY();
  
  public abstract Resource TRANSITIVE_PROPERTY();
  
  public abstract Resource SYMMETRIC_PROPERTY();
  
  public abstract Resource FUNCTIONAL_PROPERTY();
  
  public abstract Resource INVERSE_FUNCTIONAL_PROPERTY();
  
  public abstract Resource ALL_DIFFERENT();
  
  public abstract Resource ONTOLOGY();
  
  public abstract Resource DEPRECATED_CLASS();
  
  public abstract Resource DEPRECATED_PROPERTY();
  
  public abstract Resource ANNOTATION_PROPERTY();
  
  public abstract Resource ONTOLOGY_PROPERTY();
  
  public abstract Resource DATARANGE();
  
  public abstract Property EQUIVALENT_PROPERTY();
  
  public abstract Property EQUIVALENT_CLASS();
  
  public abstract Property DISJOINT_WITH();
  
  public abstract Property SAME_INDIVIDUAL_AS();
  
  public abstract Property SAME_AS();
  
  public abstract Property DIFFERENT_FROM();
  
  public abstract Property DISTINCT_MEMBERS();
  
  public abstract Property UNION_OF();
  
  public abstract Property INTERSECTION_OF();
  
  public abstract Property COMPLEMENT_OF();
  
  public abstract Property ONE_OF();
  
  public abstract Property ON_PROPERTY();
  
  public abstract Property ALL_VALUES_FROM();
  
  public abstract Property HAS_VALUE();
  
  public abstract Property SOME_VALUES_FROM();
  
  public abstract Property MIN_CARDINALITY();
  
  public abstract Property MAX_CARDINALITY();
  
  public abstract Property CARDINALITY();
  
  public abstract Property MIN_CARDINALITY_Q();
  
  public abstract Property MAX_CARDINALITY_Q();
  
  public abstract Property CARDINALITY_Q();
  
  public abstract Property HAS_CLASS_Q();
  
  public abstract Property INVERSE_OF();
  
  public abstract Property IMPORTS();
  
  public abstract Property VERSION_INFO();
  
  public abstract Property PRIOR_VERSION();
  
  public abstract Property BACKWARD_COMPATIBLE_WITH();
  
  public abstract Property INCOMPATIBLE_WITH();
  
  public abstract Property SUB_CLASS_OF();
  
  public abstract Property SUB_PROPERTY_OF();
  
  public abstract Property DOMAIN();
  
  public abstract Property RANGE();
  
  public abstract Property LABEL();
  
  public abstract Property COMMENT();
  
  public abstract Property SEE_ALSO();
  
  public abstract Property IS_DEFINED_BY();
  
  public abstract Property FIRST();
  
  public abstract Property REST();
  
  public abstract Resource LIST();
  
  public abstract Resource NIL();
  
  public abstract Iterator getAxiomTypes();
  
  public abstract Iterator getAnnotationProperties();
  
  public abstract Iterator getClassDescriptionTypes();
  
  public abstract boolean isSupported(Node paramNode, EnhGraph paramEnhGraph, Class paramClass);
  
  public abstract String getLabel();
}

