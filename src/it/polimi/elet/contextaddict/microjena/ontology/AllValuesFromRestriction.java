package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;

public abstract interface AllValuesFromRestriction
  extends Restriction
{
  public abstract void setAllValuesFrom(Resource paramResource);
  
  public abstract Resource getAllValuesFrom();
  
  public abstract boolean hasAllValuesFrom(Resource paramResource);
  
  public abstract void removeAllValuesFrom(Resource paramResource);
}

