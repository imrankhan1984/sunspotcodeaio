package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.util.Iterator;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface BooleanClassDescription
  extends OntClass
{
  public abstract void setOperands(RDFList paramRDFList);
  
  public abstract void addOperand(Resource paramResource);
  
  public abstract void addOperands(Iterator paramIterator);
  
  public abstract RDFList getOperands();
  
  public abstract ExtendedIterator listOperands();
  
  public abstract boolean hasOperand(Resource paramResource);
  
  public abstract void removeOperand(Resource paramResource);
  
  public abstract Property operator();
}

