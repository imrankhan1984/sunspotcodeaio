package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface OntModel
  extends Model
{
  public abstract ExtendedIterator listOntologies();
  
  public abstract ExtendedIterator listOntProperties();
  
  public abstract ExtendedIterator listObjectProperties();
  
  public abstract ExtendedIterator listDatatypeProperties();
  
  public abstract ExtendedIterator listFunctionalProperties();
  
  public abstract ExtendedIterator listTransitiveProperties();
  
  public abstract ExtendedIterator listSymmetricProperties();
  
  public abstract ExtendedIterator listInverseFunctionalProperties();
  
  public abstract ExtendedIterator listIndividuals();
  
  public abstract ExtendedIterator listIndividuals(Resource paramResource);
  
  public abstract ExtendedIterator listClasses();
  
  public abstract ExtendedIterator listHierarchyRootClasses();
  
  public abstract ExtendedIterator listEnumeratedClasses();
  
  public abstract ExtendedIterator listUnionClasses();
  
  public abstract ExtendedIterator listComplementClasses();
  
  public abstract ExtendedIterator listIntersectionClasses();
  
  public abstract ExtendedIterator listNamedClasses();
  
  public abstract ExtendedIterator listRestrictions();
  
  public abstract ExtendedIterator listAnnotationProperties();
  
  public abstract ExtendedIterator listAllDifferent();
  
  public abstract ExtendedIterator listDataRanges();
  
  public abstract Ontology getOntology(String paramString);
  
  public abstract Individual getIndividual(String paramString);
  
  public abstract OntProperty getOntProperty(String paramString);
  
  public abstract ObjectProperty getObjectProperty(String paramString);
  
  public abstract TransitiveProperty getTransitiveProperty(String paramString);
  
  public abstract SymmetricProperty getSymmetricProperty(String paramString);
  
  public abstract InverseFunctionalProperty getInverseFunctionalProperty(String paramString);
  
  public abstract DatatypeProperty getDatatypeProperty(String paramString);
  
  public abstract AnnotationProperty getAnnotationProperty(String paramString);
  
  public abstract OntResource getOntResource(String paramString);
  
  public abstract OntResource getOntResource(Resource paramResource);
  
  public abstract OntClass getOntClass(String paramString);
  
  public abstract ComplementClass getComplementClass(String paramString);
  
  public abstract EnumeratedClass getEnumeratedClass(String paramString);
  
  public abstract UnionClass getUnionClass(String paramString);
  
  public abstract IntersectionClass getIntersectionClass(String paramString);
  
  public abstract Restriction getRestriction(String paramString);
  
  public abstract HasValueRestriction getHasValueRestriction(String paramString);
  
  public abstract SomeValuesFromRestriction getSomeValuesFromRestriction(String paramString);
  
  public abstract AllValuesFromRestriction getAllValuesFromRestriction(String paramString);
  
  public abstract CardinalityRestriction getCardinalityRestriction(String paramString);
  
  public abstract MinCardinalityRestriction getMinCardinalityRestriction(String paramString);
  
  public abstract MaxCardinalityRestriction getMaxCardinalityRestriction(String paramString);
  
  public abstract Ontology createOntology(String paramString);
  
  public abstract Individual createIndividual(Resource paramResource);
  
  public abstract Individual createIndividual(String paramString, Resource paramResource);
  
  public abstract OntProperty createOntProperty(String paramString);
  
  public abstract ObjectProperty createObjectProperty(String paramString);
  
  public abstract ObjectProperty createObjectProperty(String paramString, boolean paramBoolean);
  
  public abstract TransitiveProperty createTransitiveProperty(String paramString);
  
  public abstract TransitiveProperty createTransitiveProperty(String paramString, boolean paramBoolean);
  
  public abstract SymmetricProperty createSymmetricProperty(String paramString);
  
  public abstract SymmetricProperty createSymmetricProperty(String paramString, boolean paramBoolean);
  
  public abstract InverseFunctionalProperty createInverseFunctionalProperty(String paramString);
  
  public abstract InverseFunctionalProperty createInverseFunctionalProperty(String paramString, boolean paramBoolean);
  
  public abstract DatatypeProperty createDatatypeProperty(String paramString);
  
  public abstract DatatypeProperty createDatatypeProperty(String paramString, boolean paramBoolean);
  
  public abstract AnnotationProperty createAnnotationProperty(String paramString);
  
  public abstract OntClass createClass();
  
  public abstract OntClass createClass(String paramString);
  
  public abstract ComplementClass createComplementClass(String paramString, Resource paramResource);
  
  public abstract EnumeratedClass createEnumeratedClass(String paramString, RDFList paramRDFList);
  
  public abstract UnionClass createUnionClass(String paramString, RDFList paramRDFList);
  
  public abstract IntersectionClass createIntersectionClass(String paramString, RDFList paramRDFList);
  
  public abstract Restriction createRestriction(Property paramProperty);
  
  public abstract Restriction createRestriction(String paramString, Property paramProperty);
  
  public abstract HasValueRestriction createHasValueRestriction(String paramString, Property paramProperty, RDFNode paramRDFNode);
  
  public abstract SomeValuesFromRestriction createSomeValuesFromRestriction(String paramString, Property paramProperty, Resource paramResource);
  
  public abstract AllValuesFromRestriction createAllValuesFromRestriction(String paramString, Property paramProperty, Resource paramResource);
  
  public abstract CardinalityRestriction createCardinalityRestriction(String paramString, Property paramProperty, int paramInt);
  
  public abstract MinCardinalityRestriction createMinCardinalityRestriction(String paramString, Property paramProperty, int paramInt);
  
  public abstract MaxCardinalityRestriction createMaxCardinalityRestriction(String paramString, Property paramProperty, int paramInt);
  
  public abstract DataRange createDataRange(RDFList paramRDFList);
  
  public abstract AllDifferent createAllDifferent();
  
  public abstract AllDifferent createAllDifferent(RDFList paramRDFList);
  
  public abstract OntResource createOntResource(String paramString);
  
  public abstract Profile getProfile();
  
  public abstract boolean strictMode();
  
  public abstract void setStrictMode(boolean paramBoolean);
}

