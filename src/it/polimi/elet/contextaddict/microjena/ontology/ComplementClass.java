package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;

public abstract interface ComplementClass
  extends BooleanClassDescription
{
  public abstract OntClass getOperand();
  
  public abstract void setOperand(Resource paramResource);
}

