package it.polimi.elet.contextaddict.microjena.ontology;

import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;

public abstract interface HasValueRestriction
  extends Restriction
{
  public abstract void setHasValue(RDFNode paramRDFNode);
  
  public abstract RDFNode getHasValue();
  
  public abstract boolean hasValue(RDFNode paramRDFNode);
  
  public abstract void removeHasValue(RDFNode paramRDFNode);
}

