/*  1:   */ package it.polimi.elet.contextaddict.microjena.util;
/*  2:   */ 
/*  3:   */ public class Util
/*  4:   */ {
/*  5:   */   public static int splitNamespace(String s)
/*  6:   */   {
/*  7:28 */     int i = s.length();
/*  8:29 */     boolean found = false;
/*  9:30 */     while ((!found) && (i > 0))
/* 10:   */     {
/* 11:31 */       i--;
/* 12:32 */       found = (s.charAt(i) == '#') || (s.charAt(i) == ':');
/* 13:   */     }
/* 14:34 */     return i + 1;
/* 15:   */   }
/* 16:   */ }
