/*  1:   */ package it.polimi.elet.contextaddict.microjena.util.iterator;
/*  2:   */ 
/*  3:   */ public class NullIterator
/*  4:   */   extends NiceIterator
/*  5:   */ {
/*  6:15 */   public static NullIterator instance = new NullIterator();
/*  7:   */   
/*  8:   */   public ExtendedIterator andThen(ClosableIterator it)
/*  9:   */   {
/* 10:18 */     return (it instanceof ExtendedIterator) ? (ExtendedIterator)it : super.andThen(it);
/* 11:   */   }
/* 12:   */ }

