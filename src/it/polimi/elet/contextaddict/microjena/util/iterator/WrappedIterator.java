/*   1:    */ package it.polimi.elet.contextaddict.microjena.util.iterator;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaUnsupportedOperationException;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.util.UnsupportedOperationException;
/*   6:    */ 
/*   7:    */ public class WrappedIterator
/*   8:    */   extends NiceIterator
/*   9:    */ {
/*  10:    */   protected boolean removeDenied;
/*  11:    */   protected final Iterator base;
/*  12:    */   
/*  13:    */   public static ExtendedIterator create(Iterator it)
/*  14:    */   {
/*  15: 34 */     return (it instanceof ExtendedIterator) ? (ExtendedIterator)it : new WrappedIterator(it, false);
/*  16:    */   }
/*  17:    */   
/*  18:    */   public static WrappedIterator createNoRemove(Iterator it)
/*  19:    */   {
/*  20: 44 */     return new WrappedIterator(it, true);
/*  21:    */   }
/*  22:    */   
/*  23:    */   public Iterator forTestingOnly_getBase()
/*  24:    */   {
/*  25: 51 */     return this.base;
/*  26:    */   }
/*  27:    */   
/*  28:    */   protected WrappedIterator(Iterator base)
/*  29:    */   {
/*  30: 56 */     this(base, false);
/*  31:    */   }
/*  32:    */   
/*  33:    */   protected WrappedIterator(Iterator base, boolean removeDenied)
/*  34:    */   {
/*  35: 65 */     this.base = base;
/*  36: 66 */     this.removeDenied = removeDenied;
/*  37:    */   }
/*  38:    */   
/*  39:    */   public boolean hasNext()
/*  40:    */   {
/*  41: 71 */     return this.base.hasNext();
/*  42:    */   }
/*  43:    */   
/*  44:    */   public Object next()
/*  45:    */   {
/*  46: 76 */     return this.base.next();
/*  47:    */   }
/*  48:    */   
/*  49:    */   public void remove()
/*  50:    */   {
/*  51: 84 */     if (this.removeDenied) {
/*  52: 84 */       throw new JenaUnsupportedOperationException("remove is denied");
/*  53:    */     }
/*  54:    */     try
/*  55:    */     {
/*  56: 86 */       this.base.remove();
/*  57:    */     }
/*  58:    */     catch (UnsupportedOperationException ex) {}
/*  59:    */   }
/*  60:    */   
/*  61:    */   public void close()
/*  62:    */   {
/*  63: 93 */     close(this.base);
/*  64:    */   }
/*  65:    */   
/*  66:    */   public static void close(Iterator it)
/*  67:    */   {
/*  68:101 */     NiceIterator.close(it);
/*  69:    */   }
/*  70:    */ }
