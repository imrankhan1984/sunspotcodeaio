package it.polimi.elet.contextaddict.microjena.util.iterator;

import it.polimi.elet.contextaddict.microjena.util.List;
import it.polimi.elet.contextaddict.microjena.util.Set;

public abstract interface ExtendedIterator
  extends ClosableIterator
{
  public abstract ExtendedIterator andThen(ClosableIterator paramClosableIterator);
  
  public abstract List toList();
  
  public abstract Set toSet();
}

