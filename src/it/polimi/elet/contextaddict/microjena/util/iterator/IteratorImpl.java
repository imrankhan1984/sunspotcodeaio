/*  1:   */ package it.polimi.elet.contextaddict.microjena.util.iterator;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  4:   */ import java.io.PrintStream;
/*  5:   */ import java.util.NoSuchElementException;
/*  6:   */ import java.util.Vector;
/*  7:   */ 
/*  8:   */ public class IteratorImpl
/*  9:   */   implements Iterator
/* 10:   */ {
/* 11:   */   protected Vector v;
/* 12:   */   protected int index;
/* 13:   */   
/* 14:   */   public IteratorImpl()
/* 15:   */   {
/* 16:24 */     this(new Vector(0));
/* 17:   */   }
/* 18:   */   
/* 19:   */   public IteratorImpl(Vector newVector)
/* 20:   */   {
/* 21:29 */     this.v = newVector;
/* 22:30 */     this.index = 0;
/* 23:   */   }
/* 24:   */   
/* 25:   */   public boolean hasNext()
/* 26:   */   {
/* 27:34 */     if (this.v.size() > this.index) {
/* 28:35 */       return true;
/* 29:   */     }
/* 30:37 */     return false;
/* 31:   */   }
/* 32:   */   
/* 33:   */   public Object next()
/* 34:   */     throws NoSuchElementException
/* 35:   */   {
/* 36:41 */     return this.v.elementAt(this.index++);
/* 37:   */   }
/* 38:   */   
/* 39:   */   public void remove()
/* 40:   */   {
/* 41:46 */     System.err.println("Remove not implemented for IteratorImpl");
/* 42:   */   }
/* 43:   */ }
