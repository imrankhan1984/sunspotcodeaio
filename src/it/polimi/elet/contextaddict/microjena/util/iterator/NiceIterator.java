/*   1:    */ package it.polimi.elet.contextaddict.microjena.util.iterator;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaUnsupportedOperationException;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.util.List;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*   7:    */ import java.util.NoSuchElementException;
/*   8:    */ import java.util.Vector;
/*   9:    */ 
/*  10:    */ public class NiceIterator
/*  11:    */   implements ExtendedIterator
/*  12:    */ {
/*  13:    */   public void close() {}
/*  14:    */   
/*  15:    */   public boolean hasNext()
/*  16:    */   {
/*  17: 40 */     return false;
/*  18:    */   }
/*  19:    */   
/*  20:    */   protected void ensureHasNext()
/*  21:    */   {
/*  22: 44 */     if (!hasNext()) {
/*  23: 45 */       throw new NoSuchElementException();
/*  24:    */     }
/*  25:    */   }
/*  26:    */   
/*  27:    */   public Object next()
/*  28:    */   {
/*  29: 52 */     return noElements("empty NiceIterator");
/*  30:    */   }
/*  31:    */   
/*  32:    */   protected Object noElements(String message)
/*  33:    */   {
/*  34: 64 */     throw new NoSuchElementException(message);
/*  35:    */   }
/*  36:    */   
/*  37:    */   public void remove()
/*  38:    */   {
/*  39: 71 */     throw new JenaUnsupportedOperationException("remove not supported for this iterator");
/*  40:    */   }
/*  41:    */   
/*  42:    */   public Object removeNext()
/*  43:    */   {
/*  44: 78 */     Object result = next();
/*  45: 79 */     remove();
/*  46: 80 */     return result;
/*  47:    */   }
/*  48:    */   
/*  49:    */   public static ExtendedIterator andThen(Iterator a, Iterator b)
/*  50:    */   {
/*  51: 88 */     Vector newIt = new Vector(5, 5);
/*  52: 89 */     while (a.hasNext()) {
/*  53: 90 */       newIt.addElement(a.next());
/*  54:    */     }
/*  55: 91 */     while (b.hasNext()) {
/*  56: 92 */       newIt.addElement(b.next());
/*  57:    */     }
/*  58: 93 */     Iterator result = new IteratorImpl(newIt);
/*  59: 94 */     return WrappedIterator.create(result);
/*  60:    */   }
/*  61:    */   
/*  62:    */   public ExtendedIterator andThen(ClosableIterator other)
/*  63:    */   {
/*  64:101 */     return andThen(this, other);
/*  65:    */   }
/*  66:    */   
/*  67:    */   public static void close(Iterator it)
/*  68:    */   {
/*  69:109 */     if ((it instanceof ClosableIterator)) {
/*  70:110 */       ((ClosableIterator)it).close();
/*  71:    */     }
/*  72:    */   }
/*  73:    */   
/*  74:113 */   private static final NiceIterator emptyInstance = new NiceIterator();
/*  75:    */   
/*  76:    */   public static ExtendedIterator emptyIterator()
/*  77:    */   {
/*  78:120 */     return emptyInstance;
/*  79:    */   }
/*  80:    */   
/*  81:    */   public List toList()
/*  82:    */   {
/*  83:127 */     return asList(this);
/*  84:    */   }
/*  85:    */   
/*  86:    */   public Set toSet()
/*  87:    */   {
/*  88:134 */     return asSet(this);
/*  89:    */   }
/*  90:    */   
/*  91:    */   public static Set asSet(ExtendedIterator it)
/*  92:    */   {
/*  93:142 */     Set result = new Set();
/*  94:143 */     while (it.hasNext()) {
/*  95:144 */       result.add(it.next());
/*  96:    */     }
/*  97:145 */     return result;
/*  98:    */   }
/*  99:    */   
/* 100:    */   public static List asList(ExtendedIterator it)
/* 101:    */   {
/* 102:153 */     List result = new List();
/* 103:154 */     while (it.hasNext()) {
/* 104:155 */       result.add(it.next());
/* 105:    */     }
/* 106:156 */     return result;
/* 107:    */   }
/* 108:    */ }

