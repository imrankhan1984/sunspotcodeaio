package it.polimi.elet.contextaddict.microjena.util.iterator;

import it.polimi.elet.contextaddict.microjena.util.Iterator;

public abstract interface ClosableIterator
  extends Iterator
{
  public abstract void close();
}

