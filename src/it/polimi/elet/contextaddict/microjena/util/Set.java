/*  1:   */ package it.polimi.elet.contextaddict.microjena.util;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.IteratorImpl;
/*  4:   */ import java.util.Vector;
/*  5:   */ 
/*  6:   */ public class Set
/*  7:   */   extends List
/*  8:   */ {
/*  9:   */   public Set() {}
/* 10:   */   
/* 11:   */   public Set(Vector newVector)
/* 12:   */   {
/* 13:27 */     Iterator it = new IteratorImpl(newVector);
/* 14:28 */     while (it.hasNext()) {
/* 15:29 */       add(it.next());
/* 16:   */     }
/* 17:   */   }
/* 18:   */   
/* 19:   */   public boolean add(Object o)
/* 20:   */   {
/* 21:33 */     if (contains(o)) {
/* 22:34 */       return false;
/* 23:   */     }
/* 24:36 */     this.v.addElement(o);
/* 25:37 */     return true;
/* 26:   */   }
/* 27:   */ }

