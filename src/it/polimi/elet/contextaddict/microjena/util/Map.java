/*   1:    */ package it.polimi.elet.contextaddict.microjena.util;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.IteratorImpl;
/*   5:    */ import java.util.Vector;
/*   6:    */ 
/*   7:    */ public class Map
/*   8:    */ {
/*   9:    */   private Vector v;
/*  10:    */   
/*  11:    */   public Map()
/*  12:    */   {
/*  13: 25 */     this(5, 5);
/*  14:    */   }
/*  15:    */   
/*  16:    */   public Map(Map other)
/*  17:    */   {
/*  18: 29 */     this();
/*  19: 30 */     putAll(other);
/*  20:    */   }
/*  21:    */   
/*  22:    */   public Map(int initialVectorCapacity, int vectorAutoIncrement)
/*  23:    */   {
/*  24: 34 */     v = new Vector(initialVectorCapacity, vectorAutoIncrement);                    
/*  25:    */   }
/*  26:    */   
/*  27:    */   public void clear()
/*  28:    */   {
/*  29: 38 */     this.v.removeAllElements();
/*  30: 39 */     this.v.trimToSize();
/*  31:    */   }
/*  32:    */   
/*  33:    */   public boolean containsKey(Object key)
/*  34:    */   {
/*  35: 43 */     Iterator i = new IteratorImpl(this.v);
/*  36: 44 */     boolean found = false;
/*  37: 45 */     while ((i.hasNext()) && (!found)) {
/*  38: 46 */       found = ((Entry)i.next()).getKey().equals(key);
/*  39:    */     }
/*  40: 47 */     return found;
/*  41:    */   }
/*  42:    */   
/*  43:    */   public boolean containsValue(Object value)
/*  44:    */   {
/*  45: 51 */     Iterator i = new IteratorImpl(this.v);
/*  46: 52 */     boolean found = false;
/*  47: 53 */     while ((i.hasNext()) && (!found)) {
/*  48: 54 */       found = ((Entry)i.next()).getValue().equals(value);
/*  49:    */     }
/*  50: 55 */     return found;
/*  51:    */   }
/*  52:    */   
/*  53:    */   public Set entrySet()
/*  54:    */   {
/*  55: 59 */     return new Set(v);
/*  56:    */   }
/*  57:    */   
/*  58:    */   public boolean equals(Object other)
/*  59:    */   {
/*  60: 63 */     if (!(other instanceof Map)) {
/*  61: 64 */       return false;
/*  62:    */     }
/*  63: 66 */     if (size() != ((Map)other).size()) {
/*  64: 67 */       return false;
/*  65:    */     }
/*  66: 69 */     Iterator i = new IteratorImpl(this.v);
/*  67: 70 */     boolean eq = true;
/*  68: 72 */     while ((eq) && (i.hasNext()))
/*  69:    */     {
/*  70: 73 */       Entry aus = (Entry)i.next();
/*  71: 74 */       if ((!((Map)other).containsKey(aus.getKey())) || (!((Map)other).containsValue(aus.getValue()))) {
/*  72: 75 */         eq = false;
/*  73:    */       }
/*  74:    */     }
/*  75: 77 */     return eq;
/*  76:    */   }
/*  77:    */   
/*  78:    */   public Object get(Object searchKey)
/*  79:    */   {
/*  80: 82 */     for (int i = 0; i < size(); i++) {
/*  81: 83 */       if (searchKey.equals(((Entry)this.v.elementAt(i)).getKey())) {
/*  82: 84 */         return ((Entry)this.v.elementAt(i)).getValue();
/*  83:    */       }
/*  84:    */     }
/*  85: 86 */     return null;
/*  86:    */   }
/*  87:    */   
/*  88:    */   public boolean isEmpty()
/*  89:    */   {
/*  90: 90 */     return size() == 0;
/*  91:    */   }
/*  92:    */   
/*  93:    */   public Set keySet()
/*  94:    */   {
/*  95: 94 */     Vector vOut = new Vector(this.v.size());
/*  96: 95 */     Iterator i = new IteratorImpl(this.v);
/*  97: 96 */     while (i.hasNext()) {
/*  98: 97 */       vOut.addElement(((Entry)i.next()).getKey());
/*  99:    */     }
/* 100: 98 */     return new Set(vOut);
/* 101:    */   }
/* 102:    */   
/* 103:    */   public Object put(Object newKey, Object newValue)
/* 104:    */   {
/* 105:102 */     Iterator i = new IteratorImpl(this.v);
/* 106:103 */     boolean found = false;
/* 107:104 */     Entry aus = null;
/* 108:105 */     while ((i.hasNext()) && (!found))
/* 109:    */     {
/* 110:106 */       aus = (Entry)i.next();
/* 111:107 */       found = aus.getKey().equals(newKey);
/* 112:    */     }
/* 113:109 */     if (found)
/* 114:    */     {
/* 115:110 */       Object ausValue = aus.getValue();
/* 116:111 */       aus.setValue(newValue);
/* 117:112 */       return ausValue;
/* 118:    */     }
/* 119:115 */     v.addElement(new Entry(newKey, newValue));
/* 120:116 */     return null;
/* 121:    */   }
/* 122:    */   
/* 123:    */   public void putAll(Map m)
/* 124:    */   {
/* 125:121 */     Iterator i = m.entrySet().iterator();
/* 126:123 */     while (i.hasNext())
/* 127:    */     {
/* 128:124 */       Entry aus = (Entry)i.next();
/* 129:125 */       put(aus.getKey(), aus.getValue());
/* 130:    */     }
/* 131:    */   }
/* 132:    */   
/* 133:    */   public Object remove(Object searchKey)
/* 134:    */   {
/* 135:131 */     for (int i = 0; i < size(); i++) {
/* 136:132 */       if (((Entry)this.v.elementAt(i)).getKey().equals(searchKey))
/* 137:    */       {
/* 138:133 */         Object ausValue = ((Entry)this.v.elementAt(i)).getValue();
/* 139:134 */         this.v.removeElementAt(i);
/* 140:135 */         return ausValue;
/* 141:    */       }
/* 142:    */     }
/* 143:138 */     return null;
/* 144:    */   }
/* 145:    */   
/* 146:    */   public int size()
/* 147:    */   {
/* 148:142 */     return this.v.size();
/* 149:    */   }
/* 150:    */   
/* 151:    */   public Vector valuesVector()
/* 152:    */   {
/* 153:146 */     Vector vOut = new Vector(this.v.size());
/* 154:147 */     Iterator i = new IteratorImpl(this.v);
/* 155:148 */     while (i.hasNext()) {
/* 156:149 */       vOut.addElement(((Entry)i.next()).getValue());
/* 157:    */     }
/* 158:150 */     return vOut;
/* 159:    */   }
/* 160:    */   
/* 161:    */   public Set values()
/* 162:    */   {
/* 163:154 */     return new Set(valuesVector());
/* 164:    */   }
/* 165:    */   
/* 166:    */   public class Entry
/* 167:    */   {
/* 168:    */     private Object key;
/* 169:    */     private Object value;
/* 170:    */     
/* 171:    */     public Entry(Object newKey, Object newValue)
/* 172:    */     {
/* 173:172 */       if ((newKey == null) || (newValue == null)) {
/* 174:173 */         throw new JenaException("neither key or value can be NULL");
/* 175:    */       }
/* 176:175 */       this.key = newKey;
/* 177:176 */       this.value = newValue;
/* 178:    */     }
/* 179:    */     
/* 180:    */     public Object getKey()
/* 181:    */     {
/* 182:181 */       return this.key;
/* 183:    */     }
/* 184:    */     
/* 185:    */     public Object getValue()
/* 186:    */     {
/* 187:185 */       return this.value;
/* 188:    */     }
/* 189:    */     
/* 190:    */     public void setKey(Object newKey)
/* 191:    */     {
/* 192:189 */       this.key = newKey;
/* 193:    */     }
/* 194:    */     
/* 195:    */     public void setValue(Object newValue)
/* 196:    */     {
/* 197:193 */       this.value = newValue;
/* 198:    */     }
/* 199:    */     
/* 200:    */     public boolean equals(Object other)
/* 201:    */     {
/* 202:197 */       if (!(other instanceof Entry)) {
/* 203:198 */         return false;
/* 204:    */       }
/* 205:200 */       if ((((Entry)other).getKey().equals(this.key)) && (((Entry)other).getValue().equals(this.value))) {
/* 206:201 */         return true;
/* 207:    */       }
/* 208:203 */       return false;
/* 209:    */     }
/* 210:    */   }
/* 211:    */ }
