/*  1:   */ package it.polimi.elet.contextaddict.microjena.util;
/*  2:   */ 
/*  3:   */ public class UnsupportedOperationException
/*  4:   */   extends Exception
/*  5:   */ {
/*  6:   */   public UnsupportedOperationException() {}
/*  7:   */   
/*  8:   */   public UnsupportedOperationException(String message)
/*  9:   */   {
/* 10:25 */     super(message);
/* 11:   */   }
/* 12:   */ }
