package it.polimi.elet.contextaddict.microjena.util;

import java.util.NoSuchElementException;

public abstract interface Iterator
{
  public abstract Object next()
    throws NoSuchElementException;
  
  public abstract boolean hasNext();
  
  public abstract void remove()
    throws UnsupportedOperationException;
}

