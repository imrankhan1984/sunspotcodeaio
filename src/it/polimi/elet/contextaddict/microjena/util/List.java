/*   1:    */ package it.polimi.elet.contextaddict.microjena.util;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.IteratorImpl;
/*   4:    */ import java.util.Vector;
/*   5:    */ 
/*   6:    */ public class List
/*   7:    */ {
/*   8: 20 */   private final int INCREMENT = 1;
/*   9:    */   protected Vector v;
/*  10:    */   
/*  11:    */   public List()
/*  12:    */   {
/*  13: 26 */     this.v = new Vector(0, 1);
/*  14:    */   }
/*  15:    */   
/*  16:    */   public List(Vector newVector)
/*  17:    */   {
/*  18: 30 */     this.v = newVector;
/*  19:    */   }
/*  20:    */   
/*  21:    */   public List(int i)
/*  22:    */   {
/*  23: 34 */     this(new Vector(i, 1));
/*  24:    */   }
/*  25:    */   
/*  26:    */   public boolean add(Object o)
/*  27:    */   {
/*  28: 38 */     if (o != null)
/*  29:    */     {
/*  30: 39 */       this.v.addElement(o);
/*  31: 40 */       return true;
/*  32:    */     }
/*  33: 43 */     return false;
/*  34:    */   }
/*  35:    */   
/*  36:    */   public boolean contains(Object o)
/*  37:    */   {
/*  38: 47 */     int i = 0;
/*  39: 48 */     boolean found = false;
/*  40: 49 */     while ((i < this.v.size()) && (!found)) {
/*  41: 50 */       found = this.v.elementAt(i++).equals(o);
/*  42:    */     }
/*  43: 51 */     return found;
/*  44:    */   }
/*  45:    */   
/*  46:    */   public boolean equals(Object o)
/*  47:    */   {
/*  48: 55 */     if (!(o instanceof List)) {
/*  49: 56 */       return false;
/*  50:    */     }
/*  51: 58 */     if (this.v.size() != ((List)o).size()) {
/*  52: 59 */       return false;
/*  53:    */     }
/*  54: 61 */     boolean different = false;
/*  55: 62 */     int i = 0;
/*  56: 63 */     while ((!different) && (i < this.v.size()))
/*  57:    */     {
/*  58: 64 */       different = this.v.elementAt(i).equals(((List)o).get(i));
/*  59: 65 */       i++;
/*  60:    */     }
/*  61: 67 */     return different;
/*  62:    */   }
/*  63:    */   
/*  64:    */   public int indexOf(Object o)
/*  65:    */   {
/*  66: 72 */     int result = -1;
/*  67: 73 */     int i = 0;
/*  68: 74 */     while ((i < this.v.size()) && (result == -1))
/*  69:    */     {
/*  70: 75 */       if (this.v.elementAt(i).equals(o)) {
/*  71: 76 */         result = i;
/*  72:    */       }
/*  73: 77 */       i++;
/*  74:    */     }
/*  75: 79 */     return result;
/*  76:    */   }
/*  77:    */   
/*  78:    */   public int lastIndexOf(Object o)
/*  79:    */   {
/*  80: 83 */     int result = -1;
/*  81: 84 */     int i = this.v.size();
/*  82: 85 */     while ((i > 0) && (result == -1)) {
/*  83: 86 */       if (this.v.elementAt(--i).equals(o)) {
/*  84: 87 */         result = i;
/*  85:    */       }
/*  86:    */     }
/*  87: 88 */     return result;
/*  88:    */   }
/*  89:    */   
/*  90:    */   public boolean isEmpty()
/*  91:    */   {
/*  92: 92 */     return this.v.size() == 0;
/*  93:    */   }
/*  94:    */   
/*  95:    */   public Iterator iterator()
/*  96:    */   {
/*  97: 96 */     return new IteratorImpl(this.v);
/*  98:    */   }
/*  99:    */   
/* 100:    */   public Object get(int index)
/* 101:    */   {
/* 102:100 */     return this.v.elementAt(index);
/* 103:    */   }
/* 104:    */   
/* 105:    */   public int size()
/* 106:    */   {
/* 107:104 */     return this.v.size();
/* 108:    */   }
/* 109:    */   
/* 110:    */   public boolean remove(Object o)
/* 111:    */   {
/* 112:108 */     return this.v.removeElement(o);
/* 113:    */   }
/* 114:    */   
/* 115:    */   public Object remove(int i)
/* 116:    */   {
/* 117:112 */     Object aus = this.v.elementAt(i);
/* 118:113 */     this.v.removeElementAt(i);
/* 119:114 */     return aus;
/* 120:    */   }
/* 121:    */ }
