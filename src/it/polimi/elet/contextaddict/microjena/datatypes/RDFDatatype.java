package it.polimi.elet.contextaddict.microjena.datatypes;

import it.polimi.elet.contextaddict.microjena.graph.impl.LiteralLabel;

public abstract interface RDFDatatype
{
  public abstract String getURI();
  
  public abstract String unparse(Object paramObject);
  
  public abstract Object parse(String paramString)
    throws DatatypeFormatException;
  
  public abstract boolean isValid(String paramString);
  
  public abstract boolean isValidValue(Object paramObject);
  
  public abstract boolean isValidLiteral(LiteralLabel paramLiteralLabel);
  
  public abstract boolean isEqual(LiteralLabel paramLiteralLabel1, LiteralLabel paramLiteralLabel2);
  
  public abstract int getHashCode(LiteralLabel paramLiteralLabel);
  
  public abstract Class getJavaClass();
  
  public abstract Object cannonicalise(Object paramObject);
  
  public abstract Object extendedTypeDefinition();
  
  public abstract RDFDatatype normalizeSubType(Object paramObject, RDFDatatype paramRDFDatatype);
}
