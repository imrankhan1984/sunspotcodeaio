/*  1:   */ package it.polimi.elet.contextaddict.microjena.datatypes;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  4:   */ 
/*  5:   */ public class DatatypeFormatException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public DatatypeFormatException(String lexicalForm, RDFDatatype dtype, String msg)
/*  9:   */   {
/* 10:33 */     super("Lexical form '" + lexicalForm + "' is not a legal instance of " + dtype + " " + msg);
/* 11:   */   }
/* 12:   */   
/* 13:   */   public DatatypeFormatException() {}
/* 14:   */   
/* 15:   */   public DatatypeFormatException(String msg)
/* 16:   */   {
/* 17:50 */     super(msg);
/* 18:   */   }
/* 19:   */ }
