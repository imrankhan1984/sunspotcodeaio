/*   1:    */ package it.polimi.elet.contextaddict.microjena.datatypes;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.impl.LiteralLabel;
/*   4:    */ 
/*   5:    */ public class BaseDatatype
/*   6:    */   implements RDFDatatype
/*   7:    */ {
/*   8:    */   protected String uri;
/*   9:    */   
/*  10:    */   public BaseDatatype(String uri)
/*  11:    */   {
/*  12: 32 */     this.uri = uri;
/*  13:    */   }
/*  14:    */   
/*  15:    */   public String getURI()
/*  16:    */   {
/*  17: 39 */     return this.uri;
/*  18:    */   }
/*  19:    */   
/*  20:    */   public static class TypedValue
/*  21:    */   {
/*  22:    */     public final String lexicalValue;
/*  23:    */     public final String datatypeURI;
/*  24:    */     
/*  25:    */     public TypedValue(String lexicalValue, String datatypeURI)
/*  26:    */     {
/*  27: 52 */       this.lexicalValue = lexicalValue;
/*  28: 53 */       this.datatypeURI = datatypeURI;
/*  29:    */     }
/*  30:    */     
/*  31:    */     public boolean equals(Object other)
/*  32:    */     {
/*  33: 57 */       if ((other instanceof TypedValue)) {
/*  34: 58 */         return (this.lexicalValue.equals(((TypedValue)other).lexicalValue)) && (this.datatypeURI.equals(((TypedValue)other).datatypeURI));
/*  35:    */       }
/*  36: 61 */       return false;
/*  37:    */     }
/*  38:    */     
/*  39:    */     public int hashCode()
/*  40:    */     {
/*  41: 66 */       return this.lexicalValue.hashCode() ^ this.datatypeURI.hashCode();
/*  42:    */     }
/*  43:    */   }
/*  44:    */   
/*  45:    */   public String unparse(Object value)
/*  46:    */   {
/*  47: 78 */     if ((value instanceof TypedValue)) {
/*  48: 79 */       return ((TypedValue)value).lexicalValue;
/*  49:    */     }
/*  50: 81 */     return value.toString();
/*  51:    */   }
/*  52:    */   
/*  53:    */   public Object parse(String lexicalForm)
/*  54:    */     throws DatatypeFormatException
/*  55:    */   {
/*  56: 89 */     return new TypedValue(lexicalForm, getURI());
/*  57:    */   }
/*  58:    */   
/*  59:    */   public boolean isValid(String lexicalForm)
/*  60:    */   {
/*  61:    */     try
/*  62:    */     {
/*  63: 98 */       parse(lexicalForm);
/*  64: 99 */       return true;
/*  65:    */     }
/*  66:    */     catch (DatatypeFormatException e) {}
/*  67:101 */     return false;
/*  68:    */   }
/*  69:    */   
/*  70:    */   public boolean isValidLiteral(LiteralLabel lit)
/*  71:    */   {
/*  72:114 */     return equals(lit.getDatatype());
/*  73:    */   }
/*  74:    */   
/*  75:    */   public boolean isValidValue(Object valueForm)
/*  76:    */   {
/*  77:123 */     return isValid(unparse(valueForm));
/*  78:    */   }
/*  79:    */   
/*  80:    */   public boolean isEqual(LiteralLabel value1, LiteralLabel value2)
/*  81:    */   {
/*  82:131 */     return (value1.getDatatype() == value2.getDatatype()) && (value1.getValue().equals(value2.getValue()));
/*  83:    */   }
/*  84:    */   
/*  85:    */   public int getHashCode(LiteralLabel lit)
/*  86:    */   {
/*  87:140 */     return lit.getDefaultHashcode();
/*  88:    */   }
/*  89:    */   
/*  90:    */   public boolean langTagCompatible(LiteralLabel value1, LiteralLabel value2)
/*  91:    */   {
/*  92:147 */     if (value1.language() == null) {
/*  93:148 */       return (value2.language() == null) || (value2.language().equals(""));
/*  94:    */     }
/*  95:150 */     return value1.language().toLowerCase().equals(value2.language().toLowerCase());
/*  96:    */   }
/*  97:    */   
/*  98:    */   public Class getJavaClass()
/*  99:    */   {
/* 100:159 */     return null;
/* 101:    */   }
/* 102:    */   
/* 103:    */   public Object cannonicalise(Object value)
/* 104:    */   {
/* 105:169 */     return value;
/* 106:    */   }
/* 107:    */   
/* 108:    */   public Object extendedTypeDefinition()
/* 109:    */   {
/* 110:179 */     return null;
/* 111:    */   }
/* 112:    */   
/* 113:    */   public RDFDatatype normalizeSubType(Object value, RDFDatatype dt)
/* 114:    */   {
/* 115:195 */     return this;
/* 116:    */   }
/* 117:    */   
/* 118:    */   public String toString()
/* 119:    */   {
/* 120:202 */     return "Datatype[" + this.uri + (getJavaClass() == null ? "" : new StringBuffer().append(" -> ").append(getJavaClass()).toString()) + "]";
/* 121:    */   }
/* 122:    */ }