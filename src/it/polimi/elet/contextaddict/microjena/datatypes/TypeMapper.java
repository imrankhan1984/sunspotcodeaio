/*   1:    */ package it.polimi.elet.contextaddict.microjena.datatypes;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.XSDDatatype;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.shared.impl.JenaParameters;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*   8:    */ 
/*   9:    */ public class TypeMapper
/*  10:    */ {
/*  11:    */   public static TypeMapper getInstance()
/*  12:    */   {
/*  13: 39 */     return theTypeMap;
/*  14:    */   }
/*  15:    */   
/*  16: 51 */   private static TypeMapper theTypeMap = new TypeMapper();
/*  17:    */   
/*  18:    */   static
/*  19:    */   {
/*  20: 53 */     XSDDatatype.loadXSDSimpleTypes(theTypeMap);
/*  21:    */   }
/*  22:    */   
/*  23: 60 */   private Map uriToDT = new Map();
/*  24: 63 */   private Map classToDT = new Map();
/*  25:    */   
/*  26:    */   public RDFDatatype getSafeTypeByName(String uri)
/*  27:    */   {
/*  28: 80 */     RDFDatatype dtype = (RDFDatatype)this.uriToDT.get(uri);
/*  29: 81 */     if (dtype == null)
/*  30:    */     {
/*  31: 82 */       if (uri == null) {
/*  32: 84 */         return null;
/*  33:    */       }
/*  34: 87 */       if (JenaParameters.enableSilentAcceptanceOfUnknownDatatypes)
/*  35:    */       {
/*  36: 88 */         dtype = new BaseDatatype(uri);
/*  37: 89 */         registerDatatype(dtype);
/*  38:    */       }
/*  39:    */       else
/*  40:    */       {
/*  41: 91 */         throw new DatatypeFormatException("Attempted to created typed literal using an unknown datatype - " + uri);
/*  42:    */       }
/*  43:    */     }
/*  44: 96 */     return dtype;
/*  45:    */   }
/*  46:    */   
/*  47:    */   public RDFDatatype getTypeByName(String uri)
/*  48:    */   {
/*  49:107 */     return (RDFDatatype)this.uriToDT.get(uri);
/*  50:    */   }
/*  51:    */   
/*  52:    */   public RDFDatatype getTypeByValue(Object value)
/*  53:    */   {
/*  54:119 */     return (RDFDatatype)this.classToDT.get(value.getClass());
/*  55:    */   }
/*  56:    */   
/*  57:    */   public Iterator listTypes()
/*  58:    */   {
/*  59:126 */     return this.uriToDT.values().iterator();
/*  60:    */   }
/*  61:    */   
/*  62:    */   public void registerDatatype(RDFDatatype type)
/*  63:    */   {
/*  64:133 */     this.uriToDT.put(type.getURI(), type);
/*  65:134 */     Class jc = type.getJavaClass();
/*  66:135 */     if (jc != null) {
/*  67:136 */       this.classToDT.put(jc, type);
/*  68:    */     }
/*  69:    */   }
/*  70:    */ }
