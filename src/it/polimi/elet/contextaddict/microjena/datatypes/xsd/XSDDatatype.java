/*   1:    */ package it.polimi.elet.contextaddict.microjena.datatypes.xsd;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.BaseDatatype;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.datatypes.TypeMapper;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.impl.XSDBaseNumericType;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.impl.XSDBaseStringType;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.impl.XSDDouble;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.impl.XSDFloat;
/*   9:    */ 
/*  10:    */ public class XSDDatatype
/*  11:    */   extends BaseDatatype
/*  12:    */ {
/*  13:    */   public static final String XSD = "http://www.w3.org/2001/XMLSchema";
/*  14: 39 */   public static final XSDDatatype XSDfloat = new XSDFloat("float", Float.class);
/*  15: 42 */   public static final XSDDatatype XSDdouble = new XSDDouble("double", Double.class);
/*  16: 45 */   public static final XSDDatatype XSDint = new XSDBaseNumericType("int", Integer.class);
/*  17: 48 */   public static final XSDDatatype XSDlong = new XSDBaseNumericType("long", Long.class);
/*  18: 51 */   public static final XSDDatatype XSDinteger = new XSDBaseNumericType("integer", Integer.class);
/*  19: 54 */   public static final XSDDatatype XSDboolean = new XSDDatatype("boolean", Boolean.class);
/*  20: 57 */   public static final XSDDatatype XSDstring = new XSDBaseStringType("string", String.class);
/*  21: 63 */   protected String typeName = null;
/*  22: 65 */   protected Class javaClass = null;
/*  23:    */   
/*  24:    */   protected XSDDatatype(String typeName, Class javaClass)
/*  25:    */   {
/*  26: 78 */     super("http://www.w3.org/2001/XMLSchema#" + typeName);
/*  27: 79 */     this.typeName = typeName;
/*  28: 80 */     this.javaClass = javaClass;
/*  29:    */   }
/*  30:    */   
/*  31:    */   protected Object suitableInteger(String lexical)
/*  32:    */   {
/*  33: 89 */     long number = Long.parseLong(lexical);
/*  34: 90 */     return suitableInteger(number);
/*  35:    */   }
/*  36:    */   
/*  37:    */   protected Object suitableInteger(long number)
/*  38:    */   {
/*  39: 99 */     if ((number > 2147483647L) || (number < -2147483648L)) {
/*  40:100 */       return new Long(number);
/*  41:    */     }
/*  42:102 */     return new Integer((int)number);
/*  43:    */   }
/*  44:    */   
/*  45:    */   public Class getJavaClass()
/*  46:    */   {
/*  47:111 */     return this.javaClass;
/*  48:    */   }
/*  49:    */   
/*  50:    */   public Object parseValidated(String lexical)
/*  51:    */   {
/*  52:120 */     return lexical;
/*  53:    */   }
/*  54:    */   
/*  55:    */   public static String trimPlus(String str)
/*  56:    */   {
/*  57:128 */     int i = str.indexOf('+');
/*  58:129 */     if (i == -1) {
/*  59:130 */       return str;
/*  60:    */     }
/*  61:132 */     return str.substring(i + 1);
/*  62:    */   }
/*  63:    */   
/*  64:    */   public static void loadXSDSimpleTypes(TypeMapper tm)
/*  65:    */   {
/*  66:142 */     tm.registerDatatype(XSDinteger);
/*  67:143 */     tm.registerDatatype(XSDdouble);
/*  68:144 */     tm.registerDatatype(XSDfloat);
/*  69:145 */     tm.registerDatatype(XSDlong);
/*  70:146 */     tm.registerDatatype(XSDint);
/*  71:147 */     tm.registerDatatype(XSDboolean);
/*  72:148 */     tm.registerDatatype(XSDstring);
/*  73:    */   }
/*  74:    */   
/*  75:    */   public int getHashCode(byte[] bytes)
/*  76:    */   {
/*  77:153 */     int length = bytes.length;
/*  78:154 */     return length == 0 ? 0 : bytes[0] << 12 ^ bytes[(length / 2)] << 6 ^ bytes[(length - 1)] ^ length;
/*  79:    */   }
/*  80:    */ }
