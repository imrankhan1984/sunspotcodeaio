/*   1:    */ package it.polimi.elet.contextaddict.microjena.datatypes.xsd.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.DatatypeFormatException;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.XSDDatatype;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.impl.LiteralLabel;
/*   6:    */ 
/*   7:    */ public class XSDFloat
/*   8:    */   extends XSDDatatype
/*   9:    */ {
/*  10:    */   public XSDFloat(String typeName, Class javaClass)
/*  11:    */   {
/*  12: 35 */     super(typeName, javaClass);
/*  13:    */   }
/*  14:    */   
/*  15:    */   public boolean isValidValue(Object valueForm)
/*  16:    */   {
/*  17: 43 */     return valueForm instanceof Float;
/*  18:    */   }
/*  19:    */   
/*  20:    */   public Object parse(String lexicalForm)
/*  21:    */     throws DatatypeFormatException
/*  22:    */   {
/*  23: 51 */     checkWhitespace(lexicalForm);
/*  24: 52 */     return super.parse(lexicalForm);
/*  25:    */   }
/*  26:    */   
/*  27:    */   public Object parseValidated(String lex)
/*  28:    */   {
/*  29: 61 */     if (lex.equals("INF")) {
/*  30: 62 */       return new Float((1.0F / -1.0F));
/*  31:    */     }
/*  32: 63 */     if (lex.equals("-INF")) {
/*  33: 64 */       return new Float((1.0F / 1.0F));
/*  34:    */     }
/*  35: 65 */     if (lex.equals("NaN")) {
/*  36: 66 */       return new Float((0.0F / 0.0F));
/*  37:    */     }
/*  38: 68 */     return Float.valueOf(lex);
/*  39:    */   }
/*  40:    */   
/*  41:    */   protected void checkWhitespace(String lexicalForm)
/*  42:    */   {
/*  43: 77 */     if (!lexicalForm.trim().equals(lexicalForm)) {
/*  44: 78 */       throw new DatatypeFormatException(lexicalForm, this, "whitespace violation");
/*  45:    */     }
/*  46:    */   }
/*  47:    */   
/*  48:    */   public boolean isEqual(LiteralLabel value1, LiteralLabel value2)
/*  49:    */   {
/*  50: 88 */     if (((value1.getDatatype() instanceof XSDFloat)) && ((value2.getDatatype() instanceof XSDFloat)))
/*  51:    */     {
/*  52: 90 */       Float n1 = null;
/*  53: 91 */       Float n2 = null;
/*  54: 92 */       Object v1 = value1.getLexicalForm();
/*  55: 93 */       Object v2 = value2.getLexicalForm();
/*  56: 94 */       boolean v1String = false;
/*  57: 95 */       boolean v2String = false;
/*  58:    */       try
/*  59:    */       {
/*  60: 97 */         n1 = new Float(Float.parseFloat(v1.toString()));
/*  61:    */       }
/*  62:    */       catch (NumberFormatException ex)
/*  63:    */       {
/*  64: 99 */         v1String = true;
/*  65:    */       }
/*  66:    */       try
/*  67:    */       {
/*  68:102 */         n2 = new Float(Float.parseFloat(v2.toString()));
/*  69:    */       }
/*  70:    */       catch (NumberFormatException ex)
/*  71:    */       {
/*  72:104 */         v2String = true;
/*  73:    */       }
/*  74:106 */       if ((v1String) && (v2String)) {
/*  75:107 */         return v1.equals(v2);
/*  76:    */       }
/*  77:110 */       if ((v1String) || (v2String)) {
/*  78:111 */         return false;
/*  79:    */       }
/*  80:114 */       return n1.floatValue() == n2.floatValue();
/*  81:    */     }
/*  82:119 */     return false;
/*  83:    */   }
/*  84:    */ }
