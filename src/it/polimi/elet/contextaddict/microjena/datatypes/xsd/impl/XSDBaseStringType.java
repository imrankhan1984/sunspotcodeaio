/*  1:   */ package it.polimi.elet.contextaddict.microjena.datatypes.xsd.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.XSDDatatype;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.graph.impl.LiteralLabel;
/*  5:   */ 
/*  6:   */ public class XSDBaseStringType
/*  7:   */   extends XSDDatatype
/*  8:   */ {
/*  9:   */   public XSDBaseStringType(String typeName, Class javaClass)
/* 10:   */   {
/* 11:33 */     super(typeName, javaClass);
/* 12:   */   }
/* 13:   */   
/* 14:   */   public boolean isEqual(LiteralLabel value1, LiteralLabel value2)
/* 15:   */   {
/* 16:44 */     if ((value2.getDatatype() == null) || ((value2.getDatatype() instanceof XSDBaseStringType))) {
/* 17:45 */       return value1.getValue().equals(value2.getValue());
/* 18:   */     }
/* 19:47 */     return false;
/* 20:   */   }
/* 21:   */ }
