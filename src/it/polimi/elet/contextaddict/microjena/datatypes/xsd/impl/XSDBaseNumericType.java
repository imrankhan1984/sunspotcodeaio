/*   1:    */ package it.polimi.elet.contextaddict.microjena.datatypes.xsd.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.DatatypeFormatException;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.datatypes.xsd.XSDDatatype;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.impl.LiteralLabel;
/*   6:    */ 
/*   7:    */ public class XSDBaseNumericType
/*   8:    */   extends XSDDatatype
/*   9:    */ {
/*  10:    */   public XSDBaseNumericType(String typeName, Class javaClass)
/*  11:    */   {
/*  12: 36 */     super(typeName, javaClass);
/*  13:    */   }
/*  14:    */   
/*  15:    */   public Object cannonicalise(Object value)
/*  16:    */   {
/*  17: 46 */     if ((value instanceof Long)) {
/*  18: 47 */       return suitableInteger(((Long)value).longValue());
/*  19:    */     }
/*  20: 49 */     if ((value instanceof Integer)) {
/*  21: 50 */       return suitableInteger(((Integer)value).longValue());
/*  22:    */     }
/*  23: 52 */     throw new NumberFormatException();
/*  24:    */   }
/*  25:    */   
/*  26:    */   public Object parse(String lexicalForm)
/*  27:    */     throws DatatypeFormatException
/*  28:    */   {
/*  29: 60 */     checkWhitespace(lexicalForm);
/*  30: 61 */     return super.parse(lexicalForm);
/*  31:    */   }
/*  32:    */   
/*  33:    */   protected void checkWhitespace(String lexicalForm)
/*  34:    */   {
/*  35: 69 */     if (!lexicalForm.trim().equals(lexicalForm)) {
/*  36: 70 */       throw new DatatypeFormatException(lexicalForm, this, "whitespace violation");
/*  37:    */     }
/*  38:    */   }
/*  39:    */   
/*  40:    */   public boolean isEqual(LiteralLabel value1, LiteralLabel value2)
/*  41:    */   {
/*  42: 80 */     if (((value1.getDatatype() instanceof XSDBaseNumericType)) && ((value2.getDatatype() instanceof XSDBaseNumericType)))
/*  43:    */     {
/*  44: 82 */       Long n1 = null;
/*  45: 83 */       Long n2 = null;
/*  46: 84 */       Object v1 = value1.getLexicalForm();
/*  47: 85 */       Object v2 = value2.getLexicalForm();
/*  48: 86 */       boolean v1String = false;
/*  49: 87 */       boolean v2String = false;
/*  50:    */       try
/*  51:    */       {
/*  52: 89 */         n1 = new Long(Long.parseLong(v1.toString()));
/*  53:    */       }
/*  54:    */       catch (NumberFormatException ex)
/*  55:    */       {
/*  56: 91 */         v1String = true;
/*  57:    */       }
/*  58:    */       try
/*  59:    */       {
/*  60: 94 */         n2 = new Long(Long.parseLong(v2.toString()));
/*  61:    */       }
/*  62:    */       catch (NumberFormatException ex)
/*  63:    */       {
/*  64: 96 */         v2String = true;
/*  65:    */       }
/*  66: 98 */       if ((v1String) && (v2String)) {
/*  67: 99 */         return v1.equals(v2);
/*  68:    */       }
/*  69:102 */       if ((v1String) || (v2String)) {
/*  70:103 */         return false;
/*  71:    */       }
/*  72:106 */       return n1.longValue() == n2.longValue();
/*  73:    */     }
/*  74:111 */     return false;
/*  75:    */   }
/*  76:    */ }
