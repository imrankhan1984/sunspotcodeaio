/*   1:    */ package it.polimi.elet.contextaddict.microjena.shared.impl;
/*   2:    */ 
/*   3:    */ public class JenaParameters
/*   4:    */ {
/*   5: 40 */   public static boolean enableEagerLiteralValidation = false;
/*   6: 54 */   public static boolean enablePlainLiteralSameAsString = true;
/*   7: 72 */   public static boolean enableSilentAcceptanceOfUnknownDatatypes = true;
/*   8: 80 */   public static boolean enableWhitespaceCheckingOfTypedLiterals = false;
/*   9: 90 */   public static boolean enableFilteringOfHiddenInfNodes = true;
/*  10: 96 */   public static boolean enableOWLRuleOverOWLRuleWarnings = true;
/*  11:106 */   public static boolean disableBNodeUIDGeneration = false;
/*  12:    */ }

