/*   1:    */ package it.polimi.elet.contextaddict.microjena.shared.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping.JenaLockedException;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.util.Map.Entry;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.util.Util;
/*  10:    */ 
/*  11:    */ public class PrefixMappingImpl
/*  12:    */   implements PrefixMapping
/*  13:    */ {
/*  14:    */   protected Map prefixToURI;
/*  15:    */   protected Map URItoPrefix;
/*  16:    */   protected boolean locked;
/*  17:    */   
/*  18:    */   public PrefixMappingImpl()
/*  19:    */   {
/*  20: 29 */     this.prefixToURI = new Map();
/*  21: 30 */     this.URItoPrefix = new Map();
/*  22:    */   }
/*  23:    */   
/*  24:    */   protected void set(String prefix, String uri)
/*  25:    */   {
/*  26: 34 */     this.prefixToURI.put(prefix, uri);
/*  27: 35 */     this.URItoPrefix.put(uri, prefix);
/*  28:    */   }
/*  29:    */   
/*  30:    */   protected String get(String prefix)
/*  31:    */   {
/*  32: 39 */     return (String)this.prefixToURI.get(prefix);
/*  33:    */   }
/*  34:    */   
/*  35:    */   public PrefixMapping lock()
/*  36:    */   {
/*  37: 43 */     this.locked = true;
/*  38: 44 */     return this;
/*  39:    */   }
/*  40:    */   
/*  41:    */   public PrefixMapping setNsPrefix(String prefix, String uri)
/*  42:    */   {
/*  43: 48 */     checkUnlocked();
/*  44: 49 */     if (uri == null) {
/*  45: 50 */       throw new NullPointerException("null URIs are prohibited as arguments to setNsPrefix");
/*  46:    */     }
/*  47: 51 */     set(prefix, uri);
/*  48: 52 */     return this;
/*  49:    */   }
/*  50:    */   
/*  51:    */   public PrefixMapping removeNsPrefix(String prefix)
/*  52:    */   {
/*  53: 56 */     checkUnlocked();
/*  54: 57 */     String uri = (String)this.prefixToURI.remove(prefix);
/*  55: 58 */     regenerateReverseMapping();
/*  56: 59 */     return this;
/*  57:    */   }
/*  58:    */   
/*  59:    */   protected void regenerateReverseMapping()
/*  60:    */   {
/*  61: 63 */     this.URItoPrefix.clear();
/*  62: 64 */     Iterator it = this.prefixToURI.entrySet().iterator();
/*  63: 65 */     while (it.hasNext())
/*  64:    */     {
/*  65: 66 */       Map.Entry e = (Map.Entry)it.next();
/*  66: 67 */       this.URItoPrefix.put(e.getValue(), e.getKey());
/*  67:    */     }
/*  68:    */   }
/*  69:    */   
/*  70:    */   protected void checkUnlocked()
/*  71:    */   {
/*  72: 72 */     if (this.locked) {
/*  73: 72 */       throw new PrefixMapping.JenaLockedException(this);
/*  74:    */     }
/*  75:    */   }
/*  76:    */   
/*  77:    */   public static boolean isNiceURI(String uri)
/*  78:    */   {
/*  79: 75 */     if (uri.equals("")) {
/*  80: 76 */       return false;
/*  81:    */     }
/*  82: 77 */     char last = uri.charAt(uri.length() - 1);
/*  83: 78 */     return last != ':';
/*  84:    */   }
/*  85:    */   
/*  86:    */   public PrefixMapping setNsPrefixes(PrefixMapping other)
/*  87:    */   {
/*  88: 88 */     return setNsPrefixes(other.getNsPrefixMap());
/*  89:    */   }
/*  90:    */   
/*  91:    */   public PrefixMapping withDefaultMappings(PrefixMapping other)
/*  92:    */   {
/*  93: 98 */     checkUnlocked();
/*  94: 99 */     Iterator it = other.getNsPrefixMap().entrySet().iterator();
/*  95:100 */     while (it.hasNext())
/*  96:    */     {
/*  97:101 */       Map.Entry e = (Map.Entry)it.next();
/*  98:102 */       String prefix = (String)e.getKey();String uri = (String)e.getValue();
/*  99:103 */       if ((getNsPrefixURI(prefix) == null) && (getNsURIPrefix(uri) == null)) {
/* 100:104 */         setNsPrefix(prefix, uri);
/* 101:    */       }
/* 102:    */     }
/* 103:106 */     return this;
/* 104:    */   }
/* 105:    */   
/* 106:    */   public PrefixMapping setNsPrefixes(Map other)
/* 107:    */   {
/* 108:118 */     checkUnlocked();
/* 109:119 */     Iterator it = other.entrySet().iterator();
/* 110:120 */     while (it.hasNext())
/* 111:    */     {
/* 112:121 */       Map.Entry e = (Map.Entry)it.next();
/* 113:122 */       setNsPrefix((String)e.getKey(), (String)e.getValue());
/* 114:    */     }
/* 115:124 */     return this;
/* 116:    */   }
/* 117:    */   
/* 118:    */   public String getNsPrefixURI(String prefix)
/* 119:    */   {
/* 120:128 */     return get(prefix);
/* 121:    */   }
/* 122:    */   
/* 123:    */   public Map getNsPrefixMap()
/* 124:    */   {
/* 125:131 */     return new Map(this.prefixToURI);
/* 126:    */   }
/* 127:    */   
/* 128:    */   public String getNsURIPrefix(String uri)
/* 129:    */   {
/* 130:135 */     return (String)this.URItoPrefix.get(uri);
/* 131:    */   }
/* 132:    */   
/* 133:    */   public String expandPrefix(String prefixed)
/* 134:    */   {
/* 135:144 */     int colon = prefixed.indexOf(':');
/* 136:145 */     if (colon < 0) {
/* 137:146 */       return prefixed;
/* 138:    */     }
/* 139:148 */     String uri = get(prefixed.substring(0, colon));
/* 140:149 */     return uri + prefixed.substring(colon + 1);
/* 141:    */   }
/* 142:    */   
/* 143:    */   public String toString()
/* 144:    */   {
/* 145:157 */     return "pm:" + this.prefixToURI;
/* 146:    */   }
/* 147:    */   
/* 148:    */   public String qnameFor(String uri)
/* 149:    */   {
/* 150:171 */     int split = Util.splitNamespace(uri);
/* 151:172 */     String ns = uri.substring(0, split);String local = uri.substring(split);
/* 152:173 */     if (local.equals("")) {
/* 153:173 */       return null;
/* 154:    */     }
/* 155:174 */     String prefix = (String)this.URItoPrefix.get(ns);
/* 156:175 */     return prefix + ":" + local;
/* 157:    */   }
/* 158:    */   
/* 159:    */   public String usePrefix(String uri)
/* 160:    */   {
/* 161:183 */     return shortForm(uri);
/* 162:    */   }
/* 163:    */   
/* 164:    */   public String shortForm(String uri)
/* 165:    */   {
/* 166:194 */     Map.Entry e = findMapping(uri, true);
/* 167:195 */     return e.getKey() + ":" + uri.substring(((String)e.getValue()).length());
/* 168:    */   }
/* 169:    */   
/* 170:    */   public boolean samePrefixMappingAs(PrefixMapping other)
/* 171:    */   {
/* 172:199 */     return (other instanceof PrefixMappingImpl) ? equals((PrefixMappingImpl)other) : equalsByMap(other);
/* 173:    */   }
/* 174:    */   
/* 175:    */   protected boolean equals(PrefixMappingImpl other)
/* 176:    */   {
/* 177:206 */     return other.sameAs(this);
/* 178:    */   }
/* 179:    */   
/* 180:    */   protected boolean sameAs(PrefixMappingImpl other)
/* 181:    */   {
/* 182:209 */     return this.prefixToURI.equals(other.prefixToURI);
/* 183:    */   }
/* 184:    */   
/* 185:    */   protected final boolean equalsByMap(PrefixMapping other)
/* 186:    */   {
/* 187:212 */     return getNsPrefixMap().equals(other.getNsPrefixMap());
/* 188:    */   }
/* 189:    */   
/* 190:    */   private Map.Entry findMapping(String uri, boolean partial)
/* 191:    */   {
/* 192:225 */     Iterator it = prefixToURI.entrySet().iterator();
                  Map.Entry e;
                  while (it.hasNext())
                  {
                      e = (Map.Entry)it.next();
                      String ss = (String)e.getValue();                      
                      if (uri.startsWith(ss) && (partial || ss.length() == uri.length())) 
                     // if ((!uri.startsWith(ss)) || ((!partial) && (ss.length() != uri.length()))) 
                        return e;                      
                  }
/* 200:231 */     return null;
/* 201:    */   }
/* 202:    */ }
