/*   1:    */ package it.polimi.elet.contextaddict.microjena.shared;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.shared.impl.PrefixMappingImpl;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.OWL;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*   8:    */ 
/*   9:    */ public abstract interface PrefixMapping
/*  10:    */ {
/*  11:    */   public abstract PrefixMapping setNsPrefix(String paramString1, String paramString2);
/*  12:    */   
/*  13:    */   public abstract PrefixMapping removeNsPrefix(String paramString);
/*  14:    */   
/*  15:    */   public abstract PrefixMapping setNsPrefixes(PrefixMapping paramPrefixMapping);
/*  16:    */   
/*  17:    */   public abstract PrefixMapping setNsPrefixes(Map paramMap);
/*  18:    */   
/*  19:    */   public abstract PrefixMapping withDefaultMappings(PrefixMapping paramPrefixMapping);
/*  20:    */   
/*  21:    */   public abstract String getNsPrefixURI(String paramString);
/*  22:    */   
/*  23:    */   public abstract String getNsURIPrefix(String paramString);
/*  24:    */   
/*  25:    */   public abstract Map getNsPrefixMap();
/*  26:    */   
/*  27:    */   public abstract String expandPrefix(String paramString);
/*  28:    */   
/*  29:    */   public abstract String shortForm(String paramString);
/*  30:    */   
/*  31:    */   /**
/*  32:    */    
/*  34:    */   public abstract String usePrefix(String paramString);
/*  35:    */   
/*  36:    */   public abstract String qnameFor(String paramString);
/*  37:    */   
/*  38:    */   public abstract PrefixMapping lock();
/*  39:    */   
/*  40:    */   public abstract boolean samePrefixMappingAs(PrefixMapping paramPrefixMapping);
/*  41:    */   
/*  42:    */   public static class IllegalPrefixException
/*  43:    */     extends JenaException
/*  44:    */   {
/*  45:    */     public IllegalPrefixException(String prefixName)
/*  46:    */     {
/*  47:169 */       super();
/*  48:    */     }
/*  49:    */   }
/*  50:    */   
/*  51:    */   public static class JenaLockedException
/*  52:    */     extends JenaException
/*  53:    */   {
/*  54:    */     public JenaLockedException(PrefixMapping pm)
/*  55:    */     {
/*  56:178 */       super();
/*  57:    */     }
/*  58:    */   }
/*  59:    */   
/*  60:    */   public static class Factory
/*  61:    */   {
/*  62:    */     public static PrefixMapping create()
/*  63:    */     {
/*  64:187 */       return new PrefixMappingImpl();
/*  65:    */     }
/*  66:    */   }
/*  67:    */   
/*  68:195 */   public static final PrefixMapping Standard = Factory.create().setNsPrefix("rdfs", RDFS.getURI()).setNsPrefix("rdf", RDF.getURI()).setNsPrefix("owl", OWL.getURI()).setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#").lock();
/*  69:    */ }
