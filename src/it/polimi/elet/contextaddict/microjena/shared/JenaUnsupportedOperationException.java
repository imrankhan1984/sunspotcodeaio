/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ public class JenaUnsupportedOperationException
/*  4:   */   extends JenaException
/*  5:   */ {
/*  6:   */   public JenaUnsupportedOperationException(String message)
/*  7:   */   {
/*  8:20 */     super(message);
/*  9:   */   }
/* 10:   */ }
