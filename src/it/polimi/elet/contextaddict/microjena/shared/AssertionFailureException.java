/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ public class AssertionFailureException
/*  4:   */   extends JenaException
/*  5:   */ {
/*  6:   */   public AssertionFailureException(String message)
/*  7:   */   {
/*  8:16 */     super(message);
/*  9:   */   }
/* 10:   */ }

