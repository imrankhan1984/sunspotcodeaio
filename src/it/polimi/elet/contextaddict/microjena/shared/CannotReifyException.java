/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  4:   */ 
/*  5:   */ public class CannotReifyException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public CannotReifyException(Node n)
/*  9:   */   {
/* 10:18 */     super(n.toString());
/* 11:   */   }
/* 12:   */ }
