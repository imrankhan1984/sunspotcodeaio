/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ public class ReificationStyle
/*  4:   */ {
/*  5:16 */   public static final ReificationStyle Standard = new ReificationStyle(true, false);
/*  6:17 */   public static final ReificationStyle Convenient = new ReificationStyle(true, true);
/*  7:18 */   public static final ReificationStyle Minimal = new ReificationStyle(false, true);
/*  8:   */   private boolean intercept;
/*  9:   */   private boolean conceal;
/* 10:   */   
/* 11:   */   public ReificationStyle(boolean intercept, boolean conceal)
/* 12:   */   {
/* 13:24 */     this.intercept = intercept;
/* 14:25 */     this.conceal = conceal;
/* 15:   */   }
/* 16:   */   
/* 17:   */   public boolean intercepts()
/* 18:   */   {
/* 19:29 */     return this.intercept;
/* 20:   */   }
/* 21:   */   
/* 22:   */   public boolean conceals()
/* 23:   */   {
/* 24:33 */     return this.conceal;
/* 25:   */   }
/* 26:   */   
/* 27:   */   public String toString()
/* 28:   */   {
/* 29:43 */     if (this == Minimal) {
/* 30:43 */       return "Minimal";
/* 31:   */     }
/* 32:44 */     if (this == Standard) {
/* 33:44 */       return "Standard";
/* 34:   */     }
/* 35:45 */     if (this == Convenient) {
/* 36:45 */       return "Convenient";
/* 37:   */     }
/* 38:46 */     return "<style int=" + this.intercept + ", con=" + this.conceal + ">";
/* 39:   */   }
/* 40:   */ }
