/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ public class BadCharLiteralException
/*  4:   */   extends JenaException
/*  5:   */ {
/*  6:   */   public BadCharLiteralException(String spelling)
/*  7:   */   {
/*  8:15 */     super(spelling);
/*  9:   */   }
/* 10:   */ }

