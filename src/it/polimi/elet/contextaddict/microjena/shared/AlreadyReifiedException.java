/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  4:   */ 
/*  5:   */ public class AlreadyReifiedException
/*  6:   */   extends CannotReifyException
/*  7:   */ {
/*  8:   */   public AlreadyReifiedException(Node n)
/*  9:   */   {
/* 10:18 */     super(n);
/* 11:   */   }
/* 12:   */ }

