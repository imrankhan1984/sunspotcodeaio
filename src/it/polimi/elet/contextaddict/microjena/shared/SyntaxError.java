/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ public class SyntaxError
/*  4:   */   extends JenaException
/*  5:   */ {
/*  6:   */   public SyntaxError(String message)
/*  7:   */   {
/*  8:15 */     super(message);
/*  9:   */   }
/* 10:   */ }
