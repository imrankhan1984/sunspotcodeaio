/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ public class BadBooleanException
/*  4:   */   extends JenaException
/*  5:   */ {
/*  6:   */   public BadBooleanException(String spelling)
/*  7:   */   {
/*  8:16 */     super(spelling);
/*  9:   */   }
/* 10:   */ }

