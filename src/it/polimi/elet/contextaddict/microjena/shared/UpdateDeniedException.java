/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*  4:   */ 
/*  5:   */ public class UpdateDeniedException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   private Triple triple;
/*  9:   */   
/* 10:   */   public UpdateDeniedException(String message)
/* 11:   */   {
/* 12:21 */     super(message);
/* 13:   */   }
/* 14:   */   
/* 15:   */   public UpdateDeniedException(String message, Triple triple)
/* 16:   */   {
/* 17:25 */     super(message + triple.toString());
/* 18:26 */     this.triple = triple;
/* 19:   */   }
/* 20:   */   
/* 21:   */   public Triple getTriple()
/* 22:   */   {
/* 23:30 */     return this.triple;
/* 24:   */   }
/* 25:   */ }
