/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  4:   */ 
/*  5:   */ public class PropertyNotFoundException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public PropertyNotFoundException(Property p)
/*  9:   */   {
/* 10:17 */     super(p.toString());
/* 11:   */   }
/* 12:   */ }

