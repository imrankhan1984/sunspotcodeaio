/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*  4:   */ 
/*  5:   */ public class ClosedException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   private Graph graph;
/*  9:   */   
/* 10:   */   public ClosedException(String message, Graph graph)
/* 11:   */   {
/* 12:20 */     super(message);
/* 13:21 */     this.graph = graph;
/* 14:   */   }
/* 15:   */   
/* 16:   */   public Graph getGraph()
/* 17:   */   {
/* 18:28 */     return this.graph;
/* 19:   */   }
/* 20:   */ }

