/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*  4:   */ 
/*  5:   */ public class AddDeniedException
/*  6:   */   extends UpdateDeniedException
/*  7:   */ {
/*  8:   */   public AddDeniedException(String message)
/*  9:   */   {
/* 10:17 */     super(message);
/* 11:   */   }
/* 12:   */   
/* 13:   */   public AddDeniedException(String message, Triple triple)
/* 14:   */   {
/* 15:21 */     super(message, triple);
/* 16:   */   }
/* 17:   */ }

