/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ public class InvalidPropertyURIException
/*  4:   */   extends JenaException
/*  5:   */ {
/*  6:   */   public InvalidPropertyURIException(String uri)
/*  7:   */   {
/*  8:17 */     super(uri);
/*  9:   */   }
/* 10:   */ }
