/*  1:   */ package it.polimi.elet.contextaddict.microjena.shared;
/*  2:   */ 
/*  3:   */ public class JenaException
/*  4:   */   extends RuntimeException
/*  5:   */ {
/*  6:   */   private Throwable cause;
/*  7:   */   
/*  8:   */   public JenaException(String message)
/*  9:   */   {
/* 10:23 */     super(message);
/* 11:   */   }
/* 12:   */   
/* 13:   */   public JenaException() {}
/* 14:   */   
/* 15:   */   public Throwable getCause()
/* 16:   */   {
/* 17:35 */     return this.cause;
/* 18:   */   }
/* 19:   */   
/* 20:   */   public JenaException(Throwable cause)
/* 21:   */   {
/* 22:44 */     this("rethrew: " + cause.toString(), cause);
/* 23:   */   }
/* 24:   */   
/* 25:   */   public JenaException(String message, Throwable cause)
/* 26:   */   {
/* 27:47 */     super(message);this.cause = cause;
/* 28:   */   }
/* 29:   */   
/* 30:   */   public void printStackTrace()
/* 31:   */   {
/* 32:54 */     if (this.cause != null) {
/* 33:54 */       this.cause.printStackTrace();
/* 34:   */     }
/* 35:55 */     super.printStackTrace();
/* 36:   */   }
/* 37:   */ }

