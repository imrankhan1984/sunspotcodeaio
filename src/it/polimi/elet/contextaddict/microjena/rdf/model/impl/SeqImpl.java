/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Alt;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Bag;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Container;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.NodeIterator;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ObjectF;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResourceF;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Seq;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.SeqIndexBoundsException;
/*  20:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  21:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  22:    */ 
/*  23:    */ public class SeqImpl
/*  24:    */   extends ContainerImpl
/*  25:    */   implements Seq
/*  26:    */ {
/*  27: 35 */   public static final Implementation factory = new Implementation()
/*  28:    */   {
/*  29:    */     public boolean canWrap(Node n, EnhGraph eg)
/*  30:    */     {
/*  31: 37 */       return true;
/*  32:    */     }
/*  33:    */     
/*  34:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  35:    */     {
/*  36: 39 */       return new SeqImpl(n, eg);
/*  37:    */     }
/*  38:    */   };
/*  39:    */   
/*  40:    */   public SeqImpl(ModelCom model)
/*  41:    */   {
/*  42: 45 */     super(model);
/*  43:    */   }
/*  44:    */   
/*  45:    */   public SeqImpl(String uri, ModelCom model)
/*  46:    */   {
/*  47: 49 */     super(uri, model);
/*  48:    */   }
/*  49:    */   
/*  50:    */   public SeqImpl(Resource r, ModelCom m)
/*  51:    */   {
/*  52: 53 */     super(r, m);
/*  53:    */   }
/*  54:    */   
/*  55:    */   public SeqImpl(Node n, EnhGraph g)
/*  56:    */   {
/*  57: 57 */     super(n, g);
/*  58:    */   }
/*  59:    */   
/*  60:    */   public Resource getResource(int index)
/*  61:    */   {
/*  62: 61 */     return getRequiredProperty(RDF.li(index)).getResource();
/*  63:    */   }
/*  64:    */   
/*  65:    */   public Literal getLiteral(int index)
/*  66:    */   {
/*  67: 65 */     return getRequiredProperty(RDF.li(index)).getLiteral();
/*  68:    */   }
/*  69:    */   
/*  70:    */   public RDFNode getObject(int index)
/*  71:    */   {
/*  72: 69 */     return getRequiredProperty(RDF.li(index)).getObject();
/*  73:    */   }
/*  74:    */   
/*  75:    */   public boolean getBoolean(int index)
/*  76:    */   {
/*  77: 73 */     checkIndex(index);
/*  78: 74 */     return getRequiredProperty(RDF.li(index)).getBoolean();
/*  79:    */   }
/*  80:    */   
/*  81:    */   public byte getByte(int index)
/*  82:    */   {
/*  83: 78 */     checkIndex(index);
/*  84: 79 */     return getRequiredProperty(RDF.li(index)).getByte();
/*  85:    */   }
/*  86:    */   
/*  87:    */   public short getShort(int index)
/*  88:    */   {
/*  89: 83 */     checkIndex(index);
/*  90: 84 */     return getRequiredProperty(RDF.li(index)).getShort();
/*  91:    */   }
/*  92:    */   
/*  93:    */   public int getInt(int index)
/*  94:    */   {
/*  95: 88 */     checkIndex(index);
/*  96: 89 */     return getRequiredProperty(RDF.li(index)).getInt();
/*  97:    */   }
/*  98:    */   
/*  99:    */   public long getLong(int index)
/* 100:    */   {
/* 101: 93 */     checkIndex(index);
/* 102: 94 */     return getRequiredProperty(RDF.li(index)).getLong();
/* 103:    */   }
/* 104:    */   
/* 105:    */   public char getChar(int index)
/* 106:    */   {
/* 107: 98 */     checkIndex(index);
/* 108: 99 */     return getRequiredProperty(RDF.li(index)).getChar();
/* 109:    */   }
/* 110:    */   
/* 111:    */   public float getFloat(int index)
/* 112:    */   {
/* 113:103 */     checkIndex(index);
/* 114:104 */     return getRequiredProperty(RDF.li(index)).getFloat();
/* 115:    */   }
/* 116:    */   
/* 117:    */   public double getDouble(int index)
/* 118:    */   {
/* 119:108 */     checkIndex(index);
/* 120:109 */     return getRequiredProperty(RDF.li(index)).getDouble();
/* 121:    */   }
/* 122:    */   
/* 123:    */   public String getString(int index)
/* 124:    */   {
/* 125:113 */     checkIndex(index);
/* 126:114 */     return getRequiredProperty(RDF.li(index)).getString();
/* 127:    */   }
/* 128:    */   
/* 129:    */   public String getLanguage(int index)
/* 130:    */   {
/* 131:118 */     checkIndex(index);
/* 132:119 */     return getRequiredProperty(RDF.li(index)).getLanguage();
/* 133:    */   }
/* 134:    */   
/* 135:    */   public Object getObject(int index, ObjectF f)
/* 136:    */   {
/* 137:123 */     return getRequiredProperty(RDF.li(index)).getObject(f);
/* 138:    */   }
/* 139:    */   
/* 140:    */   public Resource getResource(int index, ResourceF f)
/* 141:    */   {
/* 142:127 */     return getRequiredProperty(RDF.li(index)).getResource(f);
/* 143:    */   }
/* 144:    */   
/* 145:    */   public Bag getBag(int index)
/* 146:    */   {
/* 147:131 */     checkIndex(index);
/* 148:132 */     return getRequiredProperty(RDF.li(index)).getBag();
/* 149:    */   }
/* 150:    */   
/* 151:    */   public Alt getAlt(int index)
/* 152:    */   {
/* 153:136 */     checkIndex(index);
/* 154:137 */     return getRequiredProperty(RDF.li(index)).getAlt();
/* 155:    */   }
/* 156:    */   
/* 157:    */   public Seq getSeq(int index)
/* 158:    */   {
/* 159:141 */     checkIndex(index);
/* 160:142 */     return getRequiredProperty(RDF.li(index)).getSeq();
/* 161:    */   }
/* 162:    */   
/* 163:    */   public Seq set(int index, RDFNode o)
/* 164:    */   {
/* 165:146 */     checkIndex(index);
/* 166:147 */     getRequiredProperty(RDF.li(index)).changeObject(o);
/* 167:148 */     return this;
/* 168:    */   }
/* 169:    */   
/* 170:    */   public Seq set(int index, boolean o)
/* 171:    */   {
/* 172:152 */     checkIndex(index);
/* 173:153 */     getRequiredProperty(RDF.li(index)).changeObject(o);
/* 174:154 */     return this;
/* 175:    */   }
/* 176:    */   
/* 177:    */   public Seq set(int index, long o)
/* 178:    */   {
/* 179:158 */     checkIndex(index);
/* 180:159 */     getRequiredProperty(RDF.li(index)).changeObject(o);
/* 181:160 */     return this;
/* 182:    */   }
/* 183:    */   
/* 184:    */   public Seq set(int index, float o)
/* 185:    */   {
/* 186:164 */     checkIndex(index);
/* 187:165 */     getRequiredProperty(RDF.li(index)).changeObject(o);
/* 188:166 */     return this;
/* 189:    */   }
/* 190:    */   
/* 191:    */   public Seq set(int index, double o)
/* 192:    */   {
/* 193:170 */     checkIndex(index);
/* 194:171 */     getRequiredProperty(RDF.li(index)).changeObject(o);
/* 195:172 */     return this;
/* 196:    */   }
/* 197:    */   
/* 198:    */   public Seq set(int index, char o)
/* 199:    */   {
/* 200:176 */     checkIndex(index);
/* 201:177 */     getRequiredProperty(RDF.li(index)).changeObject(o);
/* 202:178 */     return this;
/* 203:    */   }
/* 204:    */   
/* 205:    */   public Seq set(int index, String o)
/* 206:    */   {
/* 207:182 */     checkIndex(index);
/* 208:183 */     getRequiredProperty(RDF.li(index)).changeObject(o);
/* 209:184 */     return this;
/* 210:    */   }
/* 211:    */   
/* 212:    */   public Seq set(int index, String o, String l)
/* 213:    */   {
/* 214:188 */     checkIndex(index);
/* 215:189 */     getRequiredProperty(RDF.li(index)).changeObject(o, l);
/* 216:190 */     return this;
/* 217:    */   }
/* 218:    */   
/* 219:    */   public Seq set(int index, Object o)
/* 220:    */   {
/* 221:194 */     checkIndex(index);
/* 222:195 */     getRequiredProperty(RDF.li(index)).changeObject(o);
/* 223:196 */     return this;
/* 224:    */   }
/* 225:    */   
/* 226:    */   public Seq add(int index, boolean o)
/* 227:    */   {
/* 228:200 */     return add(index, String.valueOf(o));
/* 229:    */   }
/* 230:    */   
/* 231:    */   public Seq add(int index, long o)
/* 232:    */   {
/* 233:204 */     return add(index, String.valueOf(o));
/* 234:    */   }
/* 235:    */   
/* 236:    */   public Seq add(int index, char o)
/* 237:    */   {
/* 238:208 */     return add(index, String.valueOf(o));
/* 239:    */   }
/* 240:    */   
/* 241:    */   public Seq add(int index, float o)
/* 242:    */   {
/* 243:212 */     return add(index, String.valueOf(o));
/* 244:    */   }
/* 245:    */   
/* 246:    */   public Seq add(int index, double o)
/* 247:    */   {
/* 248:216 */     return add(index, String.valueOf(o));
/* 249:    */   }
/* 250:    */   
/* 251:    */   public Seq add(int index, Object o)
/* 252:    */   {
/* 253:220 */     return add(index, String.valueOf(o));
/* 254:    */   }
/* 255:    */   
/* 256:    */   public Seq add(int index, String o)
/* 257:    */   {
/* 258:224 */     return add(index, o, "");
/* 259:    */   }
/* 260:    */   
/* 261:    */   public Seq add(int index, String o, String l)
/* 262:    */   {
/* 263:228 */     return add(index, literal(o, l));
/* 264:    */   }
/* 265:    */   
/* 266:    */   public Seq add(int index, RDFNode o)
/* 267:    */   {
/* 268:232 */     int size = size();
/* 269:233 */     checkIndex(index, size + 1);
/* 270:234 */     shiftUp(index, size);
/* 271:235 */     addProperty(RDF.li(index), o);
/* 272:236 */     return this;
/* 273:    */   }
/* 274:    */   
/* 275:    */   public NodeIterator iterator()
/* 276:    */   {
/* 277:240 */     return listContainerMembers();
/* 278:    */   }
/* 279:    */   
/* 280:    */   public Container remove(Statement s)
/* 281:    */   {
/* 282:244 */     getModel().remove(s);
/* 283:245 */     shiftDown(s.getPredicate().getOrdinal() + 1, size() + 1);
/* 284:246 */     return this;
/* 285:    */   }
/* 286:    */   
/* 287:    */   public Seq remove(int index)
/* 288:    */   {
/* 289:250 */     getRequiredProperty(RDF.li(index)).remove();
/* 290:251 */     shiftDown(index + 1, size() + 1);
/* 291:252 */     return this;
/* 292:    */   }
/* 293:    */   
/* 294:    */   public Container remove(int index, RDFNode o)
/* 295:    */   {
/* 296:256 */     return remove(getModel().createStatement(this, RDF.li(index), o).remove());
/* 297:    */   }
/* 298:    */   
/* 299:    */   public int indexOf(RDFNode o)
/* 300:    */   {
/* 301:260 */     return containerIndexOf(o);
/* 302:    */   }
/* 303:    */   
/* 304:    */   public int indexOf(boolean o)
/* 305:    */   {
/* 306:264 */     return indexOf(String.valueOf(o));
/* 307:    */   }
/* 308:    */   
/* 309:    */   public int indexOf(long o)
/* 310:    */   {
/* 311:268 */     return indexOf(String.valueOf(o));
/* 312:    */   }
/* 313:    */   
/* 314:    */   public int indexOf(char o)
/* 315:    */   {
/* 316:272 */     return indexOf(String.valueOf(o));
/* 317:    */   }
/* 318:    */   
/* 319:    */   public int indexOf(float o)
/* 320:    */   {
/* 321:276 */     return indexOf(String.valueOf(o));
/* 322:    */   }
/* 323:    */   
/* 324:    */   public int indexOf(double o)
/* 325:    */   {
/* 326:280 */     return indexOf(String.valueOf(o));
/* 327:    */   }
/* 328:    */   
/* 329:    */   public int indexOf(Object o)
/* 330:    */   {
/* 331:284 */     return indexOf(String.valueOf(o));
/* 332:    */   }
/* 333:    */   
/* 334:    */   public int indexOf(String o)
/* 335:    */   {
/* 336:288 */     return indexOf(o, "");
/* 337:    */   }
/* 338:    */   
/* 339:    */   public int indexOf(String o, String l)
/* 340:    */   {
/* 341:292 */     return indexOf(literal(o, l));
/* 342:    */   }
/* 343:    */   
/* 344:    */   private Literal literal(String s, String lang)
/* 345:    */   {
/* 346:296 */     return new LiteralImpl(Node.createLiteral(s, lang, false), getModelCom());
/* 347:    */   }
/* 348:    */   
/* 349:    */   protected void shiftUp(int start, int finish)
/* 350:    */   {
/* 351:300 */     Statement stmt = null;
/* 352:301 */     for (int i = finish; i >= start; i--)
/* 353:    */     {
/* 354:302 */       stmt = getRequiredProperty(RDF.li(i));
/* 355:303 */       getModel().remove(stmt);
/* 356:304 */       addProperty(RDF.li(i + 1), stmt.getObject());
/* 357:    */     }
/* 358:    */   }
/* 359:    */   
/* 360:    */   protected void shiftDown(int start, int finish)
/* 361:    */   {
/* 362:309 */     for (int i = start; i <= finish; i++)
/* 363:    */     {
/* 364:310 */       Statement stmt = getRequiredProperty(RDF.li(i));
/* 365:311 */       stmt.remove();
/* 366:312 */       addProperty(RDF.li(i - 1), stmt.getObject());
/* 367:    */     }
/* 368:    */   }
/* 369:    */   
/* 370:    */   protected void checkIndex(int index)
/* 371:    */   {
/* 372:317 */     checkIndex(index, size());
/* 373:    */   }
/* 374:    */   
/* 375:    */   protected void checkIndex(int index, int max)
/* 376:    */   {
/* 377:321 */     if ((1 > index) || (index > max)) {
/* 378:322 */       throw new SeqIndexBoundsException(max, index);
/* 379:    */     }
/* 380:    */   }
/* 381:    */ }
