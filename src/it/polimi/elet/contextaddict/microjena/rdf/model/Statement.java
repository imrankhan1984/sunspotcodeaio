package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.graph.FrontsTriple;

public abstract interface Statement
  extends FrontsTriple
{
  public abstract boolean equals(Object paramObject);
  
  public abstract int hashCode();
  
  public abstract Resource getSubject();
  
  public abstract Property getPredicate();
  
  public abstract RDFNode getObject();
  
  public abstract Statement getProperty(Property paramProperty);
  
  public abstract Statement getStatementProperty(Property paramProperty);
  
  public abstract Resource getResource();
  
  public abstract Literal getLiteral();
  
  public abstract boolean getBoolean();
  
  public abstract byte getByte();
  
  public abstract short getShort();
  
  public abstract int getInt();
  
  public abstract long getLong();
  
  public abstract char getChar();
  
  public abstract float getFloat();
  
  public abstract double getDouble();
  
  public abstract String getString();
  
  public abstract Resource getResource(ResourceF paramResourceF);
  
  public abstract Object getObject(ObjectF paramObjectF);
  
  public abstract Bag getBag();
  
  public abstract Alt getAlt();
  
  public abstract Seq getSeq();
  
  public abstract String getLanguage();
  
  /**
   * @deprecated
   */
  public abstract boolean getWellFormed();
  
  public abstract boolean hasWellFormedXML();
  
  public abstract Statement changeObject(boolean paramBoolean);
  
  public abstract Statement changeObject(long paramLong);
  
  public abstract Statement changeObject(char paramChar);
  
  public abstract Statement changeObject(float paramFloat);
  
  public abstract Statement changeObject(double paramDouble);
  
  public abstract Statement changeObject(String paramString);
  
  public abstract Statement changeObject(String paramString, boolean paramBoolean);
  
  public abstract Statement changeObject(String paramString1, String paramString2);
  
  public abstract Statement changeObject(String paramString1, String paramString2, boolean paramBoolean);
  
  public abstract Statement changeObject(RDFNode paramRDFNode);
  
  public abstract Statement changeObject(Object paramObject);
  
  public abstract Statement remove();
  
  public abstract Statement remove(boolean paramBoolean);
  
  public abstract boolean isReified();
  
  public abstract ReifiedStatement createReifiedStatement();
  
  public abstract ReifiedStatement createReifiedStatement(String paramString);
  
  public abstract RSIterator listReifiedStatements();
  
  public abstract Model getModel();
  
  public abstract void removeReification();
}
