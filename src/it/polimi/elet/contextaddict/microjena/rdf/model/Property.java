package it.polimi.elet.contextaddict.microjena.rdf.model;

public abstract interface Property
  extends Resource
{
  public abstract boolean isProperty();
  
  public abstract String getNameSpace();
  
  public abstract String getLocalName();
  
  public abstract int getOrdinal();
}

