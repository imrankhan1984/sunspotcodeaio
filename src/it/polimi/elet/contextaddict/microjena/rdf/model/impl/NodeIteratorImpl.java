/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.NodeIterator;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  7:   */ 
/*  8:   */ public class NodeIteratorImpl
/*  9:   */   extends WrappedIterator
/* 10:   */   implements NodeIterator
/* 11:   */ {
/* 12:   */   public NodeIteratorImpl(Iterator iter, Object object)
/* 13:   */   {
/* 14:24 */     super(iter);
/* 15:   */   }
/* 16:   */   
/* 17:   */   public NodeIteratorImpl(Iterator iter)
/* 18:   */   {
/* 19:28 */     super(iter);
/* 20:   */   }
/* 21:   */   
/* 22:   */   public RDFNode nextNode()
/* 23:   */   {
/* 24:32 */     return (RDFNode)super.next();
/* 25:   */   }
/* 26:   */ }

