/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.RSIterator;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.ReifiedStatement;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  6:   */ 
/*  7:   */ public class RSIteratorImpl
/*  8:   */   extends ResIteratorImpl
/*  9:   */   implements RSIterator
/* 10:   */ {
/* 11:   */   public RSIteratorImpl(Iterator iterator)
/* 12:   */   {
/* 13:17 */     super(iterator);
/* 14:   */   }
/* 15:   */   
/* 16:   */   public ReifiedStatement nextRS()
/* 17:   */   {
/* 18:22 */     return (ReifiedStatement)next();
/* 19:   */   }
/* 20:   */ }
