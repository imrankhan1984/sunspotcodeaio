package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;
import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
import it.polimi.elet.contextaddict.microjena.shared.ReificationStyle;
import it.polimi.elet.contextaddict.microjena.util.Iterator;
import it.polimi.elet.contextaddict.microjena.util.List;
import java.io.InputStream;
import java.io.OutputStream;

public abstract interface Model
  extends ModelCon, PrefixMapping, ModelGraphInterface
{
  public abstract long size();
  
  public abstract boolean isEmpty();
  
  public abstract ResIterator listSubjects();
  
  public abstract NsIterator listNameSpaces();
  
  public abstract Resource getResource(String paramString);
  
  public abstract Property getProperty(String paramString1, String paramString2);
  
  public abstract Resource createResource();
  
  public abstract Resource createResource(AnonId paramAnonId);
  
  public abstract Resource createResource(String paramString);
  
  public abstract Property createProperty(String paramString1, String paramString2);
  
  public abstract Literal createLiteral(String paramString1, String paramString2);
  
  /**
   * @deprecated
   */
  public abstract Literal createLiteral(String paramString1, String paramString2, boolean paramBoolean);
  
  public abstract Literal createLiteral(String paramString, boolean paramBoolean);
  
  public abstract Literal createTypedLiteral(String paramString, RDFDatatype paramRDFDatatype);
  
  public abstract Literal createTypedLiteral(Object paramObject, RDFDatatype paramRDFDatatype);
  
  public abstract Literal createTypedLiteral(Object paramObject);
  
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, RDFNode paramRDFNode);
  
  public abstract RDFList createList();
  
  public abstract RDFList createList(Iterator paramIterator);
  
  public abstract RDFList createList(RDFNode[] paramArrayOfRDFNode);
  
  public abstract Model add(Statement paramStatement);
  
  public abstract Model add(Statement[] paramArrayOfStatement);
  
  public abstract Model remove(Statement[] paramArrayOfStatement);
  
  public abstract Model add(List paramList);
  
  public abstract Model remove(List paramList);
  
  public abstract Model add(StmtIterator paramStmtIterator);
  
  public abstract Model add(Model paramModel);
  
  public abstract Model write(OutputStream paramOutputStream, String paramString);
  
  public abstract Model read(InputStream paramInputStream, String paramString1, String paramString2);
  
  public abstract Model read(InputStream paramInputStream, String paramString);
  
  public abstract Model remove(Statement paramStatement);
  
  public abstract Statement getRequiredProperty(Resource paramResource, Property paramProperty);
  
  public abstract Statement getProperty(Resource paramResource, Property paramProperty);
  
  public abstract ResIterator listSubjectsWithProperty(Property paramProperty);
  
  public abstract ResIterator listSubjectsWithProperty(Property paramProperty, RDFNode paramRDFNode);
  
  public abstract NodeIterator listObjects();
  
  public abstract NodeIterator listObjectsOfProperty(Property paramProperty);
  
  public abstract NodeIterator listObjectsOfProperty(Resource paramResource, Property paramProperty);
  
  public abstract boolean contains(Resource paramResource, Property paramProperty);
  
  public abstract boolean containsResource(RDFNode paramRDFNode);
  
  public abstract boolean contains(Resource paramResource, Property paramProperty, RDFNode paramRDFNode);
  
  public abstract boolean contains(Statement paramStatement);
  
  public abstract boolean containsAny(StmtIterator paramStmtIterator);
  
  public abstract boolean containsAll(StmtIterator paramStmtIterator);
  
  public abstract boolean containsAny(Model paramModel);
  
  public abstract boolean containsAll(Model paramModel);
  
  public abstract boolean isReified(Statement paramStatement);
  
  public abstract Resource getAnyReifiedStatement(Statement paramStatement);
  
  public abstract void removeAllReifications(Statement paramStatement);
  
  public abstract void removeReification(ReifiedStatement paramReifiedStatement);
  
  public abstract StmtIterator listStatements();
  
  public abstract StmtIterator listStatements(Selector paramSelector);
  
  public abstract StmtIterator listStatements(Resource paramResource, Property paramProperty, RDFNode paramRDFNode);
  
  public abstract ReifiedStatement createReifiedStatement(Statement paramStatement);
  
  public abstract ReifiedStatement createReifiedStatement(String paramString, Statement paramStatement);
  
  public abstract RSIterator listReifiedStatements();
  
  public abstract RSIterator listReifiedStatements(Statement paramStatement);
  
  public abstract ReificationStyle getReificationStyle();
  
  public abstract Model query(Selector paramSelector);
  
  public abstract Model union(Model paramModel);
  
  public abstract Model intersection(Model paramModel);
  
  public abstract Model difference(Model paramModel);
  
  public abstract boolean equals(Object paramObject);
  
  public abstract boolean independent();
  
  public abstract boolean supportsSetOperations();
  
  public abstract void close();
  
  public abstract Model removeAll();
  
  public abstract Model removeAll(Resource paramResource, Property paramProperty, RDFNode paramRDFNode);
  
  public abstract boolean isClosed();
}
