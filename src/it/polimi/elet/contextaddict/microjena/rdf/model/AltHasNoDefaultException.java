/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  4:   */ 
/*  5:   */ public class AltHasNoDefaultException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public AltHasNoDefaultException(Alt a)
/*  9:   */   {
/* 10:17 */     super(a.toString());
/* 11:   */   }
/* 12:   */ }
