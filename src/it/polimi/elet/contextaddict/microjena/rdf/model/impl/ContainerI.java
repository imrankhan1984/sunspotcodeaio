package it.polimi.elet.contextaddict.microjena.rdf.model.impl;

import it.polimi.elet.contextaddict.microjena.rdf.model.Container;
import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;

abstract interface ContainerI
{
  public abstract Container remove(int paramInt, RDFNode paramRDFNode);
}
