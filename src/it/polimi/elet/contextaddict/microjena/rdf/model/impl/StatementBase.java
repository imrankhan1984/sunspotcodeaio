/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResourceRequiredException;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  12:    */ 
/*  13:    */ public abstract class StatementBase
/*  14:    */ {
/*  15:    */   protected final ModelCom model;
/*  16:    */   
/*  17:    */   protected StatementBase(ModelCom model)
/*  18:    */   {
/*  19: 30 */     if (model == null) {
/*  20: 30 */       throw new JenaException("Statement models must no be null");
/*  21:    */     }
/*  22: 31 */     this.model = model;
/*  23:    */   }
/*  24:    */   
/*  25:    */   public Model getModel()
/*  26:    */   {
/*  27: 35 */     return this.model;
/*  28:    */   }
/*  29:    */   
/*  30:    */   protected abstract StatementImpl replace(RDFNode paramRDFNode);
/*  31:    */   
/*  32:    */   public abstract Literal getLiteral();
/*  33:    */   
/*  34:    */   public abstract Resource getResource();
/*  35:    */   
/*  36:    */   public abstract Resource getSubject();
/*  37:    */   
/*  38:    */   public abstract Property getPredicate();
/*  39:    */   
/*  40:    */   public abstract RDFNode getObject();
/*  41:    */   
/*  42:    */   protected StatementImpl stringReplace(String s, String lang, boolean wellFormed)
/*  43:    */   {
/*  44: 60 */     return replace(new LiteralImpl(Node.createLiteral(s, lang, wellFormed), this.model));
/*  45:    */   }
/*  46:    */   
/*  47:    */   protected StatementImpl stringReplace(String s)
/*  48:    */   {
/*  49: 70 */     return stringReplace(s, "", false);
/*  50:    */   }
/*  51:    */   
/*  52:    */   public Statement changeObject(boolean o)
/*  53:    */   {
/*  54: 73 */     return stringReplace(String.valueOf(o));
/*  55:    */   }
/*  56:    */   
/*  57:    */   public Statement changeObject(long o)
/*  58:    */   {
/*  59: 76 */     return stringReplace(String.valueOf(o));
/*  60:    */   }
/*  61:    */   
/*  62:    */   public Statement changeObject(char o)
/*  63:    */   {
/*  64: 79 */     return stringReplace(String.valueOf(o));
/*  65:    */   }
/*  66:    */   
/*  67:    */   public Statement changeObject(float o)
/*  68:    */   {
/*  69: 82 */     return stringReplace(String.valueOf(o));
/*  70:    */   }
/*  71:    */   
/*  72:    */   public Statement changeObject(double o)
/*  73:    */   {
/*  74: 85 */     return stringReplace(String.valueOf(o));
/*  75:    */   }
/*  76:    */   
/*  77:    */   public Statement changeObject(String o)
/*  78:    */   {
/*  79: 88 */     return stringReplace(String.valueOf(o));
/*  80:    */   }
/*  81:    */   
/*  82:    */   public Statement changeObject(String o, boolean wellFormed)
/*  83:    */   {
/*  84: 91 */     return stringReplace(String.valueOf(o), "", wellFormed);
/*  85:    */   }
/*  86:    */   
/*  87:    */   public Statement changeObject(String o, String l)
/*  88:    */   {
/*  89: 94 */     return stringReplace(String.valueOf(o), l, false);
/*  90:    */   }
/*  91:    */   
/*  92:    */   public Statement changeObject(String o, String l, boolean wellFormed)
/*  93:    */   {
/*  94: 97 */     return stringReplace(String.valueOf(o), l, wellFormed);
/*  95:    */   }
/*  96:    */   
/*  97:    */   public Statement changeObject(RDFNode o)
/*  98:    */   {
/*  99:100 */     return replace(o);
/* 100:    */   }
/* 101:    */   
/* 102:    */   public Statement changeObject(Object o)
/* 103:    */   {
/* 104:103 */     return (o instanceof RDFNode) ? replace((RDFNode)o) : stringReplace(o.toString());
/* 105:    */   }
/* 106:    */   
/* 107:    */   public boolean getBoolean()
/* 108:    */   {
/* 109:109 */     return getLiteral().getBoolean();
/* 110:    */   }
/* 111:    */   
/* 112:    */   public byte getByte()
/* 113:    */   {
/* 114:112 */     return getLiteral().getByte();
/* 115:    */   }
/* 116:    */   
/* 117:    */   public short getShort()
/* 118:    */   {
/* 119:115 */     return getLiteral().getShort();
/* 120:    */   }
/* 121:    */   
/* 122:    */   public int getInt()
/* 123:    */   {
/* 124:118 */     return getLiteral().getInt();
/* 125:    */   }
/* 126:    */   
/* 127:    */   public long getLong()
/* 128:    */   {
/* 129:121 */     return getLiteral().getLong();
/* 130:    */   }
/* 131:    */   
/* 132:    */   public char getChar()
/* 133:    */   {
/* 134:124 */     return getLiteral().getChar();
/* 135:    */   }
/* 136:    */   
/* 137:    */   public float getFloat()
/* 138:    */   {
/* 139:127 */     return getLiteral().getFloat();
/* 140:    */   }
/* 141:    */   
/* 142:    */   public double getDouble()
/* 143:    */   {
/* 144:130 */     return getLiteral().getDouble();
/* 145:    */   }
/* 146:    */   
/* 147:    */   public String getString()
/* 148:    */   {
/* 149:133 */     return getLiteral().getLexicalForm();
/* 150:    */   }
/* 151:    */   
/* 152:    */   protected Resource mustBeResource(RDFNode n)
/* 153:    */   {
/* 154:139 */     if ((n instanceof Resource)) {
/* 155:140 */       return (Resource)n;
/* 156:    */     }
/* 157:142 */     throw new ResourceRequiredException(n);
/* 158:    */   }
/* 159:    */   
/* 160:    */   public String getLanguage()
/* 161:    */   {
/* 162:146 */     return getLiteral().getLanguage();
/* 163:    */   }
/* 164:    */   
/* 165:    */   public boolean getWellFormed()
/* 166:    */   {
/* 167:149 */     return hasWellFormedXML();
/* 168:    */   }
/* 169:    */   
/* 170:    */   public boolean hasWellFormedXML()
/* 171:    */   {
/* 172:152 */     return getLiteral().isWellFormedXML();
/* 173:    */   }
/* 174:    */   
/* 175:    */   public String toString()
/* 176:    */   {
/* 177:159 */     return "[" + getSubject().toString() + ", " + getPredicate().toString() + ", " + objectString(getObject()) + "]";
/* 178:    */   }
/* 179:    */   
/* 180:    */   protected String objectString(RDFNode object)
/* 181:    */   {
/* 182:171 */     return object.asNode().toString(null, true);
/* 183:    */   }
/* 184:    */ }
