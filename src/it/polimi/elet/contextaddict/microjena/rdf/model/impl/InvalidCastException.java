/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  4:   */ 
/*  5:   */ public class InvalidCastException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public InvalidCastException() {}
/*  9:   */   
/* 10:   */   public InvalidCastException(String destination, String original)
/* 11:   */   {
/* 12:26 */     super("Cannot convert " + original + " instance in a " + destination + " instance.");
/* 13:   */   }
/* 14:   */ }
