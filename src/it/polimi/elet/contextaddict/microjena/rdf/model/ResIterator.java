package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface ResIterator
  extends ExtendedIterator
{
  public abstract Resource nextResource();
}
