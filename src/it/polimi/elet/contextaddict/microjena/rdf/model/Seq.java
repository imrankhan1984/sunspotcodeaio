package it.polimi.elet.contextaddict.microjena.rdf.model;

public abstract interface Seq
  extends Container
{
  public abstract Seq add(int paramInt, RDFNode paramRDFNode);
  
  public abstract Seq add(int paramInt, boolean paramBoolean);
  
  public abstract Seq add(int paramInt, long paramLong);
  
  public abstract Seq add(int paramInt, char paramChar);
  
  public abstract Seq add(int paramInt, float paramFloat);
  
  public abstract Seq add(int paramInt, double paramDouble);
  
  public abstract Seq add(int paramInt, String paramString);
  
  public abstract Seq add(int paramInt, String paramString1, String paramString2);
  
  public abstract Seq add(int paramInt, Object paramObject);
  
  public abstract boolean getBoolean(int paramInt);
  
  public abstract byte getByte(int paramInt);
  
  public abstract short getShort(int paramInt);
  
  public abstract int getInt(int paramInt);
  
  public abstract long getLong(int paramInt);
  
  public abstract char getChar(int paramInt);
  
  public abstract float getFloat(int paramInt);
  
  public abstract double getDouble(int paramInt);
  
  public abstract String getString(int paramInt);
  
  public abstract String getLanguage(int paramInt);
  
  public abstract Object getObject(int paramInt, ObjectF paramObjectF);
  
  public abstract Resource getResource(int paramInt, ResourceF paramResourceF);
  
  public abstract Literal getLiteral(int paramInt);
  
  public abstract Resource getResource(int paramInt);
  
  public abstract RDFNode getObject(int paramInt);
  
  public abstract Bag getBag(int paramInt);
  
  public abstract Alt getAlt(int paramInt);
  
  public abstract Seq getSeq(int paramInt);
  
  public abstract Seq remove(int paramInt);
  
  public abstract int indexOf(RDFNode paramRDFNode);
  
  public abstract int indexOf(boolean paramBoolean);
  
  public abstract int indexOf(long paramLong);
  
  public abstract int indexOf(char paramChar);
  
  public abstract int indexOf(float paramFloat);
  
  public abstract int indexOf(double paramDouble);
  
  public abstract int indexOf(String paramString);
  
  public abstract int indexOf(String paramString1, String paramString2);
  
  public abstract int indexOf(Object paramObject);
  
  public abstract Seq set(int paramInt, RDFNode paramRDFNode);
  
  public abstract Seq set(int paramInt, boolean paramBoolean);
  
  public abstract Seq set(int paramInt, long paramLong);
  
  public abstract Seq set(int paramInt, char paramChar);
  
  public abstract Seq set(int paramInt, float paramFloat);
  
  public abstract Seq set(int paramInt, double paramDouble);
  
  public abstract Seq set(int paramInt, String paramString);
  
  public abstract Seq set(int paramInt, String paramString1, String paramString2);
  
  public abstract Seq set(int paramInt, Object paramObject);
}
