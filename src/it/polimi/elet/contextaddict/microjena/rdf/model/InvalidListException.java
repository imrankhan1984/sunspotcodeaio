/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  4:   */ 
/*  5:   */ public class InvalidListException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public InvalidListException()
/*  9:   */   {
/* 10:42 */     super("Tried to operate on a list that is not well-formed");
/* 11:   */   }
/* 12:   */   
/* 13:   */   public InvalidListException(String msg)
/* 14:   */   {
/* 15:51 */     super(msg);
/* 16:   */   }
/* 17:   */ }

