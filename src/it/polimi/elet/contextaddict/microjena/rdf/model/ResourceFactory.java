/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.impl.LiteralLabel;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.LiteralImpl;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.PropertyImpl;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.ResourceImpl;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.StatementImpl;
/*  10:    */ 
/*  11:    */ public class ResourceFactory
/*  12:    */ {
/*  13: 38 */   protected static Interface instance = new Impl();
/*  14:    */   
/*  15:    */   public static Interface getInstance()
/*  16:    */   {
/*  17: 48 */     return instance;
/*  18:    */   }
/*  19:    */   
/*  20:    */   public static Interface setInstance(Interface newInstance)
/*  21:    */   {
/*  22: 56 */     Interface previousInstance = instance;
/*  23: 57 */     instance = newInstance;
/*  24: 58 */     return previousInstance;
/*  25:    */   }
/*  26:    */   
/*  27:    */   public static Resource createResource()
/*  28:    */   {
/*  29: 68 */     return instance.createResource();
/*  30:    */   }
/*  31:    */   
/*  32:    */   public static Resource createResource(String uriref)
/*  33:    */   {
/*  34: 79 */     return instance.createResource(uriref);
/*  35:    */   }
/*  36:    */   
/*  37:    */   public static Literal createPlainLiteral(String string)
/*  38:    */   {
/*  39: 83 */     return instance.createPlainLiteral(string);
/*  40:    */   }
/*  41:    */   
/*  42:    */   public static Literal createTypedLiteral(String string, RDFDatatype dType)
/*  43:    */   {
/*  44: 88 */     return instance.createTypedLiteral(string, dType);
/*  45:    */   }
/*  46:    */   
/*  47:    */   public static Literal createTypedLiteral(Object value)
/*  48:    */   {
/*  49: 97 */     return instance.createTypedLiteral(value);
/*  50:    */   }
/*  51:    */   
/*  52:    */   public static Property createProperty(String uriref)
/*  53:    */   {
/*  54:108 */     return instance.createProperty(uriref);
/*  55:    */   }
/*  56:    */   
/*  57:    */   public static Property createProperty(String namespace, String localName)
/*  58:    */   {
/*  59:120 */     return instance.createProperty(namespace, localName);
/*  60:    */   }
/*  61:    */   
/*  62:    */   public static Statement createStatement(Resource subject, Property predicate, RDFNode object)
/*  63:    */   {
/*  64:135 */     return instance.createStatement(subject, predicate, object);
/*  65:    */   }
/*  66:    */   
/*  67:    */   static class Impl
/*  68:    */     implements ResourceFactory.Interface
/*  69:    */   {
/*  70:    */     public Resource createResource()
/*  71:    */     {
/*  72:212 */       return new ResourceImpl();
/*  73:    */     }
/*  74:    */     
/*  75:    */     public Resource createResource(String uriref)
/*  76:    */     {
/*  77:216 */       return new ResourceImpl(uriref);
/*  78:    */     }
/*  79:    */     
/*  80:    */     public Literal createPlainLiteral(String string)
/*  81:    */     {
/*  82:220 */       return new LiteralImpl(string);
/*  83:    */     }
/*  84:    */     
/*  85:    */     public Literal createTypedLiteral(String string, RDFDatatype dType)
/*  86:    */     {
/*  87:225 */       return new LiteralImpl(Node.createLiteral(string, "", dType), null);
/*  88:    */     }
/*  89:    */     
/*  90:    */     public Literal createTypedLiteral(Object value)
/*  91:    */     {
/*  92:229 */       return new LiteralImpl(Node.createLiteral(new LiteralLabel(value)), null);
/*  93:    */     }
/*  94:    */     
/*  95:    */     public Property createProperty(String uriref)
/*  96:    */     {
/*  97:233 */       return new PropertyImpl(uriref);
/*  98:    */     }
/*  99:    */     
/* 100:    */     public Property createProperty(String namespace, String localName)
/* 101:    */     {
/* 102:237 */       return new PropertyImpl(namespace, localName);
/* 103:    */     }
/* 104:    */     
/* 105:    */     public Statement createStatement(Resource subject, Property predicate, RDFNode object)
/* 106:    */     {
/* 107:244 */       return new StatementImpl(subject, predicate, object);
/* 108:    */     }
/* 109:    */   }
/* 110:    */   
/* 111:    */   public static abstract interface Interface
/* 112:    */   {
/* 113:    */     public abstract Resource createResource();
/* 114:    */     
/* 115:    */     public abstract Resource createResource(String paramString);
/* 116:    */     
/* 117:    */     public abstract Literal createPlainLiteral(String paramString);
/* 118:    */     
/* 119:    */     public abstract Literal createTypedLiteral(String paramString, RDFDatatype paramRDFDatatype);
/* 120:    */     
/* 121:    */     public abstract Literal createTypedLiteral(Object paramObject);
/* 122:    */     
/* 123:    */     public abstract Property createProperty(String paramString);
/* 124:    */     
/* 125:    */     public abstract Property createProperty(String paramString1, String paramString2);
/* 126:    */     
/* 127:    */     public abstract Statement createStatement(Resource paramResource, Property paramProperty, RDFNode paramRDFNode);
/* 128:    */   }
/* 129:    */ }
