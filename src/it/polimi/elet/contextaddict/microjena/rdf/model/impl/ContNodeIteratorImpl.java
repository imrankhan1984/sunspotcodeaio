/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Container;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.NodeIterator;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  9:   */ import java.util.NoSuchElementException;
/* 10:   */ import java.util.Vector;
/* 11:   */ 
/* 12:   */ public class ContNodeIteratorImpl
/* 13:   */   extends WrappedIterator
/* 14:   */   implements NodeIterator
/* 15:   */ {
/* 16:25 */   protected Statement stmt = null;
/* 17:   */   protected Container cont;
/* 18:   */   protected int size;
/* 19:28 */   protected int index = 0;
/* 20:29 */   protected int numDeleted = 0;
/* 21:30 */   protected Vector moved = new Vector();
/* 22:   */   
/* 23:   */   public ContNodeIteratorImpl(Iterator iterator, Object object, Container cont)
/* 24:   */   {
/* 25:34 */     super(iterator);
/* 26:35 */     this.cont = cont;
/* 27:36 */     this.size = cont.size();
/* 28:   */   }
/* 29:   */   
/* 30:   */   public Object next()
/* 31:   */     throws NoSuchElementException
/* 32:   */   {
/* 33:40 */     this.stmt = ((Statement)super.next());
/* 34:41 */     this.index += 1;
/* 35:42 */     return this.stmt.getObject();
/* 36:   */   }
/* 37:   */   
/* 38:   */   public RDFNode nextNode()
/* 39:   */     throws NoSuchElementException
/* 40:   */   {
/* 41:46 */     return (RDFNode)next();
/* 42:   */   }
/* 43:   */   
/* 44:   */   public void remove()
/* 45:   */     throws NoSuchElementException
/* 46:   */   {
/* 47:50 */     if (this.stmt == null) {
/* 48:51 */       throw new NoSuchElementException();
/* 49:   */     }
/* 50:52 */     super.remove();
/* 51:54 */     if (this.index > this.size - this.numDeleted)
/* 52:   */     {
/* 53:55 */       ((ContainerI)this.cont).remove(((Integer)this.moved.elementAt(this.size - this.index)).intValue(), this.stmt.getObject());
/* 54:   */     }
/* 55:   */     else
/* 56:   */     {
/* 57:59 */       this.cont.remove(this.stmt);
/* 58:60 */       this.moved.addElement(new Integer(this.index));
/* 59:   */     }
/* 60:62 */     this.stmt = null;
/* 61:63 */     this.numDeleted += 1;
/* 62:   */   }
/* 63:   */ }
