package it.polimi.elet.contextaddict.microjena.rdf.model;

public abstract interface Selector
{
  public abstract boolean test(Statement paramStatement);
  
  public abstract boolean isSimple();
  
  public abstract Resource getSubject();
  
  public abstract Property getPredicate();
  
  public abstract RDFNode getObject();
}

