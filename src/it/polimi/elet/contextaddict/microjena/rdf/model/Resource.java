package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;
import it.polimi.elet.contextaddict.microjena.graph.Node;

public abstract interface Resource
  extends RDFNode
{
  public abstract AnonId getId();
  
  /**
   * @deprecated
   */
  public abstract Node getNode();
  
  public abstract boolean hasURI(String paramString);
  
  public abstract String getURI();
  
  public abstract String getNameSpace();
  
  public abstract String getLocalName();
  
  public abstract String toString();
  
  public abstract boolean equals(Object paramObject);
  
  public abstract Statement getRequiredProperty(Property paramProperty);
  
  public abstract Statement getProperty(Property paramProperty);
  
  public abstract StmtIterator listProperties(Property paramProperty);
  
  public abstract StmtIterator listProperties();
  
  /**
   * @deprecated
   */
  public abstract Resource addProperty(Property paramProperty, boolean paramBoolean);
  
  /**
   * @deprecated
   */
  public abstract Resource addProperty(Property paramProperty, long paramLong);
  
  /**
   * @deprecated
   */
  public abstract Resource addProperty(Property paramProperty, char paramChar);
  
  /**
   * @deprecated
   */
  public abstract Resource addProperty(Property paramProperty, float paramFloat);
  
  /**
   * @deprecated
   */
  public abstract Resource addProperty(Property paramProperty, double paramDouble);
  
  public abstract Resource addProperty(Property paramProperty, String paramString);
  
  public abstract Resource addProperty(Property paramProperty, String paramString1, String paramString2);
  
  public abstract Resource addProperty(Property paramProperty, String paramString, RDFDatatype paramRDFDatatype);
  
  /**
   * @deprecated
   */
  public abstract Resource addProperty(Property paramProperty, Object paramObject);
  
  public abstract Resource addProperty(Property paramProperty, RDFNode paramRDFNode);
  
  public abstract boolean hasProperty(Property paramProperty);
  
  /**
   * @deprecated
   */
  public abstract boolean hasProperty(Property paramProperty, boolean paramBoolean);
  
  /**
   * @deprecated
   */
  public abstract boolean hasProperty(Property paramProperty, long paramLong);
  
  /**
   * @deprecated
   */
  public abstract boolean hasProperty(Property paramProperty, char paramChar);
  
  /**
   * @deprecated
   */
  public abstract boolean hasProperty(Property paramProperty, float paramFloat);
  
  /**
   * @deprecated
   */
  public abstract boolean hasProperty(Property paramProperty, double paramDouble);
  
  public abstract boolean hasProperty(Property paramProperty, String paramString);
  
  public abstract boolean hasProperty(Property paramProperty, String paramString1, String paramString2);
  
  /**
   * @deprecated
   */
  public abstract boolean hasProperty(Property paramProperty, Object paramObject);
  
  public abstract boolean hasProperty(Property paramProperty, RDFNode paramRDFNode);
  
  public abstract Resource removeProperties();
  
  public abstract Resource removeAll(Property paramProperty);
  
  public abstract Model getModel();
}
