/*    1:     */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*    2:     */ 
/*    3:     */ import it.polimi.elet.contextaddict.microjena.datatypes.DatatypeFormatException;
/*    4:     */ import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;
/*    5:     */ import it.polimi.elet.contextaddict.microjena.datatypes.TypeMapper;
/*    6:     */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*    7:     */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*    8:     */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*    9:     */ import it.polimi.elet.contextaddict.microjena.graph.Factory;
/*   10:     */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   11:     */ import it.polimi.elet.contextaddict.microjena.graph.GraphUtil;
/*   12:     */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   13:     */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*   14:     */ import it.polimi.elet.contextaddict.microjena.graph.impl.LiteralLabel;
/*   15:     */ import it.polimi.elet.contextaddict.microjena.ontology.ConversionException;
/*   16:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Alt;
/*   17:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.AnonId;
/*   18:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Bag;
/*   19:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Container;
/*   20:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.GraphWriter;
/*   21:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
/*   22:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*   23:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.ModelFactory;
/*   24:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.NodeIterator;
/*   25:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.NsIterator;
/*   26:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*   27:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*   28:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*   29:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RSIterator;
/*   30:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.ReifiedStatement;
/*   31:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResIterator;
/*   32:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*   33:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResourceF;
/*   34:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Selector;
/*   35:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Seq;
/*   36:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*   37:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*   38:     */ import it.polimi.elet.contextaddict.microjena.shared.InvalidPropertyURIException;
/*   39:     */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*   40:     */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*   42:     */ import it.polimi.elet.contextaddict.microjena.shared.PropertyNotFoundException;
/*   43:     */ import it.polimi.elet.contextaddict.microjena.shared.ReificationStyle;
/*   44:     */ import it.polimi.elet.contextaddict.microjena.shared.impl.PrefixMappingImpl;
/*   45:     */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*   46:     */ import it.polimi.elet.contextaddict.microjena.util.List;
/*   47:     */ import it.polimi.elet.contextaddict.microjena.util.Map;
/*   48:     */ import it.polimi.elet.contextaddict.microjena.util.Map.Entry;
/*   49:     */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*   50:     */ import it.polimi.elet.contextaddict.microjena.util.Util;
/*   51:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.ClosableIterator;
/*   52:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*   53:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.IteratorImpl;
/*   54:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*   55:     */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*   56:     */ import java.io.IOException;
/*   57:     */ import java.io.InputStream;
/*   58:     */ import java.io.OutputStream;
/*   59:     */ import java.util.Calendar;
/*   60:     */ import java.util.Vector;
/*   61:     */ 
/*   62:     */ public class ModelCom
/*   63:     */   extends EnhGraph
/*   64:     */   implements Model, PrefixMapping
/*   65:     */ {
/*   66:     */   GraphWriter graphWriter;
/*   67:     */   
/*   68:     */   public ModelCom(Graph base)
/*   69:     */   {
/*   70:  80 */     this(base, false);
/*   71:     */   }
/*   72:     */   
/*   73:     */   public ModelCom(Graph base, boolean excludeAxioms)
/*   74:     */   {
/*   75:  84 */     super(base);
/*   76:  85 */     withDefaultMappings(PrefixMapping.Standard);

/*   77:  86 */     this.graphWriter = new GraphWriter(this);

/*   78:  87 */     if (!excludeAxioms)
                    {            
/*   79:  88 */       RDFAxiomWriter.writeAxioms(getGraph());
/*   80:     */     }
/*   81:     */   }
/*   82:     */   
/*   83:  91 */   private static PrefixMapping defaultPrefixMapping = PrefixMapping.Standard;
/*   84:     */   
/*   85:     */   public static PrefixMapping getDefaultModelPrefixes()
/*   86:     */   {
/*   87:  94 */     return defaultPrefixMapping;
/*   88:     */   }
/*   89:     */   
/*   90:     */   public static PrefixMapping setDefaultModelPrefixes(PrefixMapping pm)
/*   91:     */   {
/*   92:  98 */     PrefixMapping result = defaultPrefixMapping;
/*   93:  99 */     defaultPrefixMapping = pm;
/*   94: 100 */     return result;
/*   95:     */   }
/*   96:     */   
/*   97:     */   public Graph getGraph()
/*   98:     */   {
/*   99: 105 */     return this.graph;
/*  100:     */   }
/*  101:     */   
/*  102:     */   protected static Model createWorkModel()
/*  103:     */   {
/*  104: 109 */     return ModelFactory.createDefaultModel();
/*  105:     */   }
/*  106:     */   
/*  107:     */   public RDFNode asRDFNode(Node n)
/*  108:     */   {
/*  109: 113 */     return new ResourceImpl(n, this);
/*  110:     */   }
/*  111:     */   
/*  112: 119 */   protected ModelReifier modelReifier = new ModelReifier(this);
/*  113:     */   
/*  114:     */   public Resource getResource(String uri, ResourceF f)
/*  115:     */   {
/*  116:     */     try
/*  117:     */     {
/*  118: 123 */       return f.createResource(getResource(uri));
/*  119:     */     }
/*  120:     */     catch (Exception e)
/*  121:     */     {
/*  122: 125 */       throw new JenaException(e);
/*  123:     */     }
/*  124:     */   }
/*  125:     */   
/*  126:     */   public Model add(Resource s, Property p, boolean o)
/*  127:     */   {
/*  128: 130 */     return add(s, p, String.valueOf(o));
/*  129:     */   }
/*  130:     */   
/*  131:     */   public Model add(Resource s, Property p, long o)
/*  132:     */   {
/*  133: 134 */     return add(s, p, String.valueOf(o));
/*  134:     */   }
/*  135:     */   
/*  136:     */   public Model add(Resource s, Property p, char o)
/*  137:     */   {
/*  138: 138 */     return add(s, p, String.valueOf(o));
/*  139:     */   }
/*  140:     */   
/*  141:     */   public Model add(Resource s, Property p, float o)
/*  142:     */   {
/*  143: 142 */     return add(s, p, String.valueOf(o));
/*  144:     */   }
/*  145:     */   
/*  146:     */   public Model add(Resource s, Property p, double o)
/*  147:     */   {
/*  148: 146 */     return add(s, p, String.valueOf(o));
/*  149:     */   }
/*  150:     */   
/*  151:     */   public Model add(Resource s, Property p, String o)
/*  152:     */   {
/*  153: 150 */     return add(s, p, o, "", false);
/*  154:     */   }
/*  155:     */   
/*  156:     */   public Model add(Resource s, Property p, String o, boolean wellFormed)
/*  157:     */   {
/*  158: 154 */     add(s, p, literal(o, "", wellFormed));
/*  159: 155 */     return this;
/*  160:     */   }
/*  161:     */   
/*  162:     */   public Model add(Resource s, Property p, String o, String lang, boolean wellFormed)
/*  163:     */   {
/*  164: 159 */     add(s, p, literal(o, lang, wellFormed));
/*  165: 160 */     return this;
/*  166:     */   }
/*  167:     */   
/*  168:     */   public Model add(Resource s, Property p, String lex, RDFDatatype datatype)
/*  169:     */   {
/*  170: 164 */     add(s, p, literal(lex, datatype));
/*  171: 165 */     return this;
/*  172:     */   }
/*  173:     */   
/*  174:     */   private Literal literal(String s, String lang, boolean wellFormed)
/*  175:     */   {
/*  176: 169 */     return new LiteralImpl(Node.createLiteral(s, lang, wellFormed), this);
/*  177:     */   }
/*  178:     */   
/*  179:     */   private Literal literal(String lex, RDFDatatype datatype)
/*  180:     */   {
/*  181: 172 */     return new LiteralImpl(Node.createLiteral(lex, "", datatype), this);
/*  182:     */   }
/*  183:     */   
/*  184:     */   public Model add(Resource s, Property p, String o, String l)
/*  185:     */   {
/*  186: 175 */     return add(s, p, o, l, false);
/*  187:     */   }
/*  188:     */   
/*  189:     */   private RDFNode ensureRDFNode(Object o)
/*  190:     */   {
/*  191: 183 */     return (o instanceof RDFNode) ? (RDFNode)o : literal(o.toString(), null, false);
/*  192:     */   }
/*  193:     */   
/*  194:     */   public Model add(Resource s, Property p, Object o)
/*  195:     */   {
/*  196: 190 */     return add(s, p, ensureRDFNode(o));
/*  197:     */   }
/*  198:     */   
/*  199:     */   public Model add(StmtIterator iter)
/*  200:     */   {
/*  201:     */     try
/*  202:     */     {
/*  203: 195 */       while (iter.hasNext()) {
/*  204: 196 */         add(iter.nextStatement());
/*  205:     */       }
/*  206:     */     }
/*  207:     */     finally
/*  208:     */     {
/*  209: 198 */       iter.close();
/*  210:     */     }
/*  211: 200 */     return this;
/*  212:     */   }
/*  213:     */   
/*  214:     */   public Model add(Model m)
/*  215:     */   {
/*  216: 204 */     return add(m.listStatements());
/*  217:     */   }
/*  218:     */   
/*  219:     */   public Model read(InputStream reader, String lang)
/*  220:     */   {
/*  221: 208 */     return read(reader, null, lang);
/*  222:     */   }
/*  223:     */   
/*  224:     */   public Model read(InputStream reader, String base, String lang)
/*  225:     */   {
/*  226: 213 */     if (lang.equals("N-TRIPLE"))
/*  227:     */     {
/*  228:     */       try
/*  229:     */       {
/*  230: 215 */         this.graphWriter.readNTriple(reader);
/*  231:     */       }
/*  232:     */       catch (IOException ex)
/*  233:     */       {
/*  234: 217 */         throw new JenaException("An error occoured while reading the model");
/*  235:     */       }
/*  236: 219 */       return this;
/*  237:     */     }
/*  238: 221 */     throw new JenaException("Language " + lang + "is not implemented for input");
/*  239:     */   }
/*  240:     */   
/*  241:     */   public Model write(OutputStream writer, String lang)
/*  242:     */   {
/*  243: 226 */     if (lang.equals("N-TRIPLE"))
/*  244:     */     {
/*  245:     */       try
/*  246:     */       {
/*  247: 228 */         this.graphWriter.writeNTriple(writer);
/*  248:     */       }
/*  249:     */       catch (IOException ex)
/*  250:     */       {
/*  251: 230 */         throw new JenaException("An error occoured while writing the model");
/*  252:     */       }
/*  253: 232 */       return this;
/*  254:     */     }
/*  255: 234 */     throw new JenaException("Language " + lang + "is not implemented for output");
/*  256:     */   }
/*  257:     */   
/*  258:     */   public Model remove(Statement s)
/*  259:     */   {
/*  260: 239 */     return remove(s.getSubject(), s.getPredicate(), s.getObject());
/*  261:     */   }
/*  262:     */   
/*  263:     */   public Model remove(Resource s, Property p, RDFNode o)
/*  264:     */   {
/*  265: 243 */     return remove(s, p, o, false);
/*  266:     */   }
/*  267:     */   
/*  268:     */   public Model remove(Resource s, Property p, RDFNode o, boolean removeAxiom)
/*  269:     */   {
/*  270: 247 */     this.graph.delete(Triple.create(s.asNode(), p.asNode(), o.asNode()), removeAxiom);
/*  271: 248 */     return this;
/*  272:     */   }
/*  273:     */   
/*  274:     */   public Model remove(StmtIterator iter)
/*  275:     */   {
/*  276: 252 */     while (iter.hasNext()) {
/*  277: 253 */       remove(iter.nextStatement());
/*  278:     */     }
/*  279: 254 */     return this;
/*  280:     */   }
/*  281:     */   
/*  282:     */   public Model remove(Model m)
/*  283:     */   {
/*  284: 258 */     return remove(m.listStatements());
/*  285:     */   }
/*  286:     */   
/*  287:     */   public Model removeAll()
/*  288:     */   {
/*  289: 262 */     removeAll(Node.ANY, Node.ANY, Node.ANY);
/*  290: 263 */     return this;
/*  291:     */   }
/*  292:     */   
/*  293:     */   public Model removeAll(Resource s, Property p, RDFNode o)
/*  294:     */   {
/*  295: 267 */     removeAll(asNode(s), asNode(p), asNode(o));
/*  296: 268 */     return this;
/*  297:     */   }
/*  298:     */   
/*  299:     */   private Model removeAll(Node s, Node p, Node o)
/*  300:     */   {
/*  301: 272 */     ExtendedIterator it = getGraph().find(s, p, o);
/*  302: 273 */     while (it.hasNext()) {
/*  303: 274 */       getGraph().delete((Triple)it.next());
/*  304:     */     }
/*  305: 275 */     return this;
/*  306:     */   }
/*  307:     */   
/*  308:     */   public boolean contains(Resource s, Property p, boolean o)
/*  309:     */   {
/*  310: 279 */     return contains(s, p, String.valueOf(o));
/*  311:     */   }
/*  312:     */   
/*  313:     */   public boolean contains(Resource s, Property p, long o)
/*  314:     */   {
/*  315: 283 */     return contains(s, p, String.valueOf(o));
/*  316:     */   }
/*  317:     */   
/*  318:     */   public boolean contains(Resource s, Property p, char o)
/*  319:     */   {
/*  320: 287 */     return contains(s, p, String.valueOf(o));
/*  321:     */   }
/*  322:     */   
/*  323:     */   public boolean contains(Resource s, Property p, float o)
/*  324:     */   {
/*  325: 291 */     return contains(s, p, String.valueOf(o));
/*  326:     */   }
/*  327:     */   
/*  328:     */   public boolean contains(Resource s, Property p, double o)
/*  329:     */   {
/*  330: 295 */     return contains(s, p, String.valueOf(o));
/*  331:     */   }
/*  332:     */   
/*  333:     */   public boolean contains(Resource s, Property p, String o)
/*  334:     */   {
/*  335: 299 */     return contains(s, p, o, "");
/*  336:     */   }
/*  337:     */   
/*  338:     */   public boolean contains(Resource s, Property p, String o, String l)
/*  339:     */   {
/*  340: 303 */     return contains(s, p, literal(o, l, false));
/*  341:     */   }
/*  342:     */   
/*  343:     */   public boolean contains(Resource s, Property p, Object o)
/*  344:     */   {
/*  345: 307 */     return contains(s, p, ensureRDFNode(o));
/*  346:     */   }
/*  347:     */   
/*  348:     */   public boolean containsAny(Model model)
/*  349:     */   {
/*  350: 311 */     return containsAnyThenClose(model.listStatements());
/*  351:     */   }
/*  352:     */   
/*  353:     */   public boolean containsAll(Model model)
/*  354:     */   {
/*  355: 315 */     return containsAllThenClose(model.listStatements());
/*  356:     */   }
/*  357:     */   
/*  358:     */   protected boolean containsAnyThenClose(StmtIterator iter)
/*  359:     */   {
/*  360:     */     try
/*  361:     */     {
/*  362: 320 */       return containsAny(iter);
/*  363:     */     }
/*  364:     */     finally
/*  365:     */     {
/*  366: 322 */       iter.close();
/*  367:     */     }
/*  368:     */   }
/*  369:     */   
/*  370:     */   protected boolean containsAllThenClose(StmtIterator iter)
/*  371:     */   {
/*  372:     */     try
/*  373:     */     {
/*  374: 328 */       return containsAll(iter);
/*  375:     */     }
/*  376:     */     finally
/*  377:     */     {
/*  378: 330 */       iter.close();
/*  379:     */     }
/*  380:     */   }
/*  381:     */   
/*  382:     */   public boolean containsAny(StmtIterator iter)
/*  383:     */   {
/*  384: 335 */     while (iter.hasNext()) {
/*  385: 335 */       if (contains(iter.nextStatement())) {
/*  386: 335 */         return true;
/*  387:     */       }
/*  388:     */     }
/*  389: 336 */     return false;
/*  390:     */   }
/*  391:     */   
/*  392:     */   public boolean containsAll(StmtIterator iter)
/*  393:     */   {
/*  394: 340 */     while (iter.hasNext()) {
/*  395: 340 */       if (!contains(iter.nextStatement())) {
/*  396: 340 */         return false;
/*  397:     */       }
/*  398:     */     }
/*  399: 341 */     return true;
/*  400:     */   }
/*  401:     */   
/*  402:     */   protected StmtIterator listStatements(Resource S, Property P, Node O)
/*  403:     */   {
/*  404: 345 */     return IteratorCast.asStmtIterator(this.graph.find(asNode(S), asNode(P), O), this);
/*  405:     */   }
/*  406:     */   
/*  407:     */   public StmtIterator listStatements(Resource S, Property P, RDFNode O)
/*  408:     */   {
/*  409: 349 */     return listStatements(S, P, asNode(O));
/*  410:     */   }
/*  411:     */   
/*  412:     */   public StmtIterator listStatements(Resource S, Property P, String O)
/*  413:     */   {
/*  414: 353 */     return O == null ? listStatements(S, P, Node.ANY) : listStatements(S, P, Node.createLiteral(O));
/*  415:     */   }
/*  416:     */   
/*  417:     */   public StmtIterator listStatements(Resource S, Property P, String O, String L)
/*  418:     */   {
/*  419: 359 */     return O == null ? listStatements(S, P, Node.ANY) : listStatements(S, P, Node.createLiteral(O, L, false));
/*  420:     */   }
/*  421:     */   
/*  422:     */   public StmtIterator listStatements(Resource S, Property P, boolean O)
/*  423:     */   {
/*  424: 365 */     return listStatements(S, P, String.valueOf(O));
/*  425:     */   }
/*  426:     */   
/*  427:     */   public StmtIterator listStatements(Resource S, Property P, long O)
/*  428:     */   {
/*  429: 369 */     return listStatements(S, P, String.valueOf(O));
/*  430:     */   }
/*  431:     */   
/*  432:     */   public StmtIterator listStatements(Resource S, Property P, char O)
/*  433:     */   {
/*  434: 373 */     return listStatements(S, P, String.valueOf(O));
/*  435:     */   }
/*  436:     */   
/*  437:     */   public StmtIterator listStatements(Resource S, Property P, float O)
/*  438:     */   {
/*  439: 377 */     return listStatements(S, P, String.valueOf(O));
/*  440:     */   }
/*  441:     */   
/*  442:     */   public StmtIterator listStatements(Resource S, Property P, double O)
/*  443:     */   {
/*  444: 381 */     return listStatements(S, P, String.valueOf(O));
/*  445:     */   }
/*  446:     */   
/*  447:     */   public ResIterator listSubjectsWithProperty(Property p, boolean o)
/*  448:     */   {
/*  449: 385 */     return listSubjectsWithProperty(p, String.valueOf(o));
/*  450:     */   }
/*  451:     */   
/*  452:     */   public ResIterator listSubjectsWithProperty(Property p, long o)
/*  453:     */   {
/*  454: 389 */     return listSubjectsWithProperty(p, String.valueOf(o));
/*  455:     */   }
/*  456:     */   
/*  457:     */   public ResIterator listSubjectsWithProperty(Property p, char o)
/*  458:     */   {
/*  459: 393 */     return listSubjectsWithProperty(p, String.valueOf(o));
/*  460:     */   }
/*  461:     */   
/*  462:     */   public ResIterator listSubjectsWithProperty(Property p, float o)
/*  463:     */   {
/*  464: 397 */     return listSubjectsWithProperty(p, String.valueOf(o));
/*  465:     */   }
/*  466:     */   
/*  467:     */   public ResIterator listSubjectsWithProperty(Property p, double o)
/*  468:     */   {
/*  469: 401 */     return listSubjectsWithProperty(p, String.valueOf(o));
/*  470:     */   }
/*  471:     */   
/*  472:     */   public ResIterator listSubjectsWithProperty(Property p, String o)
/*  473:     */   {
/*  474: 405 */     return listSubjectsWithProperty(p, o, "");
/*  475:     */   }
/*  476:     */   
/*  477:     */   public ResIterator listSubjectsWithProperty(Property p, String o, String l)
/*  478:     */   {
/*  479: 409 */     return listSubjectsWithProperty(p, literal(o, l, false));
/*  480:     */   }
/*  481:     */   
/*  482:     */   public ResIterator listSubjectsWithProperty(Property p, Object o)
/*  483:     */   {
/*  484: 413 */     return listSubjectsWithProperty(p, ensureRDFNode(o));
/*  485:     */   }
/*  486:     */   
/*  487:     */   public Resource createResource(Resource type)
/*  488:     */   {
/*  489: 417 */     return createResource().addProperty(RDF.type, type);
/*  490:     */   }
/*  491:     */   
/*  492:     */   public Resource createResource(String uri, Resource type)
/*  493:     */   {
/*  494: 421 */     return getResource(uri).addProperty(RDF.type, type);
/*  495:     */   }
/*  496:     */   
/*  497:     */   public Resource createResource(ResourceF f)
/*  498:     */   {
/*  499: 425 */     return createResource(null, f);
/*  500:     */   }
/*  501:     */   
/*  502:     */   public Resource createResource(AnonId id)
/*  503:     */   {
/*  504: 429 */     return new ResourceImpl(id, this);
/*  505:     */   }
/*  506:     */   
/*  507:     */   public Resource createResource(String uri, ResourceF f)
/*  508:     */   {
/*  509: 433 */     return f.createResource(createResource(uri));
/*  510:     */   }
/*  511:     */   
/*  512:     */   public Literal createTypedLiteral(boolean v)
/*  513:     */   {
/*  514: 446 */     return createTypedLiteral(new Boolean(v));
/*  515:     */   }
/*  516:     */   
/*  517:     */   public Literal createTypedLiteral(int v)
/*  518:     */   {
/*  519: 456 */     return createTypedLiteral(new Integer(v));
/*  520:     */   }
/*  521:     */   
/*  522:     */   public Literal createTypedLiteral(long v)
/*  523:     */   {
/*  524: 466 */     return createTypedLiteral(new Long(v));
/*  525:     */   }
/*  526:     */   
/*  527:     */   public Literal createTypedLiteral(char v)
/*  528:     */   {
/*  529: 476 */     return createTypedLiteral(new Character(v));
/*  530:     */   }
/*  531:     */   
/*  532:     */   public Literal createTypedLiteral(float v)
/*  533:     */   {
/*  534: 486 */     return createTypedLiteral(new Float(v));
/*  535:     */   }
/*  536:     */   
/*  537:     */   public Literal createTypedLiteral(double v)
/*  538:     */   {
/*  539: 496 */     return createTypedLiteral(new Double(v));
/*  540:     */   }
/*  541:     */   
/*  542:     */   public Literal createTypedLiteral(String v)
/*  543:     */   {
/*  544: 506 */     LiteralLabel ll = new LiteralLabel(v);
/*  545: 507 */     return new LiteralImpl(Node.createLiteral(ll), this);
/*  546:     */   }
/*  547:     */   
/*  548:     */   public Literal createTypedLiteral(String lex, RDFDatatype dtype)
/*  549:     */     throws DatatypeFormatException
/*  550:     */   {
/*  551: 520 */     return new LiteralImpl(Node.createLiteral(lex, "", dtype), this);
/*  552:     */   }
/*  553:     */   
/*  554:     */   public Literal createTypedLiteral(Object value, RDFDatatype dtype)
/*  555:     */   {
/*  556: 530 */     LiteralLabel ll = new LiteralLabel(value, "", dtype);
/*  557: 531 */     return new LiteralImpl(Node.createLiteral(ll), this);
/*  558:     */   }
/*  559:     */   
/*  560:     */   public Literal createTypedLiteral(String lex, String typeURI)
/*  561:     */   {
/*  562: 544 */     RDFDatatype dt = TypeMapper.getInstance().getSafeTypeByName(typeURI);
/*  563: 545 */     LiteralLabel ll = LiteralLabel.createLiteralLabel(lex, "", dt);
/*  564: 546 */     return new LiteralImpl(Node.createLiteral(ll), this);
/*  565:     */   }
/*  566:     */   
/*  567:     */   public Literal createTypedLiteral(Object value, String typeURI)
/*  568:     */   {
/*  569: 556 */     RDFDatatype dt = TypeMapper.getInstance().getSafeTypeByName(typeURI);
/*  570: 557 */     LiteralLabel ll = new LiteralLabel(value, "", dt);
/*  571: 558 */     return new LiteralImpl(Node.createLiteral(ll), this);
/*  572:     */   }
/*  573:     */   
/*  574:     */   public Literal createTypedLiteral(Object value)
/*  575:     */   {
/*  576: 569 */     if ((value instanceof Calendar)) {
/*  577: 570 */       return createTypedLiteral((Calendar)value);
/*  578:     */     }
/*  579: 571 */     LiteralLabel ll = new LiteralLabel(value);
/*  580: 572 */     return new LiteralImpl(Node.createLiteral(ll), this);
/*  581:     */   }
/*  582:     */   
/*  583:     */   public Literal createLiteral(boolean v)
/*  584:     */   {
/*  585: 576 */     return createLiteral(String.valueOf(v), "");
/*  586:     */   }
/*  587:     */   
/*  588:     */   public Literal createLiteral(int v)
/*  589:     */   {
/*  590: 580 */     return createLiteral(String.valueOf(v), "");
/*  591:     */   }
/*  592:     */   
/*  593:     */   public Literal createLiteral(long v)
/*  594:     */   {
/*  595: 584 */     return createLiteral(String.valueOf(v), "");
/*  596:     */   }
/*  597:     */   
/*  598:     */   public Literal createLiteral(char v)
/*  599:     */   {
/*  600: 588 */     return createLiteral(String.valueOf(v), "");
/*  601:     */   }
/*  602:     */   
/*  603:     */   public Literal createLiteral(float v)
/*  604:     */   {
/*  605: 592 */     return createLiteral(String.valueOf(v), "");
/*  606:     */   }
/*  607:     */   
/*  608:     */   public Literal createLiteral(double v)
/*  609:     */   {
/*  610: 596 */     return createLiteral(String.valueOf(v), "");
/*  611:     */   }
/*  612:     */   
/*  613:     */   public Literal createLiteral(String v)
/*  614:     */   {
/*  615: 600 */     return createLiteral(v, "");
/*  616:     */   }
/*  617:     */   
/*  618:     */   public Literal createLiteral(String v, String l)
/*  619:     */   {
/*  620: 604 */     return literal(v, l, false);
/*  621:     */   }
/*  622:     */   
/*  623:     */   public Literal createLiteral(String v, boolean wellFormed)
/*  624:     */   {
/*  625: 608 */     return literal(v, "", wellFormed);
/*  626:     */   }
/*  627:     */   
/*  628:     */   public Literal createLiteral(String v, String l, boolean wellFormed)
/*  629:     */   {
/*  630: 612 */     return literal(v, l, wellFormed);
/*  631:     */   }
/*  632:     */   
/*  633:     */   public Literal createLiteral(Object v)
/*  634:     */   {
/*  635: 616 */     return createLiteral(v.toString(), "");
/*  636:     */   }
/*  637:     */   
/*  638:     */   public Statement createStatement(Resource r, Property p, boolean o)
/*  639:     */   {
/*  640: 620 */     return createStatement(r, p, createLiteral(o));
/*  641:     */   }
/*  642:     */   
/*  643:     */   public Statement createStatement(Resource r, Property p, long o)
/*  644:     */   {
/*  645: 624 */     return createStatement(r, p, createLiteral(o));
/*  646:     */   }
/*  647:     */   
/*  648:     */   public Statement createStatement(Resource r, Property p, char o)
/*  649:     */   {
/*  650: 628 */     return createStatement(r, p, createLiteral(o));
/*  651:     */   }
/*  652:     */   
/*  653:     */   public Statement createStatement(Resource r, Property p, float o)
/*  654:     */   {
/*  655: 632 */     return createStatement(r, p, createLiteral(o));
/*  656:     */   }
/*  657:     */   
/*  658:     */   public Statement createStatement(Resource r, Property p, double o)
/*  659:     */   {
/*  660: 636 */     return createStatement(r, p, createLiteral(o));
/*  661:     */   }
/*  662:     */   
/*  663:     */   public Statement createStatement(Resource r, Property p, String o)
/*  664:     */   {
/*  665: 640 */     return createStatement(r, p, createLiteral(o));
/*  666:     */   }
/*  667:     */   
/*  668:     */   public Statement createStatement(Resource r, Property p, Object o)
/*  669:     */   {
/*  670: 644 */     return createStatement(r, p, ensureRDFNode(o));
/*  671:     */   }
/*  672:     */   
/*  673:     */   public Statement createStatement(Resource r, Property p, String o, boolean wellFormed)
/*  674:     */   {
/*  675: 648 */     return createStatement(r, p, o, "", wellFormed);
/*  676:     */   }
/*  677:     */   
/*  678:     */   public Statement createStatement(Resource r, Property p, String o, String l)
/*  679:     */   {
/*  680: 652 */     return createStatement(r, p, o, l, false);
/*  681:     */   }
/*  682:     */   
/*  683:     */   public Statement createStatement(Resource r, Property p, String o, String l, boolean wellFormed)
/*  684:     */   {
/*  685: 656 */     return createStatement(r, p, literal(o, l, wellFormed));
/*  686:     */   }
/*  687:     */   
/*  688:     */   public Bag createBag()
/*  689:     */   {
/*  690: 660 */     return createBag(null);
/*  691:     */   }
/*  692:     */   
/*  693:     */   public Alt createAlt()
/*  694:     */   {
/*  695: 664 */     return createAlt(null);
/*  696:     */   }
/*  697:     */   
/*  698:     */   public Seq createSeq()
/*  699:     */   {
/*  700: 668 */     return createSeq(null);
/*  701:     */   }
/*  702:     */   
/*  703:     */   private RDFList createNewList()
/*  704:     */   {
/*  705: 673 */     return new RDFListImpl(Node.createURI(RDF.nil.getURI()), this);
/*  706:     */   }
/*  707:     */   
/*  708:     */   private RDFList createNewList(Resource res)
/*  709:     */   {
/*  710: 678 */     return new RDFListImpl(res.asNode(), this);
/*  711:     */   }
/*  712:     */   
/*  713:     */   public RDFList createList()
/*  714:     */   {
/*  715: 686 */     return createNewList();
/*  716:     */   }
/*  717:     */   
/*  718:     */   public RDFList createList(Iterator members)
/*  719:     */   {
/*  720: 695 */     RDFList list = createList();
/*  721: 697 */     while ((members != null) && (members.hasNext())) {
/*  722: 698 */       list = list.with((RDFNode)members.next());
/*  723:     */     }
/*  724: 701 */     return list;
/*  725:     */   }
/*  726:     */   
/*  727:     */   public RDFList createList(RDFNode[] members)
/*  728:     */   {
/*  729: 711 */     Vector result = new Vector(10, 10);
/*  730: 712 */     for (int i = 0; i < members.length; i++) {
/*  731: 713 */       result.addElement(members[i]);
/*  732:     */     }
/*  733: 714 */     return createList(new IteratorImpl(result));
/*  734:     */   }
/*  735:     */   
/*  736:     */   private Resource getResourceByUri(String uri)
/*  737:     */   {
/*  738: 718 */     Node result = Node.createURI(uri);
/*  739: 719 */     return new ResourceImpl(result, this);
/*  740:     */   }
/*  741:     */   
/*  742:     */   public RDFNode getRDFNode(Node n)
/*  743:     */   {
/*  744: 723 */     return asRDFNode(n);
/*  745:     */   }
/*  746:     */   
/*  747:     */   public Resource getResource(String uri)
/*  748:     */   {
/*  749: 727 */     Resource catched = getResourceByUri(uri);
/*  750: 728 */     return catched;
/*  751:     */   }
/*  752:     */   
/*  753:     */   public Property getProperty(String uri)
/*  754:     */   {
/*  755: 732 */     if (uri == null) {
/*  756: 733 */       throw new InvalidPropertyURIException(null);
/*  757:     */     }
/*  758: 734 */     Resource aus = getResourceByUri(uri);
/*  759: 735 */     if (PropertyImpl.factory.canWrap(aus.asNode(), this)) {
/*  760: 736 */       return new PropertyImpl(aus.asNode(), this);
/*  761:     */     }
/*  762: 738 */     throw new ConversionException("Cannot convert node " + aus + " to OntProperty");
/*  763:     */   }
/*  764:     */   
/*  765:     */   public Alt getAlt(String uri)
/*  766:     */   {
/*  767: 742 */     return getAlt(getResourceByUri(uri));
/*  768:     */   }
/*  769:     */   
/*  770:     */   public Bag getBag(String uri)
/*  771:     */   {
/*  772: 746 */     return getBag(getResourceByUri(uri));
/*  773:     */   }
/*  774:     */   
/*  775:     */   public Seq getSeq(String uri)
/*  776:     */   {
/*  777: 750 */     return getSeq(getResourceByUri(uri));
/*  778:     */   }
/*  779:     */   
/*  780:     */   public Property getProperty(String nameSpace, String localName)
/*  781:     */   {
/*  782: 754 */     return getProperty(nameSpace + localName);
/*  783:     */   }
/*  784:     */   
/*  785:     */   public Seq getSeq(Resource r)
/*  786:     */   {
/*  787: 758 */     if (SeqImpl.factory.canWrap(r.asNode(), this)) {
/*  788: 759 */       return (Seq)SeqImpl.factory.wrap(r.asNode(), this);
/*  789:     */     }
/*  790: 762 */     throw new InvalidCastException("Seq", "Resource");
/*  791:     */   }
/*  792:     */   
/*  793:     */   public Bag getBag(Resource r)
/*  794:     */   {
/*  795: 767 */     if (BagImpl.factory.canWrap(r.asNode(), this)) {
/*  796: 768 */       return (Bag)BagImpl.factory.wrap(r.asNode(), this);
/*  797:     */     }
/*  798: 771 */     throw new InvalidCastException("Bag", "Resource");
/*  799:     */   }
/*  800:     */   
/*  801:     */   private static Node makeURI(String uri)
/*  802:     */   {
/*  803: 776 */     return uri == null ? Node.createAnon() : Node.createURI(uri);
/*  804:     */   }
/*  805:     */   
/*  806:     */   public Alt getAlt(Resource r)
/*  807:     */   {
/*  808: 782 */     if (AltImpl.factory.canWrap(r.asNode(), this)) {
/*  809: 783 */       return (Alt)AltImpl.factory.wrap(r.asNode(), this);
/*  810:     */     }
/*  811: 786 */     throw new InvalidCastException("Alt", "Resource");
/*  812:     */   }
/*  813:     */   
/*  814:     */   public long size()
/*  815:     */   {
/*  816: 791 */     return this.graph.size();
/*  817:     */   }
/*  818:     */   
/*  819:     */   public boolean isEmpty()
/*  820:     */   {
/*  821: 795 */     return this.graph.isEmpty();
/*  822:     */   }
/*  823:     */   
/*  824:     */   private void updateNamespace(Set set, Iterator it)
/*  825:     */   {
/*  826: 799 */     while (it.hasNext())
/*  827:     */     {
/*  828: 800 */       Node node = (Node)it.next();
/*  829: 801 */       if (node.isURI())
/*  830:     */       {
/*  831: 802 */         String uri = node.getURI();
/*  832: 803 */         String ns = uri.substring(0, Util.splitNamespace(uri));
/*  833:     */         
/*  834: 805 */         set.add(ns);
/*  835:     */       }
/*  836:     */     }
/*  837:     */   }
/*  838:     */   
/*  839:     */   private Iterator listPredicates()
/*  840:     */   {
/*  841: 811 */     Set predicates = new Set();
/*  842: 812 */     ClosableIterator it = this.graph.find(Triple.ANY);
/*  843: 813 */     while (it.hasNext()) {
/*  844: 814 */       predicates.add(((Triple)it.next()).getPredicate());
/*  845:     */     }
/*  846: 815 */     return predicates.iterator();
/*  847:     */   }
/*  848:     */   
/*  849:     */   private Iterator listTypes()
/*  850:     */   {
/*  851: 819 */     Set types = new Set();
/*  852: 820 */     ClosableIterator it = this.graph.find(null, RDF.type.asNode(), null);
/*  853: 821 */     while (it.hasNext()) {
/*  854: 821 */       types.add(((Triple)it.next()).getObject());
/*  855:     */     }
/*  856: 822 */     return types.iterator();
/*  857:     */   }
/*  858:     */   
/*  859:     */   public NsIterator listNameSpaces()
/*  860:     */   {
/*  861: 826 */     Set nameSpaces = new Set();
/*  862: 827 */     updateNamespace(nameSpaces, listPredicates());
/*  863: 828 */     updateNamespace(nameSpaces, listTypes());
/*  864: 829 */     return new NsIteratorImpl(nameSpaces.iterator(), nameSpaces);
/*  865:     */   }
/*  866:     */   
/*  867:     */   private PrefixMapping getPrefixMapping()
/*  868:     */   {
/*  869: 833 */     return getGraph().getPrefixMapping();
/*  870:     */   }
/*  871:     */   
/*  872:     */   public boolean samePrefixMappingAs(PrefixMapping other)
/*  873:     */   {
/*  874: 836 */     return getPrefixMapping().samePrefixMappingAs(other);
/*  875:     */   }
/*  876:     */   
/*  877:     */   public PrefixMapping lock()
/*  878:     */   {
/*  879: 839 */     getPrefixMapping().lock();
/*  880: 840 */     return this;
/*  881:     */   }
/*  882:     */   
/*  883:     */   public PrefixMapping setNsPrefix(String prefix, String uri)
/*  884:     */   {
/*  885: 844 */     getPrefixMapping().setNsPrefix(prefix, uri);
/*  886: 845 */     return this;
/*  887:     */   }
/*  888:     */   
/*  889:     */   public PrefixMapping removeNsPrefix(String prefix)
/*  890:     */   {
/*  891: 849 */     getPrefixMapping().removeNsPrefix(prefix);
/*  892: 850 */     return this;
/*  893:     */   }
/*  894:     */   
/*  895:     */   public PrefixMapping setNsPrefixes(PrefixMapping pm)
/*  896:     */   {
/*  897: 854 */     getPrefixMapping().setNsPrefixes(pm);
/*  898: 855 */     return this;
/*  899:     */   }
/*  900:     */   
/*  901:     */   public PrefixMapping setNsPrefixes(Map map)
/*  902:     */   {
/*  903: 859 */     getPrefixMapping().setNsPrefixes(map);
/*  904: 860 */     return this;
/*  905:     */   }
/*  906:     */   
/*  907:     */   public PrefixMapping withDefaultMappings(PrefixMapping other)
/*  908:     */   {
/*  909: 864 */     getPrefixMapping().withDefaultMappings(other);
/*  910: 865 */     return this;
/*  911:     */   }
/*  912:     */   
/*  913:     */   public String getNsPrefixURI(String prefix)
/*  914:     */   {
/*  915: 869 */     return getPrefixMapping().getNsPrefixURI(prefix);
/*  916:     */   }
/*  917:     */   
/*  918:     */   public String getNsURIPrefix(String uri)
/*  919:     */   {
/*  920: 873 */     return getPrefixMapping().getNsURIPrefix(uri);
/*  921:     */   }
/*  922:     */   
/*  923:     */   public Map getNsPrefixMap()
/*  924:     */   {
/*  925: 877 */     return getPrefixMapping().getNsPrefixMap();
/*  926:     */   }
/*  927:     */   
/*  928:     */   public String expandPrefix(String prefixed)
/*  929:     */   {
/*  930: 881 */     return getPrefixMapping().expandPrefix(prefixed);
/*  931:     */   }
/*  932:     */   
/*  933:     */   public String usePrefix(String uri)
/*  934:     */   {
/*  935: 885 */     return getPrefixMapping().shortForm(uri);
/*  936:     */   }
/*  937:     */   
/*  938:     */   public String qnameFor(String uri)
/*  939:     */   {
/*  940: 889 */     return getPrefixMapping().qnameFor(uri);
/*  941:     */   }
/*  942:     */   
/*  943:     */   public String shortForm(String uri)
/*  944:     */   {
/*  945: 893 */     return getPrefixMapping().shortForm(uri);
/*  946:     */   }
/*  947:     */   
/*  948:     */   public static void addNamespaces(Model m, Map ns)
/*  949:     */   {
/*  950: 906 */     PrefixMapping pm = m;
/*  951: 907 */     Iterator it = ns.entrySet().iterator();
/*  952: 908 */     while (it.hasNext())
/*  953:     */     {
/*  954: 909 */       Map.Entry e = (Map.Entry)it.next();
/*  955: 910 */       String key = (String)e.getKey();
/*  956: 911 */       Set values = (Set)e.getValue();
/*  957: 912 */       Set niceValues = new Set();
/*  958: 913 */       Iterator them = values.iterator();
/*  959: 914 */       while (them.hasNext())
/*  960:     */       {
/*  961: 915 */         String uri = (String)them.next();
/*  962: 916 */         if (PrefixMappingImpl.isNiceURI(uri)) {
/*  963: 917 */           niceValues.add(uri);
/*  964:     */         }
/*  965:     */       }
/*  966: 919 */       if (niceValues.size() == 1) {
/*  967: 920 */         pm.setNsPrefix(key, (String)niceValues.iterator().next());
/*  968:     */       }
/*  969:     */     }
/*  970:     */   }
/*  971:     */   
/*  972:     */   public StmtIterator listStatements()
/*  973:     */   {
/*  974: 925 */     return IteratorCast.asStmtIterator(GraphUtil.findAll(this.graph), this);
/*  975:     */   }
/*  976:     */   
/*  977:     */   public Model add(Statement s)
/*  978:     */   {
/*  979: 932 */     add(s.getSubject(), s.getPredicate(), s.getObject());
/*  980: 933 */     return this;
/*  981:     */   }
/*  982:     */   
/*  983:     */   public Model add(Statement[] statements)
/*  984:     */   {
/*  985: 941 */     int i = 0;
/*  986: 942 */     for (i = 0; i < statements.length; i++) {
/*  987: 943 */       add(statements[i]);
/*  988:     */     }
/*  989: 945 */     return this;
/*  990:     */   }
/*  991:     */   
/*  992:     */   public Model add(List statements)
/*  993:     */   {
/*  994: 953 */     ExtendedIterator it = WrappedIterator.create(statements.iterator());
/*  995: 954 */     while (it.hasNext()) {
/*  996: 955 */       add((Statement)it.next());
/*  997:     */     }
/*  998: 957 */     return this;
/*  999:     */   }
/* 1000:     */   
/* 1001:     */   public Model remove(Statement[] statements)
/* 1002:     */   {
/* 1003: 965 */     for (int i = 0; i < statements.length; i++) {
/* 1004: 966 */       remove(statements[i]);
/* 1005:     */     }
/* 1006: 967 */     return this;
/* 1007:     */   }
/* 1008:     */   
/* 1009:     */   public Model remove(List statements)
/* 1010:     */   {
/* 1011: 975 */     ExtendedIterator it = WrappedIterator.create(statements.iterator());
/* 1012: 976 */     while (it.hasNext()) {
/* 1013: 977 */       remove((Statement)it.next());
/* 1014:     */     }
/* 1015: 979 */     return this;
/* 1016:     */   }
/* 1017:     */   
/* 1018:     */   public Model add(Resource s, Property p, RDFNode o)
/* 1019:     */   {
/* 1020: 983 */     this.modelReifier.noteIfReified(s, p, o);
/* 1021: 984 */     this.graph.add(Triple.create(s.asNode(), p.asNode(), o.asNode()));
/* 1022: 985 */     return this;
/* 1023:     */   }
/* 1024:     */   
/* 1025:     */   public ReificationStyle getReificationStyle()
/* 1026:     */   {
/* 1027: 989 */     return this.modelReifier.getReificationStyle();
/* 1028:     */   }
/* 1029:     */   
/* 1030:     */   public RSIterator listReifiedStatements()
/* 1031:     */   {
/* 1032: 995 */     return this.modelReifier.listReifiedStatements();
/* 1033:     */   }
/* 1034:     */   
/* 1035:     */   public RSIterator listReifiedStatements(Statement st)
/* 1036:     */   {
/* 1037:1002 */     return this.modelReifier.listReifiedStatements(st);
/* 1038:     */   }
/* 1039:     */   
/* 1040:     */   public boolean isReified(Statement s)
/* 1041:     */   {
/* 1042:1008 */     return this.modelReifier.isReified(s);
/* 1043:     */   }
/* 1044:     */   
/* 1045:     */   public Resource getAnyReifiedStatement(Statement s)
/* 1046:     */   {
/* 1047:1018 */     return this.modelReifier.getAnyReifiedStatement(s);
/* 1048:     */   }
/* 1049:     */   
/* 1050:     */   public void removeAllReifications(Statement s)
/* 1051:     */   {
/* 1052:1025 */     this.modelReifier.removeAllReifications(s);
/* 1053:     */   }
/* 1054:     */   
/* 1055:     */   public void removeReification(ReifiedStatement rs)
/* 1056:     */   {
/* 1057:1028 */     this.modelReifier.removeReification(rs);
/* 1058:     */   }
/* 1059:     */   
/* 1060:     */   public ReifiedStatement createReifiedStatement(Statement s)
/* 1061:     */   {
/* 1062:1034 */     return this.modelReifier.createReifiedStatement(s);
/* 1063:     */   }
/* 1064:     */   
/* 1065:     */   public ReifiedStatement createReifiedStatement(String uri, Statement s)
/* 1066:     */   {
/* 1067:1037 */     return this.modelReifier.createReifiedStatement(uri, s);
/* 1068:     */   }
/* 1069:     */   
/* 1070:     */   public boolean contains(Statement s)
/* 1071:     */   {
/* 1072:1040 */     return this.graph.contains(s.asTriple());
/* 1073:     */   }
/* 1074:     */   
/* 1075:     */   public boolean containsResource(RDFNode r)
/* 1076:     */   {
/* 1077:1043 */     Node n = r.asNode();
/* 1078:1044 */     Iterator it = this.enhNodes.iterator();
/* 1079:1045 */     while (it.hasNext()) {
/* 1080:1046 */       if (((EnhNode)it.next()).asNode().equals(n)) {
/* 1081:1047 */         return true;
/* 1082:     */       }
/* 1083:     */     }
/* 1084:1048 */     return false;
/* 1085:     */   }
/* 1086:     */   
/* 1087:     */   public boolean contains(Resource s, Property p)
/* 1088:     */   {
/* 1089:1052 */     return contains(s, p, (RDFNode)null);
/* 1090:     */   }
/* 1091:     */   
/* 1092:     */   public boolean contains(Resource s, Property p, RDFNode o)
/* 1093:     */   {
/* 1094:1055 */     return this.graph.contains(asNode(s), asNode(p), asNode(o));
/* 1095:     */   }
/* 1096:     */   
/* 1097:     */   public Statement getRequiredProperty(Resource s, Property p)
/* 1098:     */   {
/* 1099:1059 */     Statement st = getProperty(s, p);
/* 1100:1060 */     if (st == null) {
/* 1101:1061 */       throw new PropertyNotFoundException(p);
/* 1102:     */     }
/* 1103:1062 */     return st;
/* 1104:     */   }
/* 1105:     */   
/* 1106:     */   public Statement getProperty(Resource s, Property p)
/* 1107:     */   {
/* 1108:1066 */     StmtIterator iter = listStatements(s, p, (RDFNode)null);
/* 1109:     */     try
/* 1110:     */     {
/* 1111:1068 */       return iter.hasNext() ? iter.nextStatement() : null;
/* 1112:     */     }
/* 1113:     */     finally
/* 1114:     */     {
/* 1115:1070 */       iter.close();
/* 1116:     */     }
/* 1117:     */   }
/* 1118:     */   
/* 1119:     */   public static Node asNode(RDFNode x)
/* 1120:     */   {
/* 1121:1075 */     return x == null ? Node.ANY : x.asNode();
/* 1122:     */   }
/* 1123:     */   
/* 1124:     */   private NodeIterator listObjectsFor(RDFNode s, RDFNode p)
/* 1125:     */   {
/* 1126:1079 */     StmtIterator it = listStatements((Resource)s, (Property)p, (RDFNode)null);
/* 1127:1080 */     Set objects = new Set();
/* 1128:1081 */     while (it.hasNext()) {
/* 1129:1082 */       objects.add(it.nextStatement().getObject());
/* 1130:     */     }
/* 1131:1083 */     return IteratorCast.asNodeIterator(objects.iterator());
/* 1132:     */   }
/* 1133:     */   
/* 1134:     */   private ResIterator listSubjectsFor(RDFNode p, RDFNode o)
/* 1135:     */   {
/* 1136:1087 */     ExtendedIterator it = listStatements((Resource)null, (Property)p, o);
/* 1137:1088 */     Set subjects = new Set();
/* 1138:1089 */     while (it.hasNext()) {
/* 1139:1090 */       subjects.add(((Statement)it.next()).getSubject());
/* 1140:     */     }
/* 1141:1091 */     return IteratorCast.asResIterator(subjects.iterator());
/* 1142:     */   }
/* 1143:     */   
/* 1144:     */   public ResIterator listSubjects()
/* 1145:     */   {
/* 1146:1095 */     return listSubjectsFor(null, null);
/* 1147:     */   }
/* 1148:     */   
/* 1149:     */   public ResIterator listSubjectsWithProperty(Property p)
/* 1150:     */   {
/* 1151:1098 */     return listSubjectsFor(p, null);
/* 1152:     */   }
/* 1153:     */   
/* 1154:     */   public ResIterator listSubjectsWithProperty(Property p, RDFNode o)
/* 1155:     */   {
/* 1156:1101 */     return listSubjectsFor(p, o);
/* 1157:     */   }
/* 1158:     */   
/* 1159:     */   public NodeIterator listObjects()
/* 1160:     */   {
/* 1161:1104 */     return listObjectsFor(null, null);
/* 1162:     */   }
/* 1163:     */   
/* 1164:     */   public NodeIterator listObjectsOfProperty(Property p)
/* 1165:     */   {
/* 1166:1107 */     return listObjectsFor(null, p);
/* 1167:     */   }
/* 1168:     */   
/* 1169:     */   public NodeIterator listObjectsOfProperty(Resource s, Property p)
/* 1170:     */   {
/* 1171:1110 */     return listObjectsFor(s, p);
/* 1172:     */   }
/* 1173:     */   
/* 1174:     */   public StmtIterator listStatements(Selector selector)
/* 1175:     */   {
/* 1176:1113 */     return listStatements(selector.getSubject(), selector.getPredicate(), selector.getObject());
/* 1177:     */   }
/* 1178:     */   
/* 1179:     */   public ExtendedIterator findTriplesFrom(Selector s)
/* 1180:     */   {
/* 1181:1125 */     return this.graph.find(asNode(s.getSubject()), asNode(s.getPredicate()), asNode(s.getObject()));
/* 1182:     */   }
/* 1183:     */   
/* 1184:     */   public boolean independent()
/* 1185:     */   {
/* 1186:1129 */     return true;
/* 1187:     */   }
/* 1188:     */   
/* 1189:     */   public Resource createResource()
/* 1190:     */   {
/* 1191:1132 */     return new ResourceImpl(Node.createAnon(), this);
/* 1192:     */   }
/* 1193:     */   
/* 1194:     */   public Resource createResource(String uri)
/* 1195:     */   {
/* 1196:1136 */     return getResource(uri);
/* 1197:     */   }
/* 1198:     */   
/* 1199:     */   public Property createProperty(String uri)
/* 1200:     */   {
/* 1201:1140 */     return getProperty(uri);
/* 1202:     */   }
/* 1203:     */   
/* 1204:     */   public Property createProperty(String nameSpace, String localName)
/* 1205:     */   {
/* 1206:1144 */     return getProperty(nameSpace, localName);
/* 1207:     */   }
/* 1208:     */   
/* 1209:     */   public Statement createStatement(Resource r, Property p, RDFNode o)
/* 1210:     */   {
/* 1211:1151 */     return new StatementImpl(r, p, o, this);
/* 1212:     */   }
/* 1213:     */   
/* 1214:     */   public Bag createBag(String uri)
/* 1215:     */   {
/* 1216:1154 */     return (Bag)getBag(uri).addProperty(RDF.type, RDF.Bag);
/* 1217:     */   }
/* 1218:     */   
/* 1219:     */   public Alt createAlt(String uri)
/* 1220:     */   {
/* 1221:1157 */     return (Alt)getAlt(uri).addProperty(RDF.type, RDF.Alt);
/* 1222:     */   }
/* 1223:     */   
/* 1224:     */   public Seq createSeq(String uri)
/* 1225:     */   {
/* 1226:1160 */     return (Seq)getSeq(uri).addProperty(RDF.type, RDF.Seq);
/* 1227:     */   }
/* 1228:     */   
/* 1229:     */   public Statement asStatement(Triple t)
/* 1230:     */   {
/* 1231:1168 */     return StatementImpl.toStatement(t, this);
/* 1232:     */   }
/* 1233:     */   
/* 1234:     */   public Statement[] asStatements(Triple[] triples)
/* 1235:     */   {
/* 1236:1171 */     Statement[] result = new Statement[triples.length];
/* 1237:1172 */     for (int i = 0; i < triples.length; i++) {
/* 1238:1172 */       result[i] = asStatement(triples[i]);
/* 1239:     */     }
/* 1240:1173 */     return result;
/* 1241:     */   }
/* 1242:     */   
/* 1243:     */   public List asStatements(List triples)
/* 1244:     */   {
/* 1245:1177 */     List L = new List(triples.size());
/* 1246:1178 */     for (int i = 0; i < triples.size(); i++) {
/* 1247:1179 */       L.add(asStatement((Triple)triples.get(i)));
/* 1248:     */     }
/* 1249:1180 */     return L;
/* 1250:     */   }
/* 1251:     */   
/* 1252:     */   public Model asModel(Graph g)
/* 1253:     */   {
/* 1254:1184 */     return new ModelCom(g);
/* 1255:     */   }
/* 1256:     */   
/* 1257:     */   public StmtIterator listBySubject(Container cont)
/* 1258:     */   {
/* 1259:1188 */     return listStatements(cont, null, (RDFNode)null);
/* 1260:     */   }
/* 1261:     */   
/* 1262:     */   public void close()
/* 1263:     */   {
/* 1264:1192 */     this.graph.close();
/* 1265:     */   }
/* 1266:     */   
/* 1267:     */   public boolean isClosed()
/* 1268:     */   {
/* 1269:1196 */     return this.graph.isClosed();
/* 1270:     */   }
/* 1271:     */   
/* 1272:     */   public boolean supportsSetOperations()
/* 1273:     */   {
/* 1274:1200 */     return true;
/* 1275:     */   }
/* 1276:     */   
/* 1277:     */   public Model query(Selector selector)
/* 1278:     */   {
/* 1279:1204 */     return createWorkModel().add(listStatements(selector));
/* 1280:     */   }
/* 1281:     */   
/* 1282:     */   public Model union(Model model)
/* 1283:     */   {
/* 1284:1208 */     return createWorkModel().add(this).add(model);
/* 1285:     */   }
/* 1286:     */   
/* 1287:     */   public Model intersection(Model other)
/* 1288:     */   {
/* 1289:1220 */     return size() < other.size() ? intersect(this, other) : intersect(other, this);
/* 1290:     */   }
/* 1291:     */   
/* 1292:     */   public static Model intersect(Model smaller, Model larger)
/* 1293:     */   {
/* 1294:1232 */     Model result = createWorkModel();
/* 1295:1233 */     StmtIterator it = smaller.listStatements();
/* 1296:     */     try
/* 1297:     */     {
/* 1298:1235 */       return addCommon(result, it, larger);
/* 1299:     */     }
/* 1300:     */     finally
/* 1301:     */     {
/* 1302:1237 */       it.close();
/* 1303:     */     }
/* 1304:     */   }
/* 1305:     */   
/* 1306:     */   protected static Model addCommon(Model result, StmtIterator it, Model other)
/* 1307:     */   {
/* 1308:1251 */     while (it.hasNext())
/* 1309:     */     {
/* 1310:1252 */       Statement s = it.nextStatement();
/* 1311:1253 */       if (other.getGraph().contains(s.asTriple())) {
/* 1312:1254 */         result.getGraph().add(s.asTriple());
/* 1313:     */       }
/* 1314:     */     }
/* 1315:1256 */     return result;
/* 1316:     */   }
/* 1317:     */   
/* 1318:     */   public Model difference(Model model)
/* 1319:     */   {
/* 1320:1261 */     Model resultModel = new ModelCom(it.polimi.elet.contextaddict.microjena.graph.Factory.createDefaultGraph(), true);
/* 1321:1262 */     StmtIterator iter = null;
/* 1322:     */     try
/* 1323:     */     {
/* 1324:1265 */       iter = listStatements();
/* 1325:1266 */       while (iter.hasNext())
/* 1326:     */       {
/* 1327:1267 */         Statement stmt = iter.nextStatement();
/* 1328:1268 */         if (!model.getGraph().contains(stmt.asTriple())) {
/* 1329:1269 */           resultModel.getGraph().add(stmt.asTriple());
/* 1330:     */         }
/* 1331:     */       }
/* 1332:1272 */       return resultModel;
/* 1333:     */     }
/* 1334:     */     finally
/* 1335:     */     {
/* 1336:1274 */       iter.close();
/* 1337:     */     }
/* 1338:     */   }
/* 1339:     */   
/* 1340:     */   public String toString()
/* 1341:     */   {
/* 1342:1279 */     return "<ModelCom  " + getGraph() + " | " + reifiedToString() + ">";
/* 1343:     */   }
/* 1344:     */   
/* 1345:     */   public String reifiedToString()
/* 1346:     */   {
/* 1347:1283 */     return statementsToString(getHiddenStatements().listStatements());
/* 1348:     */   }
/* 1349:     */   
/* 1350:     */   protected String statementsToString(StmtIterator it)
/* 1351:     */   {
/* 1352:1287 */     StringBuffer b = new StringBuffer();
/* 1353:1288 */     while (it.hasNext()) {
/* 1354:1288 */       b.append(" ").append(it.nextStatement());
/* 1355:     */     }
/* 1356:1289 */     return b.toString();
/* 1357:     */   }
/* 1358:     */   
/* 1359:     */   public Model getHiddenStatements()
/* 1360:     */   {
/* 1361:1298 */     return this.modelReifier.getHiddenStatements();
/* 1362:     */   }
/* 1363:     */ }