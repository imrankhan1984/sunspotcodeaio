package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface NsIterator
  extends ExtendedIterator
{
  public abstract String nextNs();
}

