/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.NsIterator;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  6:   */ import java.util.NoSuchElementException;
/*  7:   */ 
/*  8:   */ public class NsIteratorImpl
/*  9:   */   extends WrappedIterator
/* 10:   */   implements NsIterator
/* 11:   */ {
/* 12:   */   public NsIteratorImpl(Iterator iter, Object o)
/* 13:   */   {
/* 14:24 */     super(iter);
/* 15:   */   }
/* 16:   */   
/* 17:   */   public String nextNs()
/* 18:   */     throws NoSuchElementException
/* 19:   */   {
/* 20:28 */     return (String)next();
/* 21:   */   }
/* 22:   */ }

