package it.polimi.elet.contextaddict.microjena.rdf.model;

public abstract interface ResourceF
{
  public abstract Resource createResource(Resource paramResource);
}
