/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  4:   */ 
/*  5:   */ public class ListIndexException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public ListIndexException()
/*  9:   */   {
/* 10:41 */     super("Tried to index beyond the length of a list");
/* 11:   */   }
/* 12:   */   
/* 13:   */   public ListIndexException(String msg)
/* 14:   */   {
/* 15:50 */     super(msg);
/* 16:   */   }
/* 17:   */ }

