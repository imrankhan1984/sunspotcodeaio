/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.Factory;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.ontology.ProfileRegistry;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.ontology.impl.OntModelImpl;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.impl.ModelCom;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.shared.ReificationStyle;
/*  11:    */ 
/*  12:    */ public class ModelFactory
/*  13:    */ {
/*  14: 35 */   public static final ReificationStyle Standard = ReificationStyle.Standard;
/*  15: 41 */   public static final ReificationStyle Convenient = ReificationStyle.Convenient;
/*  16: 47 */   public static final ReificationStyle Minimal = ReificationStyle.Minimal;
/*  17:    */   
/*  18:    */   public static PrefixMapping setDefaultModelPrefixes(PrefixMapping pm)
/*  19:    */   {
/*  20: 64 */     return ModelCom.setDefaultModelPrefixes(pm);
/*  21:    */   }
/*  22:    */   
/*  23:    */   public static PrefixMapping getDefaultModelPrefixes()
/*  24:    */   {
/*  25: 70 */     return ModelCom.getDefaultModelPrefixes();
/*  26:    */   }
/*  27:    */   
/*  28:    */   public static Model createDefaultModel()
/*  29:    */   {
/*  30: 78 */     return createDefaultModel(Standard);
/*  31:    */   }
/*  32:    */   
/*  33:    */   public static Model createDefaultModel(ReificationStyle style)
/*  34:    */   {
/*  35: 84 */     return new ModelCom(Factory.createDefaultGraph(style));
/*  36:    */   }
/*  37:    */   
/*  38:    */   public static Model createNonreifyingModel()
/*  39:    */   {
/*  40: 91 */     return createDefaultModel(Minimal);
/*  41:    */   }
/*  42:    */   
/*  43:    */   public static Model createModelForGraph(Graph g)
/*  44:    */   {
/*  45:100 */     return new ModelCom(g);
/*  46:    */   }
/*  47:    */   
/*  48:    */   public static OntModel createOntologyModel()
/*  49:    */   {
/*  50:122 */     return createOntologyModel(ProfileRegistry.OWL_LANG);
/*  51:    */   }
/*  52:    */   
/*  53:    */   public static OntModel createOntologyModel(String languageURI)
/*  54:    */   {
/*  55:138 */     return new OntModelImpl(languageURI);
/*  56:    */   }
/*  57:    */   
/*  58:    */   public static OntModel createOntologyModel(String languageURI, Model base)
/*  59:    */   {
/*  60:142 */     return new OntModelImpl(languageURI, base.getGraph());
/*  61:    */   }
/*  62:    */ }
