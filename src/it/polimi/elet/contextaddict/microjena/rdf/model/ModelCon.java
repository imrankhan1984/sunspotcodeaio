package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;
import it.polimi.elet.contextaddict.microjena.graph.Node;

public abstract interface ModelCon
{
  public abstract Resource getResource(String paramString, ResourceF paramResourceF);
  
  public abstract Property getProperty(String paramString);
  
  public abstract Bag getBag(String paramString);
  
  public abstract Bag getBag(Resource paramResource);
  
  public abstract Alt getAlt(String paramString);
  
  public abstract Alt getAlt(Resource paramResource);
  
  public abstract Seq getSeq(String paramString);
  
  public abstract Seq getSeq(Resource paramResource);
  
  public abstract Resource createResource(Resource paramResource);
  
  public abstract RDFNode getRDFNode(Node paramNode);
  
  public abstract Resource createResource(String paramString, Resource paramResource);
  
  public abstract Resource createResource(ResourceF paramResourceF);
  
  public abstract Resource createResource(String paramString, ResourceF paramResourceF);
  
  public abstract Property createProperty(String paramString);
  
  /**
   * @deprecated
   */
  public abstract Literal createLiteral(boolean paramBoolean);
  
  /**
   * @deprecated
   */
  public abstract Literal createLiteral(long paramLong);
  
  /**
   * @deprecated
   */
  public abstract Literal createLiteral(char paramChar);
  
  /**
   * @deprecated
   */
  public abstract Literal createLiteral(float paramFloat);
  
  /**
   * @deprecated
   */
  public abstract Literal createLiteral(double paramDouble);
  
  public abstract Literal createLiteral(String paramString);
  
  /**
   * @deprecated
   */
  public abstract Literal createLiteral(Object paramObject);
  
  public abstract Literal createTypedLiteral(boolean paramBoolean);
  
  public abstract Literal createTypedLiteral(int paramInt);
  
  public abstract Literal createTypedLiteral(long paramLong);
  
  public abstract Literal createTypedLiteral(char paramChar);
  
  public abstract Literal createTypedLiteral(float paramFloat);
  
  public abstract Literal createTypedLiteral(double paramDouble);
  
  public abstract Literal createTypedLiteral(String paramString);
  
  public abstract Literal createTypedLiteral(Object paramObject);
  
  public abstract Literal createTypedLiteral(String paramString1, String paramString2);
  
  public abstract Literal createTypedLiteral(Object paramObject, String paramString);
  
  /**
   * @deprecated
   */
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, boolean paramBoolean);
  
  /**
   * @deprecated
   */
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, long paramLong);
  
  /**
   * @deprecated
   */
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, char paramChar);
  
  /**
   * @deprecated
   */
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, float paramFloat);
  
  /**
   * @deprecated
   */
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, double paramDouble);
  
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, String paramString);
  
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, String paramString1, String paramString2);
  
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, String paramString, boolean paramBoolean);
  
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, String paramString1, String paramString2, boolean paramBoolean);
  
  /**
   * @deprecated
   */
  public abstract Statement createStatement(Resource paramResource, Property paramProperty, Object paramObject);
  
  public abstract Bag createBag();
  
  public abstract Bag createBag(String paramString);
  
  public abstract Alt createAlt();
  
  public abstract Alt createAlt(String paramString);
  
  public abstract Seq createSeq();
  
  public abstract Seq createSeq(String paramString);
  
  public abstract Model add(Resource paramResource, Property paramProperty, RDFNode paramRDFNode);
  
  /**
   * @deprecated
   */
  public abstract Model add(Resource paramResource, Property paramProperty, boolean paramBoolean);
  
  /**
   * @deprecated
   */
  public abstract Model add(Resource paramResource, Property paramProperty, long paramLong);
  
  /**
   * @deprecated
   */
  public abstract Model add(Resource paramResource, Property paramProperty, char paramChar);
  
  /**
   * @deprecated
   */
  public abstract Model add(Resource paramResource, Property paramProperty, float paramFloat);
  
  /**
   * @deprecated
   */
  public abstract Model add(Resource paramResource, Property paramProperty, double paramDouble);
  
  public abstract Model add(Resource paramResource, Property paramProperty, String paramString);
  
  public abstract Model add(Resource paramResource, Property paramProperty, String paramString, RDFDatatype paramRDFDatatype);
  
  public abstract Model add(Resource paramResource, Property paramProperty, String paramString, boolean paramBoolean);
  
  public abstract Model add(Resource paramResource, Property paramProperty, String paramString1, String paramString2);
  
  /**
   * @deprecated
   */
  public abstract Model add(Resource paramResource, Property paramProperty, String paramString1, String paramString2, boolean paramBoolean);
  
  /**
   * @deprecated
   */
  public abstract Model add(Resource paramResource, Property paramProperty, Object paramObject);
  
  public abstract Model remove(Resource paramResource, Property paramProperty, RDFNode paramRDFNode);
  
  public abstract Model remove(Resource paramResource, Property paramProperty, RDFNode paramRDFNode, boolean paramBoolean);
  
  public abstract Model remove(StmtIterator paramStmtIterator);
  
  public abstract Model remove(Model paramModel);
  
  /**
   * @deprecated
   */
  public abstract StmtIterator listStatements(Resource paramResource, Property paramProperty, boolean paramBoolean);
  
  /**
   * @deprecated
   */
  public abstract StmtIterator listStatements(Resource paramResource, Property paramProperty, long paramLong);
  
  /**
   * @deprecated
   */
  public abstract StmtIterator listStatements(Resource paramResource, Property paramProperty, char paramChar);
  
  /**
   * @deprecated
   */
  public abstract StmtIterator listStatements(Resource paramResource, Property paramProperty, float paramFloat);
  
  /**
   * @deprecated
   */
  public abstract StmtIterator listStatements(Resource paramResource, Property paramProperty, double paramDouble);
  
  public abstract StmtIterator listStatements(Resource paramResource, Property paramProperty, String paramString);
  
  public abstract StmtIterator listStatements(Resource paramResource, Property paramProperty, String paramString1, String paramString2);
  
  /**
   * @deprecated
   */
  public abstract ResIterator listSubjectsWithProperty(Property paramProperty, boolean paramBoolean);
  
  /**
   * @deprecated
   */
  public abstract ResIterator listSubjectsWithProperty(Property paramProperty, long paramLong);
  
  /**
   * @deprecated
   */
  public abstract ResIterator listSubjectsWithProperty(Property paramProperty, char paramChar);
  
  /**
   * @deprecated
   */
  public abstract ResIterator listSubjectsWithProperty(Property paramProperty, float paramFloat);
  
  /**
   * @deprecated
   */
  public abstract ResIterator listSubjectsWithProperty(Property paramProperty, double paramDouble);
  
  public abstract ResIterator listSubjectsWithProperty(Property paramProperty, String paramString);
  
  public abstract ResIterator listSubjectsWithProperty(Property paramProperty, String paramString1, String paramString2);
  
  /**
   * @deprecated
   */
  public abstract ResIterator listSubjectsWithProperty(Property paramProperty, Object paramObject);
  
  /**
   * @deprecated
   */
  public abstract boolean contains(Resource paramResource, Property paramProperty, boolean paramBoolean);
  
  /**
   * @deprecated
   */
  public abstract boolean contains(Resource paramResource, Property paramProperty, long paramLong);
  
  /**
   * @deprecated
   */
  public abstract boolean contains(Resource paramResource, Property paramProperty, char paramChar);
  
  /**
   * @deprecated
   */
  public abstract boolean contains(Resource paramResource, Property paramProperty, float paramFloat);
  
  /**
   * @deprecated
   */
  public abstract boolean contains(Resource paramResource, Property paramProperty, double paramDouble);
  
  public abstract boolean contains(Resource paramResource, Property paramProperty, String paramString);
  
  public abstract boolean contains(Resource paramResource, Property paramProperty, String paramString1, String paramString2);
  
  /**
   * @deprecated
   */
  public abstract boolean contains(Resource paramResource, Property paramProperty, Object paramObject);
}

