/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.NodeIterator;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResIterator;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.IteratorImpl;
/*  9:   */ import java.util.Vector;
/* 10:   */ 
/* 11:   */ public class IteratorCast
/* 12:   */ {
/* 13:   */   public static StmtIterator asStmtIterator(Iterator it, ModelCom eg)
/* 14:   */   {
/* 15:33 */     Vector result = new Vector(20, 20);
/* 16:34 */     while (it.hasNext()) {
/* 17:35 */       result.addElement(StatementImpl.toStatement((Triple)it.next(), eg));
/* 18:   */     }
/* 19:37 */     Iterator itResult = new IteratorImpl(result);
/* 20:38 */     return new StmtIteratorImpl(itResult);
/* 21:   */   }
/* 22:   */   
/* 23:   */   public static NodeIterator asNodeIterator(Iterator it)
/* 24:   */   {
/* 25:43 */     return new NodeIteratorImpl(it);
/* 26:   */   }
/* 27:   */   
/* 28:   */   public static ResIterator asResIterator(Iterator it)
/* 29:   */   {
/* 30:48 */     return new ResIteratorImpl(it);
/* 31:   */   }
/* 32:   */ }
