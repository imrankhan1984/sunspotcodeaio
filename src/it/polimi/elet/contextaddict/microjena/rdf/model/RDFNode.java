package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.graph.FrontsNode;

public abstract interface RDFNode
  extends FrontsNode
{
  public abstract String toString();
  
  public abstract boolean isAnon();
  
  public abstract boolean isLiteral();
  
  public abstract boolean isURIResource();
  
  public abstract boolean isResource();
  
  public abstract RDFNode inModel(Model paramModel);
}

