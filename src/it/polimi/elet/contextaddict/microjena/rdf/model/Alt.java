package it.polimi.elet.contextaddict.microjena.rdf.model;

public abstract interface Alt
  extends Container
{
  public abstract Alt setDefault(RDFNode paramRDFNode);
  
  public abstract Alt setDefault(boolean paramBoolean);
  
  public abstract Alt setDefault(long paramLong);
  
  public abstract Alt setDefault(char paramChar);
  
  public abstract Alt setDefault(float paramFloat);
  
  public abstract Alt setDefault(double paramDouble);
  
  public abstract Alt setDefault(String paramString);
  
  public abstract Alt setDefault(String paramString1, String paramString2);
  
  public abstract Alt setDefault(Object paramObject);
  
  public abstract RDFNode getDefault();
  
  public abstract Resource getDefaultResource();
  
  public abstract Literal getDefaultLiteral();
  
  public abstract boolean getDefaultBoolean();
  
  public abstract byte getDefaultByte();
  
  public abstract short getDefaultShort();
  
  public abstract int getDefaultInt();
  
  public abstract long getDefaultLong();
  
  public abstract char getDefaultChar();
  
  public abstract float getDefaultFloat();
  
  public abstract double getDefaultDouble();
  
  public abstract String getDefaultString();
  
  public abstract String getDefaultLanguage();
  
  public abstract Resource getDefaultResource(ResourceF paramResourceF);
  
  public abstract Object getDefaultObject(ObjectF paramObjectF);
  
  public abstract Alt getDefaultAlt();
  
  public abstract Bag getDefaultBag();
  
  public abstract Seq getDefaultSeq();
  
  public abstract Container remove(Statement paramStatement);
}
