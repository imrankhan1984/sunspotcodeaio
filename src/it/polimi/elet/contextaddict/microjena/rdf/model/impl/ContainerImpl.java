/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Container;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.NodeIterator;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.shared.AssertionFailureException;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.IteratorImpl;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  17:    */ import java.util.Vector;
/*  18:    */ 
/*  19:    */ public class ContainerImpl
/*  20:    */   extends ResourceImpl
/*  21:    */   implements Container, ContainerI
/*  22:    */ {
/*  23:    */   public ContainerImpl(ModelCom model)
/*  24:    */   {
/*  25: 35 */     super(model);
/*  26:    */   }
/*  27:    */   
/*  28:    */   public ContainerImpl(String uri, ModelCom model)
/*  29:    */   {
/*  30: 39 */     super(uri, model);
/*  31:    */   }
/*  32:    */   
/*  33:    */   public ContainerImpl(Resource r, ModelCom model)
/*  34:    */   {
/*  35: 43 */     super(r.asNode(), model);
/*  36:    */   }
/*  37:    */   
/*  38:    */   public ContainerImpl(Node n, EnhGraph g)
/*  39:    */   {
/*  40: 47 */     super(n, g);
/*  41:    */   }
/*  42:    */   
/*  43:    */   protected ContainerImpl(Resource r)
/*  44:    */   {
/*  45: 51 */     this(r, (ModelCom)r.getModel());
/*  46:    */   }
/*  47:    */   
/*  48:    */   private boolean is(Resource r)
/*  49:    */   {
/*  50: 54 */     return hasProperty(RDF.type, r);
/*  51:    */   }
/*  52:    */   
/*  53:    */   public boolean isAlt()
/*  54:    */   {
/*  55: 58 */     return is(RDF.Alt);
/*  56:    */   }
/*  57:    */   
/*  58:    */   public boolean isBag()
/*  59:    */   {
/*  60: 62 */     return is(RDF.Bag);
/*  61:    */   }
/*  62:    */   
/*  63:    */   public boolean isSeq()
/*  64:    */   {
/*  65: 66 */     return is(RDF.Seq);
/*  66:    */   }
/*  67:    */   
/*  68:    */   public Container add(RDFNode n)
/*  69:    */   {
/*  70: 70 */     int i = size();
/*  71: 71 */     addProperty(RDF.li(i + 1), n);
/*  72: 72 */     return this;
/*  73:    */   }
/*  74:    */   
/*  75:    */   public Container add(boolean o)
/*  76:    */   {
/*  77: 76 */     return add(String.valueOf(o));
/*  78:    */   }
/*  79:    */   
/*  80:    */   public Container add(long o)
/*  81:    */   {
/*  82: 80 */     return add(String.valueOf(o));
/*  83:    */   }
/*  84:    */   
/*  85:    */   public Container add(char o)
/*  86:    */   {
/*  87: 84 */     return add(String.valueOf(o));
/*  88:    */   }
/*  89:    */   
/*  90:    */   public Container add(float o)
/*  91:    */   {
/*  92: 88 */     return add(String.valueOf(o));
/*  93:    */   }
/*  94:    */   
/*  95:    */   public Container add(double o)
/*  96:    */   {
/*  97: 92 */     return add(String.valueOf(o));
/*  98:    */   }
/*  99:    */   
/* 100:    */   public Container add(Object o)
/* 101:    */   {
/* 102: 96 */     return add(String.valueOf(o));
/* 103:    */   }
/* 104:    */   
/* 105:    */   public Container add(String o)
/* 106:    */   {
/* 107:100 */     return add(o, "");
/* 108:    */   }
/* 109:    */   
/* 110:    */   public Container add(String o, String l)
/* 111:    */   {
/* 112:104 */     return add(literal(o, l));
/* 113:    */   }
/* 114:    */   
/* 115:    */   public boolean contains(RDFNode n)
/* 116:    */   {
/* 117:108 */     return containerContains(n);
/* 118:    */   }
/* 119:    */   
/* 120:    */   public boolean contains(boolean o)
/* 121:    */   {
/* 122:112 */     return contains(String.valueOf(o));
/* 123:    */   }
/* 124:    */   
/* 125:    */   public boolean contains(long o)
/* 126:    */   {
/* 127:116 */     return contains(String.valueOf(o));
/* 128:    */   }
/* 129:    */   
/* 130:    */   public boolean contains(char o)
/* 131:    */   {
/* 132:120 */     return contains(String.valueOf(o));
/* 133:    */   }
/* 134:    */   
/* 135:    */   public boolean contains(float o)
/* 136:    */   {
/* 137:124 */     return contains(String.valueOf(o));
/* 138:    */   }
/* 139:    */   
/* 140:    */   public boolean contains(double o)
/* 141:    */   {
/* 142:128 */     return contains(String.valueOf(o));
/* 143:    */   }
/* 144:    */   
/* 145:    */   public boolean contains(Object o)
/* 146:    */   {
/* 147:132 */     return contains(String.valueOf(o));
/* 148:    */   }
/* 149:    */   
/* 150:    */   public boolean contains(String o)
/* 151:    */   {
/* 152:136 */     return contains(o, "");
/* 153:    */   }
/* 154:    */   
/* 155:    */   public boolean contains(String o, String l)
/* 156:    */   {
/* 157:140 */     return contains(literal(o, l));
/* 158:    */   }
/* 159:    */   
/* 160:    */   private Literal literal(String s, String lang)
/* 161:    */   {
/* 162:144 */     return new LiteralImpl(Node.createLiteral(s, lang, false), getModelCom());
/* 163:    */   }
/* 164:    */   
/* 165:    */   public NodeIterator iterator()
/* 166:    */   {
/* 167:148 */     return listContainerMembers();
/* 168:    */   }
/* 169:    */   
/* 170:    */   public int size()
/* 171:    */   {
/* 172:152 */     int result = 0;
/* 173:153 */     StmtIterator iter = listProperties();
/* 174:154 */     while (iter.hasNext()) {
/* 175:155 */       if (iter.nextStatement().getPredicate().getOrdinal() != 0) {
/* 176:156 */         result++;
/* 177:    */       }
/* 178:    */     }
/* 179:157 */     iter.close();
/* 180:158 */     return result;
/* 181:    */   }
/* 182:    */   
/* 183:    */   public Container remove(Statement s)
/* 184:    */   {
/* 185:162 */     int size = size();
/* 186:163 */     Statement last = null;
/* 187:164 */     if (s.getPredicate().getOrdinal() == size)
/* 188:    */     {
/* 189:165 */       getModel().remove(s);
/* 190:    */     }
/* 191:    */     else
/* 192:    */     {
/* 193:167 */       last = getModel().getRequiredProperty(this, RDF.li(size));
/* 194:168 */       s.changeObject(last.getObject());
/* 195:169 */       getModel().remove(last);
/* 196:    */     }
/* 197:171 */     if (size() != size - 1) {
/* 198:172 */       throw new AssertionFailureException("container size");
/* 199:    */     }
/* 200:173 */     return this;
/* 201:    */   }
/* 202:    */   
/* 203:    */   public Container remove(int index, RDFNode object)
/* 204:    */   {
/* 205:177 */     remove(getModel().createStatement(this, RDF.li(index), object));
/* 206:178 */     return this;
/* 207:    */   }
/* 208:    */   
/* 209:    */   public NodeIterator listContainerMembers(Object f)
/* 210:    */   {
/* 211:187 */     return listContainerMembers();
/* 212:    */   }
/* 213:    */   
/* 214:    */   public NodeIterator listContainerMembers()
/* 215:    */   {
/* 216:191 */     StmtIterator iter = listProperties();
/* 217:192 */     Vector result = new Vector();
/* 218:193 */     int maxOrdinal = 0;
/* 219:194 */     while (iter.hasNext())
/* 220:    */     {
/* 221:195 */       Statement stmt = iter.nextStatement();
/* 222:196 */       int ordinal = stmt.getPredicate().getOrdinal();
/* 223:197 */       if (ordinal != 0)
/* 224:    */       {
/* 225:198 */         if (ordinal > maxOrdinal)
/* 226:    */         {
/* 227:199 */           maxOrdinal = ordinal;
/* 228:200 */           result.setSize(ordinal);
/* 229:    */         }
/* 230:202 */         result.setElementAt(stmt, ordinal - 1);
/* 231:    */       }
/* 232:    */     }
/* 233:205 */     iter.close();
/* 234:206 */     return new ContNodeIteratorImpl(new IteratorImpl(result), result, this);
/* 235:    */   }
/* 236:    */   
/* 237:    */   public int containerIndexOf(RDFNode n)
/* 238:    */   {
/* 239:210 */     int result = 0;
/* 240:211 */     StmtIterator iter = listProperties();
/* 241:212 */     while (iter.hasNext())
/* 242:    */     {
/* 243:213 */       Statement stmt = iter.nextStatement();
/* 244:214 */       int ordinal = stmt.getPredicate().getOrdinal();
/* 245:215 */       if ((ordinal != 0) && (n.equals(stmt.getObject())))
/* 246:    */       {
/* 247:216 */         result = ordinal;
/* 248:217 */         break;
/* 249:    */       }
/* 250:    */     }
/* 251:220 */     iter.close();
/* 252:221 */     return result;
/* 253:    */   }
/* 254:    */   
/* 255:    */   public boolean containerContains(RDFNode n)
/* 256:    */   {
/* 257:225 */     return containerIndexOf(n) != 0;
/* 258:    */   }
/* 259:    */ }
