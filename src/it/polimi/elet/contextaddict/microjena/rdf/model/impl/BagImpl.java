/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  7:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Bag;
/*  8:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  9:   */ 
/* 10:   */ public class BagImpl
/* 11:   */   extends ContainerImpl
/* 12:   */   implements Bag
/* 13:   */ {
/* 14:48 */   public static final Implementation factory = new Implementation()
/* 15:   */   {
/* 16:   */     public boolean canWrap(Node n, EnhGraph eg)
/* 17:   */     {
/* 18:50 */       return true;
/* 19:   */     }
/* 20:   */     
/* 21:   */     public EnhNode wrap(Node n, EnhGraph eg)
/* 22:   */     {
/* 23:53 */       return new BagImpl(n, eg);
/* 24:   */     }
/* 25:   */   };
/* 26:   */   
/* 27:   */   public BagImpl(ModelCom model)
/* 28:   */   {
/* 29:59 */     super(model);
/* 30:   */   }
/* 31:   */   
/* 32:   */   public BagImpl(String uri, ModelCom model)
/* 33:   */   {
/* 34:63 */     super(uri, model);
/* 35:   */   }
/* 36:   */   
/* 37:   */   public BagImpl(Resource r, ModelCom m)
/* 38:   */   {
/* 39:67 */     super(r, m);
/* 40:   */   }
/* 41:   */   
/* 42:   */   public BagImpl(Node n, EnhGraph g)
/* 43:   */   {
/* 44:71 */     super(n, g);
/* 45:   */   }
/* 46:   */ }
