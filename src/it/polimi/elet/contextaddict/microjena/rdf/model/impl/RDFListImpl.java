/*    1:     */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*    2:     */ 
/*    3:     */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*    4:     */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*    5:     */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*    6:     */ import it.polimi.elet.contextaddict.microjena.graph.Axiom;
/*    7:     */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*    8:     */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*    9:     */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*   10:     */ import it.polimi.elet.contextaddict.microjena.ontology.Profile;
/*   11:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.EmptyListException;
/*   12:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.EmptyListUpdateException;
/*   13:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.InvalidListException;
/*   14:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.ListIndexException;
/*   15:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*   16:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*   17:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList;
/*   18:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList.ApplyFn;
/*   19:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFList.ReduceFn;
/*   20:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*   21:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*   22:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*   23:     */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*   24:     */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*   25:     */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*   26:     */ import it.polimi.elet.contextaddict.microjena.util.List;
/*   27:     */ import it.polimi.elet.contextaddict.microjena.util.Set;
/*   28:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*   29:     */ import it.polimi.elet.contextaddict.microjena.util.iterator.NiceIterator;
/*   30:     */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*   31:     */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*   32:     */ 
/*   33:     */ public class RDFListImpl
/*   34:     */   extends ResourceImpl
/*   35:     */   implements RDFList
/*   36:     */ {
/*   37:  68 */   public static Implementation factory = new Implementation()
/*   38:     */   {
/*   39:     */     public EnhNode wrap(Node n, EnhGraph eg)
/*   40:     */     {
/*   41:  70 */       if (canWrap(n, eg))
/*   42:     */       {
/*   43:  71 */         RDFListImpl impl = new RDFListImpl(n, eg);
/*   44:  74 */         if ((eg instanceof OntModel))
/*   45:     */         {
/*   46:  75 */           Profile prof = ((OntModel)eg).getProfile();
/*   47:  76 */           impl.m_listFirst = prof.FIRST();
/*   48:  77 */           impl.m_listRest = prof.REST();
/*   49:  78 */           impl.m_listNil = prof.NIL();
/*   50:  79 */           impl.m_listType = prof.LIST();
/*   51:     */         }
/*   52:  81 */         return impl;
/*   53:     */       }
/*   54:  84 */       throw new JenaException("Cannot convert node " + n + " to RDFList");
/*   55:     */     }
/*   56:     */     
/*   57:     */     public boolean canWrap(Node node, EnhGraph eg)
/*   58:     */     {
/*   59:  89 */       Graph g = eg.asGraph();
/*   60:     */       
/*   61:     */ 
/*   62:  92 */       Resource first = RDF.first;
/*   63:  93 */       Resource rest = RDF.rest;
/*   64:  94 */       Resource nil = RDF.nil;
/*   65:  96 */       if ((eg instanceof OntModel))
/*   66:     */       {
/*   67:  97 */         Profile prof = ((OntModel)eg).getProfile();
/*   68:  98 */         first = prof.FIRST();
/*   69:  99 */         rest = prof.REST();
/*   70: 100 */         nil = prof.NIL();
/*   71:     */       }
/*   72: 104 */       return (node.equals(nil.asNode())) || (g.contains(node, first.asNode(), Node.ANY)) || (g.contains(node, rest.asNode(), Node.ANY)) || (g.contains(node, RDF.type.asNode(), RDF.List.asNode()));
/*   73:     */     }
/*   74:     */   };
/*   75: 113 */   protected static boolean s_checkValid = false;
/*   76: 116 */   protected String m_errorMsg = null;
/*   77: 119 */   protected RDFList m_tail = null;
/*   78: 122 */   protected Property m_listFirst = RDF.first;
/*   79: 125 */   protected Property m_listRest = RDF.rest;
/*   80: 128 */   protected Resource m_listNil = RDF.nil;
/*   81: 131 */   protected Resource m_listType = RDF.List;
/*   82:     */   
/*   83:     */   public RDFListImpl(Node n, EnhGraph g)
/*   84:     */   {
/*   85: 143 */     super(n, g);
/*   86:     */   }
/*   87:     */   
/*   88:     */   public Resource listType()
/*   89:     */   {
/*   90: 147 */     return this.m_listType;
/*   91:     */   }
/*   92:     */   
/*   93:     */   public Resource listNil()
/*   94:     */   {
/*   95: 148 */     return this.m_listNil;
/*   96:     */   }
/*   97:     */   
/*   98:     */   public Property listFirst()
/*   99:     */   {
/*  100: 149 */     return this.m_listFirst;
/*  101:     */   }
/*  102:     */   
/*  103:     */   public Property listRest()
/*  104:     */   {
/*  105: 150 */     return this.m_listRest;
/*  106:     */   }
/*  107:     */   
/*  108:     */   public Class listAbstractionClass()
/*  109:     */   {
/*  110: 151 */     return RDFList.class;
/*  111:     */   }
/*  112:     */   
/*  113:     */   public int size()
/*  114:     */   {
/*  115: 162 */     if (s_checkValid) {
/*  116: 163 */       checkValid();
/*  117:     */     }
/*  118: 166 */     int size = 0;
/*  119: 168 */     for (Iterator i = iterator(); i.hasNext(); i.next()) {
/*  120: 169 */       size++;
/*  121:     */     }
/*  122: 171 */     return size;
/*  123:     */   }
/*  124:     */   
/*  125:     */   public RDFNode getHead()
/*  126:     */   {
/*  127: 184 */     if (s_checkValid) {
/*  128: 185 */       checkValid();
/*  129:     */     }
/*  130: 188 */     checkNotNil("Tried to get the head of an empty list");
/*  131:     */     
/*  132: 190 */     return getRequiredProperty(listFirst()).getObject();
/*  133:     */   }
/*  134:     */   
/*  135:     */   public RDFNode setHead(RDFNode value)
/*  136:     */   {
/*  137: 204 */     if (s_checkValid) {
/*  138: 205 */       checkValid();
/*  139:     */     }
/*  140: 208 */     checkNotNil("Tried to set the head of an empty list");
/*  141:     */     
/*  142:     */ 
/*  143: 211 */     Statement current = getRequiredProperty(listFirst());
/*  144: 212 */     RDFNode n = current.getObject();
/*  145: 213 */     current.remove();
/*  146:     */     
/*  147:     */ 
/*  148: 216 */     addProperty(listFirst(), value);
/*  149:     */     
/*  150: 218 */     return n;
/*  151:     */   }
/*  152:     */   
/*  153:     */   public RDFList getTail()
/*  154:     */   {
/*  155: 231 */     if (s_checkValid) {
/*  156: 232 */       checkValid();
/*  157:     */     }
/*  158: 235 */     checkNotNil("Tried to get the tail of an empty list");
/*  159: 236 */     Resource tail = getRequiredProperty(listRest()).getResource();
/*  160: 237 */     return new RDFListImpl(tail.asNode(), getModelCom());
/*  161:     */   }
/*  162:     */   
/*  163:     */   public RDFList setTail(RDFList tail)
/*  164:     */   {
/*  165: 251 */     if (s_checkValid) {
/*  166: 252 */       checkValid();
/*  167:     */     }
/*  168: 255 */     checkNotNil("Tried to set the tail of an empty list");
/*  169:     */     
/*  170: 257 */     Resource result = setTailAux(this, tail, listRest());
/*  171: 258 */     return new RDFListImpl(result.asNode(), getModelCom());
/*  172:     */   }
/*  173:     */   
/*  174:     */   public boolean isEmpty()
/*  175:     */   {
/*  176: 268 */     if (s_checkValid) {
/*  177: 269 */       checkValid();
/*  178:     */     }
/*  179: 272 */     return equals(listNil());
/*  180:     */   }
/*  181:     */   
/*  182:     */   public RDFList cons(RDFNode value)
/*  183:     */   {
/*  184: 286 */     if (s_checkValid) {
/*  185: 287 */       checkValid();
/*  186:     */     }
/*  187: 292 */     Resource cell = newListCell(value, this);
/*  188: 293 */     return new RDFListImpl(cell.asNode(), getModelCom());
/*  189:     */   }
/*  190:     */   
/*  191:     */   public void add(RDFNode value)
/*  192:     */   {
/*  193: 314 */     if (s_checkValid) {
/*  194: 315 */       checkValid();
/*  195:     */     }
/*  196: 319 */     if (isEmpty()) {
/*  197: 320 */       throw new EmptyListUpdateException("Attempt to add() to the empty list (rdf:nil)");
/*  198:     */     }
/*  199: 324 */     RDFList tail = findElement(true, 0);
/*  200:     */     
/*  201:     */ 
/*  202: 327 */     setTailAux(tail, newListCell(value, listNil()), listRest());
/*  203:     */   }
/*  204:     */   
/*  205:     */   public RDFList with(RDFNode value)
/*  206:     */   {
/*  207: 346 */     if (s_checkValid) {
/*  208: 347 */       checkValid();
/*  209:     */     }
/*  210: 351 */     if (isEmpty()) {
/*  211: 352 */       return cons(value);
/*  212:     */     }
/*  213: 356 */     RDFList tail = findElement(true, 0);
/*  214:     */     
/*  215:     */ 
/*  216: 359 */     setTailAux(tail, newListCell(value, listNil()), listRest());
/*  217: 360 */     return this;
/*  218:     */   }
/*  219:     */   
/*  220:     */   public RDFNode get(int i)
/*  221:     */   {
/*  222: 377 */     if (s_checkValid) {
/*  223: 378 */       checkValid();
/*  224:     */     }
/*  225: 381 */     checkNotNil("Tried to get an element from the empty list");
/*  226: 382 */     return findElement(false, i).getHead();
/*  227:     */   }
/*  228:     */   
/*  229:     */   public RDFNode replace(int i, RDFNode value)
/*  230:     */   {
/*  231: 400 */     if (s_checkValid) {
/*  232: 401 */       checkValid();
/*  233:     */     }
/*  234: 404 */     checkNotNil("Tried to replace a value in the empty list");
/*  235: 405 */     return findElement(false, i).setHead(value);
/*  236:     */   }
/*  237:     */   
/*  238:     */   public boolean contains(RDFNode value)
/*  239:     */   {
/*  240: 419 */     return indexOf(value, 0) >= 0;
/*  241:     */   }
/*  242:     */   
/*  243:     */   public int indexOf(RDFNode value)
/*  244:     */   {
/*  245: 434 */     return indexOf(value, 0);
/*  246:     */   }
/*  247:     */   
/*  248:     */   public int indexOf(RDFNode value, int start)
/*  249:     */   {
/*  250: 454 */     if (s_checkValid) {
/*  251: 455 */       checkValid();
/*  252:     */     }
/*  253: 459 */     Resource l = findElement(false, start);
/*  254: 460 */     int index = start;
/*  255:     */     
/*  256: 462 */     Property head = listFirst();
/*  257: 463 */     Property tail = listRest();
/*  258: 464 */     Resource nil = listNil();
/*  259:     */     
/*  260: 466 */     boolean found = l.hasProperty(head, value);
/*  261: 469 */     while ((!found) && (!l.equals(nil)))
/*  262:     */     {
/*  263: 470 */       l = l.getRequiredProperty(tail).getResource();
/*  264: 471 */       index++;
/*  265: 472 */       found = l.hasProperty(head, value);
/*  266:     */     }
/*  267: 475 */     return found ? index : -1;
/*  268:     */   }
/*  269:     */   
/*  270:     */   public RDFList append(Iterator nodes)
/*  271:     */   {
/*  272: 494 */     return append(copy(nodes));
/*  273:     */   }
/*  274:     */   
/*  275:     */   public RDFList append(RDFList list)
/*  276:     */   {
/*  277: 512 */     if (s_checkValid) {
/*  278: 513 */       checkValid();
/*  279:     */     }
/*  280: 516 */     if (isEmpty()) {
/*  281: 518 */       return list;
/*  282:     */     }
/*  283: 522 */     RDFList copy = copy(iterator());
/*  284: 523 */     copy.concatenate(list);
/*  285: 524 */     return copy;
/*  286:     */   }
/*  287:     */   
/*  288:     */   public void concatenate(RDFList list)
/*  289:     */   {
/*  290: 546 */     if (s_checkValid) {
/*  291: 547 */       checkValid();
/*  292:     */     }
/*  293: 550 */     if (isEmpty()) {
/*  294: 552 */       throw new EmptyListUpdateException("Tried to concatenate onto the empty list");
/*  295:     */     }
/*  296: 555 */     findElement(true, 0).setTail(list);
/*  297:     */   }
/*  298:     */   
/*  299:     */   public void concatenate(Iterator nodes)
/*  300:     */   {
/*  301: 571 */     concatenate(copy(nodes));
/*  302:     */   }
/*  303:     */   
/*  304:     */   public RDFList copy()
/*  305:     */   {
/*  306: 584 */     if (s_checkValid) {
/*  307: 585 */       checkValid();
/*  308:     */     }
/*  309: 588 */     return copy(iterator());
/*  310:     */   }
/*  311:     */   
/*  312:     */   public void apply(RDFList.ApplyFn fn)
/*  313:     */   {
/*  314: 600 */     if (s_checkValid) {
/*  315: 601 */       checkValid();
/*  316:     */     }
/*  317: 604 */     for (Iterator i = iterator(); i.hasNext();) {
/*  318: 605 */       fn.apply((RDFNode)i.next());
/*  319:     */     }
/*  320:     */   }
/*  321:     */   
/*  322:     */   public Object reduce(RDFList.ReduceFn fn, Object initial)
/*  323:     */   {
/*  324: 622 */     if (s_checkValid) {
/*  325: 623 */       checkValid();
/*  326:     */     }
/*  327: 626 */     Object acc = initial;
/*  328: 628 */     for (Iterator i = iterator(); i.hasNext();) {
/*  329: 629 */       acc = fn.reduce((RDFNode)i.next(), acc);
/*  330:     */     }
/*  331: 632 */     return acc;
/*  332:     */   }
/*  333:     */   
/*  334:     */   public RDFList removeHead()
/*  335:     */   {
/*  336: 648 */     if (s_checkValid) {
/*  337: 649 */       checkValid();
/*  338:     */     }
/*  339: 652 */     checkNotNil("Attempted to delete the head of a nil list");
/*  340:     */     
/*  341: 654 */     RDFList tail = getTail();
/*  342: 655 */     removeProperties();
/*  343:     */     
/*  344: 657 */     return tail;
/*  345:     */   }
/*  346:     */   
/*  347:     */   public RDFList remove(RDFNode val)
/*  348:     */   {
/*  349: 673 */     if (s_checkValid) {
/*  350: 674 */       checkValid();
/*  351:     */     }
/*  352: 677 */     RDFList prev = null;
/*  353: 678 */     RDFList cell = this;
/*  354: 679 */     boolean searching = true;
/*  355: 681 */     while ((searching) && (!cell.isEmpty()))
/*  356:     */     {
/*  357: 682 */       if (cell.getHead().equals(val))
/*  358:     */       {
/*  359: 684 */         RDFList tail = cell.getTail();
/*  360: 685 */         if (prev != null) {
/*  361: 686 */           prev.setTail(tail);
/*  362:     */         }
/*  363: 690 */         StmtIterator sts = getModel().listStatements(cell, null, (RDFNode)null);
/*  364: 691 */         while (sts.hasNext()) {
/*  365: 692 */           sts.nextStatement().remove(true);
/*  366:     */         }
/*  367: 695 */         return prev == null ? tail : this;
/*  368:     */       }
/*  369: 698 */       prev = cell;
/*  370: 699 */       cell = cell.getTail();
/*  371:     */     }
/*  372: 704 */     return this;
/*  373:     */   }
/*  374:     */   
/*  375:     */   /**
/*  376:     */    
/*  378:     */   public void removeAll()
/*  379:     */   {
/*  380: 715 */     removeList();
/*  381:     */   }
/*  382:     */   
/*  383:     */   public void removeList()
/*  384:     */   {
/*  385: 736 */     for (Iterator i = collectStatements().iterator(); i.hasNext();) {
/*  386: 737 */       ((Statement)i.next()).remove();
/*  387:     */     }
/*  388:     */   }
/*  389:     */   
/*  390:     */   public Set collectStatements()
/*  391:     */   {
/*  392: 748 */     Set stmts = new Set();
/*  393: 749 */     RDFList l = this;
/*  394:     */     do
/*  395:     */     {
/*  396: 753 */       for (Iterator i = l.listProperties(); i.hasNext();) {
/*  397: 754 */         stmts.add(i.next());
/*  398:     */       }
/*  399: 758 */       l = l.getTail();
/*  400: 759 */     } while (!l.isEmpty());
/*  401: 761 */     return stmts;
/*  402:     */   }
/*  403:     */   
/*  404:     */   public ExtendedIterator iterator()
/*  405:     */   {
/*  406: 777 */     return new RDFListIterator(this);
/*  407:     */   }
/*  408:     */   
/*  409:     */   public List asJavaList()
/*  410:     */   {
/*  411: 789 */     List l = new List();
/*  412: 791 */     for (Iterator i = iterator(); i.hasNext();) {
/*  413: 792 */       l.add(i.next());
/*  414:     */     }
/*  415: 795 */     return l;
/*  416:     */   }
/*  417:     */   
/*  418:     */   public boolean sameListAs(RDFList list)
/*  419:     */   {
/*  420: 813 */     if (s_checkValid) {
/*  421: 814 */       checkValid();
/*  422:     */     }
/*  423: 817 */     Resource r0 = this;
/*  424: 818 */     Resource r1 = list;
/*  425:     */     
/*  426: 820 */     Property head = listFirst();
/*  427: 821 */     Property tail = listRest();
/*  428: 822 */     Resource nil = listNil();
/*  429: 825 */     while ((!r0.equals(nil)) && (!r1.equals(nil)))
/*  430:     */     {
/*  431: 826 */       RDFNode n0 = r0.getRequiredProperty(head).getObject();
/*  432: 827 */       RDFNode n1 = r1.getRequiredProperty(head).getObject();
/*  433: 829 */       if ((n0 == null) || (!n0.equals(n1))) {
/*  434: 831 */         return false;
/*  435:     */       }
/*  436: 834 */       r0 = r0.getRequiredProperty(tail).getResource();
/*  437: 835 */       r1 = r1.getRequiredProperty(tail).getResource();
/*  438:     */     }
/*  439: 840 */     return (r0.equals(nil)) && (r1.equals(nil));
/*  440:     */   }
/*  441:     */   
/*  442:     */   public boolean getStrict()
/*  443:     */   {
/*  444: 853 */     return s_checkValid;
/*  445:     */   }
/*  446:     */   
/*  447:     */   public void setStrict(boolean strict)
/*  448:     */   {
/*  449: 868 */     s_checkValid = strict;
/*  450:     */   }
/*  451:     */   
/*  452:     */   public boolean isValid()
/*  453:     */   {
/*  454: 882 */     this.m_errorMsg = null;
/*  455:     */     try
/*  456:     */     {
/*  457: 885 */       checkValid();
/*  458:     */     }
/*  459:     */     catch (InvalidListException e)
/*  460:     */     {
/*  461: 887 */       this.m_errorMsg = e.getMessage();
/*  462:     */     }
/*  463: 890 */     return this.m_errorMsg == null;
/*  464:     */   }
/*  465:     */   
/*  466:     */   public String getValidityErrorMessage()
/*  467:     */   {
/*  468: 903 */     return this.m_errorMsg;
/*  469:     */   }
/*  470:     */   
/*  471:     */   public Resource newListCell(RDFNode value, Resource tail)
/*  472:     */   {
/*  473: 916 */     Resource cell = getModel().createResource();
/*  474:     */     
/*  475:     */ 
/*  476: 919 */     cell.addProperty(listFirst(), value);
/*  477: 920 */     cell.addProperty(listRest(), tail);
/*  478:     */     
/*  479:     */ 
/*  480:     */ 
/*  481:     */ 
/*  482:     */ 
/*  483: 926 */     cell.getModel().getGraph().add(new Axiom(cell.asNode(), RDF.type.asNode(), listType().asNode()));
/*  484: 927 */     cell.getModel().getGraph().add(new Axiom(cell.asNode(), RDF.type.asNode(), RDFS.Resource.asNode()));
/*  485:     */     
/*  486: 929 */     return cell;
/*  487:     */   }
/*  488:     */   
/*  489:     */   protected void checkValid()
/*  490:     */   {
/*  491: 945 */     if (!equals(listNil()))
/*  492:     */     {
/*  493: 950 */       checkValidProperty(listFirst(), null);
/*  494: 951 */       checkValidProperty(listRest(), null);
/*  495:     */     }
/*  496:     */   }
/*  497:     */   
/*  498:     */   private void checkValidProperty(Property p, RDFNode expected)
/*  499:     */   {
/*  500: 956 */     int count = 0;
/*  501: 958 */     for (StmtIterator j = getModel().listStatements(this, p, expected); j.hasNext(); j.next()) {
/*  502: 959 */       count++;
/*  503:     */     }
/*  504: 963 */     if (count == 0) {
/*  505: 964 */       throw new InvalidListException("List node " + toString() + " is not valid: it should have property " + p.toString() + (expected == null ? "" : new StringBuffer().append(" with value ").append(expected).toString()));
/*  506:     */     }
/*  507: 967 */     if (count > 1) {
/*  508: 968 */       throw new InvalidListException("List node " + toString() + " is not valid: it has more than one value for " + p.toString());
/*  509:     */     }
/*  510:     */   }
/*  511:     */   
/*  512:     */   protected void checkNotNil(String msg)
/*  513:     */   {
/*  514: 985 */     if (isEmpty()) {
/*  515: 986 */       throw new EmptyListException(msg);
/*  516:     */     }
/*  517:     */   }
/*  518:     */   
                protected RDFList findElement( boolean last, int index ) {
                       Property tail = listRest();
                        Resource nil = listNil();

                       Resource l = this;
                        int i = index;
                        boolean found = (last && l.hasProperty( tail, nil )) || (!last && (i == 0));

                        // search for the element whose tail is nil, or whose index is now zero
                        while (!found  &&  !l.equals( nil )) {
                            l = l.getRequiredProperty( tail ).getResource();
                            found = (last && l.hasProperty( tail, nil )) || (!last && (--i == 0));
                        }

                       if (!found) {
                            // premature end of list
                            if (!last) {
                                throw new ListIndexException( "Tried to access element " + index + " that is beyond the length of the list" );
                            }
                            else {
                                throw new InvalidListException( "Could not find last element of list (suggests list is not valid)" );
                            }
                       }
                        else {
                            return new RDFListImpl(l.asNode(), getModelCom());
                        }
                }
/*  546:     */   
/*  547:     */   protected RDFList copy(Iterator i)
/*  548:     */   {
/*  549:1045 */     Resource list = null;
/*  550:1046 */     Resource start = null;
/*  551:     */     
/*  552:1048 */     Property head = listFirst();
/*  553:1049 */     Property tail = listRest();
/*  554:1050 */     Resource cellType = listType();
/*  555:1052 */     while (i.hasNext())
/*  556:     */     {
/*  557:1054 */       Resource cell = getModel().createResource(cellType);
/*  558:1055 */       cell.addProperty(head, (RDFNode)i.next());
/*  559:1058 */       if (list != null) {
/*  560:1059 */         list.addProperty(tail, cell);
/*  561:     */       } else {
/*  562:1062 */         start = cell;
/*  563:     */       }
/*  564:1065 */       list = cell;
/*  565:     */     }
/*  566:1069 */     list.addProperty(tail, listNil());
/*  567:     */     
/*  568:1071 */     return new RDFListImpl(start.asNode(), getModelCom());
/*  569:     */   }
/*  570:     */   
/*  571:     */   protected static Resource setTailAux(Resource root, Resource tail, Property pTail)
/*  572:     */   {
/*  573:1087 */     Statement current = root.getRequiredProperty(pTail);
/*  574:1088 */     Resource oldTail = current.getResource();
/*  575:     */     
/*  576:     */ 
/*  577:1091 */     current.remove();
/*  578:1092 */     root.addProperty(pTail, tail);
/*  579:     */     
/*  580:1094 */     return oldTail;
/*  581:     */   }
/*  582:     */   
/*  583:     */   protected class RDFListIterator
/*  584:     */     extends NiceIterator
/*  585:     */   {
/*  586:     */     protected RDFList m_head;
/*  587:1111 */     protected RDFList m_seen = null;
/*  588:     */     
/*  589:     */     protected RDFListIterator(RDFList head)
/*  590:     */     {
/*  591:1117 */       this.m_head = head;
/*  592:     */     }
/*  593:     */     
/*  594:     */     public boolean hasNext()
/*  595:     */     {
/*  596:1124 */       return !this.m_head.isEmpty();
/*  597:     */     }
/*  598:     */     
/*  599:     */     public Object next()
/*  600:     */     {
/*  601:1131 */       this.m_seen = this.m_head;
/*  602:1132 */       this.m_head = this.m_head.getTail();
/*  603:     */       
/*  604:1134 */       return this.m_seen.getHead();
/*  605:     */     }
/*  606:     */     
/*  607:     */     public void remove()
/*  608:     */     {
/*  609:1141 */       if (this.m_seen == null) {
/*  610:1142 */         throw new IllegalStateException("Illegal remove from list operator");
/*  611:     */       }
/*  612:1146 */       this.m_seen.removeProperties();
/*  613:1147 */       this.m_seen = null;
/*  614:     */     }
/*  615:     */   }
/*  616:     */ }
