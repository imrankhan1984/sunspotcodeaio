/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  5:   */ 
/*  6:   */ public class DoesNotReifyException
/*  7:   */   extends JenaException
/*  8:   */ {
/*  9:   */   public DoesNotReifyException(Node n)
/* 10:   */   {
/* 11:22 */     super(n.toString());
/* 12:   */   }
/* 13:   */ }

