/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  5:   */ 
/*  6:   */ public class LiteralRequiredException
/*  7:   */   extends JenaException
/*  8:   */ {
/*  9:   */   public LiteralRequiredException(RDFNode n)
/* 10:   */   {
/* 11:19 */     super(n.toString());
/* 12:   */   }
/* 13:   */   
/* 14:   */   public LiteralRequiredException(Node n)
/* 15:   */   {
/* 16:23 */     super(n.toString());
/* 17:   */   }
/* 18:   */ }
