/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*  2:   */ 
/*  3:   */ public class AnonId
/*  4:   */ {
/*  5:46 */   protected String id = null;
/*  6:53 */   private static int idCount = 100000;
/*  7:   */   
/*  8:   */   public static AnonId create()
/*  9:   */   {
/* 10:56 */     return new AnonId();
/* 11:   */   }
/* 12:   */   
/* 13:   */   public static AnonId create(String id)
/* 14:   */   {
/* 15:59 */     return new AnonId(id);
/* 16:   */   }
/* 17:   */   
/* 18:   */   public AnonId()
/* 19:   */   {
/* 20:69 */     this.id = ("A" + idCount++);
/* 21:   */   }
/* 22:   */   
/* 23:   */   public AnonId(String id)
/* 24:   */   {
/* 25:76 */     this.id = id;
/* 26:   */   }
/* 27:   */   
/* 28:   */   public boolean equals(Object o)
/* 29:   */   {
/* 30:84 */     return ((o instanceof AnonId)) && (this.id.equals(((AnonId)o).id));
/* 31:   */   }
/* 32:   */   
/* 33:   */   public String toString()
/* 34:   */   {
/* 35:91 */     return this.id;
/* 36:   */   }
/* 37:   */   
/* 38:   */   public String getLabelString()
/* 39:   */   {
/* 40:99 */     return this.id;
/* 41:   */   }
/* 42:   */ }

