/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Alt;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.AltHasNoDefaultException;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Bag;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ObjectF;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResourceF;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Seq;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  20:    */ 
/*  21:    */ public class AltImpl
/*  22:    */   extends ContainerImpl
/*  23:    */   implements Alt
/*  24:    */ {
/*  25: 58 */   public static final Implementation factory = new Implementation()
/*  26:    */   {
/*  27:    */     public boolean canWrap(Node n, EnhGraph eg)
/*  28:    */     {
/*  29: 60 */       return true;
/*  30:    */     }
/*  31:    */     
/*  32:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  33:    */     {
/*  34: 63 */       return new AltImpl(n, eg);
/*  35:    */     }
/*  36:    */   };
/*  37:    */   
/*  38:    */   public AltImpl(ModelCom model)
/*  39:    */   {
/*  40: 69 */     super(model);
/*  41:    */   }
/*  42:    */   
/*  43:    */   public AltImpl(String uri, ModelCom model)
/*  44:    */   {
/*  45: 73 */     super(uri, model);
/*  46:    */   }
/*  47:    */   
/*  48:    */   public AltImpl(Resource r, ModelCom m)
/*  49:    */   {
/*  50: 77 */     super(r, m);
/*  51:    */   }
/*  52:    */   
/*  53:    */   public AltImpl(Node n, EnhGraph g)
/*  54:    */   {
/*  55: 81 */     super(n, g);
/*  56:    */   }
/*  57:    */   
/*  58:    */   private Statement needDefaultStatement()
/*  59:    */   {
/*  60: 87 */     Statement stmt = getDefaultStatement();
/*  61: 88 */     if (stmt == null) {
/*  62: 89 */       throw new AltHasNoDefaultException(this);
/*  63:    */     }
/*  64: 90 */     return stmt;
/*  65:    */   }
/*  66:    */   
/*  67:    */   public RDFNode getDefault()
/*  68:    */   {
/*  69: 94 */     return needDefaultStatement().getObject();
/*  70:    */   }
/*  71:    */   
/*  72:    */   public Resource getDefaultResource()
/*  73:    */   {
/*  74: 98 */     return needDefaultStatement().getResource();
/*  75:    */   }
/*  76:    */   
/*  77:    */   public Literal getDefaultLiteral()
/*  78:    */   {
/*  79:102 */     return needDefaultStatement().getLiteral();
/*  80:    */   }
/*  81:    */   
/*  82:    */   public boolean getDefaultBoolean()
/*  83:    */   {
/*  84:106 */     return needDefaultStatement().getBoolean();
/*  85:    */   }
/*  86:    */   
/*  87:    */   public byte getDefaultByte()
/*  88:    */   {
/*  89:110 */     return needDefaultStatement().getByte();
/*  90:    */   }
/*  91:    */   
/*  92:    */   public short getDefaultShort()
/*  93:    */   {
/*  94:114 */     return needDefaultStatement().getShort();
/*  95:    */   }
/*  96:    */   
/*  97:    */   public int getDefaultInt()
/*  98:    */   {
/*  99:118 */     return needDefaultStatement().getInt();
/* 100:    */   }
/* 101:    */   
/* 102:    */   public long getDefaultLong()
/* 103:    */   {
/* 104:122 */     return needDefaultStatement().getLong();
/* 105:    */   }
/* 106:    */   
/* 107:    */   public char getDefaultChar()
/* 108:    */   {
/* 109:126 */     return needDefaultStatement().getChar();
/* 110:    */   }
/* 111:    */   
/* 112:    */   public float getDefaultFloat()
/* 113:    */   {
/* 114:130 */     return needDefaultStatement().getFloat();
/* 115:    */   }
/* 116:    */   
/* 117:    */   public double getDefaultDouble()
/* 118:    */   {
/* 119:134 */     return needDefaultStatement().getDouble();
/* 120:    */   }
/* 121:    */   
/* 122:    */   public String getDefaultString()
/* 123:    */   {
/* 124:138 */     return needDefaultStatement().getString();
/* 125:    */   }
/* 126:    */   
/* 127:    */   public String getDefaultLanguage()
/* 128:    */   {
/* 129:142 */     return needDefaultStatement().getLanguage();
/* 130:    */   }
/* 131:    */   
/* 132:    */   public Resource getDefaultResource(ResourceF f)
/* 133:    */   {
/* 134:146 */     return needDefaultStatement().getResource();
/* 135:    */   }
/* 136:    */   
/* 137:    */   public Object getDefaultObject(ObjectF f)
/* 138:    */   {
/* 139:150 */     return needDefaultStatement().getObject(f);
/* 140:    */   }
/* 141:    */   
/* 142:    */   public Alt getDefaultAlt()
/* 143:    */   {
/* 144:154 */     return needDefaultStatement().getAlt();
/* 145:    */   }
/* 146:    */   
/* 147:    */   public Bag getDefaultBag()
/* 148:    */   {
/* 149:158 */     return needDefaultStatement().getBag();
/* 150:    */   }
/* 151:    */   
/* 152:    */   public Seq getDefaultSeq()
/* 153:    */   {
/* 154:162 */     return needDefaultStatement().getSeq();
/* 155:    */   }
/* 156:    */   
/* 157:    */   public Alt setDefault(RDFNode o)
/* 158:    */   {
/* 159:166 */     Statement stmt = getDefaultStatement();
/* 160:167 */     if (stmt != null) {
/* 161:167 */       getModel().remove(stmt);
/* 162:    */     }
/* 163:168 */     getModel().add(this, RDF.li(1), o);
/* 164:169 */     return this;
/* 165:    */   }
/* 166:    */   
/* 167:    */   public Alt setDefault(boolean o)
/* 168:    */   {
/* 169:173 */     return setDefault(String.valueOf(o));
/* 170:    */   }
/* 171:    */   
/* 172:    */   public Alt setDefault(long o)
/* 173:    */   {
/* 174:177 */     return setDefault(String.valueOf(o));
/* 175:    */   }
/* 176:    */   
/* 177:    */   public Alt setDefault(char o)
/* 178:    */   {
/* 179:181 */     return setDefault(String.valueOf(o));
/* 180:    */   }
/* 181:    */   
/* 182:    */   public Alt setDefault(float o)
/* 183:    */   {
/* 184:185 */     return setDefault(String.valueOf(o));
/* 185:    */   }
/* 186:    */   
/* 187:    */   public Alt setDefault(double o)
/* 188:    */   {
/* 189:189 */     return setDefault(String.valueOf(o));
/* 190:    */   }
/* 191:    */   
/* 192:    */   public Alt setDefault(Object o)
/* 193:    */   {
/* 194:193 */     return setDefault(String.valueOf(o));
/* 195:    */   }
/* 196:    */   
/* 197:    */   public Alt setDefault(String o)
/* 198:    */   {
/* 199:197 */     return setDefault(o, "");
/* 200:    */   }
/* 201:    */   
/* 202:    */   public Alt setDefault(String o, String l)
/* 203:    */   {
/* 204:201 */     return setDefault(new LiteralImpl(Node.createLiteral(o, l, false), getModelCom()));
/* 205:    */   }
/* 206:    */   
/* 207:    */   protected Statement getDefaultStatement()
/* 208:    */   {
/* 209:205 */     StmtIterator iter = getModel().listStatements(this, RDF.li(1), (RDFNode)null);
/* 210:    */     try
/* 211:    */     {
/* 212:206 */       return iter.hasNext() ? iter.nextStatement() : null;
/* 213:    */     }
/* 214:    */     finally
/* 215:    */     {
/* 216:206 */       iter.close();
/* 217:    */     }
/* 218:    */   }
/* 219:    */ }

