/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  7:   */ 
/*  8:   */ public class StmtIteratorImpl
/*  9:   */   extends WrappedIterator
/* 10:   */   implements StmtIterator
/* 11:   */ {
/* 12:   */   private Statement current;
/* 13:   */   
/* 14:   */   public StmtIteratorImpl(Iterator iterator)
/* 15:   */   {
/* 16:27 */     super(iterator);
/* 17:   */   }
/* 18:   */   
/* 19:   */   public Object next()
/* 20:   */   {
/* 21:35 */     return this.current = (Statement)super.next();
/* 22:   */   }
/* 23:   */   
/* 24:   */   public void remove()
/* 25:   */   {
/* 26:39 */     super.remove();
/* 27:40 */     this.current.remove();
/* 28:   */   }
/* 29:   */   
/* 30:   */   public Statement nextStatement()
/* 31:   */   {
/* 32:44 */     return (Statement)next();
/* 33:   */   }
/* 34:   */ }
