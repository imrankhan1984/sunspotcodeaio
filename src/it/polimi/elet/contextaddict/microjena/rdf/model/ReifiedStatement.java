package it.polimi.elet.contextaddict.microjena.rdf.model;

public abstract interface ReifiedStatement
  extends Resource
{
  public abstract Statement getStatement();
}

