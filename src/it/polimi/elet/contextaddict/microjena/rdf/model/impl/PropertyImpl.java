/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.shared.InvalidPropertyURIException;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  13:    */ 
/*  14:    */ public class PropertyImpl
/*  15:    */   extends ResourceImpl
/*  16:    */   implements Property
/*  17:    */ {
/*  18: 53 */   public static final Implementation factory = new Implementation()
/*  19:    */   {
/*  20:    */     public boolean canWrap(Node n, EnhGraph eg)
/*  21:    */     {
/*  22: 55 */       return n.isURI();
/*  23:    */     }
/*  24:    */     
/*  25:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  26:    */     {
/*  27: 58 */       return new PropertyImpl(n, eg);
/*  28:    */     }
/*  29:    */   };
/*  30: 61 */   protected int ordinal = -1;
/*  31:    */   
/*  32:    */   public PropertyImpl(String uri)
/*  33:    */   {
/*  34: 65 */     super(uri);
/*  35: 66 */     checkLocalName();
/*  36: 67 */     checkOrdinal();
/*  37:    */   }
/*  38:    */   
/*  39:    */   public RDFNode inModel(Model m)
/*  40:    */   {
/*  41: 71 */     return getModel() == m ? this : m.createProperty(getURI());
/*  42:    */   }
/*  43:    */   
/*  44:    */   private void checkLocalName()
/*  45:    */   {
/*  46: 75 */     String localName = getLocalName();
/*  47: 76 */     if ((localName == null) || (localName.equals(""))) {
/*  48: 77 */       throw new InvalidPropertyURIException(getURI());
/*  49:    */     }
/*  50:    */   }
/*  51:    */   
/*  52:    */   public PropertyImpl(String nameSpace, String localName)
/*  53:    */   {
/*  54: 81 */     super(nameSpace, localName);
/*  55: 82 */     checkLocalName();
/*  56: 83 */     checkOrdinal();
/*  57:    */   }
/*  58:    */   
/*  59:    */   public PropertyImpl(String uri, ModelCom m)
/*  60:    */   {
/*  61: 87 */     super(uri, m);
/*  62: 88 */     checkOrdinal();
/*  63:    */   }
/*  64:    */   
/*  65:    */   public PropertyImpl(String nameSpace, String localName, ModelCom m)
/*  66:    */   {
/*  67: 92 */     super(nameSpace, localName, m);
/*  68: 93 */     checkOrdinal();
/*  69:    */   }
/*  70:    */   
/*  71:    */   public PropertyImpl(Node n, EnhGraph m)
/*  72:    */   {
/*  73: 97 */     super(n, m);
/*  74: 98 */     checkOrdinal();
/*  75:    */   }
/*  76:    */   
/*  77:    */   public PropertyImpl(String nameSpace, String localName, int ordinal, ModelCom m)
/*  78:    */   {
/*  79:102 */     super(nameSpace, localName, m);
/*  80:103 */     checkLocalName();
/*  81:104 */     this.ordinal = ordinal;
/*  82:    */   }
/*  83:    */   
/*  84:    */   public boolean isProperty()
/*  85:    */   {
/*  86:108 */     return true;
/*  87:    */   }
/*  88:    */   
/*  89:    */   public int getOrdinal()
/*  90:    */   {
/*  91:112 */     if (this.ordinal < 0) {
/*  92:113 */       this.ordinal = computeOrdinal();
/*  93:    */     }
/*  94:114 */     return this.ordinal;
/*  95:    */   }
/*  96:    */   
/*  97:    */   private int computeOrdinal()
/*  98:    */   {
/*  99:118 */     String localName = getLocalName();
/* 100:    */     
/* 101:120 */     boolean aus = true;
/* 102:121 */     if (localName.charAt(0) != '_')
/* 103:    */     {
/* 104:122 */       aus = false;
/* 105:    */     }
/* 106:    */     else
/* 107:    */     {
/* 108:124 */       int i = localName.length();
/* 109:126 */       while (aus)
/* 110:    */       {
/* 111:126 */         i--;
/* 112:126 */         if (i <= 0) {
/* 113:    */           break;
/* 114:    */         }
/* 115:127 */         char ausCh = localName.charAt(i);
/* 116:128 */         if ((ausCh < '0') || (ausCh > '9')) {
/* 117:129 */           aus = false;
/* 118:    */         }
/* 119:    */       }
/* 120:    */     }
/* 121:132 */     if ((getNameSpace().equals(RDF.getURI())) && (aus)) {
/* 122:133 */       return parseInt(localName.substring(1));
/* 123:    */     }
/* 124:134 */     return 0;
/* 125:    */   }
/* 126:    */   
/* 127:    */   private int parseInt(String digits)
/* 128:    */   {
/* 129:    */     try
/* 130:    */     {
/* 131:139 */       return Integer.parseInt(digits);
/* 132:    */     }
/* 133:    */     catch (NumberFormatException e)
/* 134:    */     {
/* 135:141 */       throw new JenaException("checkOrdinal fails on " + digits, e);
/* 136:    */     }
/* 137:    */   }
/* 138:    */   
/* 139:    */   protected void checkOrdinal() {}
/* 140:    */ }
