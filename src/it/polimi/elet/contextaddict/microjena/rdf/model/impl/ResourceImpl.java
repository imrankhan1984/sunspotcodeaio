/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.AnonId;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.HasNoModelException;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResourceRequiredException;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.StmtIterator;
/*  17:    */ 
/*  18:    */ public class ResourceImpl
/*  19:    */   extends EnhNode
/*  20:    */   implements Resource
/*  21:    */ {
/*  22: 32 */   public static final Implementation factory = new Implementation()
/*  23:    */   {
/*  24:    */     public boolean canWrap(Node n, EnhGraph eg)
/*  25:    */     {
/*  26: 34 */       return !n.isLiteral();
/*  27:    */     }
/*  28:    */     
/*  29:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  30:    */     {
/*  31: 36 */       if (n.isLiteral()) {
/*  32: 36 */         throw new ResourceRequiredException(n);
/*  33:    */       }
/*  34: 37 */       return new ResourceImpl(n, eg);
/*  35:    */     }
/*  36:    */   };
/*  37: 41 */   public static final Implementation rdfNodeFactory = new Implementation()
/*  38:    */   {
/*  39:    */     public boolean canWrap(Node n, EnhGraph eg)
/*  40:    */     {
/*  41: 43 */       return true;
/*  42:    */     }
/*  43:    */     
/*  44:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  45:    */     {
/*  46: 45 */       if ((n.isURI()) || (n.isBlank())) {
/*  47: 46 */         return new ResourceImpl(n, eg);
/*  48:    */       }
/*  49: 47 */       if (n.isLiteral()) {
/*  50: 48 */         return new LiteralImpl(n, eg);
/*  51:    */       }
/*  52: 49 */       return null;
/*  53:    */     }
/*  54:    */   };
/*  55:    */   
/*  56:    */   public ResourceImpl(Node n, ModelCom m)
/*  57:    */   {
/*  58: 60 */     super(n, m);
/*  59:    */   }
/*  60:    */   
/*  61:    */   public ResourceImpl()
/*  62:    */   {
/*  63: 66 */     this((ModelCom)null);
/*  64:    */   }
/*  65:    */   
/*  66:    */   public ResourceImpl(ModelCom m)
/*  67:    */   {
/*  68: 70 */     this(fresh(null), m);
/*  69:    */   }
/*  70:    */   
/*  71:    */   public ResourceImpl(Node n, EnhGraph m)
/*  72:    */   {
/*  73: 75 */     super(n, m);
/*  74:    */   }
/*  75:    */   
/*  76:    */   public ResourceImpl(String uri)
/*  77:    */   {
/*  78: 79 */     super(fresh(uri), null);
/*  79:    */   }
/*  80:    */   
/*  81:    */   public ResourceImpl(String nameSpace, String localName)
/*  82:    */   {
/*  83: 83 */     super(Node.createURI(nameSpace + localName), null);
/*  84:    */   }
/*  85:    */   
/*  86:    */   public ResourceImpl(AnonId id)
/*  87:    */   {
/*  88: 87 */     this(id, null);
/*  89:    */   }
/*  90:    */   
/*  91:    */   public ResourceImpl(AnonId id, ModelCom m)
/*  92:    */   {
/*  93: 91 */     this(Node.createAnon(id), m);
/*  94:    */   }
/*  95:    */   
/*  96:    */   public ResourceImpl(String uri, ModelCom m)
/*  97:    */   {
/*  98: 95 */     this(fresh(uri), m);
/*  99:    */   }
/* 100:    */   
/* 101:    */   public ResourceImpl(Resource r, ModelCom m)
/* 102:    */   {
/* 103: 99 */     this(r.asNode(), m);
/* 104:    */   }
/* 105:    */   
/* 106:    */   public ResourceImpl(String nameSpace, String localName, ModelCom m)
/* 107:    */   {
/* 108:103 */     this(Node.createURI(nameSpace + localName), m);
/* 109:    */   }
/* 110:    */   
/* 111:    */   public RDFNode inModel(Model m)
/* 112:    */   {
/* 113:107 */     return !asNode().isConcrete() ? ((ModelCom)m).getRDFNode(asNode()) : isAnon() ? m.createResource(getId()) : getModel() == m ? this : m.createResource(getURI());
/* 114:    */   }
/* 115:    */   
/* 116:    */   private static Node fresh(String uri)
/* 117:    */   {
/* 118:118 */     return uri == null ? Node.createAnon() : Node.createURI(uri);
/* 119:    */   }
/* 120:    */   
/* 121:    */   public Node getNode()
/* 122:    */   {
/* 123:122 */     return asNode();
/* 124:    */   }
/* 125:    */   
/* 126:    */   public AnonId getId()
/* 127:    */   {
/* 128:126 */     return asNode().getBlankNodeId();
/* 129:    */   }
/* 130:    */   
/* 131:    */   public String getURI()
/* 132:    */   {
/* 133:130 */     return isAnon() ? null : this.node.getURI();
/* 134:    */   }
/* 135:    */   
/* 136:    */   public String getNameSpace()
/* 137:    */   {
/* 138:134 */     return isAnon() ? null : this.node.getNameSpace();
/* 139:    */   }
/* 140:    */   
/* 141:    */   public String getLocalName()
/* 142:    */   {
/* 143:138 */     return isAnon() ? null : this.node.getLocalName();
/* 144:    */   }
/* 145:    */   
/* 146:    */   public boolean hasURI(String uri)
/* 147:    */   {
/* 148:142 */     return this.node.hasURI(uri);
/* 149:    */   }
/* 150:    */   
/* 151:    */   public String toString()
/* 152:    */   {
/* 153:146 */     return asNode().toString();
/* 154:    */   }
/* 155:    */   
/* 156:    */   protected ModelCom mustHaveModel()
/* 157:    */   {
/* 158:150 */     ModelCom model = getModelCom();
/* 159:151 */     if (model == null) {
/* 160:152 */       throw new HasNoModelException(this);
/* 161:    */     }
/* 162:153 */     return model;
/* 163:    */   }
/* 164:    */   
/* 165:    */   public Statement getRequiredProperty(Property p)
/* 166:    */   {
/* 167:157 */     return mustHaveModel().getRequiredProperty(this, p);
/* 168:    */   }
/* 169:    */   
/* 170:    */   public Statement getProperty(Property p)
/* 171:    */   {
/* 172:161 */     return mustHaveModel().getProperty(this, p);
/* 173:    */   }
/* 174:    */   
/* 175:    */   public StmtIterator listProperties(Property p)
/* 176:    */   {
/* 177:165 */     return mustHaveModel().listStatements(this, p, (RDFNode)null);
/* 178:    */   }
/* 179:    */   
/* 180:    */   public StmtIterator listProperties()
/* 181:    */   {
/* 182:169 */     return mustHaveModel().listStatements(this, null, (RDFNode)null);
/* 183:    */   }
/* 184:    */   
/* 185:    */   public Resource addProperty(Property p, boolean o)
/* 186:    */   {
/* 187:173 */     mustHaveModel().add(this, p, o);
/* 188:174 */     return this;
/* 189:    */   }
/* 190:    */   
/* 191:    */   public Resource addProperty(Property p, long o)
/* 192:    */   {
/* 193:178 */     mustHaveModel().add(this, p, o);
/* 194:179 */     return this;
/* 195:    */   }
/* 196:    */   
/* 197:    */   public Resource addProperty(Property p, char o)
/* 198:    */   {
/* 199:183 */     mustHaveModel().add(this, p, o);
/* 200:184 */     return this;
/* 201:    */   }
/* 202:    */   
/* 203:    */   public Resource addProperty(Property p, float o)
/* 204:    */   {
/* 205:188 */     mustHaveModel().add(this, p, o);
/* 206:189 */     return this;
/* 207:    */   }
/* 208:    */   
/* 209:    */   public Resource addProperty(Property p, double o)
/* 210:    */   {
/* 211:193 */     mustHaveModel().add(this, p, o);
/* 212:194 */     return this;
/* 213:    */   }
/* 214:    */   
/* 215:    */   public Resource addProperty(Property p, String o)
/* 216:    */   {
/* 217:198 */     mustHaveModel().add(this, p, o);
/* 218:199 */     return this;
/* 219:    */   }
/* 220:    */   
/* 221:    */   public Resource addProperty(Property p, String o, String lang)
/* 222:    */   {
/* 223:203 */     mustHaveModel().add(this, p, o, lang);
/* 224:204 */     return this;
/* 225:    */   }
/* 226:    */   
/* 227:    */   public Resource addProperty(Property p, String lexicalForm, RDFDatatype datatype)
/* 228:    */   {
/* 229:208 */     mustHaveModel().add(this, p, lexicalForm, datatype);
/* 230:209 */     return this;
/* 231:    */   }
/* 232:    */   
/* 233:    */   public Resource addProperty(Property p, Object o)
/* 234:    */   {
/* 235:213 */     mustHaveModel().add(this, p, o);
/* 236:214 */     return this;
/* 237:    */   }
/* 238:    */   
/* 239:    */   public Resource addProperty(Property p, RDFNode o)
/* 240:    */   {
/* 241:218 */     mustHaveModel().add(this, p, o);
/* 242:219 */     return this;
/* 243:    */   }
/* 244:    */   
/* 245:    */   public boolean hasProperty(Property p)
/* 246:    */   {
/* 247:223 */     return mustHaveModel().contains(this, p);
/* 248:    */   }
/* 249:    */   
/* 250:    */   public boolean hasProperty(Property p, boolean o)
/* 251:    */   {
/* 252:227 */     return mustHaveModel().contains(this, p, o);
/* 253:    */   }
/* 254:    */   
/* 255:    */   public boolean hasProperty(Property p, long o)
/* 256:    */   {
/* 257:231 */     return mustHaveModel().contains(this, p, o);
/* 258:    */   }
/* 259:    */   
/* 260:    */   public boolean hasProperty(Property p, char o)
/* 261:    */   {
/* 262:235 */     return mustHaveModel().contains(this, p, o);
/* 263:    */   }
/* 264:    */   
/* 265:    */   public boolean hasProperty(Property p, float o)
/* 266:    */   {
/* 267:239 */     return mustHaveModel().contains(this, p, o);
/* 268:    */   }
/* 269:    */   
/* 270:    */   public boolean hasProperty(Property p, double o)
/* 271:    */   {
/* 272:243 */     return mustHaveModel().contains(this, p, o);
/* 273:    */   }
/* 274:    */   
/* 275:    */   public boolean hasProperty(Property p, String o)
/* 276:    */   {
/* 277:247 */     return mustHaveModel().contains(this, p, o);
/* 278:    */   }
/* 279:    */   
/* 280:    */   public boolean hasProperty(Property p, String o, String lang)
/* 281:    */   {
/* 282:251 */     return mustHaveModel().contains(this, p, o, lang);
/* 283:    */   }
/* 284:    */   
/* 285:    */   public boolean hasProperty(Property p, Object o)
/* 286:    */   {
/* 287:255 */     return mustHaveModel().contains(this, p, o);
/* 288:    */   }
/* 289:    */   
/* 290:    */   public boolean hasProperty(Property p, RDFNode o)
/* 291:    */   {
/* 292:259 */     return mustHaveModel().contains(this, p, o);
/* 293:    */   }
/* 294:    */   
/* 295:    */   public Resource removeProperties()
/* 296:    */   {
/* 297:263 */     removeAll(null);
/* 298:264 */     return this;
/* 299:    */   }
/* 300:    */   
/* 301:    */   public Resource removeAll(Property p)
/* 302:    */   {
/* 303:268 */     mustHaveModel().removeAll(this, p, (RDFNode)null);
/* 304:269 */     return this;
/* 305:    */   }
/* 306:    */   
/* 307:    */   public Model getModel()
/* 308:    */   {
/* 309:273 */     return (Model)getGraph();
/* 310:    */   }
/* 311:    */   
/* 312:    */   protected ModelCom getModelCom()
/* 313:    */   {
/* 314:277 */     return (ModelCom)getGraph();
/* 315:    */   }
/* 316:    */ }
