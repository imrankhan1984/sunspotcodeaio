package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
import java.util.NoSuchElementException;

public abstract interface NodeIterator
  extends ExtendedIterator
{
  public abstract boolean hasNext();
  
  public abstract Object next()
    throws NoSuchElementException;
  
  public abstract RDFNode nextNode()
    throws NoSuchElementException;
  
  public abstract void remove()
    throws NoSuchElementException;
  
  public abstract void close();
}

