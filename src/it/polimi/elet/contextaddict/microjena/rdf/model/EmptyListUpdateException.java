/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  4:   */ 
/*  5:   */ public class EmptyListUpdateException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public EmptyListUpdateException() {}
/*  9:   */   
/* 10:   */   public EmptyListUpdateException(String message)
/* 11:   */   {
/* 12:48 */     super(message);
/* 13:   */   }
/* 14:   */ }

