package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.graph.Graph;
import it.polimi.elet.contextaddict.microjena.graph.Node;
import it.polimi.elet.contextaddict.microjena.graph.Triple;

public abstract interface ModelGraphInterface
{
  public abstract Statement asStatement(Triple paramTriple);
  
  public abstract Graph getGraph();
  
  public abstract RDFNode asRDFNode(Node paramNode);
}

