/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.graph.Reifier;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.DoesNotReifyException;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ReifiedStatement;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  14:    */ 
/*  15:    */ public class ReifiedStatementImpl
/*  16:    */   extends ResourceImpl
/*  17:    */   implements ReifiedStatement
/*  18:    */ {
/*  19:    */   protected Statement statement;
/*  20:    */   
/*  21:    */   private ReifiedStatementImpl(ModelCom m, String uri, Statement s)
/*  22:    */   {
/*  23: 34 */     super(uri, m);
/*  24: 35 */     assertStatement(s);
/*  25:    */   }
/*  26:    */   
/*  27:    */   protected ReifiedStatementImpl(EnhGraph m, Node n, Statement s)
/*  28:    */   {
/*  29: 39 */     super(n, m);
/*  30: 40 */     assertStatement(s);
/*  31:    */   }
/*  32:    */   
/*  33:    */   private void assertStatement(Statement s)
/*  34:    */   {
/*  35: 44 */     this.statement = s;
/*  36:    */   }
/*  37:    */   
/*  38:    */   public Statement getStatement()
/*  39:    */   {
/*  40: 52 */     return this.statement;
/*  41:    */   }
/*  42:    */   
/*  43: 54 */   public static final Implementation reifiedStatementFactory = new Implementation()
/*  44:    */   {
/*  45:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  46:    */     {
/*  47: 61 */       Triple x = getTriple(eg, n);
/*  48: 62 */       if (x == null) {
/*  49: 63 */         throw new DoesNotReifyException(n);
/*  50:    */       }
/*  51: 64 */       Statement st = StatementImpl.toStatement(x, (ModelCom)eg);
/*  52: 65 */       return new ReifiedStatementImpl(eg, n, st);
/*  53:    */     }
/*  54:    */     
/*  55:    */     public boolean canWrap(Node n, EnhGraph eg)
/*  56:    */     {
/*  57: 76 */       return getTriple(eg, n) != null;
/*  58:    */     }
/*  59:    */     
/*  60:    */     private Triple getTriple(EnhGraph eg, Node n)
/*  61:    */     {
/*  62: 85 */       return eg.asGraph().getReifier().getTriple(n);
/*  63:    */     }
/*  64:    */   };
/*  65:    */   
/*  66:    */   protected Reifier getReifier()
/*  67:    */   {
/*  68: 92 */     return getModel().getGraph().getReifier();
/*  69:    */   }
/*  70:    */   
/*  71:    */   public boolean isValid()
/*  72:    */   {
/*  73: 96 */     return getModel().getGraph().getReifier().getTriple(asNode()) != null;
/*  74:    */   }
/*  75:    */   
/*  76:    */   private ReifiedStatementImpl installInReifier()
/*  77:    */   {
/*  78:105 */     getReifier().reifyAs(asNode(), this.statement.asTriple());
/*  79:106 */     return this;
/*  80:    */   }
/*  81:    */   
/*  82:    */   public static ReifiedStatement create(Statement s)
/*  83:    */   {
/*  84:114 */     return create((ModelCom)s.getModel(), (String)null, s);
/*  85:    */   }
/*  86:    */   
/*  87:    */   public static ReifiedStatementImpl create(ModelCom m, String uri, Statement s)
/*  88:    */   {
/*  89:122 */     return new ReifiedStatementImpl(m, uri, s).installInReifier();
/*  90:    */   }
/*  91:    */   
/*  92:    */   public static ReifiedStatementImpl create(EnhGraph eg, Node n, Statement s)
/*  93:    */   {
/*  94:126 */     return new ReifiedStatementImpl(eg, n, s).installInReifier();
/*  95:    */   }
/*  96:    */   
/*  97:    */   public String toString()
/*  98:    */   {
/*  99:130 */     return super.toString() + "=>" + this.statement;
/* 100:    */   }
/* 101:    */   
/* 102:    */   public static ReifiedStatement createExistingReifiedStatement(ModelCom model, Node n)
/* 103:    */   {
/* 104:134 */     Triple t = model.getGraph().getReifier().getTriple(n);
/* 105:135 */     return new ReifiedStatementImpl(model, n, model.asStatement(t));
/* 106:    */   }
/* 107:    */ }
