/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  4:   */ 
/*  5:   */ public class HasNoModelException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public HasNoModelException(Object x)
/*  9:   */   {
/* 10:18 */     super(x.toString());
/* 11:   */   }
/* 12:   */ }

