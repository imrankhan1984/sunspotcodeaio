package it.polimi.elet.contextaddict.microjena.rdf.model;

public abstract interface ObjectF
{
  public abstract Object createObject(String paramString)
    throws Exception;
}

