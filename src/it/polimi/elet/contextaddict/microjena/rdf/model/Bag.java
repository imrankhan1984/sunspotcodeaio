package it.polimi.elet.contextaddict.microjena.rdf.model;

public abstract interface Bag
  extends Container
{
  public abstract Container remove(Statement paramStatement);
}

