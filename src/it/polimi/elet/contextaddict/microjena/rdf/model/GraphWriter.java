/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.BaseDatatype;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Axiom;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.GraphUtil;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.graph.impl.LiteralLabel;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.shared.InvalidPropertyURIException;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.shared.SyntaxError;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.OWL;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.vocabulary.RDFS;
/*  17:    */ import java.io.IOException;
/*  18:    */ import java.io.InputStream;
/*  19:    */ import java.io.OutputStream;
/*  20:    */ 
/*  21:    */ public class GraphWriter
/*  22:    */ {
/*  23:    */   private Model model;
/*  24:    */   private int line;
/*  25:    */   private int column;
/*  26:    */   private boolean catchOwlAxioms;
/*  27:    */   
/*  28:    */   public GraphWriter(Model m)
/*  29:    */   {
/*  30: 39 */     this.model = m;
/*  31: 40 */     this.catchOwlAxioms = (m instanceof OntModel);
/*  32:    */   }
/*  33:    */   
/*  34:    */   public void readNTriple(InputStream is)
/*  35:    */     throws IOException
/*  36:    */   {
/*  37: 44 */     this.line = 1;
/*  38: 45 */     this.column = 1;
/*  39:    */     for (;;)
/*  40:    */     {
/*  41: 48 */       Node subject = readNode(is, false, false, true);
/*  42: 49 */       if (subject == null) {
/*  43:    */         break;
/*  44:    */       }
/*  45: 50 */       Node predicate = readNode(is, true, false, false);
/*  46: 51 */       Node object = readNode(is, false, true, false);
/*  47: 52 */       Triple addTriple = new Triple(subject, predicate, object);
/*  48: 53 */       this.model.getGraph().add(addTriple);
/*  49: 54 */       if (this.catchOwlAxioms) {
/*  50: 55 */         catchAxioms(addTriple);
/*  51:    */       }
/*  52: 56 */       closeLine(is);
/*  53:    */     }
/*  54:    */   }
/*  55:    */   
/*  56:    */   private Node readNode(InputStream is, boolean requestURI, boolean allowLiteral, boolean allowEndOfFile)
/*  57:    */     throws IOException
/*  58:    */   {
/*  59: 65 */     int aus = nextValidChar(is, !allowEndOfFile);
/*  60: 66 */     if ((requestURI) && (aus != 60)) {
/*  61: 67 */       throw new InvalidPropertyURIException("");
/*  62:    */     }
/*  63: 68 */     if ((allowEndOfFile) && (aus == -1)) {
/*  64: 70 */       return null;
/*  65:    */     }
/*  66: 72 */     if (aus == 60) {
/*  67: 74 */       return Node.createURI(readURI(is));
/*  68:    */     }
/*  69: 77 */     if (aus == 95)
/*  70:    */     {
/*  71: 78 */       if ((aus = readChar(is, true)) != 58) {
/*  72: 79 */         throw new SyntaxError("Syntax error at line " + this.line + " position " + this.column + ": expected \":\"");
/*  73:    */       }
/*  74: 80 */       if ((aus = readChar(is, true)) != 65) {
/*  75: 81 */         return Node.createAnon(new AnonId(new String(new byte[] { Byte.parseByte(String.valueOf(aus)) }).concat(parseString(is, ' '))));
/*  76:    */       }
/*  77: 85 */       return Node.createAnon(new AnonId(parseString(is, ' ')));
/*  78:    */     }
/*  79: 88 */     if ((allowLiteral) && (aus == 34)) {
/*  80: 89 */       return Node.createLiteral(readLiteral(is));
/*  81:    */     }
/*  82: 92 */     throw new SyntaxError("Syntax error at line " + this.line + " position " + this.column + ": unexpected input "+aus);
/*  83:    */   }
/*  84:    */   
/*  85:    */   public void writeNTriple(OutputStream os)
/*  86:    */     throws IOException
/*  87:    */   {
/*  88:100 */     ExtendedIterator it = GraphUtil.findAll(this.model.getGraph());
/*  89:103 */     while (it.hasNext())
/*  90:    */     {
/*  91:104 */       Triple t = (Triple)it.next();
/*  92:105 */       if (!(t instanceof Axiom))
/*  93:    */       {
/*  94:106 */         writeNode(os, t.getSubject());
/*  95:107 */         os.write(32);
/*  96:108 */         writeNode(os, t.getPredicate());
/*  97:109 */         os.write(32);
/*  98:110 */         writeNode(os, t.getObject());
/*  99:111 */         os.write(32);
/* 100:112 */         os.write(46);
/* 101:113 */         os.write(10);
/* 102:    */       }
/* 103:    */     }
/* 104:    */   }
/* 105:    */   
/* 106:    */   private void writeNode(OutputStream os, Node n)
/* 107:    */     throws IOException
/* 108:    */   {
/* 109:125 */     if (n.isURI()) {
/* 110:126 */       writeURI(os, n.getURI());
/* 111:129 */     } else if (n.isBlank()) {
/* 112:130 */       os.write("_:A".concat(n.toString()).getBytes());
/* 113:132 */     } else if (n.isLiteral()) {
/* 114:133 */       writeLiteral(os, n);
/* 115:    */     }
/* 116:    */   }
/* 117:    */   
/* 118:    */   private void writeURI(OutputStream os, String uri)
/* 119:    */     throws IOException
/* 120:    */   {
/* 121:140 */     os.write(60);
/* 122:141 */     os.write(uri.getBytes());
/* 123:142 */     os.write(62);
/* 124:    */   }
/* 125:    */   
/* 126:    */   private void writeLiteral(OutputStream os, Node n)
/* 127:    */     throws IOException
/* 128:    */   {
/* 129:146 */     os.write(34);
/* 130:147 */     os.write(n.getLiteralLexicalForm().getBytes());
/* 131:148 */     os.write(34);
/* 132:149 */     String aus = n.getLiteralLanguage();
/* 133:150 */     if ((aus != null) && 
/* 134:151 */       (!aus.equals("")))
/* 135:    */     {
/* 136:152 */       os.write(64);
/* 137:153 */       os.write(aus.getBytes());
/* 138:    */     }
/* 139:156 */     aus = n.getLiteralDatatypeURI();
/* 140:157 */     if ((aus != null) && 
/* 141:158 */       (!aus.equals("")))
/* 142:    */     {
/* 143:159 */       os.write(94);
/* 144:160 */       os.write(94);
/* 145:161 */       writeURI(os, aus);
/* 146:    */     }
/* 147:    */   }
/* 148:    */   
/* 149:    */   private String parseString(InputStream is, char term)
/* 150:    */     throws IOException
/* 151:    */   {
/* 152:169 */     StringBuffer result = new StringBuffer();
/* 153:170 */     int aus = readChar(is, true);
/* 154:171 */     while ((aus != -1) && (aus != term))
/* 155:    */     {
/* 156:172 */       result.append((char)aus);
/* 157:173 */       aus = readChar(is, true);
/* 158:    */     }
/* 159:175 */     return result.toString();
/* 160:    */   }
/* 161:    */   
/* 162:    */   private int nextValidChar(InputStream is, boolean catchEndOfFile)
/* 163:    */     throws IOException
/* 164:    */   {
/* 165:180 */     int result = readChar(is, catchEndOfFile);
/* 166:181 */     while ((result == 32) || (result == 10) || (result == 13)) {
/* 167:182 */       result = readChar(is, catchEndOfFile);
/* 168:    */     }
/* 169:183 */     return result;
/* 170:    */   }
/* 171:    */   
/* 172:    */   private String readURI(InputStream is)
/* 173:    */     throws IOException
/* 174:    */   {
/* 175:187 */     return parseString(is, '>');
/* 176:    */   }
/* 177:    */   
/* 178:    */   private LiteralLabel readLiteral(InputStream is)
/* 179:    */     throws IOException
/* 180:    */   {
/* 181:198 */     String lexical = null;String lang = null;String datatypeURI = null;
/* 182:199 */     lexical = parseString(is, '"');
/* 183:200 */     int aus = readChar(is, true);
/* 184:201 */     if (aus == 64)
/* 185:    */     {
/* 186:202 */       StringBuffer langBuffer = new StringBuffer();
/* 187:203 */       aus = readChar(is, true);
/* 188:204 */       while ((aus != 94) && (aus != 32) && (aus != 46))
/* 189:    */       {
/* 190:205 */         langBuffer.append((char)aus);
/* 191:206 */         aus = readChar(is, true);
/* 192:    */       }
/* 193:208 */       lang = langBuffer.toString();
/* 194:    */     }
/* 195:210 */     if (aus == 94)
/* 196:    */     {
/* 197:211 */       if ((aus = readChar(is, true)) != 94) {
/* 198:212 */         throw new SyntaxError("Syntax error at line " + this.line + " position " + this.column + ": expected \"^^\"");
/* 199:    */       }
/* 200:213 */       if ((aus = readChar(is, true)) != 60) {
/* 201:214 */         throw new SyntaxError("Syntax error at line " + this.line + " position " + this.column + ": expected \"^^<\"");
/* 202:    */       }
/* 203:215 */       datatypeURI = readURI(is);
/* 204:    */     }
/* 205:220 */     if ((datatypeURI == null) && (lang != null)) {
/* 206:221 */       return new LiteralLabel(lexical, lang, false);
/* 207:    */     }
/* 208:223 */     if (datatypeURI != null) {
/* 209:225 */       return new LiteralLabel(lexical, null, new BaseDatatype(datatypeURI));
/* 210:    */     }
/* 211:228 */     return new LiteralLabel(lexical);
/* 212:    */   }
/* 213:    */   
/* 214:    */   private void closeLine(InputStream is)
/* 215:    */     throws IOException
/* 216:    */   {
/* 217:233 */     if (!catchRequiredChar(is, '.')) {
/* 218:234 */       throw new SyntaxError("Syntax error at line " + this.line + " position " + this.column + ": expected \".\"");
/* 219:    */     }
/* 220:    */   }
/* 221:    */   
/* 222:    */   private boolean catchRequiredChar(InputStream is, char find)
/* 223:    */     throws IOException
/* 224:    */   {
/* 225:238 */     int aus = nextValidChar(is, false);
/* 226:239 */     return (char)aus == find;
/* 227:    */   }
/* 228:    */   
/* 229:    */   private int readChar(InputStream is, boolean catchEndOfFile)
/* 230:    */     throws IOException
/* 231:    */   {
/* 232:243 */     int aus = is.read();
/* 233:244 */     if (aus != -1)
/* 234:    */     {
/* 235:245 */       if ((char)aus == '\n')
/* 236:    */       {
/* 237:246 */         this.line += 1;
/* 238:247 */         this.column = 1;
/* 239:    */       }
/* 240:    */       else
/* 241:    */       {
/* 242:250 */         this.column += 1;
/* 243:    */       }
/* 244:    */     }
/* 245:254 */     else if (catchEndOfFile) {
/* 246:255 */       throw new SyntaxError("Syntax error at line " + this.line + " position " + this.column + ": premature end of file");
/* 247:    */     }
/* 248:257 */     return aus;
/* 249:    */   }
/* 250:    */   
/* 251:    */   public void catchAxioms(Triple t)
/* 252:    */   {
/* 253:261 */     Node subject = t.getSubject();
/* 254:262 */     if (t.matches(Node.ANY, RDF.first.asNode(), Node.ANY))
/* 255:    */     {
/* 256:264 */       addResourceAxiom(subject);
/* 257:265 */       addAxiom(subject, RDF.type.asNode(), RDF.List.asNode());
/* 258:    */     }
/* 259:268 */     else if (t.matches(Node.ANY, RDF.type.asNode(), RDF.Property.asNode()))
/* 260:    */     {
/* 261:270 */       addResourceAxiom(subject);
/* 262:271 */       addAxiom(subject, RDFS.subPropertyOf.asNode(), subject);
/* 263:    */     }
/* 264:275 */     else if (t.matches(Node.ANY, RDF.type.asNode(), OWL.Class.asNode()))
/* 265:    */     {
/* 266:276 */       addResourceAxiom(subject);
/* 267:277 */       addClassAxiom(subject);
/* 268:278 */       addAxiom(subject, RDFS.subClassOf.asNode(), subject);
/* 269:    */     }
/* 270:    */   }
/* 271:    */   
/* 272:    */   private void addAxiom(Node subject, Node predicate, Node object)
/* 273:    */   {
/* 274:285 */     this.model.getGraph().add(new Axiom(subject, predicate, object));
/* 275:    */   }
/* 276:    */   
/* 277:    */   private void addResourceAxiom(Node value)
/* 278:    */   {
/* 279:289 */     this.model.getGraph().add(new Axiom(value, RDF.type.asNode(), RDFS.Resource.asNode()));
/* 280:    */   }
/* 281:    */   
/* 282:    */   private void addClassAxiom(Node value)
/* 283:    */   {
/* 284:293 */     this.model.getGraph().add(new Axiom(value, RDF.type.asNode(), RDFS.Class.asNode()));
/* 285:    */   }
/* 286:    */ }
