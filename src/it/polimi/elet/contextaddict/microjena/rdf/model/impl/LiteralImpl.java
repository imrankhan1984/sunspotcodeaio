/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.datatypes.DatatypeFormatException;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhNode;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.LiteralRequiredException;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ObjectF;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.shared.BadBooleanException;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.shared.BadCharLiteralException;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*  18:    */ 
/*  19:    */ public class LiteralImpl
/*  20:    */   extends EnhNode
/*  21:    */   implements Literal
/*  22:    */ {
/*  23: 58 */   public static final Implementation factory = new Implementation()
/*  24:    */   {
/*  25:    */     public boolean canWrap(Node n, EnhGraph eg)
/*  26:    */     {
/*  27: 60 */       return n.isLiteral();
/*  28:    */     }
/*  29:    */     
/*  30:    */     public EnhNode wrap(Node n, EnhGraph eg)
/*  31:    */     {
/*  32: 63 */       if (!n.isLiteral()) {
/*  33: 63 */         throw new LiteralRequiredException(n);
/*  34:    */       }
/*  35: 64 */       return new LiteralImpl(n, eg);
/*  36:    */     }
/*  37:    */   };
/*  38:    */   
/*  39:    */   public LiteralImpl(Node n, ModelCom m)
/*  40:    */   {
/*  41: 69 */     super(n, m);
/*  42:    */   }
/*  43:    */   
/*  44:    */   public LiteralImpl(Node n, EnhGraph m)
/*  45:    */   {
/*  46: 73 */     super(n, m);
/*  47:    */   }
/*  48:    */   
/*  49:    */   public RDFNode inModel(Model m)
/*  50:    */   {
/*  51: 82 */     return this;
/*  52:    */   }
/*  53:    */   
/*  54:    */   /**
/*  55:    */    
/*  57:    */   public LiteralImpl(boolean b)
/*  58:    */   {
/*  59: 88 */     this(String.valueOf(b));
/*  60:    */   }
/*  61:    */   
/*  62:    */   /**
/*  63:    */    
/*  65:    */   public LiteralImpl(long l)
/*  66:    */   {
/*  67: 94 */     this(String.valueOf(l));
/*  68:    */   }
/*  69:    */   
/*  70:    */   /**
/*  71:    */    
/*  73:    */   public LiteralImpl(char c)
/*  74:    */   {
/*  75:100 */     this(String.valueOf(c));
/*  76:    */   }
/*  77:    */   
/*  78:    */   /**
/*  79:    */    
/*  81:    */   public LiteralImpl(float f)
/*  82:    */   {
/*  83:106 */     this(String.valueOf(f));
/*  84:    */   }
/*  85:    */   
/*  86:    */   /**
/*  87:    */    
/*  89:    */   public LiteralImpl(double d)
/*  90:    */   {
/*  91:112 */     this(String.valueOf(d));
/*  92:    */   }
/*  93:    */   
/*  94:    */   /**
/*  95:    */   
/*  97:    */   public LiteralImpl(String s)
/*  98:    */   {
/*  99:118 */     this(s, "");
/* 100:    */   }
/* 101:    */   
/* 102:    */   /**
/* 103:    */    
/* 105:    */   public LiteralImpl(String s, String l)
/* 106:    */   {
/* 107:124 */     this(s, l, false);
/* 108:    */   }
/* 109:    */   
/* 110:    */   /**
/* 111:    */    
/* 113:    */   public LiteralImpl(String s, boolean wellFormed)
/* 114:    */   {
/* 115:131 */     this(s, "", wellFormed);
/* 116:    */   }
/* 117:    */   
/* 118:    */   /**
/* 119:    */    
/* 121:    */   public LiteralImpl(String s, String l, boolean wellFormed)
/* 122:    */   {
/* 123:139 */     this(s, l, wellFormed, null);
/* 124:    */   }
/* 125:    */   
/* 126:    */   /**
/* 127:    */   
/* 129:    */   public LiteralImpl(String s, String l, boolean wellFormed, ModelCom m)
/* 130:    */   {
/* 131:148 */     this(Node.createLiteral(s, l, wellFormed), m);
/* 132:    */   }
/* 133:    */   
/* 134:    */   /**
/* 135:    */    
/* 137:    */   public LiteralImpl(Object o)
/* 138:    */   {
/* 139:155 */     this(o.toString());
/* 140:    */   }
/* 141:    */   
/* 142:    */   public String toString()
/* 143:    */   {
/* 144:158 */     return asNode().toString(PrefixMapping.Standard, false);
/* 145:    */   }
/* 146:    */   
/* 147:    */   public Object getValue()
/* 148:    */   {
/* 149:169 */     return asNode().getLiteralValue();
/* 150:    */   }
/* 151:    */   
/* 152:    */   public RDFDatatype getDatatype()
/* 153:    */   {
/* 154:177 */     return asNode().getLiteralDatatype();
/* 155:    */   }
/* 156:    */   
/* 157:    */   public String getDatatypeURI()
/* 158:    */   {
/* 159:185 */     return asNode().getLiteralDatatypeURI();
/* 160:    */   }
/* 161:    */   
/* 162:    */   public boolean isPlainLiteral()
/* 163:    */   {
/* 164:192 */     return asNode().getLiteralDatatype() == null;
/* 165:    */   }
/* 166:    */   
/* 167:    */   public String getLexicalForm()
/* 168:    */   {
/* 169:199 */     return asNode().getLiteralLexicalForm();
/* 170:    */   }
/* 171:    */   
/* 172:    */   public boolean getBoolean()
/* 173:    */   {
/* 174:203 */     Object value = asNode().getLiteralValue();
/* 175:204 */     if (isPlainLiteral())
/* 176:    */     {
/* 177:206 */       if (value.equals("true")) {
/* 178:207 */         return true;
/* 179:    */       }
/* 180:210 */       if (value.equals("false")) {
/* 181:211 */         return false;
/* 182:    */       }
/* 183:214 */       throw new BadBooleanException(value.toString());
/* 184:    */     }
/* 185:219 */     if ((value instanceof Boolean)) {
/* 186:220 */       return ((Boolean)value).booleanValue();
/* 187:    */     }
/* 188:223 */     throw new DatatypeFormatException(toString() + " is not a Boolean");
/* 189:    */   }
/* 190:    */   
/* 191:    */   public byte getByte()
/* 192:    */   {
/* 193:229 */     if (isPlainLiteral()) {
/* 194:230 */       return Byte.parseByte(getLexicalForm());
/* 195:    */     }
/* 196:233 */     return Byte.parseByte(asString(getValue()));
/* 197:    */   }
/* 198:    */   
/* 199:    */   public short getShort()
/* 200:    */   {
/* 201:238 */     if (isPlainLiteral()) {
/* 202:239 */       return Short.parseShort(getLexicalForm());
/* 203:    */     }
/* 204:242 */     return Short.parseShort(asString(getValue()));
/* 205:    */   }
/* 206:    */   
/* 207:    */   public int getInt()
/* 208:    */   {
/* 209:247 */     if (isPlainLiteral()) {
/* 210:248 */       return Integer.parseInt(getLexicalForm());
/* 211:    */     }
/* 212:251 */     return Integer.parseInt(asString(getValue()));
/* 213:    */   }
/* 214:    */   
/* 215:    */   public long getLong()
/* 216:    */   {
/* 217:256 */     if (isPlainLiteral()) {
/* 218:257 */       return Long.parseLong(getLexicalForm());
/* 219:    */     }
/* 220:260 */     return Long.parseLong(asString(getValue()));
/* 221:    */   }
/* 222:    */   
/* 223:    */   public char getChar()
/* 224:    */   {
/* 225:265 */     if (isPlainLiteral())
/* 226:    */     {
/* 227:266 */       if (getString().length() == 1) {
/* 228:267 */         return getString().charAt(0);
/* 229:    */       }
/* 230:270 */       throw new BadCharLiteralException(getString());
/* 231:    */     }
/* 232:274 */     Object value = getValue();
/* 233:275 */     if ((value instanceof Character)) {
/* 234:276 */       return ((Character)value).charValue();
/* 235:    */     }
/* 236:279 */     throw new DatatypeFormatException(value.toString() + " is not a Character");
/* 237:    */   }
/* 238:    */   
/* 239:    */   public float getFloat()
/* 240:    */   {
/* 241:285 */     if (isPlainLiteral()) {
/* 242:286 */       return Float.parseFloat(getLexicalForm());
/* 243:    */     }
/* 244:289 */     return Float.parseFloat(asString(getValue()));
/* 245:    */   }
/* 246:    */   
/* 247:    */   public double getDouble()
/* 248:    */   {
/* 249:294 */     if (isPlainLiteral()) {
/* 250:295 */       return Double.parseDouble(getLexicalForm());
/* 251:    */     }
/* 252:298 */     return Double.parseDouble(asString(getValue()));
/* 253:    */   }
/* 254:    */   
/* 255:    */   public String getString()
/* 256:    */   {
/* 257:303 */     return asNode().getLiteralLexicalForm();
/* 258:    */   }
/* 259:    */   
/* 260:    */   public Object getObject(ObjectF f)
/* 261:    */   {
/* 262:307 */     if (isPlainLiteral()) {
/* 263:    */       try
/* 264:    */       {
/* 265:309 */         return f.createObject(getString());
/* 266:    */       }
/* 267:    */       catch (Exception e)
/* 268:    */       {
/* 269:311 */         throw new JenaException(e);
/* 270:    */       }
/* 271:    */     }
/* 272:315 */     return getValue();
/* 273:    */   }
/* 274:    */   
/* 275:    */   public String getLanguage()
/* 276:    */   {
/* 277:320 */     return asNode().getLiteralLanguage();
/* 278:    */   }
/* 279:    */   
/* 280:    */   public boolean getWellFormed()
/* 281:    */   {
/* 282:324 */     return isWellFormedXML();
/* 283:    */   }
/* 284:    */   
/* 285:    */   public boolean isWellFormedXML()
/* 286:    */   {
/* 287:328 */     return asNode().getLiteralIsXML();
/* 288:    */   }
/* 289:    */   
/* 290:    */   public boolean sameValueAs(Literal other)
/* 291:    */   {
/* 292:340 */     return asNode().sameValueAs(other.asNode());
/* 293:    */   }
/* 294:    */   
/* 295:    */   private String asString(Object value)
/* 296:    */   {
/* 297:345 */     if (((value instanceof Byte)) || ((value instanceof Short)) || ((value instanceof Long)) || ((value instanceof Float)) || ((value instanceof Double)) || ((value instanceof Integer))) {
/* 298:352 */       return value.toString();
/* 299:    */     }
/* 300:355 */     String message = "Error converting typed value to a number. \n";
/* 301:356 */     message = message + "Datatype is: " + getDatatypeURI();
/* 302:357 */     if ((getDatatypeURI() == null) || (!getDatatypeURI().startsWith("http://www.w3.org/2001/XMLSchema"))) {
/* 303:358 */       message = message + " which is not an xsd type.";
/* 304:    */     }
/* 305:360 */     message = message + " \n";
/* 306:361 */     String type = message = message + "Java representation type is " + (value == null ? "null" : value.getClass().toString());
/* 307:    */     
/* 308:363 */     throw new DatatypeFormatException(message);
/* 309:    */   }
/* 310:    */ }
