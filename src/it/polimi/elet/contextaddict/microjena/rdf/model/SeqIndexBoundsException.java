/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  4:   */ 
/*  5:   */ public class SeqIndexBoundsException
/*  6:   */   extends JenaException
/*  7:   */ {
/*  8:   */   public SeqIndexBoundsException(int limit, int index)
/*  9:   */   {
/* 10:17 */     super("" + index + " must be in the range 1.." + limit);
/* 11:   */   }
/* 12:   */ }
