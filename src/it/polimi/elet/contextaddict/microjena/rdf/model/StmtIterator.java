package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
import java.util.NoSuchElementException;

public abstract interface StmtIterator
  extends ExtendedIterator
{
  public abstract Statement nextStatement()
    throws NoSuchElementException;
}
