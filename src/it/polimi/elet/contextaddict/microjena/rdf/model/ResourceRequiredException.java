/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.shared.PrefixMapping;
/*  6:   */ 
/*  7:   */ public class ResourceRequiredException
/*  8:   */   extends JenaException
/*  9:   */ {
/* 10:   */   public ResourceRequiredException(RDFNode n)
/* 11:   */   {
/* 12:20 */     this(n.asNode());
/* 13:   */   }
/* 14:   */   
/* 15:   */   public ResourceRequiredException(Node n)
/* 16:   */   {
/* 17:24 */     super(n.toString(PrefixMapping.Standard, true));
/* 18:   */   }
/* 19:   */ }

