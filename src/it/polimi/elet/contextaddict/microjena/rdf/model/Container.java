package it.polimi.elet.contextaddict.microjena.rdf.model;

public abstract interface Container
  extends Resource
{
  public abstract boolean isAlt();
  
  public abstract boolean isSeq();
  
  public abstract boolean isBag();
  
  public abstract Container add(RDFNode paramRDFNode);
  
  public abstract Container add(boolean paramBoolean);
  
  public abstract Container add(long paramLong);
  
  public abstract Container add(char paramChar);
  
  public abstract Container add(float paramFloat);
  
  public abstract Container add(double paramDouble);
  
  public abstract Container add(String paramString);
  
  public abstract Container add(String paramString1, String paramString2);
  
  public abstract Container add(Object paramObject);
  
  public abstract boolean contains(RDFNode paramRDFNode);
  
  public abstract boolean contains(boolean paramBoolean);
  
  public abstract boolean contains(long paramLong);
  
  public abstract boolean contains(char paramChar);
  
  public abstract boolean contains(float paramFloat);
  
  public abstract boolean contains(double paramDouble);
  
  public abstract boolean contains(String paramString);
  
  public abstract boolean contains(String paramString1, String paramString2);
  
  public abstract boolean contains(Object paramObject);
  
  public abstract Container remove(Statement paramStatement);
  
  public abstract NodeIterator iterator();
  
  public abstract int size();
}

