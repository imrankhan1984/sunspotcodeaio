package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.datatypes.RDFDatatype;

public abstract interface Literal
  extends RDFNode
{
  public abstract Object getValue();
  
  public abstract RDFDatatype getDatatype();
  
  public abstract String getDatatypeURI();
  
  public abstract String getLexicalForm();
  
  public abstract boolean getBoolean();
  
  public abstract byte getByte();
  
  public abstract short getShort();
  
  public abstract int getInt();
  
  public abstract long getLong();
  
  public abstract char getChar();
  
  public abstract float getFloat();
  
  public abstract double getDouble();
  
  public abstract String getString();
  
  public abstract Object getObject(ObjectF paramObjectF);
  
  public abstract String getLanguage();
  
  /**
   * @deprecated
   */
  public abstract boolean getWellFormed();
  
  public abstract boolean isWellFormedXML();
  
  public abstract boolean equals(Object paramObject);
  
  public abstract boolean sameValueAs(Literal paramLiteral);
}

