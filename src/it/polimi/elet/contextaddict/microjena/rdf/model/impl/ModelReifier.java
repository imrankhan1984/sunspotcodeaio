/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.graph.FrontsTriple;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.graph.Graph;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Reifier;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.graph.TripleMatch;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.graph.impl.GraphBase;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RSIterator;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ReifiedStatement;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.shared.ReificationStyle;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.util.List;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  20:    */ 
/*  21:    */ public class ModelReifier
/*  22:    */ {
/*  23:    */   private ModelCom model;
/*  24:    */   public Reifier reifier;
/*  25: 43 */   private static boolean copyingReifications = false;
/*  26:    */   
/*  27:    */   public ModelReifier(ModelCom model)
/*  28:    */   {
/*  29: 50 */     this.model = model;
/*  30: 51 */     this.reifier = model.asGraph().getReifier();
/*  31:    */   }
/*  32:    */   
/*  33:    */   public ReificationStyle getReificationStyle()
/*  34:    */   {
/*  35: 55 */     return this.reifier.getStyle();
/*  36:    */   }
/*  37:    */   
/*  38:    */   protected static Graph getHiddenTriples(Model m)
/*  39:    */   {
/*  40: 63 */     Graph mGraph = m.getGraph();
/*  41: 64 */     final Reifier r = mGraph.getReifier();
/*  42: 65 */     return new GraphBase()
/*  43:    */     {
/*  44:    */       //private final Reifier val$r;
/*  45:    */       
/*  46:    */       public ExtendedIterator graphBaseFind(TripleMatch m)
/*  47:    */       {
/*  48: 67 */         return r.findEither(m, true);
/*  49:    */       }
/*  50:    */       
/*  51:    */       public void performAdd(Triple t) {}
/*  52:    */     };
/*  53:    */   }
/*  54:    */   
/*  55:    */   public Model getHiddenStatements()
/*  56:    */   {
/*  57: 81 */     return new ModelCom(getHiddenTriples(this.model));
/*  58:    */   }
/*  59:    */   
/*  60:    */   public ReifiedStatement createReifiedStatement(Statement s)
/*  61:    */   {
/*  62: 90 */     return createReifiedStatement(null, s);
/*  63:    */   }
/*  64:    */   
/*  65:    */   public ReifiedStatement createReifiedStatement(String uri, Statement s)
/*  66:    */   {
/*  67:102 */     return ReifiedStatementImpl.create(this.model, uri, s);
/*  68:    */   }
/*  69:    */   
/*  70:    */   public Resource getAnyReifiedStatement(Statement s)
/*  71:    */   {
/*  72:112 */     RSIterator it = listReifiedStatements(s);
/*  73:113 */     if (it.hasNext()) {
/*  74:    */       try
/*  75:    */       {
/*  76:115 */         return it.nextRS();
/*  77:    */       }
/*  78:    */       finally
/*  79:    */       {
/*  80:118 */         it.close();
/*  81:    */       }
/*  82:    */     }
/*  83:120 */     return createReifiedStatement(s);
/*  84:    */   }
/*  85:    */   
/*  86:    */   public boolean isReified(FrontsTriple s)
/*  87:    */   {
/*  88:129 */     return this.reifier.hasTriple(s.asTriple());
/*  89:    */   }
/*  90:    */   
/*  91:    */   public void removeAllReifications(FrontsTriple s)
/*  92:    */   {
/*  93:138 */     this.reifier.remove(s.asTriple());
/*  94:    */   }
/*  95:    */   
/*  96:    */   public void removeReification(ReifiedStatement rs)
/*  97:    */   {
/*  98:147 */     this.reifier.remove(rs.asNode(), rs.getStatement().asTriple());
/*  99:    */   }
/* 100:    */   
/* 101:    */   public RSIterator listReifiedStatements()
/* 102:    */   {
/* 103:156 */     return new RSIteratorImpl(findReifiedStatements());
/* 104:    */   }
/* 105:    */   
/* 106:    */   public RSIterator listReifiedStatements(FrontsTriple s)
/* 107:    */   {
/* 108:166 */     return new RSIteratorImpl(findReifiedStatements(s.asTriple()));
/* 109:    */   }
/* 110:    */   
/* 111:    */   public void noteIfReified(RDFNode s, RDFNode p, RDFNode o)
/* 112:    */   {
/* 113:174 */     if (copyingReifications)
/* 114:    */     {
/* 115:175 */       noteIfReified(s);
/* 116:176 */       noteIfReified(p);
/* 117:177 */       noteIfReified(o);
/* 118:    */     }
/* 119:    */   }
/* 120:    */   
/* 121:    */   private void noteIfReified(RDFNode n)
/* 122:    */   {
/* 123:187 */     if ((n instanceof ReifiedStatement)) {
/* 124:188 */       createReifiedStatement(((ReifiedStatement)n).getURI(), ((ReifiedStatement)n).getStatement());
/* 125:    */     }
/* 126:    */   }
/* 127:    */   
/* 128:    */   private ExtendedIterator findReifiedStatements()
/* 129:    */   {
/* 130:193 */     return findReifiedStatements(Triple.ANY);
/* 131:    */   }
/* 132:    */   
/* 133:    */   private ExtendedIterator findReifiedStatements(Triple t)
/* 134:    */   {
/* 135:197 */     ExtendedIterator rsIt = this.reifier.allNodes(t);
/* 136:198 */     List result = new List();
/* 137:199 */     while (rsIt.hasNext()) {
/* 138:200 */       result.add(ReifiedStatementImpl.createExistingReifiedStatement(this.model, (Node)rsIt.next()));
/* 139:    */     }
/* 140:201 */     return WrappedIterator.create(result.iterator());
/* 141:    */   }
/* 142:    */   
/* 143:    */   private ReifiedStatement getRS(Node n)
/* 144:    */   {
/* 145:210 */     return ReifiedStatementImpl.createExistingReifiedStatement(this.model, n);
/* 146:    */   }
/* 147:    */ }

