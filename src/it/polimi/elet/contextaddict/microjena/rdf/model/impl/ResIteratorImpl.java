/*  1:   */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*  2:   */ 
/*  3:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResIterator;
/*  4:   */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  5:   */ import it.polimi.elet.contextaddict.microjena.util.Iterator;
/*  6:   */ import it.polimi.elet.contextaddict.microjena.util.iterator.WrappedIterator;
/*  7:   */ 
/*  8:   */ public class ResIteratorImpl
/*  9:   */   extends WrappedIterator
/* 10:   */   implements ResIterator
/* 11:   */ {
/* 12:   */   public ResIteratorImpl(Iterator iter, Object object)
/* 13:   */   {
/* 14:23 */     this(iter);
/* 15:   */   }
/* 16:   */   
/* 17:   */   public ResIteratorImpl(Iterator iter)
/* 18:   */   {
/* 19:28 */     super(iter);
/* 20:   */   }
/* 21:   */   
/* 22:   */   public Resource nextResource()
/* 23:   */   {
/* 24:32 */     return (Resource)next();
/* 25:   */   }
/* 26:   */ }
