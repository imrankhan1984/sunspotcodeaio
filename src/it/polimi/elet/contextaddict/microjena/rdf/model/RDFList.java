package it.polimi.elet.contextaddict.microjena.rdf.model;

import it.polimi.elet.contextaddict.microjena.util.Iterator;
import it.polimi.elet.contextaddict.microjena.util.List;
import it.polimi.elet.contextaddict.microjena.util.iterator.ExtendedIterator;

public abstract interface RDFList
  extends Resource
{
  public abstract int size();
  
  public abstract RDFNode getHead();
  
  public abstract RDFNode setHead(RDFNode paramRDFNode);
  
  public abstract RDFList getTail();
  
  public abstract RDFList setTail(RDFList paramRDFList);
  
  public abstract boolean isEmpty();
  
  public abstract RDFList cons(RDFNode paramRDFNode);
  
  public abstract void add(RDFNode paramRDFNode);
  
  public abstract RDFList with(RDFNode paramRDFNode);
  
  public abstract RDFNode get(int paramInt);
  
  public abstract RDFNode replace(int paramInt, RDFNode paramRDFNode);
  
  public abstract boolean contains(RDFNode paramRDFNode);
  
  public abstract int indexOf(RDFNode paramRDFNode);
  
  public abstract int indexOf(RDFNode paramRDFNode, int paramInt);
  
  public abstract RDFList append(RDFList paramRDFList);
  
  public abstract RDFList append(Iterator paramIterator);
  
  public abstract void concatenate(RDFList paramRDFList);
  
  public abstract void concatenate(Iterator paramIterator);
  
  public abstract RDFList copy();
  
  public abstract void apply(ApplyFn paramApplyFn);
  
  public abstract Object reduce(ReduceFn paramReduceFn, Object paramObject);
  
  public abstract RDFList removeHead();
  
  /**
   * @deprecated
   */
  public abstract void removeAll();
  
  public abstract void removeList();
  
  public abstract RDFList remove(RDFNode paramRDFNode);
  
  public abstract ExtendedIterator iterator();
  
  public abstract List asJavaList();
  
  public abstract boolean sameListAs(RDFList paramRDFList);
  
  public abstract boolean getStrict();
  
  public abstract void setStrict(boolean paramBoolean);
  
  public abstract boolean isValid();
  
  public abstract String getValidityErrorMessage();
  
  public static abstract interface ReduceFn
  {
    public abstract Object reduce(RDFNode paramRDFNode, Object paramObject);
  }
  
  public static abstract interface ApplyFn
  {
    public abstract void apply(RDFNode paramRDFNode);
  }
}

