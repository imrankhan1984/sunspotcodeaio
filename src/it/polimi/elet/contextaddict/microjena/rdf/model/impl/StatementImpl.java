/*   1:    */ package it.polimi.elet.contextaddict.microjena.rdf.model.impl;
/*   2:    */ 
/*   3:    */ import it.polimi.elet.contextaddict.microjena.enhanced.EnhGraph;
/*   4:    */ import it.polimi.elet.contextaddict.microjena.enhanced.Implementation;
/*   5:    */ import it.polimi.elet.contextaddict.microjena.graph.Node;
/*   6:    */ import it.polimi.elet.contextaddict.microjena.graph.Triple;
/*   7:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Alt;
/*   8:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Bag;
/*   9:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
/*  10:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.LiteralRequiredException;
/*  11:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
/*  12:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ModelFactory;
/*  13:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ObjectF;
/*  14:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Property;
/*  15:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RDFNode;
/*  16:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.RSIterator;
/*  17:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ReifiedStatement;
/*  18:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
/*  19:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.ResourceF;
/*  20:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Seq;
/*  21:    */ import it.polimi.elet.contextaddict.microjena.rdf.model.Statement;
/*  22:    */ import it.polimi.elet.contextaddict.microjena.shared.JenaException;
/*  23:    */ 
/*  24:    */ public class StatementImpl
/*  25:    */   extends StatementBase
/*  26:    */   implements Statement
/*  27:    */ {
/*  28:    */   protected Resource subject;
/*  29:    */   protected Property predicate;
/*  30:    */   protected RDFNode object;
/*  31:    */   
/*  32:    */   public StatementImpl(Resource subject, Property predicate, RDFNode object, ModelCom model)
/*  33:    */   {
/*  34: 42 */     super(model);
/*  35: 43 */     this.subject = ((Resource)subject.inModel(model));
/*  36: 44 */     this.predicate = ((Property)predicate.inModel(model));
/*  37: 45 */     this.object = object.inModel(model);
/*  38:    */   }
/*  39:    */   
/*  40: 48 */   protected static ModelCom empty = (ModelCom)ModelFactory.createDefaultModel();
/*  41:    */   
/*  42:    */   public StatementImpl(Resource subject, Property predicate, RDFNode object)
/*  43:    */   {
/*  44: 51 */     super(empty);
/*  45: 52 */     this.subject = ((Resource)subject.inModel(this.model));
/*  46: 53 */     this.predicate = ((Property)predicate.inModel(this.model));
/*  47: 54 */     this.object = object.inModel(this.model);
/*  48:    */   }
/*  49:    */   
/*  50:    */   public static Statement toStatement(Triple t, ModelCom eg)
/*  51:    */   {
/*  52: 63 */     Resource s = new ResourceImpl(t.getSubject(), eg);
/*  53: 64 */     Property p = new PropertyImpl(t.getPredicate(), eg);
/*  54: 65 */     RDFNode o = createObject(t.getObject(), eg);
/*  55: 66 */     return new StatementImpl(s, p, o, eg);
/*  56:    */   }
/*  57:    */   
/*  58:    */   public Resource getSubject()
/*  59:    */   {
/*  60: 70 */     return this.subject;
/*  61:    */   }
/*  62:    */   
/*  63:    */   public Property getPredicate()
/*  64:    */   {
/*  65: 74 */     return this.predicate;
/*  66:    */   }
/*  67:    */   
/*  68:    */   public RDFNode getObject()
/*  69:    */   {
/*  70: 78 */     return this.object;
/*  71:    */   }
/*  72:    */   
/*  73:    */   public Statement getStatementProperty(Property p)
/*  74:    */   {
/*  75: 82 */     return asResource().getRequiredProperty(p);
/*  76:    */   }
/*  77:    */   
/*  78:    */   public Resource getResource()
/*  79:    */   {
/*  80: 86 */     return mustBeResource(this.object);
/*  81:    */   }
/*  82:    */   
/*  83:    */   public Resource getResource(ResourceF f)
/*  84:    */   {
/*  85: 90 */     return f.createResource(getResource());
/*  86:    */   }
/*  87:    */   
/*  88:    */   public Statement getProperty(Property p)
/*  89:    */   {
/*  90: 94 */     return getResource().getRequiredProperty(p);
/*  91:    */   }
/*  92:    */   
/*  93:    */   public Literal getLiteral()
/*  94:    */   {
/*  95:102 */     if ((this.object instanceof Literal)) {
/*  96:103 */       return (Literal)this.object;
/*  97:    */     }
/*  98:105 */     throw new LiteralRequiredException(this.object);
/*  99:    */   }
/* 100:    */   
/* 101:    */   public Object getObject(ObjectF f)
/* 102:    */   {
/* 103:    */     try
/* 104:    */     {
/* 105:112 */       return f.createObject(getLiteral().toString());
/* 106:    */     }
/* 107:    */     catch (Exception e)
/* 108:    */     {
/* 109:114 */       throw new JenaException(e);
/* 110:    */     }
/* 111:    */   }
/* 112:    */   
/* 113:    */   public Bag getBag()
/* 114:    */   {
/* 115:119 */     return (Bag)BagImpl.factory.wrap(this.object.asNode(), (EnhGraph)getModel());
/* 116:    */   }
/* 117:    */   
/* 118:    */   public Alt getAlt()
/* 119:    */   {
/* 120:123 */     return (Alt)AltImpl.factory.wrap(this.object.asNode(), (EnhGraph)getModel());
/* 121:    */   }
/* 122:    */   
/* 123:    */   public Seq getSeq()
/* 124:    */   {
/* 125:127 */     return (Seq)SeqImpl.factory.wrap(this.object.asNode(), (EnhGraph)getModel());
/* 126:    */   }
/* 127:    */   
/* 128:    */   protected StatementImpl replace(RDFNode n)
/* 129:    */   {
/* 130:134 */     StatementImpl s = new StatementImpl(this.subject, this.predicate, n, this.model);
/* 131:135 */     this.model.remove(this).add(s);
/* 132:136 */     return s;
/* 133:    */   }
/* 134:    */   
/* 135:    */   public boolean equals(Object o)
/* 136:    */   {
/* 137:143 */     return ((o instanceof Statement)) && (sameAs((Statement)o));
/* 138:    */   }
/* 139:    */   
/* 140:    */   private final boolean sameAs(Statement o)
/* 141:    */   {
/* 142:150 */     return (this.subject.equals(o.getSubject())) && (this.predicate.equals(o.getPredicate())) && (this.object.equals(o.getObject()));
/* 143:    */   }
/* 144:    */   
/* 145:    */   public int hashCode()
/* 146:    */   {
/* 147:156 */     return asTriple().hashCode();
/* 148:    */   }
/* 149:    */   
/* 150:    */   public Resource asResource()
/* 151:    */   {
/* 152:160 */     return this.model.getAnyReifiedStatement(this);
/* 153:    */   }
/* 154:    */   
/* 155:    */   public Statement remove()
/* 156:    */   {
/* 157:164 */     this.model.remove(this);
/* 158:165 */     return this;
/* 159:    */   }
/* 160:    */   
/* 161:    */   public Statement remove(boolean removeAxiom)
/* 162:    */   {
/* 163:169 */     this.model.remove(getSubject(), getPredicate(), getObject(), removeAxiom);
/* 164:170 */     return this;
/* 165:    */   }
/* 166:    */   
/* 167:    */   public void removeReification()
/* 168:    */   {
/* 169:174 */     this.model.removeAllReifications(this);
/* 170:    */   }
/* 171:    */   
/* 172:    */   public Triple asTriple()
/* 173:    */   {
/* 174:178 */     return Triple.create(this.subject.asNode(), this.predicate.asNode(), this.object.asNode());
/* 175:    */   }
/* 176:    */   
/* 177:    */   public static Triple[] asTriples(Statement[] statements)
/* 178:    */   {
/* 179:188 */     Triple[] triples = new Triple[statements.length];
/* 180:189 */     for (int i = 0; i < statements.length; i++) {
/* 181:189 */       triples[i] = statements[i].asTriple();
/* 182:    */     }
/* 183:190 */     return triples;
/* 184:    */   }
/* 185:    */   
/* 186:    */   public boolean isReified()
/* 187:    */   {
/* 188:194 */     return this.model.isReified(this);
/* 189:    */   }
/* 190:    */   
/* 191:    */   public ReifiedStatement createReifiedStatement()
/* 192:    */   {
/* 193:201 */     return ReifiedStatementImpl.create(this);
/* 194:    */   }
/* 195:    */   
/* 196:    */   public ReifiedStatement createReifiedStatement(String uri)
/* 197:    */   {
/* 198:209 */     return ReifiedStatementImpl.create((ModelCom)getModel(), uri, this);
/* 199:    */   }
/* 200:    */   
/* 201:    */   public RSIterator listReifiedStatements()
/* 202:    */   {
/* 203:213 */     return this.model.listReifiedStatements(this);
/* 204:    */   }
/* 205:    */   
/* 206:    */   public static RDFNode createObject(Node n, EnhGraph g)
/* 207:    */   {
/* 208:220 */     return n.isLiteral() ? (RDFNode) new LiteralImpl(n, g) : new ResourceImpl(n, g);
/* 209:    */   }
/* 210:    */ }
