package org.sunspotworld;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ILightSensor;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.util.Utils;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class HomeTask extends MIDlet {
    
    private static final int SAMPLE_PERIOD = 9 * 1000;  // in milliseconds
    private String name = "CONCORDIA_SENSOR";

    protected void startApp() throws MIDletStateChangeException {

        String ourAddress = System.getProperty("IEEE_ADDRESS");
        ILightSensor lightSensor = (ILightSensor)Resources.lookup(ILightSensor.class);
        ITriColorLED led = (ITriColorLED)Resources.lookup(ITriColorLED.class, "LED1");
        
        //System.out.println("Starting Home Owner Task on " + ourAddress + " ...");
        
        // Listen for downloads/commands over USB connection
        new com.sun.spot.service.BootloaderListenerService().getInstance().start();
        
       while (true) {
            try {
                // Get the current time and sensor reading
                long now = System.currentTimeMillis();
                int lightReading = lightSensor.getValue();
                //System.out.println("Light value = " + lightReading);
                // Flash an LED to indicate a sampling event
                led.setRGB(255, 255, 255);
                led.setOn();
                Utils.sleep(50);
                led.setOff();

                System.out.println("This is the HOME OWNER Task\n"
                        + "Light value = " + lightReading+" on "+name+"\n");
                
                
                // Go to sleep to conserve battery
                Utils.sleep(SAMPLE_PERIOD - (System.currentTimeMillis() - now));
            } catch (Exception e) {
                System.err.println("Caught " + e + " while collecting/sending sensor sample.");
            }
        }        
    }   // startApp

    /**
     * This will never be called by the Squawk VM.
     */
    protected void pauseApp() { 
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
    }
}
