package org.sunspotworld;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.Condition;
import com.sun.spot.resources.transducers.IConditionListener;
import com.sun.spot.resources.transducers.ITemperatureInput;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.SensorEvent;
import com.sun.spot.resources.transducers.TemperatureInputEvent;
import com.sun.spot.util.Properties;
import com.sun.spot.util.Utils;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class AnnotationTask extends MIDlet {
    
    private static final int SAMPLE_PERIOD = 5 * 1000;  // in milliseconds   
    private String postURL = getAppProperty("GAE-URL");
    private String longitude = getAppProperty("Longitude");
    private String latitude = getAppProperty("Latitude");
    private String ontoFile = getAppProperty("ONTO-PATH");
    private String name = "CONCORDIA_SENSOR";
    private ITemperatureInput tempSensor = (ITemperatureInput) Resources.lookup(ITemperatureInput.class);
    ITriColorLED led = (ITriColorLED)Resources.lookup(ITriColorLED.class, "LED4");
    private String ourAddr = System.getProperty("IEEE_ADDRESS");
    Properties prop = new Properties();
    private String dow = null;
    
    protected void startApp() throws MIDletStateChangeException {                
                
        //System.out.println("Starting Semantic Task on " + ourAddr + " ...");        
	// Listen for downloads/commands over USB connection
	new com.sun.spot.service.BootloaderListenerService().getInstance().start(); 
        InputStream in = this.getClass().getResourceAsStream(ontoFile);
        try {
            new BaseOntology(in);
        } 
        catch (IOException ex) {
        }
      //  while (true) {
        IConditionListener fireListener = new IConditionListener() {
            public void conditionMet(SensorEvent evt, Condition condition) {
                try {               
                long sendTime = System.currentTimeMillis();
                Date dt = new Date(sendTime);
                dow = dt.toString(); 
                System.out.println("This is the Annotation TASK\n");
                new  GenerateRDF (name, tempSensor.getCelsius(), 
                        longitude, latitude, ourAddr, sendTime, "C", postURL, dow);                                                  
                long now = System.currentTimeMillis();                
                // Flash an LED to indicate a sampling event
                led.setRGB(0, 0, 255);
                led.setOn();
                Utils.sleep(50);
                led.setOff();                     
                // Go to sleep to conserve battery
                Utils.sleep(SAMPLE_PERIOD - (System.currentTimeMillis() - now));
            } catch (Exception e) {
                System.err.println("Caught " + e + " in City Admin Task");
             }
            }
        };
        Condition fire = new Condition(tempSensor, fireListener, SAMPLE_PERIOD) {
            public boolean isMet(SensorEvent evt) {
                if(((TemperatureInputEvent)evt).getCelsius() >= 25.0) {
                    //stop();
                    return true;
                } 
                else {
                    return false;
                }
            }            
        };
        
        fire.start();  
     //   }        // while loop
    }        
    /**
     * This will never be called by the Squawk VM.
     */
    protected void pauseApp() { 
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     * @throws javax.microedition.midlet.MIDletStateChangeException
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
    
    }   //destroyApp
}