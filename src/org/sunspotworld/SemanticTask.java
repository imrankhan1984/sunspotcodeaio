package org.sunspotworld;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.Condition;
import com.sun.spot.resources.transducers.IConditionListener;
import com.sun.spot.resources.transducers.ITemperatureInput;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.SensorEvent;
import com.sun.spot.resources.transducers.TemperatureInputEvent;
import com.sun.spot.util.Utils;
import java.io.IOException;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class SemanticTask extends MIDlet {
    
    HttpPost httpPost = new HttpPost();
    private ITemperatureInput tempSensor = (ITemperatureInput) Resources.lookup(ITemperatureInput.class);
    private ITriColorLED led = (ITriColorLED)Resources.lookup(ITriColorLED.class, "LED1");   
    private String postURL = getAppProperty("SEM-URL");
    private static final String sensorName = "CONCORDIA_SENSOR";
    private String longitude = getAppProperty("Longitude");
    private String latitude = getAppProperty("Latitude");
    private final String ourAddr = System.getProperty("IEEE_ADDRESS");
    private static final int SAMPLE_PERIOD = 7 * 1000;  // in milliseconds
    
    protected void startApp() throws MIDletStateChangeException {                
               
        IConditionListener fireListener = new IConditionListener() {
            public void conditionMet(SensorEvent evt, Condition condition) {
                try {
                    double temp = tempSensor.getCelsius();
                    System.out.println("This is the Semantic TASK\n");
                    String msg = createJSON(sensorName, temp, 
                        longitude, latitude, ourAddr, System.currentTimeMillis(), "C");
                    led.setRGB(255, 0, 0);
                    led.setOn();
                    Utils.sleep(10);
                    led.setOff(); 
                    httpPost.Post(postURL, msg);
                    //System.out.println("Fire notification sent");
                    led.setRGB(0, 0, 255);
                    led.setOn();
                    Utils.sleep(10);
                    led.setOff();
                    //Utils.sleep(2000);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        Condition fire = new Condition(tempSensor, fireListener, SAMPLE_PERIOD) {
            public boolean isMet(SensorEvent evt) {
                if(((TemperatureInputEvent)evt).getCelsius() >= 25.0) {
                    //stop();
                    return true;
                } else {
                    return false;
                }
            }            
        };
        fire.start();     
    }   // startApp  
    
    private static String createJSON(String n, double v, String sv1, String sv2, String bn, long bt, String bu){       
       String JSONString = "{\"e\":["
               + "{\"n\":\""
               +n+"\", \"v:\""+v+"\"}, "
               + "{\"sve\":\""
               +sv1+"\", \"ue\":\"lon\"},"
               + "{ \"svn\":\""
               +sv2+"\", \"un\":\"lat\"}"
               + "]"
               + " \"bn\":\""
               +bn+"\", "
               + "\"bt\":\""
               +bt+"\", "
               + "\"bu\":\""
               +bu+"\"}";       
       return JSONString;
   }
    
    /**
     * This will never be called by the Squawk VM.
     */
    protected void pauseApp() { 
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     * @throws javax.microedition.midlet.MIDletStateChangeException
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
    
    }   //destroyApp
}