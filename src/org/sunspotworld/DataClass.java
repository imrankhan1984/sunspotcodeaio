package org.sunspotworld;

import it.polimi.elet.contextaddict.microjena.rdf.model.Model;
import java.io.OutputStream;

public class DataClass {

    private Model fModel;
    
    public DataClass(Model fModel) {
        this.fModel = fModel;
    }

    public void writeInStream(OutputStream oos) {                 
        
        fModel.write(oos, "N-TRIPLE");
    }
}
