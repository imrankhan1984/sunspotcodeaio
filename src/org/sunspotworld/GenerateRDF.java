package org.sunspotworld;

import it.polimi.elet.contextaddict.microjena.datatypes.xsd.XSDDatatype;
import it.polimi.elet.contextaddict.microjena.ontology.OntModel;
import it.polimi.elet.contextaddict.microjena.rdf.model.Literal;
import it.polimi.elet.contextaddict.microjena.rdf.model.ModelFactory;
import it.polimi.elet.contextaddict.microjena.rdf.model.Resource;
import it.polimi.elet.contextaddict.microjena.vocabulary.RDF;
import java.io.IOException;

public class GenerateRDF {
    
    HttpPost httpPost = new HttpPost();
    
    long annotationStartTime = System.currentTimeMillis();
    
    public GenerateRDF (String n, double v, String sv1, String sv2, String bn, 
            long bt, String bu, String postURL, String dt) throws IOException {
        
        // sv2 is longitude while s1 is latitude
        
        OntModel model = ModelFactory.createOntologyModel();

        model.setNsPrefix("ssn",BaseOntology.ssn);
        model.setNsPrefix("base",BaseOntology.uri);
        
        String dateSS = dt.substring(10, 23);
                
        Literal dateTime = model.createTypedLiteral(dt, XSDDatatype.XSDstring);              
        Literal date = model.createTypedLiteral(dateSS, XSDDatatype.XSDstring);
        Literal sensorMeasurement = model.createTypedLiteral(new Double(v), XSDDatatype.XSDdouble);
        
        Resource SunSpot_1 = model.createResource(BaseOntology.uri+bn)
            .addProperty(RDF.type ,BaseOntology.TemperatureSensor)
            .addProperty(BaseOntology.observes, BaseOntology.Temperature)
            .addProperty(BaseOntology.hasLatitude, sv2)
            .addProperty(BaseOntology.hasLongitude, sv1);

        Resource SunSpotOutput_1 = model.createResource(BaseOntology.uri+"_Output") 
            .addProperty(RDF.type ,BaseOntology.TemperatureOutput)
            .addProperty(BaseOntology.observedBy, n+"_"+bn)
            .addProperty(BaseOntology.hasValue, sensorMeasurement)
            .addProperty(BaseOntology.hasSensingTime, dateTime)
            .addProperty(BaseOntology.hasSensingDate, date);
        
        if (bu.equalsIgnoreCase("C")){
            SunSpotOutput_1.addProperty(BaseOntology.hasUnit,BaseOntology.DegreeCelsius);
        } else 
            SunSpotOutput_1.addProperty(BaseOntology.hasUnit,BaseOntology.DegreeFahrenheit);
        long annotationEndTime = System.currentTimeMillis();
        //System.out.println("Total annotation time is: " +(annotationEndTime - annotationStartTime)+ " milliseconds");
        
        DataClass obj = new DataClass(model);
        model.write(System.out, "N-TRIPLE");
        httpPost.PostRDF(postURL, obj);
        
    }


}
