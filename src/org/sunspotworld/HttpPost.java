package org.sunspotworld;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.util.Utils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

/**
 *
 * @author umroot
 */
public class HttpPost {
      
    private static final int INACTIVE = 0;
    private static final int CONNECTING = 1;
    private static final int COMPLETED = 2;
    private static final int IOERROR = 3;
    private static final int PROTOCOLERROR = 4;

    private static int POSTstatus = INACTIVE;
    
    public void Post(String postURL, String msg) {
        HttpConnection conn = null;
        OutputStream out = null;
        InputStream in = null;
        long starttime = 0;
        String resp = null;
        
        System.out.println("Posting JSON string: <" + msg + "> to " + postURL+"\n");

        try {
            POSTstatus = CONNECTING;
            starttime = System.currentTimeMillis();
            conn = (HttpConnection) Connector.open(postURL);
            conn.setRequestMethod(HttpConnection.POST);
            conn.setRequestProperty("Connection", "close");

            out = conn.openOutputStream();
            out.write((msg + "'\n").getBytes());
            out.flush();

            in = conn.openInputStream();
            resp = conn.getResponseMessage();
            if (resp.equalsIgnoreCase("OK") || resp.equalsIgnoreCase("CREATED")) {
                POSTstatus = COMPLETED;
            } else {
                POSTstatus = PROTOCOLERROR;
            }
        } catch (Exception ex) {
            POSTstatus = IOERROR;
            resp = ex.getMessage();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (IOException ex) {
                resp = ex.getMessage();
            }
        }
        System.out.flush();
    }   // Post METHOD
    
    public void PostRDF(String postURL, DataClass d) {
        //long start = System.currentTimeMillis();
        HttpConnection conn = null;
        OutputStream out = null;
        InputStream in = null;
        long starttime = 0;
        String resp = null;
        ProgressDisplayThread displayProg = null;
       // System.out.println("Posting: <" + msg + "> to " + postURL);
        try {
            POSTstatus = CONNECTING;
            displayProg = new ProgressDisplayThread();
            displayProg.start();
            starttime = System.currentTimeMillis();
            conn = (HttpConnection) Connector.open(postURL);
            conn.setRequestMethod(HttpConnection.POST);
            conn.setRequestProperty("Content-type", "text/xml");
            conn.setRequestProperty("Connection", "close");
            out = conn.openOutputStream();
            d.writeInStream(out);

            out.flush();
            in = conn.openInputStream();
            resp = conn.getResponseMessage();
            if (resp.equalsIgnoreCase("OK") || resp.equalsIgnoreCase("CREATED")) {
                POSTstatus = COMPLETED;
                //System.out.println("E2ED of msg "+(++counter)+" is: "+(System.currentTimeMillis() - start));
            } else {
                POSTstatus = PROTOCOLERROR;
            }
        } catch (Exception ex) {
            POSTstatus = IOERROR;
            resp = ex.getMessage();
         } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (IOException ex) {
                resp = ex.getMessage();
            }
        }
        displayProg.markDone(POSTstatus == COMPLETED);
        if (POSTstatus != COMPLETED) {
            System.out.println("Posting failed: " + resp);
        } else {           
        }
        System.out.flush();
    }   // PostRDF constructor     
}   // HttpPost class

class ProgressDisplayThread extends Thread {
    private boolean done = false;
    ITriColorLED led = (ITriColorLED)Resources.lookup(ITriColorLED.class, "LED4");
    public void run() {
        while (!done) {
                led.setRGB(0, 0, 0);
                led.setOff();
                Utils.sleep(500);
                led.setRGB(50, 50, 50);
                led.setOn();                
        }
    }   //run

    public void markDone(boolean status) {
        done = true;
            if (status) { // post was successful
               led.setRGB(0, 250, 0);
            } else { // post failed
                led.setRGB(50, 0, 0);
            }
            led.setOn();
        Utils.sleep(2000);
            led.setOff();
    }   //markDone
}   //ProgressDisplayThread class
